# Lithium Battery Configuration Guide

This guide offers a comprehensive breakdown of the parameters required for configuring lithium battery storage systems. These parameters are divided into several sections, as detailed below:

- [AC-DC Converter](#ac-dc-converter)
- [Housing](#housing)
- [HVAC](#hvac)
- [DC-DC Converter](#dc-dc-converter)
- [Cell](#cell)
- [Thermal Simulation](#thermal-simulation)
- [Ambient Temperature Model](#ambient-temperature-model)
- [Solar Irradiation Model](#solar-irradiation-model)
- [Energy Management](#energy-management)

## AC-DC Converter

The parameters for this section include:

- **TYPE**: Specifies the AC-DC Converter type. Available options are listed [here](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/system/power_electronics/acdc_converter).
- **NUMBER_OF_CONVERTERS**: Determines the number of AC-DC converters used and the feasibility of cascading them. Default value: 1.
- **SWITCH_VALUE**: In cascaded setups, this per unit (p.u.) value defines the power-to-nominal power ratio at which the next converter is activated. Default value: 1.0.

## Housing

The parameter for this section is:

- **TYPE**: Identifies the type of battery system housing. Further details and available types are available [here](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/system/housing).

## HVAC

Required parameters for this section:

- **TYPE**: Indicates the HVAC type. Available types are listed [here](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/system/auxiliary/heating_ventilation_air_conditioning).
- **THERMAL_POWER**: The thermal power of the HVAC in Watts.
- **SET_TEMPERATURE**: The set temperature for the HVAC in Kelvin.

## DC-DC Converter

The required parameter for this section:

- **TYPE**: Specifies the DC-DC Converter type. Options can be found [here](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/system/power_electronics/dcdc_converter).

## Cell

Parameters for this section include:

- **TYPE**: Defines the lithium battery cell type. Possible options are available [here](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/technology/lithium_ion/cell).
- **EOL**: End of life of the battery expressed in per unit.
- **START_SOH**: Initial state of health of the cell for the simulation, in per unit (p.u.). Note: Not all cell types support this feature.

## Thermal Simulation

The required parameter for this section:

- **ENABLE**: A boolean value that, when set to `True` or `False`, enables or disables thermal simulation respectively.

## Ambient Temperature Model

The required parameter for this section:

- **TYPE**: Specifies the type of ambient temperature model used. Available types are listed [here](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/system/thermal/ambient).

## Solar Irradiation Model

The required parameter for this section:

- **TYPE**: Identifies the type of solar irradiation model. Options can be found [here](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/system/thermal/solar_irradiation).

## Energy Management

Parameters for this section include:

- **STRATEGY**: Defines the application strategy, either Single Use or Multi Use. Compatibility with load, generation, or technical profiles is crucial. Examples include a PV Home Storage application requiring both a PV-profile and a load profile. Additional information about strategies can be found:
  - [Basic](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/logic/energy_management/strategy/basic)
  - [Optimization](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/logic/energy_management/strategy/optimization)
  - [Serial](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/logic/energy_management/strategy/serial)
  - [Stacked](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/logic/energy_management/strategy/stacked)

- Additional parameters such as **POWER_FCR**, **POWER_IDM**, **SOC_SET**, **FCR_RESERVE**, **USE_LOCAL_TECHNICAL_PROFILE_DIR**, and **FREQUENCY_PROFILE** are also necessary.

**Note**: Please refer to [the examples](https://gitlab.lrz.de/open-ees-ses/simses/-/tree/master/simses/simulation/simulation_examples) for guidance on parameter usage.

**Note**: For detailed instructions on setting parameters in SimSES, consult the comments in the [configuration file](https://gitlab.lrz.de/open-ees-ses/simses/-/blob/master/simses/simulation.defaults.ini).
