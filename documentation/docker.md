# Docker Overview

Docker is an open-source platform for automating the deployment, scaling, and management of applications through containerization. This technology packages an application with its dependencies into a container, a lightweight virtualization environment. Docker images, created from a Dockerfile, are read-only templates containing the application's code, runtime, libraries, and system tools.

The Docker architecture is visualized as follows:

<div align="center">
  <img src="figures/Docker_overview.png" width="600" />
</div>

Docker streamlines application deployment, enhances scalability, and facilitates a consistent development workflow.

This documentation includes:

- [Installation Guide](#installation)
- [Using Docker for ESN](#using-docker)

## <a name="installation"></a> Docker Installation

Instructions for installing Docker on various operating systems are as follows:

### Windows:

- Download Docker Desktop for Windows 10 Pro, Enterprise, or Education editions from the [official site](https://www.docker.com/products/docker-desktop).
- Execute the installer and follow the prompts.
- Docker runs as a desktop application accessible from the system tray.

### macOS:

- Download Docker Desktop for macOS from the [official site](https://www.docker.com/products/docker-desktop).
- Run the installer and follow the prompts.
- Docker operates as a desktop application, accessible from the menu bar.

### Linux:

- Docker installation varies by Linux distribution. Generally, use Docker's convenience script:
  ```shell
  curl -fsSL https://get.docker.com -o get-docker.sh
  sudo sh get-docker.sh
  ```
- Follow terminal prompts to complete installation.
- Docker commands can then be executed from the terminal.

**Note 1:** Administrative privileges are required for installation.

**Note 2:** First-time users should run:
```shell
docker run -d -p 80:80 docker/getting-started
```

## <a name="using-docker"></a> Use Docker for ESN

### 1. Clone ESN Docker Repository

Clone the ESN Docker file system repository:
```shell
git clone https://gitlab.lrz.de/open-ees-ses/esn_docker_filesystem.git
```

Pull the latest Docker container version:
```shell
docker pull gitlab.lrz.de:5005/open-ees-ses/energy_system_network:latest
```
Navigate to **Packages and registries > Container Registry** in Gitlab to view container versions.

<div align="center">
  <img src="figures/docker_container_registry.png" width="600" />
</div>

To use a specific container version:
```shell
docker pull <copied path>
```
Replace `<copied path>` with the path copied from Gitlab.

In Docker Desktop, the container should appear as follows:
<div align="center">
   <img src="figures/docker_desktop.png" width="600" />
</div>

### 2. Customize Configuration

Navigate to the `01_input` folder:
```shell
cd ./files/04_ESNSimSES/01_input
```
Or, just navigate to the mentioned folder displayed in the following figure:

<div align="center">
  <img src="figures/docker_input_folder.png" width="600" />
</div>

You can now add your own profiles in the relative sub-folder in the `profiles` folder, or you can make changes in the `simulation.local.ini` file according to the [configuration](configuration.md) description.

### 3. Run a Simulation
To run a simulation, do the following steps:
1. Open the Docker interface and click on the **Images** tab: If you open **Docker Desktop**, you may see the following figure:
   <div align="center">
      <img src="figures/docker_desktop.png" width="600" />
   </div>
2. Select the image from the list: If you click on the ESN image in the list, you may see the following image:
   <div align="center">
      <img src="figures/docker_desktop_ESN_image.png" width="600" />
   </div>
3. Click on the Run button: After clicking on the Run button in the top right corner, you will see the optional setting as below:
   <div align="center">
      <img src="figures/docker_desktop_ESN_image_optional_settings.png" width="600" />
   </div>
4. Use the optional setting to import the project directory which you have cloned before. This step is necessary to import the customised configuration into Docker: To do so, just click on the optional settings dropdown, where you see the following figure in your desktop:
   <div align="center">
      <img src="figures/docker_desktop_ESN_image_optional_settings_inside.png" width="600" />
   </div>
   
   Here, you have to enter Host path and Container path. To do so, click on the ellipsis dots in the Host path field, select the project folder that you have already pulled from Gitlab, and then, click on the open button. For the Container path, just write `\docker_container`. You will have the following figure now:
   <div align="center">
      <img src="figures/docker_desktop_ESN_image_optional_settings_inside_with_paths.png" width="600" />
   </div>
   
5. Finally, click on the Run button: If you follow all the steps correctly, the ESN Docker image is ready to run a simulation. To run, click on the Run button and wait until the end of the simulation.

### 4. View and Analyze Results

Post-simulation, Docker logs and results will be available:

<div align="center">
   <img src="figures/docker_results.png" width="600" />
</div>

Results are stored in the `02_simresults` folder.

<div align="center">
   <img src="figures/docker_results_folder.png" width="600" />
</div>
