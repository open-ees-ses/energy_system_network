# Configuration Guide

This file explains the parameters in the `simulation.defaults.ini` file. This is an editable config file that controls all necessary simulation parameters. Feel free to customize it with your own configuration according to the following instructions.

The configuration consists of the following sections:

- [General](#general)
- [Energy Management](#energy_management)
- [Energy System Network](#energy_system_network)
- [Storage](#storage)
- [Profile](#profile)

Each section is explained in detail below.

## General <a name="general"></a>

This section defines the general settings. The following parameters must be defined:

- **START**: Start time of the simulation
- **END**: End time of the simulation
- **TIME_STEP**: Simulation time step in s
- **LOOP**: Number of simulation cycles
- **EXPORT_INTERVAL**: Interval to write results to the file

Here, you can find the example parameters for this section:
```ini
[GENERAL]
START = 2018-01-01 00:00:00
END = 2018-01-31 23:59:59
TIME_STEP = 900
LOOP = 1
EXPORT_INTERVAL = 1
```

## Energy Management <a name="energy_management"></a>

This section lists the energy forms involved in the entire simulation. These parameters are defined here:

- **STRATEGY**: This parameter defines the operation strategy/application for the network and for each energy system. Generally, the strategies are either rule-based or optimizer-based. The following strategy types are currently available:
  - Rule-based:
    - `NoStrategy`
    - `SimpleDeficitCoverage`
    - `SimplePeakShaving`
    - `SimSESExternalStrategy`
    - `MultiSourcePeakShaving`
  - Optimizer-based:
    - `ArbitrageOptimization`
    - `BSSCombinedDiscreteEMS`
    - `BSSSiteEnergyManagement`
    - `RHOptimization`
    - `OptimizerBasedOperationStrategy`
  
  The format is as follows:
  
  `System name, Strategy type, Energy types in the system`


- **SIMPLE_DEFICIT_COVERAGE_PRIORITY**: This parameter is applicable only to SimpleDeficitCoverage. So, you can ignore it in other strategies. The hierarchy is important in the definition of this parameter. That is, if the storage is placed last in this category, it ensures that the CanRunGenerationComponents and GridComponents do not supply the storage's auxiliary demand. But, this also results in the storage coming in last to discharge. Otherwise, if it is placed first, it implies that the load-like behavior of the storage at times can lead to consumption from the CanRunGenerationComponents and GridComponents. But, in this case, the storage is also the first to be brought online.
- **OPTIMIZED_STRATEGY_TIME_HORIZON**: This parameter is applicable only to optimizer-based strategies. So, it can be ignored in other strategies. It specifies number of time-steps to look into the future.
- **RERUN_OPTIMIZATION_TIMESTEPS**: This parameter is applicable only to optimizer-based strategies. So, it can be ignored in other strategies. It specifies number of time-steps after which to rerun simulation based on updated values from detailed models. It should be mentioned that the number must be less than or equal to OPTIMIZED_STRATEGY_TIME_HORIZON.
- **ARBITRAGE_TYPE**: This parameter specifies whether EMS Type ArbitrageOptimization is driven by emissions or energy-prices. It has two modes:
  - `Emissions`
  - `Economic`

Here, you can find the example parameters for this section:
```ini
[ENERGY_MANAGEMENT]
STRATEGY =
    energy_system_network, NoStrategy, Electricity
    system_1, SimpleDeficitCoverage, Electricity
SIMPLE_DEFICIT_COVERAGE_PRIORITY =
    StorageComponents
    GridComponents
    CanRunGenerationComponents
OPTIMIZED_STRATEGY_TIME_HORIZON = 96
RERUN_OPTIMIZATION_TIMESTEPS = 16
ARBITRAGE_TYPE = Emissions
```

## Energy System Network <a name="energy_system_network"></a>

This section defines the parameters of the energy system network. It includes the following parameters:

- **ENERGY_SYSTEMS**: This parameter specifies the configuration of energy system. The format is as follows: 

  `energy system name, energy forms`


- **GENERATION_COMPONENTS**: This parameter specifies the configuration of the generation components. For components dealing with more than one energy forms, specify the power rating of the energy form with the higher priority in the list of energy forms provided above. The format is as follows:

  `energy system name, componenet name, peak power in the system (W)`


- **STORAGE_COMPONENTS**: This parameter specifies the configuration of the storage components. The format is as follows:

  `energy system name, component name, peak power (W), energy capacity (Wh), optional: 'DC' if the AC/DC converter is not to be included in system`


- **LOAD_COMPONENTS**: This parameter specifies the configuration of the load components. The format is as follows:

  `energy system name, component name, peak power in W (set to 1 if profile to be used as-is), optional: number of slots for BSSLoadImplementation`


- **GRID_COMPONENTS**: This parameter specifies the configuration of the main grid. The format is as follows:

  `energy system name, component name, Peak power (W), Feed-in enabled (True/False), optional: type of AC/DC converter if output is to be DC electricity`


Note: if you intend to use the non-normalized profiles with the actual values, you should put `1` for the peak power in the mentioned profile-based components. Hence, by using `1` for the peak power, we ensure that no further scaling is done.

Here, you can find the example parameters for this section:
```ini
[ENERGY_SYSTEM_NETWORK]
ENERGY_SYSTEMS =
    system_1,Electricity
GENERATION_COMPONENTS =
    system_1,WindTurbine,1.5e6
    system_1,PhotovoltaicSolar,1.5e6
STORAGE_COMPONENTS =
    system_1,LithiumBatterySystem,1e6,1e6
LOAD_COMPONENTS =
    system_1,ElectricLoadOne,1e6
GRID_COMPONENTS =
    system_1,SimpleElectricityGridConnection,0.5e6,False
```

## Storage <a name="storage"></a>

This section describes technology specific parameters:

- **START_SOC**: State of charge (SOC) for the storage device at simulation start in p.u. It must be a number between 0 and 1.
- **START_TEMPERATURE**: Start temperature of the technology in Kelvin. This parameter is optional.
- **MIN_SOC**: Minimum SOC limit in p.u.It must be a number between 0 and 1.
- **MAX_SOC**: Maximum SOC limit in p.u.It must be a number between 0 and 1.
- **EOL**: End of life criteria. If the state of health is below this limit, the battery will be replaced. This parameter is optional.

**Note**: Please also check [lithium battery config guide](lithium_battery_config.md) for more parameters.

Here, you can find the example parameters for this section:
```ini
[STORAGE]
START_SOC = 0.01
MIN_SOC = 0.0
MAX_SOC = 1.0
```

## Profile <a name="profile"></a>

This section configures the input profiles for the simulation:

* **POWER_PROFILE_DIR**: Relative path to power profiles.
* **LOAD_PROFILE**: Name of the load profile in the specified path.
* **GENERATION_PROFILE**: Name of the generation profile in the specified path.
* **EV_DRIVE_POWER_PROFILE**: Name of the driving electric vehicles profile in the specified path.
* **DISCRETE_EVENTS_PROFILE_DIR**: Relative path to discrete events profiles.
* **SWAP_EVENTS_PROFILE**: Name of the swap events profile in the specified path.
* **EV_PARKED_PROFILE**: Name of the parked electric vehicles profile in the specified path.
* **TECHNICAL_PROFILE_DIR**: Relative path to technical profiles.
* **EMISSIONS_PROFILE_DIR**: Relative path to emissions profiles.
* **GRID_EMISSIONS_PROFILE**: Name of the grid emission profile in the specified path.
* **ECONOMIC_PROFILE_DIR**: Relative path to economic profiles.
* **ECONOMIC_PROFILE**: Name of the economic profile in the specified path.
* **GRID_EMISSIONS_SCALING_FACTOR**: The grid emission profile will be scaled to a maximum of this factor in W (e.g. 5e3 = 5 kWpeak).

**Note:** Custom profiles can be utilized in lieu of the default CSV files provided. To implement this, simply position your CSV files within the designated directories corresponding to each specific component.

Here, you can find the example parameters for this section:
```ini
[PROFILE]
POWER_PROFILE_DIR = energy_system_network/profiles/power/
LOAD_PROFILE =
    ElectricLoadOne,teneriffe_load_normalized_10m.csv
GENERATION_PROFILE =
    PhotovoltaicSolar,teneriffe_solar_normalized_10m.csv
    WindTurbine,teneriffe_wind_normalized_10m.csv
EV_DRIVE_POWER_PROFILE = drive_power_fulltime_1year_WLTC_Volkswagen.csv.gz

DISCRETE_EVENTS_PROFILE_DIR = energy_system_network/profiles/discrete_events/
SWAP_EVENTS_PROFILE = swap_events.csv
EV_PARKED_PROFILE = parked_homefulltime1yearWLTCVolkswagen.csv.gz

TECHNICAL_PROFILE_DIR = energy_system_network/profiles/technical/

EMISSIONS_PROFILE_DIR = energy_system_network/profiles/emissions/
GRID_EMISSIONS_PROFILE = grid_emissions_time_series_germany_2019_15min_trlosses.csv

ECONOMIC_PROFILE_DIR = energy_system_network/profiles/economic/
ECONOMIC_PROFILE = economic_idc_market_average_2022_15_min.csv

GRID_EMISSIONS_SCALING_FACTOR = 1
```