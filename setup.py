from setuptools import setup, find_packages

setup(
    name='energy_system_network',
    version='1.0.0',
    description='Simulation of Multi-Energy Form Multi-Component Energy Systems',
    url='https://gitlab.lrz.de/ees-ses/energy_system_network',
    author='Anupam Parlikar, Chair of Electrical Energy Storage Technology, TU Munich',
    author_email='anupam.parlikar@tum.de',
    license='BSD 3-Clause "New" or "Revised" License',
    packages=find_packages(),
    include_package_data=True,
    install_requires=open('requirements.txt').readlines(),

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Microsoft :: Windows :: Windows 10',
        'Programming Language :: Python :: 3.8'
        'Topic :: Scientific/Engineering',
    ])
