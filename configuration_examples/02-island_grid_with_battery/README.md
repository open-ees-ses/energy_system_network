# Island Grid (with Battery)
This document presents an exemplary configuration for an island energy system that incorporates battery storage. We encourage you to review and cite the following study [^1] if you plan to implement this configuration in your research or projects.

The schematic representation of the energy system is illustrated in the figure below.

<div align="center">
  <img src="figures/EnergySystem.png" width="700" />
</div>


## Contents

- [System Configuration](#system-configuration)
  - [General Settings](#general-settings)
  - [Energy Management](#energy-management)
  - [Energy System Network](#energy-system-network)
  - [Storage](#storage)
  - [Profile](#profile)
- [Battery Configuration](#battery-configuration)
  - [AC-DC Converter](#acdc-converter)
  - [HVAC](#hvac)
  - [DC-DC Converter](#dcdc-converter)
  - [Cell](#cell)
  - [Thermal Simulation](#thermal-simulation)
  - [Ambient Temperature Model](#ambient-temperature-model)
- [Reference](#reference)

## [System Configuration](#System-Config)
In this part, we explain the sections and parameters of [`simulation.example.ini`](simulation.example.ini).

### [General Settings](#General-Settings)
This section defines the temporal parameters for the simulation. It is scheduled to commence at `2018-01-01 00:00:00` and conclude on `2018-12-31 23:59:59`. The time step interval is established at `900` seconds. To facilitate the simulation of a one-year period, the `LOOP` parameter is configured to `1`. Detailed settings can be found in the general section of the [`simulation.example.ini`](simulation.example.ini) file.
```ini
[GENERAL]
START = 2018-01-01 00:00:00
END = 2018-12-31 23:59:59
TIME_STEP = 900
LOOP = 1
```
### [Energy Management](#Energy-Management)
In this section, we outline the specific energy management parameters. The strategies adopted are `NoStrategy` for the `energy_system_network` and `SimpleDeficitCoverage` for `system_1`. In both cases, `Electricity` is the sole energy form utilized. The `SimpleDeficitCoverage` strategy necessitates the establishment of a component hierarchy, delineated using the `SIMPLE_DEFICIT_COVERAGE_PRIORITY` parameter. Within `system_1`, this hierarchy is prioritized as follows:
1. `StorageComponents`
2. `GridComponents`
3. `CanRunGenerationComponents`

These settings are further detailed in the corresponding section of the configuration file.
```ini
[ENERGY_MANAGEMENT]
STRATEGY =
	energy_system_network, NoStrategy, Electricity
	system_1, SimpleDeficitCoverage, Electricity
SIMPLE_DEFICIT_COVERAGE_PRIORITY =
	StorageComponents
	GridComponents
	CanRunGenerationComponents
```

### [Energy System Network](#Energy-System-Network)
This section introduces `system_1`, a straightforward energy system exclusively utilizing `Electricity` as its energy form. The system's composition includes:

- **Generation Components**:
  - `Wind` Turbine: Capable of generating up to `3.25 MW` of electricity.
  - `PV` (Photovoltaic) Solar Panel: With a maximum output capacity of `2 MW`.
  - `Diesel Generator`: Able to produce `1.6 MW` of electricity.
- **Load Component**: `ElectricLoadOne`, which consumes a maximum of `1.59 MW` of electricity.
- **Storage Component**: The `LithiumBatterySystem`, capable of generating `1.5 MW` of electricity and featuring a capacity of `1.5 MWh`.
- Since this is an island-based energy system, the `GRID_COMPONENTS` parameter is not applicable and therefore left empty.

Further details and configurations are specified in the corresponding section of the [`simulation.example.ini`](simulation.example.ini) file.
```ini
[ENERGY_SYSTEM_NETWORK]
ENERGY_SYSTEMS =
	system_1,Electricity
GENERATION_COMPONENTS =
	system_1,WindTurbine,3.25e6
	system_1,PhotovoltaicSolar,2e6
	system_1,DieselGenerator,1.6e6
STORAGE_COMPONENTS =
    system_1,LithiumBatterySystem,1.5e6,1.5e6
LOAD_COMPONENTS =
	system_1,ElectricLoadOne,1.59e6
GRID_COMPONENTS =
```

### [Storage](#Storage)
This section is dedicated to outlining the specific parameters of the storage system. The details provided encompass the key characteristics and capabilities of the storage component within the energy system.
```ini
[STORAGE]
START_SOC = 0.2
MIN_SOC = 0.2
MAX_SOC = 0.8
```

### [Profile](#Profile)
In this section, we establish the directories for the profiles associated with both generation and load components. This configuration facilitates organized access to essential data for these specific components.
```ini
[PROFILE]
POWER_PROFILE_DIR = energy_system_network/profiles/power/
LOAD_PROFILE =
	 ElectricLoadOne,teneriffe_load_normalized_10m.csv
GENERATION_PROFILE =
    PhotovoltaicSolar,teneriffe_solar_normalized_10m.csv
    WindTurbine,teneriffe_wind_normalized_10m.csv
```

**Note:** Custom profiles may be utilized in lieu of the default CSV files provided. To implement this, simply position your CSV files within the designated directories corresponding to each specific component.

## [Battery Configuration](#Battery-Config)
This part of the document elaborates on the various sections and parameters within the [`lithium_battery_setup.example.ini`](lithium_battery_setup.example.ini) file, focusing on the setup of the battery system.

### [AC-DC Converter](#ACDC-Converter)
This section defines the settings for an AC/DC converter, which is a device that converts alternating current (AC) to direct current (DC). The `NottonAcDcConverter` suggests a specific type or model of AC/DC converter being used.
```ini
[ACDC_CONVERTER]
TYPE = NottonAcDcConverter
```

### [HVAC](#HVAC)
This section suggests that no HVAC system is integrated or active in this setup, as indicated by the type `NoHeatingVentilationAirConditioning`.
```ini
[HVAC]
TYPE = NoHeatingVentilationAirConditioning
```

### [DC-DC Converter](#DCDC-Converter)
This section configures a DC/DC converter, which converts direct current from one voltage level to another. The `NoLossDcDcConverter` type implies that this converter is designed to operate with minimal or no energy loss, which is ideal for efficient energy systems.
```ini
[DCDC_CONVERTER]
TYPE = NoLossDcDcConverter
```

### [Cell](#Cell)
Here, `SonyLFP` is utilized as the type of lithium-ion battery cell.
```ini
[CELL]
TYPE = SonyLFP
```

### [Thermal Simulation](#Thermal-Simulation)
This section pertains to a thermal simulation feature within the system. Setting `ENABLE = False` indicates that thermal simulation is turned off and not included in the current configuration.
```ini
[THERMAL_SIMULATION]
ENABLE = False
```

### [Ambient Temperature Model](#Ambient-Temperature-Model)
This section is for configuring how the ambient (surrounding) temperature is modeled in the system. The `ConstantAmbientTemperature` type suggests that the model assumes a fixed, unchanging ambient temperature, as opposed to a dynamic or varying temperature model.
```ini
[AMBIENT_TEMPERATURE_MODEL]
TYPE = ConstantAmbientTemperature
```

Note: The explanation for the battery configuration can be found [here](../../documentation/lithium_battery_config.md).

## [Reference](#Reference)

[^1]: Parlikar, A., Truong, C. N., Jossen, A., & Hesse, H. (2021). The carbon footprint of island grids with lithium-ion battery systems: An analysis based on levelized emissions of energy supply. Renewable and Sustainable Energy Reviews, 149, 111353.