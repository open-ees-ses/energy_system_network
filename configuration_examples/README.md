# Example Configurations
In this section, we provide a range of example configurations to assist you in setting up your simulations. Each example includes a pre-configured `simulation.example.ini` file. If your simulation involves a battery setup, you will also need to utilize the `lithium_battery_setup.example.ini` file. To get started, follow the steps outlined below:
1. In the root directory of the program (where `main.py` is located), create two files: `simulation.local.ini` and `lithium_battery_setup.local.ini`.
2. Open the `simulation.example.ini` and `lithium_battery_setup.example.ini` files. Copy their contents into the `simulation.local.ini` and `lithium_battery_setup.local.ini` files you created in step 1, respectively.
3. Execute the `main.py` script to run your simulation.


You can find the following configurations:
* [Island Grid without Battery](01-island_grid_without_battery)
* [Island Grid with Battery](02-island_grid_with_battery)
* [Battery Assisted High Power Charging](03-battery_assisted_high_power_charging)
* [Home Energy System](04-home_energy_system)
* [Energy Arbitrage](05-energy_arbitrage)
* [Frequency Containment Reserve with Intraday Market Trading](06-fcr_idm)

Please click on the links to access the respective configurations.