# Energy Arbitrage

This document outlines an example configuration for an energy arbitrage system. The detailed paper relating to this example is currently under preparation and will be announced in this document upon its publication.

Below is an illustrative overview of the energy system, depicted in the following figure.

<div align="center">
  <img src="figures/EnergySystem.png" width="700" />
</div>

## Contents

- [System Configuration](#system-configuration)
  - [General Settings](#general-settings)
  - [Energy Management](#energy-management)
  - [Energy System Network](#energy-system-network)
  - [Storage](#storage)
  - [Profile](#profile)
- [Battery Configuration](#battery-configuration)
  - [AC-DC Converters](#ac-dc-converters)
  - [Housing](#housing)
  - [HVAC](#hvac)
  - [DC-DC Converter](#dc-dc-converters)
  - [Cell](#cell)
  - [Thermal Simulation](#thermal-simulation)
  - [Ambient Temperature Model](#ambient-temperature-model)
  - [Solar Irradiation Model](#solar-irradiation-model)

## [System Configuration](#System-Config)
This part of the document delves into the specifics of the sections and parameters detailed within the [`simulation.example.ini`](simulation.example.ini) file for the energy arbitrage system.

### [General Settings](#General-Settings)

This section outlines the foundational temporal settings of the simulation. It is scheduled to commence at `2018-01-01 00:00:00` and conclude on `2018-12-31 23:59:59`. The simulation operates with a time step interval set to `900` seconds. For the purpose of simulating a full one-year cycle, the `LOOP` parameter is configured to `1`. Additionally, the export interval is also established at `1`, ensuring that every time step is meticulously recorded in the result file. These parameters form the basis of the general section in the [`simulation.example.ini`](simulation.example.ini) file.
```ini
[GENERAL]
START = 2018-01-01 00:00:00
END = 2018-12-31 23:59:59
TIME_STEP = 900
LOOP = 1
EXPORT_INTERVAL = 1
```
### [Energy Management](#Energy-Management)
In this section, we specify the energy management parameters for the system. The strategy `NoStrategy` is implemented for `energy_system_network`, while `ArbitrageOptimization` is employed for `system_1`. In both scenarios, `Electricity` is the exclusive form of energy managed within this grid configuration. 

These configurations are reflected in the corresponding section of the configuration file.
```ini
[ENERGY_MANAGEMENT]
STRATEGY =
	energy_system_network,NoStrategy,Electricity
	system_1, ArbitrageOptimization, Electricity
OPTIMIZED_STRATEGY_TIME_HORIZON = 96
RERUN_OPTIMIZATION_TIMESTEPS = 72
ARBITRAGE_TYPE = Economic
```

### [Energy System Network](#Energy-System-Network)
This section describes `system_1`, a straightforward energy system that solely utilizes `Electricity`. The configuration of this system includes:
- **Generation Component**: Not applicable in this setup, as indicated by an empty `GENERATION_COMPONENTS` parameter.
- **Load Component**: Similarly, there is no load component within this configuration, leaving the `LOAD_COMPONENTS` parameter empty.
- **Storage Component**: The `LithiumBatterySystem`, with a power rating of `1.6 MW` and a storage capacity of `1.6 MWh`.
- **Main Grid Connection**: This system is linked to the main grid, which can supply a maximum of `2 MW`. However, feed-in to the grid is not enabled.

Further details are specified in the corresponding section of the [`simulation.example.ini`](simulation.example.ini) file.

```ini
[ENERGY_SYSTEM_NETWORK]
ENERGY_SYSTEMS =
	system_1,Electricity
GENERATION_COMPONENTS =
STORAGE_COMPONENTS =
	system_1,LithiumBatterySystem,1.6e6,1.6e6
LOAD_COMPONENTS =
GRID_COMPONENTS =
	system_1,SimpleElectricityGridConnection,2e6,True
```

### [Storage](#Storage)
This section is dedicated to outlining the specific parameters of the storage system. These details include key characteristics and functionalities of the storage component within the energy system.
```ini
[STORAGE]
START_SOC = 0
MIN_SOC = 0.0
MAX_SOC = 1.0
```

### [Profile](#Profile)
In the last section, we specify the directories for the profiles associated with the components. These settings are critical for organizing and accessing the necessary data for each component within the energy system.
```ini
[PROFILE]
EMISSIONS_PROFILE_DIR = energy_system_network/profiles/emissions/
GRID_EMISSIONS_PROFILE = grid_emissions_time_series_germany_2019_15min_trlosses.csv
ECONOMIC_PROFILE_DIR = energy_system_network/profiles/economic/
ECONOMIC_PROFILE = economic_idc_market_average_2019_15_min.csv
GRID_EMISSIONS_SCALING_FACTOR = 1
```

**Note:** Custom profiles may be utilized in lieu of the default CSV files provided. To implement this, simply position your CSV files within the designated directories corresponding to each specific component.

## [Battery Configuration](#Battery-Config)
This part of the document elaborates on the various sections and parameters within the [`lithium_battery_setup.example.ini`](lithium_battery_setup.example.ini) file, focusing on the setup of the battery system.

### [AC-DC Converters](#AC-DC-Converters)
In this simulation, `8` number of `NottonAcDcConverter` is used for the AC-DC converters, whose switch values are `0.9` for each.
```ini
[ACDC_CONVERTER]
TYPE = NottonAcDcConverter
NUMBER_OF_CONVERTERS = 8
SWITCH_VALUE = 0.9
```

### [Housing](#Housing)
The battery and its related devices are housed in a forty-foot container.
```ini
[HOUSING]
TYPE = FortyFtContainer
```

### [HVAC](#HVAC)
A fix COP heating ventilation air conditioning is used for maintaining the temperature at `25°C`, whose thermal power is `30 kW`.
```ini
[HVAC]
TYPE = FixCOPHeatingVentilationAirConditioning
THERMAL_POWER = 50000
SET_TEMPERATURE = 25
```

### [DC-DC Converter](#DC-DC-Converters)
A fix efficiency DC-DC converter is used in this simulation.
```ini
[DCDC_CONVERTER]
TYPE = FixEfficiencyDcDcConverter
```

### [Cell](#Cell)
A `SonyLFP` lithium-ion battery cell is used which has the values of `1` and `0.6` as its starting state of health and end of life values, respectively.
```ini
[CELL]
TYPE = SonyLFP
EOL = 0.6
START_SOH = 1
```

### [Thermal Simulation](#Thermal-Simulation)
To have a thermal simulation, we enable it as below:
```ini
[THERMAL_SIMULATION]
ENABLE = True
```

### [Ambient Temperature Model](#Ambient-Temperature-Model)
The location's ambient temperature is used for modelling the ambient temperature.
```ini
[AMBIENT_TEMPERATURE_MODEL]
TYPE = LocationAmbientTemperature
```

### [Solar Irradiation Model](#Solar-Irradiation-Model)
The location's solar irradiation is used for modelling the solar irradiation.
```ini
[SOLAR_IRRADIATION_MODEL]
TYPE = LocationSolarIrradiationModel
```

Note: The explanation for the battery configuration can be found [here](../../documentation/lithium_battery_config.md).
