# Island Grid (without Battery)

This document outlines an exemplary configuration for an island energy system that operates without the integration of a battery. For those interested in implementing this setup in their work, we highly recommend reading and citing the following study [^1] for a more comprehensive understanding.

The energy system's layout is depicted in the figure below.

<div align="center">
  <img src="figures/EnergySystem.png" width="700" />
</div>


## Contents

- [System Configuration](#system-configuration)
  - [General Settings](#general-settings)
  - [Energy Management](#energy-management)
  - [Energy System Network](#energy-system-network)
  - [Storage](#storage)
  - [Profile](#profile)
- [Reference](#reference)

## [System Configuration](#System-Config)
This part the document provides detailed explanations of the various sections and parameters within the [`simulation.example.ini`](simulation.example.ini) file.

### [General Settings](#General-Settings)
This section defines the simulation's temporal parameters. The simulation is programmed to commence on `2018-01-01 00:00:00` and conclude on `2018-12-31 23:59:59`. We have set the time step interval to `900` seconds. For the purpose of simulating a full-year period, the `LOOP` parameter is configured to `1`. Detailed settings for this section can be found in the [`simulation.example.ini`](simulation.example.ini) file.

```ini
[GENERAL]
START = 2018-01-01 00:00:00
END = 2018-12-31 23:59:59
TIME_STEP = 900
LOOP = 1
```
### [Energy Management](#Energy-Management)
This section delineates the parameters governing energy management. Specifically, the `NoStrategy` and `SimpleDeficitCoverage` strategies are implemented for `energy_system_network` and `system_1`, respectively. In both instances, `Electricity` is the sole form of energy managed. The `SimpleDeficitCoverage` strategy involves establishing a prioritized hierarchy of components, which is configured using the `SIMPLE_DEFICIT_COVERAGE_PRIORITY` parameter. Within `system_1`, this hierarchy is structured as follows:
1. `StorageComponents`
2. `GridComponents`
3. `CanRunGenerationComponents`

These settings are further elaborated in the corresponding section of the configuration file as below:
```ini
[ENERGY_MANAGEMENT]
STRATEGY =
	energy_system_network, NoStrategy, Electricity
	system_1, SimpleDeficitCoverage, Electricity
SIMPLE_DEFICIT_COVERAGE_PRIORITY =
	StorageComponents
	GridComponents
	CanRunGenerationComponents
```

### [Energy System Network](#Energy-System-Network)
This section details `system_1`, a streamlined energy system utilizing exclusively `Electricity` as its energy form. The system encompasses the following key components:

- **Generation Components**:
  - `Wind` Turbine: Capable of a maximum output of `3.25 MW`.
  - `PV` (Photovoltaic) Solar Panel: With a maximum production capacity of `2 MW`.
  - `Diesel Generator`: Producing up to `1.6 MW` of electricity.
- **Load Component**: `ElectricLoadOne`, which consumes up to `1.59 MW` of electricity.

It's important to note that this grid does not include **Storage** (battery), thus the `STORAGE_COMPONENTS` parameter is intentionally left blank. Omitting this parameter is crucial to ensure the configuration reflects a battery-less system. Additionally, since this is an island-based system, the `GRID_COMPONENTS` parameter is also left empty.

Further details can be found in the corresponding section of the [`simulation.example.ini`](simulation.example.ini) file as below:
```ini
[ENERGY_SYSTEM_NETWORK]
ENERGY_SYSTEMS =
	system_1,Electricity
GENERATION_COMPONENTS =
	system_1,WindTurbine,3.25e6
	system_1,PhotovoltaicSolar,2e6
	system_1,DieselGenerator,1.6e6
STORAGE_COMPONENTS =
LOAD_COMPONENTS =
	system_1,ElectricLoadOne,1.59e6
GRID_COMPONENTS =
```

### [Storage](#Storage)
Given that this energy system operates without battery storage, it is unnecessary to define a storage section within the configuration file. This omission reflects the design choice to exclude battery storage components from the system configuration.

### [Profile](#Profile)
In this section, we establish the directories for the profiles pertinent to both generation and load components. This configuration facilitates the organization and access to the necessary data for these components.
```ini
[PROFILE]
POWER_PROFILE_DIR = energy_system_network/profiles/power/
LOAD_PROFILE =
	 ElectricLoadOne,teneriffe_load_normalized_10m.csv
GENERATION_PROFILE =
    PhotovoltaicSolar,teneriffe_solar_normalized_10m.csv
    WindTurbine,teneriffe_wind_normalized_10m.csv
```
**Note:** Custom profiles may be utilized in lieu of the default CSV files provided. To implement this, simply position your CSV files within the designated directories corresponding to each specific component.

## [Reference](#Reference)

[^1]: Parlikar, A., Truong, C. N., Jossen, A., & Hesse, H. (2021). The carbon footprint of island grids with lithium-ion battery systems: An analysis based on levelized emissions of energy supply. Renewable and Sustainable Energy Reviews, 149, 111353.