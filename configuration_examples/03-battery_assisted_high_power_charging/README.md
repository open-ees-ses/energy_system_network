# Battery-Assisted High-Power Charging

This document outlines an exemplary configuration for a battery-assisted energy system designed for high-power charging applications. We encourage those interested in implementing this setup in their projects or research to review and reference the following study [^1].

An illustrative overview of the energy system configuration is provided in the figure below.

<div align="center">
  <img src="figures/EnergySystem.png" width="700" />
</div>

## Contents

- [System Configuration](#system-configuration)
  - [General Settings](#general-settings)
  - [Energy Management](#energy-management)
  - [Energy System Network](#energy-system-network)
  - [Storage](#storage)
  - [Profile](#profile)
- [Battery Configuration](#battery-configuration)
  - [AC-DC Converter](#acdc-converter)
  - [HVAC](#hvac)
  - [DC-DC Converter](#dcdc-converter)
  - [Cell](#cell)
  - [Thermal Simulation](#thermal-simulation)
  - [Ambient Temperature Model](#ambient-temperature-model)
- [Reference](#reference)

## [System Configuration](#System-Config)
This part of the document provides a detailed explanation of the various sections and parameters within the [`simulation.example.ini`](simulation.example.ini) file.

### [General Settings](#General-Settings)

This section outlines the temporal settings for the simulation. It is scheduled to start on `2018-01-01 00:00:00` and end on `2018-12-31 23:59:59`. The time step is configured at `900` seconds. To enable a simulation spanning a full year, the `LOOP` parameter is set to `1`. Additionally, the export interval is also set to `1`, ensuring that data from every time step is captured and recorded in the result file. The general section of the [`simulation.example.ini`](simulation.example.ini) file is structured as follows:
```ini
[GENERAL]
START = 2018-01-01 00:00:00
END = 2018-12-31 23:59:59
TIME_STEP = 900
LOOP = 1
EXPORT_INTERVAL = 1
```

### [Energy Management](#Energy-Management)
This section is dedicated to defining the energy management parameters. For this setup, `NoStrategy` is applied to the `energy_system_network` while `MultiSourcePeakShaving` is the chosen strategy for `system_1`. In both instances, `Electricity` is the exclusive energy form utilized. Within the `MultiSourcePeakShaving` approach, it is crucial to establish a hierarchy of components, as dictated by the `SIMPLE_DEFICIT_COVERAGE_PRIORITY` parameter. The prioritized hierarchy for `system_1` is structured as follows:
1. `StorageComponents`
2. `CanRunGenerationComponents`
3. `GridComponents`

This organization is reflected in the corresponding section of the configuration file as below:
```ini
[ENERGY_MANAGEMENT]
STRATEGY =
	energy_system_network, NoStrategy, Electricity
	system_1, MultiSourcePeakShaving, Electricity
SIMPLE_DEFICIT_COVERAGE_PRIORITY =
	StorageComponents
	CanRunGenerationComponents
	GridComponents
```

### [Energy System Network](#Energy-System-Network)
This section introduces `system_1`, a streamlined energy system that solely utilizes `Electricity` as its energy form. The key components of this system include:

- **Generation Component**: A `PV` (Photovoltaic) Solar Panel with a maximum electricity production capacity of `1 MW`.
- **Load Component**: The `EVChargingPointDC`, which consumes up to `3.5 MW` of electricity.
- **Storage Component**: The `LithiumBatterySystem`, capable of generating `1.5 MW` of electricity and featuring a storage capacity of `1.5 MWh`.
- **Main Grid Connection**: This system is connected to the main grid, which can supply a maximum of `2.6 MW`. However, feed-in to the grid is not enabled.

Further configuration details are specified in the corresponding section of the [`simulation.example.ini`](simulation.example.ini) file.
```ini
[ENERGY_SYSTEM_NETWORK]
ENERGY_SYSTEMS =
	system_1,Electricity
GENERATION_COMPONENTS =
	system_1,PhotovoltaicSolar,1e6
STORAGE_COMPONENTS =
	system_1,LithiumBatterySystem,1.5e6,1.5e6
LOAD_COMPONENTS =
	system_1,EVChargingPointDC,3.5e6
GRID_COMPONENTS =
	system_1,SimpleElectricityGridConnection,2.60e6,False
```

### [Storage](#Storage)
This section is dedicated to outlining the specific parameters of the storage system. These details include key characteristics and functionalities of the storage component within the energy system.
```ini
[STORAGE]
START_SOC = 0.01
MIN_SOC = 0.0
MAX_SOC = 1.0
```

### [Profile](#Profile)
In the last section, we specify the directories for the profiles associated with the components. These settings are critical for organizing and accessing the necessary data for each component within the energy system.
```ini
[PROFILE]
POWER_PROFILE_DIR = energy_system_network/profiles/power/
GENERATION_EMISSIONS_PROFILE_DIR = energy_system_network/profiles/emissions/
LOAD_PROFILE =
	EVChargingPointDC,charging_load_profile.csv
PV_GENERATION_PROFILE = berlin_solar_normalized_60m.csv
```

**Note:** Custom profiles may be utilized in lieu of the default CSV files provided. To implement this, simply position your CSV files within the designated directories corresponding to each specific component.

## [Battery Configuration](#Battery-Config)
This part of the document elaborates on the various sections and parameters within the [`lithium_battery_setup.example.ini`](lithium_battery_setup.example.ini) file, focusing on the setup of the battery system.

### [AC-DC Converter](#ACDC-Converter)
This section configures the AC/DC converters in the system. The `TYPE = NottonAcDcConverter` specifies the model of the AC/DC converter being used. The `NUMBER_OF_CONVERTERS = 5` indicates that there are five such converters in the system, which could be relevant for understanding the system's capacity or redundancy. The `SWITCH_VALUE = 0.9` is a parameter that represents a threshold or a specific operational setting for the converters.
```ini
[ACDC_CONVERTER]
TYPE = NottonAcDcConverter
NUMBER_OF_CONVERTERS = 5
SWITCH_VALUE = 0.9
```
### [Housing](#housing)
In the `HOUSING` section, the configuration is focused on the physical housing of the system components. The `TYPE = FortyFtContainer` specifies that the system is housed within a forty-foot container. This information is vital for understanding the physical size, portability, and spatial limitations of the system.
```ini
[HOUSING]
TYPE = FortyFtContainer
```

### [HVAC](#HVAC)
The HVAC section details the heating, ventilation, and air conditioning system. `TYPE = FixCOPHeatingVentilationAirConditioning` suggests a specific model or type of HVAC system with a fixed coefficient of performance. `THERMAL_POWER = 30000` indicates the thermal power capacity of the HVAC system, which is essential for understanding its heating or cooling capabilities. `SET_TEMPERATURE = 25` specifies the target temperature that the HVAC system aims to maintain, a critical factor for maintaining optimal operating conditions.
```ini
[HVAC]
TYPE = FixCOPHeatingVentilationAirConditioning
THERMAL_POWER = 30000
SET_TEMPERATURE = 25
```

### [DC-DC Converter](#DCDC-Converter)
In this section, the configuration of the DC/DC converter is specified. The `TYPE = FixEfficiencyDcDcConverter` defines the model or kind of the DC/DC converter, which has a fixed efficiency characteristic.
```ini
[DCDC_CONVERTER]
TYPE = FixEfficiencyDcDcConverter
```

### [Cell](#Cell)
This section focuses on the specifications of the battery cells used in the system. `TYPE = SonyLFP` indicates the brand and type of the cell. `EOL = 0.8` defines the end-of-life threshold for the battery cell, a key parameter for battery management and replacement scheduling. `START_SOH = 1` sets the initial state of health of the cell, crucial for monitoring the degradation over time.
```ini
[CELL]
TYPE = SonyLFP
EOL = 0.8
START_SOH = 1
```

### [Thermal Simulation](#Thermal-Simulation)
This section deals with the thermal simulation settings of the system. `ENABLE = True` indicates that the thermal simulation is active or enabled.
```ini
[THERMAL_SIMULATION]
ENABLE = True
```

### [Ambient Temperature Model](#Ambient-Temperature-Model)
In this section, the configuration pertains to how the ambient temperature is modeled within the system. `TYPE = LocationAmbientTemperature` suggests that the ambient temperature model is based on a specific location.
```ini
[AMBIENT_TEMPERATURE_MODEL]
TYPE = LocationAmbientTemperature
```
### [Solar Irradiation Model](#solar-irradiation-model)
This section configures how solar irradiation is modeled. `TYPE = LocationSolarIrradiationModel` implies that the solar irradiation model is based on geographical location.
```ini
[SOLAR_IRRADIATION_MODEL]
TYPE = LocationSolarIrradiationModel
```

Note: The explanation for the battery configuration can be found [here](../../documentation/lithium_battery_config.md).

## [Reference](#Reference)

[^1]: Parlikar, A., Schott, M., Godse, K., Kucevic, D., Jossen, A., & Hesse, H. (2023). High-power electric vehicle charging: Low-carbon grid integration pathways with stationary lithium-ion battery systems and renewable generation. Applied Energy, 333, 120541.