# Frequency Containment Reserve with Intraday Market Trading

This document outlines an exemplary configuration for a frequency containment reserve system that incorporates balancing through intraday market trading. The detailed paper relating to this example is currently in development and will be announced in this document upon its publication.

Below is an illustrative overview of the energy system, depicted in the following figure.

<div align="center">
  <img src="figures/EnergySystem.png" width="700" />
</div>

## Contents

- [System Configuration](#system-configuration)
  - [General Settings](#general-settings)
  - [Energy Management](#energy-management)
  - [Energy System Network](#energy-system-network)
  - [Storage](#storage)
  - [Profile](#profile)
- [Battery Configuration](#battery-configuration)
  - [AC-DC Converters](#ac-dc-converters)
  - [Housing](#housing)
  - [HVAC](#hvac)
  - [DC-DC Converter](#dc-dc-converters)
  - [Cell](#cell)
  - [Thermal Simulation](#thermal-simulation)
  - [Ambient Temperature Model](#ambient-temperature-model)
  - [Solar Irradiation Model](#solar-irradiation-model)
  - [Energy Management for Battery](#energy-management-for-battery)

## [System Configuration](#System-Config)
This part of the document delves into the detailed explanation of the sections and parameters within the [`simulation.example.ini`](simulation.example.ini) file, specifically tailored for the frequency containment reserve system with intraday market trading.

### [General Settings](#General-Settings)
This section outlines the foundational temporal settings of the simulation. It is scheduled to start at `2018-01-01 00:00:00` and conclude on `2018-12-31 23:59:59`. The simulation operates with a time step interval set to `900` seconds. For simulating a full one-year cycle, the `LOOP` parameter is configured to `1`. Additionally, the export interval is set to `1`, ensuring meticulous recording of data at every time step in the result file. These parameters form the basis of the general section in the [`simulation.example.ini`](simulation.example.ini) file.
```ini
[GENERAL]
START = 2018-01-01 00:00:00
END = 2018-12-31 23:59:59
TIME_STEP = 900
LOOP = 1
EXPORT_INTERVAL = 1
```
### [Energy Management](#Energy-Management)
In this section, the energy management parameters are specified. For this system, the `NoStrategy` approach is adopted for the `energy_system_network`, while the `SimSESExternalStrategy` is utilized for `system_1`. Notably, `Electricity` is the sole form of energy managed within this grid for both `energy_system_network` and `system_1`.

These configurations are detailed in the corresponding section of the configuration file.
```ini
[ENERGY_MANAGEMENT]
STRATEGY =
	energy_system_network, NoStrategy, Electricity
	system_1, SimSESExternalStrategy, Electricity
OPTIMIZED_STRATEGY_TIME_HORIZON = 96
RERUN_OPTIMIZATION_TIMESTEPS = 16
ARBITRAGE_TYPE = Economic
```

### [Energy System Network](#Energy-System-Network)
This section describes `system_1`, a basic energy system that exclusively uses `Electricity`. The components of this system include:
- **Generation Component**: Not present in this configuration, as indicated by an empty `GENERATION_COMPONENTS` parameter.
- **Load Component**: Similarly, there is no load component within this setup, leaving the `LOAD_COMPONENTS` parameter empty.
- **Storage Component**: The `LithiumBatterySystem`, with a power rating of `1.6 MW` and a storage capacity of `1.6 MWh`.
- **Main Grid Connection**: This system is connected to the main grid, capable of supporting up to `2 MW` of electricity. However, feed-in to the grid is not enabled.

Further details are outlined in the corresponding section of the [`simulation.example.ini`](simulation.example.ini) file.
```ini
[ENERGY_SYSTEM_NETWORK]
ENERGY_SYSTEMS =
	system_1,Electricity
GENERATION_COMPONENTS =
STORAGE_COMPONENTS =
	system_1,LithiumBatterySystem, 1.6e6, 1.6e6
LOAD_COMPONENTS =
GRID_COMPONENTS =
	system_1,SimpleElectricityGridConnection,2e6,True
```

### [Storage](#Storage)
This section is dedicated to outlining the specific parameters of the storage system. These details include key characteristics and functionalities of the storage component within the energy system.
```ini
[STORAGE]
START_SOC = 0.52
MIN_SOC = 0.0
MAX_SOC = 1.0
```

### [Profile](#Profile)
In this configuration, setting up profiles directories is not required. This aspect is reflected in the absence of profile-related section and parameters in [`simulation.example.ini`](simulation.example.ini).

## [Battery Configuration](#Battery-Config)
This part of the document elaborates on the various sections and parameters within the [`lithium_battery_setup.example.ini`](lithium_battery_setup.example.ini) file, focusing on the setup of the battery system.

### [AC-DC Converters](#AC-DC-Converters)
In this simulation, `8` number of `NottonAcDcConverter` is used for the AC-DC converters, whose switch values are `0.9` for each.
```ini
[ACDC_CONVERTER]
TYPE = NottonAcDcConverter
NUMBER_OF_CONVERTERS = 8
SWITCH_VALUE = 0.9
```

### [Housing](#Housing)
The battery and its related devices are housed in a twenty-foot container.
```ini
[HOUSING]
TYPE = TwentyFtContainer
```

### [HVAC](#HVAC)
A fix COP heating ventilation air conditioning is used for maintaining the temperature at `25°C`, whose thermal power is `30 kW`.
```ini
[HVAC]
TYPE = FixCOPHeatingVentilationAirConditioning
THERMAL_POWER = 30000
SET_TEMPERATURE = 25
```

### [DC-DC Converter](#DC-DC-Converters)
A fix efficiency DC-DC converter is used in this simulation.
```ini
[DCDC_CONVERTER]
TYPE = FixEfficiencyDcDcConverter
```

### [Cell](#Cell)
A `SonyLFP` lithium-ion battery cell is used which has the values of `1` and `0.6` as its starting state of health and end of life values, respectively.
```ini
[CELL]
TYPE = SonyLFP
EOL = 0.6
START_SOH = 1
```

### [Thermal Simulation](#Thermal-Simulation)
To have a thermal simulation, we enable it as below:
```ini
[THERMAL_SIMULATION]
ENABLE = True
```

### [Ambient Temperature Model](#Ambient-Temperature-Model)
The location's ambient temperature is used for modelling the ambient temperature.
```ini
[AMBIENT_TEMPERATURE_MODEL]
TYPE = LocationAmbientTemperature
```

### [Solar Irradiation Model](#Solar-Irradiation-Model)
The location's solar irradiation is used for modelling the solar irradiation.
```ini
[SOLAR_IRRADIATION_MODEL]
TYPE = LocationSolarIrradiationModel
```

### [Energy Management for Battery](#Energy-Management-for-Battery)
A multi-use to participate in the FCR market (German regulations) with IDM recharge is utilized here. The battery offers `1.12` MW and `0.48` MW for the FCR and IDM markets, respectively. The target SOC for the FCR simulations is `0.52` p.u. In addition, the full power reserve for alert state is `15` minutes or `0.25` hours (i.e., `FCR_RESERVE = 0.25`). Here, we don't use the local technical profile directory; that is, we set it to `False`. And, we used the `frequency_transnetBW_2019` profile for the frequency.
```ini
[ENERGY_MANAGEMENT]
STRATEGY = FcrIdmRechargeStacked
POWER_FCR = 1.12e6
POWER_IDM = 0.48e6
SOC_SET = 0.52
FCR_RESERVE = 0.25
USE_LOCAL_TECHNICAL_PROFILE_DIR = False
FREQUENCY_PROFILE = frequency_transnetBW_2019
```

Note: The explanation for the battery configuration can be found [here](../../documentation/lithium_battery_config.md).
