# Home Energy System

This document outlines an exemplary configuration for a home energy system. The corresponding paper detailing this example is currently in preparation and will be announced here upon publication.

An illustrative overview of the energy system configuration is provided in the figure below.

<div align="center">
  <img src="figures/EnergySystem.png" width="700" />
</div>

## Contents

- [System Configuration](#system-configuration)
  - [General Settings](#general-settings)
  - [Energy Management](#energy-management)
  - [Energy System Network](#energy-system-network)
  - [Storage](#storage)
  - [Profile](#profile)
- [Battery Configuration](#battery-configuration)
  - [AC-DC Converters](#ac-dc-converters)
  - [Housing](#housing)
  - [HVAC](#hvac)
  - [DC-DC Converter](#dc-dc-converters)
  - [Cell](#cell)
  - [Thermal Simulation](#thermal-simulation)
  - [Ambient Temperature Model](#ambient-temperature-model)
  - [Solar Irradiation Model](#solar-irradiation-model)

## [System Configuration](#System-Config)
This part of the document provides a detailed explanation of the various sections and parameters within the [`simulation.example.ini`](simulation.example.ini) file, specifically tailored for the home energy system.

### [General Settings](#General-Settings)

This section outlines the temporal parameters for the simulation. It is scheduled to start on `2018-01-01 00:00:00` and conclude on `2018-12-31 23:59:59`. The time step is set at `900` seconds. To enable simulation over a full-year period, the `LOOP` parameter is configured to `1`. Additionally, the export interval is set to `1`, ensuring that data from every time step is recorded in the result file. These settings establish the general framework in the [`simulation.example.ini`](simulation.example.ini) file as illustrated below:
```ini
[GENERAL]
START = 2018-01-01 00:00:00
END = 2018-12-31 23:59:59
TIME_STEP = 900
LOOP = 1
EXPORT_INTERVAL = 1
```
### [Energy Management](#Energy-Management)
This section is devoted to defining the energy management parameters. For this configuration, the `NoStrategy` approach is applied to the `energy_system_network`, while `SimpleDeficitCoverage` is used for `system_1`. In both cases, the energy form managed is exclusively `Electricity`. The `SimpleDeficitCoverage` strategy involves setting up a hierarchy of components, as specified by the `SIMPLE_DEFICIT_COVERAGE_PRIORITY` parameter. The hierarchy for `system_1` is established in the following order:
1. `StorageComponents`
2. `GridComponents`
3. `CanRunGenerationComponents`

These configurations are further detailed in the corresponding section of the configuration file.
```ini
[ENERGY_MANAGEMENT]
STRATEGY =
	energy_system_network,NoStrategy,Electricity
	system_1, SimpleDeficitCoverage, Electricity
SIMPLE_DEFICIT_COVERAGE_PRIORITY =
	StorageComponents
	GridComponents
	CanRunGenerationComponents
```

### [Energy System Network](#Energy-System-Network)
This subsection details `system_1`, a straightforward energy system that exclusively utilizes `Electricity`. The system is composed of the following components:

- **Generation Component**: A `PV` (Photovoltaic) Solar Panel capable of producing up to `5 kW` of electricity.
- **Load Components**: 
  - `ElectricLoadOne` and `EVHomeCharger`, both of which consume electricity according to their respective profiles. It's important to note that no additional scaling is applied (i.e., a setting of `1` indicates original scale usage).
    - **Note**: When using non-normalized profiles with actual values, set the peak power to `1` in the profile-based components to avoid any further scaling.
- **Storage Component**: The `LithiumBatterySystem`, capable of generating `5 kW` and featuring a storage capacity of `5 MWh`.
- **Main Grid Connection**: This system is linked to the main grid, supporting a maximum supply of `40 kW`. However, feed-in to the grid is not enabled.

Further configuration details are specified in the corresponding section of the [`simulation.example.ini`](simulation.example.ini) file.
```ini
[ENERGY_SYSTEM_NETWORK]
ENERGY_SYSTEMS =
	system_1,Electricity
GENERATION_COMPONENTS =
	system_1,PhotovoltaicSolar,5e3
STORAGE_COMPONENTS =
	system_1,LithiumBatterySystem,5e3,5e3
LOAD_COMPONENTS =
	system_1,ElectricLoadOne,1
	system_1,EVHomeCharger,1
GRID_COMPONENTS =
	system_1,SimpleElectricityGridConnection,40e3,True
```

### [Storage](#Storage)
This section is dedicated to outlining the specific parameters of the storage system. These details include key characteristics and functionalities of the storage component within the energy system.
```ini
[STORAGE]
START_SOC = 0
MIN_SOC = 0.0
MAX_SOC = 1.0
```

### [Profile](#Profile)
In the last section, we specify the directories for the profiles associated with the components. These settings are critical for organizing and accessing the necessary data for each component within the energy system.
```ini
[PROFILE]
POWER_PROFILE_DIR = energy_system_network/profiles/power/
LOAD_PROFILE =
	ElectricLoadOne,SBAPHousehold_load_Profile.csv.gz
	EVHomeCharger,EV_load_charging_power_at_home_Volkswagen_fulltime.csv.gz
GENERATION_PROFILE =
	PhotovoltaicSolar,SBAP_PV_solar_EEN_Power_Munich_2014.csv.gz
EMISSIONS_PROFILE_DIR = energy_system_network/profiles/emissions/
GRID_EMISSIONS_PROFILE = grid_emissions_time_series_germany_2019_15min_trlosses.csv
GRID_EMISSIONS_SCALING_FACTOR = 1
```

**Note:** Custom profiles may be utilized in lieu of the default CSV files provided. To implement this, simply position your CSV files within the designated directories corresponding to each specific component.

## [Battery Configuration](#Battery-Config)
This part of the document elaborates on the various sections and parameters within the [`lithium_battery_setup.example.ini`](lithium_battery_setup.example.ini) file, focusing on the setup of the battery system.

### [AC-DC Converter](#ACDC-Converter)
This section configures the AC/DC converters in the system. The `TYPE = NottonAcDcConverter` specifies the model of the AC/DC converter being used. The `NUMBER_OF_CONVERTERS = 1` indicates that there are one such converter in the system, which could be relevant for understanding the system's capacity or redundancy. The `SWITCH_VALUE = 0.9` is a parameter that represents a threshold or a specific operational setting for the converters.
```ini
[ACDC_CONVERTER]
TYPE = NottonAcDcConverter
NUMBER_OF_CONVERTERS = 1
SWITCH_VALUE = 0.9
```
### [Housing](#housing)
In the `HOUSING` section, the configuration is focused on the physical housing of the system components. The `TYPE = NoHousing` specifies that the system is not housed. This information is vital for understanding the physical size, portability, and spatial limitations of the system.
```ini
[HOUSING]
TYPE = NoHousing
```

### [HVAC](#HVAC)
This section suggests that no HVAC system is integrated or active in this setup, as indicated by the type `NoHeatingVentilationAirConditioning`.
```ini
[HVAC]
TYPE = NoHeatingVentilationAirConditioning
```

### [DC-DC Converter](#DCDC-Converter)
In this section, the configuration of the DC/DC converter is specified. The `TYPE = FixEfficiencyDcDcConverter` defines the model or kind of the DC/DC converter, which has a fixed efficiency characteristic.
```ini
[DCDC_CONVERTER]
TYPE = FixEfficiencyDcDcConverter
```

### [Cell](#Cell)
This section focuses on the specifications of the battery cells used in the system. `TYPE = SonyLFP` indicates the brand and type of the cell. `EOL = 0.6` defines the end-of-life threshold for the battery cell, a key parameter for battery management and replacement scheduling. `START_SOH = 1` sets the initial state of health of the cell, crucial for monitoring the degradation over time.
```ini
[CELL]
TYPE = SonyLFP
EOL = 0.6
START_SOH = 1
```

### [Thermal Simulation](#Thermal-Simulation)
This section deals with the thermal simulation settings of the system. `ENABLE = True` indicates that the thermal simulation is active or enabled.
```ini
[THERMAL_SIMULATION]
ENABLE = True
```

### [Ambient Temperature Model](#Ambient-Temperature-Model)
This section is for configuring how the ambient (surrounding) temperature is modeled in the system. The `ConstantAmbientTemperature` type suggests that the model assumes a fixed, unchanging ambient temperature, as opposed to a dynamic or varying temperature model.
```ini
[AMBIENT_TEMPERATURE_MODEL]
TYPE = ConstantAmbientTemperature
```
### [Solar Irradiation Model](#solar-irradiation-model)
This section configures how solar irradiation is modeled. `TYPE = NoSolarIrradiationModel` implies that there is no solar irradiation model.
```ini
[SOLAR_IRRADIATION_MODEL]
TYPE = NoSolarIrradiationModel
```

Note: The explanation for the battery configuration can be found [here](../../documentation/lithium_battery_config.md).
