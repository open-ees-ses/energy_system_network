"""
The unit_converter script provides functions to convert quantities to different units
"""
import numpy as np

# SI multiplier prefixes
MILLI: float = 1e-3
CENTI: float = 1e-2
KILO: float = 1e3
MEGA: float = 1e6
GIGA: float = 1e9

# Conversions
HOUR: float = 3600  # seconds
MINUTE: float = 60  # seconds
PERCENT: float = 100
ANNUAL_HOURS: float = 8760  # hours


def W_to_kW(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts W to kW
    :param value: value in W to be converted as float
    :return: returns value in kW as float
    """
    return value / KILO  # in kW


def kW_to_W(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts kW to W
    :param value: value in kW to be converted as float
    :return: returns value in W as float
    """
    return value * KILO  # in W


def MW_to_W(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts MW to W
    :param value: value in MW to be converted as float
    :return: returns value in W as float
    """
    return value * MEGA  # in W


def W_to_MW(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts W to MW
    :param value: value in W to be converted as float
    :return: returns value in MW as float
    """
    return value / MEGA  # in MW


def MW_to_kW(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts MW to kW
    :param value: value in MW to be converted as float
    :return: returns value in kW as float
    """
    return value * MEGA / KILO  # in kW


def kW_to_MW(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts kW to MW
    :param value: value in kW to be converted as float
    :return: returns value in MW as float
    """
    return value / (MEGA / KILO)  # in MW


def J_to_kWh(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts J to kWh
    :param value: value in J to be converted as float
    :return: returns value in kWh as float
    """
    return value / (KILO * HOUR)  # in kWh


def kWh_to_J(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts kWh to J
    :param value: value in kWh to be converted as float
    :return: returns value in J as float
    """
    return value * (KILO * HOUR)  # in J


def Wh_to_J(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts Wh to J
    :param value: value in Wh to be converted as float
    :return: returns value in J as float
    """
    return value * HOUR  # in J


def J_to_Wh(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts J to Wh
    :param value: value in J to be converted as float
    :return: returns value in Wh as float
    """
    return value / HOUR  # in Wh


def Wh_to_kWh(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts Wh to kWh
    :param value: value in Wh to be converted as float
    :return: returns value in kWh as float
    """
    return value / KILO  # in kWh


def kWh_to_Wh(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts kWh to Wh
    :param value: value in kWh to be converted as float
    :return: returns value in Wh as float
    """
    return value * KILO  # in Wh


def g_to_kg(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts g to kg
    :param value: value in g to be converted as float
    :return: returns value in kg as float
    """
    return value / KILO  # in kg


def kg_to_g(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts kg to g
    :param value: value in kg to be converted as float
    :return: returns value in g as float
    """
    return value * KILO  # in g


def s_to_h(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts seconds to hours
    :param value: value in s to be converted as float
    :return: returns value in h as float
    """
    return value / HOUR  # in h


def h_to_s(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts hours to seconds
    :param value: value in h to be converted as float
    :return: returns value in s as float
    """
    return value * HOUR  # in s


def pc_to_pu(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts percentage to per unit
    :param value: value in percent to be converted as float
    :return: eturns value in p.u. as float
    """
    return value/PERCENT  # in p.u.


def pu_to_pc(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    Converts per unit to percent
    :param value: value in p.u. to be converted as float
    :return: returns value in percent as float
    """
    return value * PERCENT  # in percent


def years_to_hours(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    converts years to hours
    :param value: value in years to be converted as float
    :return: returns value in hours as float
    """
    return value * ANNUAL_HOURS  # in hours


def hours_to_years(value: (float, int, np.ndarray)) -> (float, np.ndarray):
    """
    converts hours to years
    :param value: value in hours to be converted as float
    :return: returns value in years as float
    """
    return value / ANNUAL_HOURS  # in years
