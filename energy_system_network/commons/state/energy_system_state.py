from abc import ABC
from energy_system_network.commons.state.state import State


class EnergySystemState(State, ABC):
    """
    Represents the current physical state of the energy system, detailing its main electrical parameters.

    Attributes:
        ENERGY_SYSTEM_ID (str): Identifier for the energy system.
        COMPONENT_ID (str): Identifier for the component within the energy system.

    Methods:
        __init__(energy_system_id: int, component_id: int): Initializes the energy system state with system and component IDs.
        id(): Property that returns a unique identifier for the energy system state.
    """

    ENERGY_SYSTEM_ID = 'EnergySystem ID'
    COMPONENT_ID = 'Component ID'

    def __init__(self, energy_system_id: int, component_id: int):
        """
        Initializes the energy system state with system and component IDs.

        Args:
            energy_system_id (int): The ID of the energy system.
            component_id (int): The ID of the component within the energy system.

        Returns:
            None
        """
        super().__init__()
        self._initialize()
        self.set(self.ENERGY_SYSTEM_ID, energy_system_id)
        self.set(self.COMPONENT_ID, component_id)

    @property
    def id(self) -> str:
        """
        Returns a unique identifier for the energy system state.

        Returns:
            str: A unique identifier in the format 'SYSTEM<energy_system_id><component_id>'.
        """
        return 'SYSTEM' + str(self.get(self.ENERGY_SYSTEM_ID)) + str(self.get(self.COMPONENT_ID))
