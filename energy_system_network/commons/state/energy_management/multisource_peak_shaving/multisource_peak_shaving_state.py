from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState


class MultiSourcePeakShavingState(EnergyManagementState):
    """
    Represents the current state of the operating strategy focused on peak shaving across multiple sources.

    Attributes:
        TITLE (str): The title identifier for this state.

    Methods:
        __init__(energy_system_id: str): Initializes a new instance of the MultiSourcePeakShavingState.
        energy_system_id() -> str: Retrieves the ID of the associated energy system.
        grid_power() -> float: Retrieves the power from the grid.
        grid_power(value: float): Sets the power from the grid.
        storage_power() -> float: Retrieves the power from the storage system.
        storage_power(value: float): Sets the power from the storage system.
        must_fulfil_load() -> float: Retrieves the power that must be fulfilled.
        must_fulfil_load(value: float): Sets the power that must be fulfilled.
        residual_load_final() -> float: Retrieves the final residual load.
        residual_load_final(value: float): Sets the final residual load.
        must_run_generation() -> float: Retrieves the power of the must-run generation.
        must_run_generation(value: float): Sets the power of the must-run generation.
        id() -> str: Retrieves the unique identifier for this state.
    """
    TITLE: str = 'MultiSourcePeakShaving'
    # TODO Need to ensure that values include the scaling factors. Currently values are written directly from the CSVs.

    def __init__(self, energy_system_id):
        """
        Initializes a new instance of the MultiSourcePeakShavingState.

        Args:
            energy_system_id (str): Identifier for the associated energy system.
        """
        super().__init__()
        self._initialize()
        self.set(self.ENERGY_SYSTEM_ID, energy_system_id)

    @property
    def energy_system_id(self) -> float:
        """
        Retrieves the ID of the associated energy system.

        Returns:
            str: Identifier for the energy system.
        """
        return self.get(self.ENERGY_SYSTEM_ID)

    @property
    def grid_power(self) -> float:
        """
        Retrieves the power from the grid.

        Returns:
            float: Power in watts from the grid.
        """
        return self.get(self.GRID_POWER)

    @grid_power.setter
    def grid_power(self, value: float) -> None:
        """
        Sets the power from the grid.

        Args:
            value (float): Power in watts from the grid.
        """
        self.set(self.GRID_POWER, value)

    @property
    def storage_power(self) -> float:
        """
        Retrieves the power from the storage system.

        Returns:
            float: Power in watts from the storage system.
        """
        return self.get(self.STORAGE_POWER)

    @storage_power.setter
    def storage_power(self, value: float) -> None:
        """
        Sets the power from the storage system.

        Args:
            value (float): Power in watts from the storage system.
        """
        self.set(self.STORAGE_POWER, value)

    @property
    def must_fulfil_load(self) -> float:
        """
        Retrieves the power that must be fulfilled.

        Returns:
            float: Power in watts that must be fulfilled.
        """
        return self.get(self.MUST_FULFIL_LOAD)

    @must_fulfil_load.setter
    def must_fulfil_load(self, value: float) -> None:
        """
        Sets the power that must be fulfilled.

        Args:
            value (float): Power in watts that must be fulfilled.
        """
        self.set(self.MUST_FULFIL_LOAD, value)

    @property
    def residual_load_final(self) -> float:
        """
        Retrieves the final residual load.

        Returns:
            float: Final residual load in watts.
        """
        return self.get(self.RESIDUAL_LOAD_FINAL)

    @residual_load_final.setter
    def residual_load_final(self, value: float) -> None:
        """
        Sets the final residual load.

        Args:
            value (float): Final residual load in watts.
        """
        self.set(self.RESIDUAL_LOAD_FINAL, value)

    @property
    def must_run_generation(self) -> float:
        """
        Retrieves the power of the must-run generation.

        Returns:
            float: Power in watts from the must-run generation.
        """
        return self.get(self.MUST_RUN_GENERATION_POWER)

    @must_run_generation.setter
    def must_run_generation(self, value: float) -> None:
        """
        Sets the power of the must-run generation.

        Args:
            value (float): Power in watts for the must-run generation.
        """
        self.set(self.MUST_RUN_GENERATION_POWER, value)

    @property
    def id(self) -> str:
        """
        Retrieves the unique identifier for this state.

        Returns:
            str: Unique identifier combining the title and energy system ID.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID
