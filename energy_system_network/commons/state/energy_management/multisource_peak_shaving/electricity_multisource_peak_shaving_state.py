from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_management.multisource_peak_shaving.multisource_peak_shaving_state import \
    MultiSourcePeakShavingState


class ElectricityMultiSourcePeakShavingState(MultiSourcePeakShavingState):
    """
    Represents the current state of the operating strategy specific to electricity form, aiming for multi-source peak shaving.

    Attributes:
        TITLE (str): The title identifier for this state with an appended indication specific to electricity.

    Methods:
        __init__(energy_system_id: int): Constructor for initializing the ElectricityMultiSourcePeakShavingState.
        id() -> str: Returns the unique identifier for the electricity-specific multi-source peak shaving state.
    """
    def __init__(self, energy_system_id: int):
        """
        Initializes the ElectricityMultiSourcePeakShavingState, extending the multi-source peak shaving state by specifying it for electricity.

        Args:
            energy_system_id (int): Identifier for the associated energy system.
        """
        super().__init__(energy_system_id)
        self.TITLE += EnergyForm.ELECTRICITY + ' '

    @property
    def id(self) -> str:
        """
        Retrieves the unique identifier for the state, including details specific to electricity form.

        Return:
            str: Unique identifier combining the title, energy system ID constant, and the instance's specific energy system ID.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID + str(self.energy_system_id)
