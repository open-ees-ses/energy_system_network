from energy_system_network.commons.state.state import State


class EnergyManagementState(State):
    """
    Represents the current state of the energy management system (such as PV, Load, etc.).

    Attributes:
        GRID_POWER (str): Descriptor for the grid power in watts.
        STORAGE_POWER (str): Descriptor for the storage power in watts.
        MUST_FULFIL_LOAD (str): Descriptor for the must-fulfil load in watts.
        RESIDUAL_LOAD_FINAL (str): Descriptor for the final residual load in watts.
        MUST_RUN_GENERATION_POWER (str): Descriptor for the must-run generation in watts.
        CAN_RUN_GENERATION_POWER (str): Descriptor for the can-run generation in watts.
        CAN_FULFIL_LOAD (str): Descriptor for the can-fulfil load in watts.
        RESIDUAL_LOAD_MUST_RUN_MUST_FULFIL (str): Descriptor for the residual load post must-run and must-fulfil operations in watts.
        GENERATION_REFERENCE_POWER (str): Descriptor for the generation reference power in watts.
        GRID_REFERENCE_POWER (str): Descriptor for the grid reference power in watts.
        STORAGE_REFERENCE_POWER (str): Descriptor for the storage reference power in watts.
        STORAGE_REFERENCE_SOC (str): Descriptor for the storage reference state of charge in per unit.
        STORAGE_POWER_FULFILMENT (str): Descriptor for the storage power fulfilment in per unit.
        CASH_FLOW_TIMESTEP (str): Descriptor for the cash flow in EUR for each timestep.
        ENERGY_PRICE_TIMESTEP (str): Descriptor for the energy price in EUR/kWh for each timestep.
        ENERGY_SYSTEM_ID (str): Identifier for the energy system.

    Methods:
        __init__(): Constructor method for initializing the EnergyManagementState.
        id(): Property that returns a constant string 'EMS' representing the ID of the energy management system.
    """
    GRID_POWER = 'Grid power in W'
    STORAGE_POWER = 'Storage Power in W'
    MUST_FULFIL_LOAD = 'Must-fulfil load in W'
    RESIDUAL_LOAD_FINAL = 'Final residual load in W'

    MUST_RUN_GENERATION_POWER = 'Must-run generation in W'
    CAN_RUN_GENERATION_POWER = 'Can-run Generation in W'
    CAN_FULFIL_LOAD = 'Can-fulfil load in W'
    RESIDUAL_LOAD_MUST_RUN_MUST_FULFIL = 'Residual load Must-run/fulfil in W'

    GENERATION_REFERENCE_POWER = 'Generation reference power in W'
    GRID_REFERENCE_POWER = 'Grid reference power in W'
    STORAGE_REFERENCE_POWER = 'Storage reference power in W'
    STORAGE_REFERENCE_SOC = 'Storage reference SOC in p.u.'

    STORAGE_POWER_FULFILMENT = 'Storage power fulfilment in p.u.'
    # SOC_PREDICTION_ACCURACY = 'SOC prediction accuracy in p.u.'

    CASH_FLOW_TIMESTEP: str = 'Cash flow in EUR'
    ENERGY_PRICE_TIMESTEP: str = 'Energy price in EUR/kWh'

    ENERGY_SYSTEM_ID = 'EnergySystem'

    def __init__(self):
        """
        Initializes the EnergyManagementState by calling the parent constructor and further initializing specific
        attributes.
        """
        super().__init__()
        self._initialize()

    @property
    def id(self) -> str:
        """
        Provides a constant ID for the Energy Management System.

        Return:
            str: Identifier for the energy management system ('EMS').
        """
        return 'EMS'
