from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_management.ev_home.ev_home_state import EVHomeState


class ElectricityEVHomeState(EVHomeState):
    """
    Represents the state of an electric vehicle (EV) home energy system focusing on electricity.

    Attributes:
        TITLE (str): The title string representing the energy form, appended with electricity information.

    Methods:
        __init__(energy_system_id: int): Initializes the state with a given energy system ID, with specific handling for electricity-focused EV home energy systems.
        id(): Generates an identifier for the electricity EV home state, incorporating energy system details.
    """
    def __init__(self, energy_system_id: int):
        """
        Initializes the state for an electric vehicle (EV) home energy system that focuses on electricity with a given energy system ID.

        Args:
            energy_system_id (int): Identifier for the energy system.
        """
        super().__init__(energy_system_id)
        self.TITLE += EnergyForm.ELECTRICITY + ' '

    @property
    def id(self) -> str:
        """
        Generates an identifier for the electricity EV home state by concatenating the title, static energy system ID,
        and the instance's energy system ID.

        Return:
            str: The constructed identifier string.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID + str(self.energy_system_id)
