from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState


class EVHomeState(EnergyManagementState):
    """
    Represents the current state of the Energy Management, especially focusing on Electric Vehicles (EV) home systems.

    Attributes:
        TITLE (str): A string representing the title or label of this energy management state.

    Methods:
        __init__(energy_system_id): Constructor method to initialize the EV home state.
        energy_system_id(): Getter for the energy system ID.
        must_run_generation_power(): Getter and setter for must run generation power.
        can_run_generation_power(): Getter and setter for can run generation power.
        grid_power(): Getter and setter for grid power.
        storage_power(): Getter and setter for storage power.
        must_fulfil_load(): Getter and setter for must fulfil load.
        can_fulfil_load(): Getter and setter for can fulfil load.
        residual_load_must_run_must_fulfil(): Getter and setter for residual load that must run and must fulfil.
        residual_load_final(): Getter and setter for the final residual load.
        generation_reference_power(): Getter and setter for generation reference power.
        grid_reference_power(): Getter and setter for grid reference power.
        storage_reference_power(): Getter and setter for storage reference power.
        storage_power_fulfilment(): Getter and setter for storage power fulfilment.
        storage_reference_soc(): Getter and setter for storage reference state of charge (SOC).
        cash_flow_timestep(): Getter and setter for cash flow at the current timestep.
        energy_price_timestep(): Getter and setter for energy price at the current timestep.
        id(): Getter for the ID which is a combination of the title and the energy system ID.
    """
    TITLE: str = 'EVHome'
    # TODO Need to ensure that values include the scaling factors. Currently values are written directly from the CSVs.

    def __init__(self, energy_system_id):
        """
        Initializes the state of the EV home energy system.

        Args:
            energy_system_id: Identifier for the energy system.
        """
        super().__init__()
        self._initialize()
        self.set(self.ENERGY_SYSTEM_ID, energy_system_id)

    @property
    def energy_system_id(self) -> float:
        """
        Retrieves the energy system ID.

        Return:
            float: The energy system ID.
        """
        return self.get(self.ENERGY_SYSTEM_ID)

    @property
    def must_run_generation_power(self) -> float:
        """
        Retrieves the power that must run for generation.

        Returns:
            float: The power value that must run for generation.
        """
        return self.get(self.MUST_RUN_GENERATION_POWER)

    @must_run_generation_power.setter
    def must_run_generation_power(self, value: float) -> None:
        """
        Sets the power that must run for generation.

        Args:
            value (float): The power value to be set.
        """
        self.set(self.MUST_RUN_GENERATION_POWER, value)

    @property
    def can_run_generation_power(self) -> float:
        """
        Retrieves the power that can run for generation.

        Returns:
            float: The power value that can run for generation.
        """
        return self.get(self.CAN_RUN_GENERATION_POWER)

    @can_run_generation_power.setter
    def can_run_generation_power(self, value: float) -> None:
        """
        Sets the power that can run for generation.

        Args:
            value (float): The power value to be set.
        """
        self.set(self.CAN_RUN_GENERATION_POWER, value)

    @property
    def grid_power(self) -> float:
        """
        Retrieves the power coming from the grid.

        Returns:
            float: Power in watts from the grid.
        """
        return self.get(self.GRID_POWER)

    @grid_power.setter
    def grid_power(self, value: float) -> None:
        """
        Sets the power coming from the grid.

        Args:
            value (float): Power in watts to be set for the grid.
        """
        self.set(self.GRID_POWER, value)

    @property
    def storage_power(self) -> float:
        """
        Returns storage power in W (pos: dch, neg: ch)
        :return: storage power in W
        """
        return self.get(self.STORAGE_POWER)

    @storage_power.setter
    def storage_power(self, value: float) -> None:
        """
        Sets storage power in W (pos: dch, neg: ch)
        :param value: storage power value in W
        :return: None
        """
        self.set(self.STORAGE_POWER, value)

    @property
    def must_fulfil_load(self) -> float:
        """
        Retrieves the load that must be fulfilled.

        Returns:
            float: The load that has to be fulfilled.
        """
        return self.get(self.MUST_FULFIL_LOAD)

    @must_fulfil_load.setter
    def must_fulfil_load(self, value: float) -> None:
        """
        Sets the load that must be fulfilled.

        Args:
            value (float): The load value to be set.
        """
        self.set(self.MUST_FULFIL_LOAD, value)

    @property
    def can_fulfil_load(self) -> float:
        """
        Retrieves the load that can be fulfilled.

        Returns:
            float: The load that can be fulfilled.
        """
        return self.get(self.CAN_FULFIL_LOAD)

    @can_fulfil_load.setter
    def can_fulfil_load(self, value: float) -> None:
        """
        Sets the load that can be fulfilled.

        Args:
            value (float): The load value to be set.
        """
        self.set(self.CAN_FULFIL_LOAD, value)

    @property
    def residual_load_must_run_must_fulfil(self) -> float:
        """
        Retrieves the residual load after accounting for the must-run and must-fulfil conditions.

        Returns:
            float: Residual load in watts.
        """
        return self.get(self.RESIDUAL_LOAD_MUST_RUN_MUST_FULFIL)

    @residual_load_must_run_must_fulfil.setter
    def residual_load_must_run_must_fulfil(self, value: float) -> None:
        """
        Sets the residual load after accounting for the must-run and must-fulfil conditions.

        Args:
            value (float): Residual load value in watts.
        """
        self.set(self.RESIDUAL_LOAD_MUST_RUN_MUST_FULFIL, value)

    @property
    def residual_load_final(self) -> float:
        """
        Retrieves the final residual load after all conditions.

        Returns:
            float: Final residual load in watts.
        """
        return self.get(self.RESIDUAL_LOAD_FINAL)

    @residual_load_final.setter
    def residual_load_final(self, value: float) -> None:
        """
        Sets the final residual load after all conditions.

        Args:
            value (float): Final residual load value in watts.
        """
        self.set(self.RESIDUAL_LOAD_FINAL, value)

    @property
    def generation_reference_power(self) -> float:
        """
        Retrieves the reference power for generation.

        Returns:
            float: Reference generation power in watts.
        """
        return self.get(self.GENERATION_REFERENCE_POWER)

    @generation_reference_power.setter
    def generation_reference_power(self, value: float):
        """
        Sets the reference power for generation.

        Args:
            value (float): Reference generation power in watts.
        """
        self.set(self.GENERATION_REFERENCE_POWER, value)

    @property
    def grid_reference_power(self) -> float:
        """
        Retrieves the reference power from the grid.

        Returns:
            float: Reference power from the grid in watts.
        """
        return self.get(self.GRID_REFERENCE_POWER)

    @grid_reference_power.setter
    def grid_reference_power(self, value: float):
        """
        Sets the reference power from the grid.

        Args:
            value (float): Reference power from the grid in watts.
        """
        self.set(self.GRID_REFERENCE_POWER, value)

    @property
    def storage_reference_power(self) -> float:
        """
        Retrieves the storage's reference power.

        Returns:
            float: Reference power of the storage system in watts.
        """
        return self.get(self.STORAGE_REFERENCE_POWER)

    @storage_reference_power.setter
    def storage_reference_power(self, value: float):
        """
        Sets the storage's reference power.

        Args:
            value (float): Reference power of the storage system in watts.
        """
        self.set(self.STORAGE_REFERENCE_POWER, value)

    @property
    def storage_power_fulfilment(self) -> float:
        """
        Retrieves the power fulfilment of the storage system.

        Returns:
            float: Power fulfilment of the storage in watts.
        """
        return self.get(self.STORAGE_POWER_FULFILMENT)

    @storage_power_fulfilment.setter
    def storage_power_fulfilment(self, value: float):
        """
        Sets the power fulfilment of the storage system.

        Args:
            value (float): Power fulfilment of the storage in watts.
        """
        self.set(self.STORAGE_POWER_FULFILMENT, value)

    @property
    def storage_reference_soc(self) -> float:
        """
        Retrieves the reference state-of-charge (SOC) of the storage system.

        Returns:
            float: Reference SOC of the storage system in percentage.
        """
        return self.get(self.STORAGE_REFERENCE_SOC)

    @storage_reference_soc.setter
    def storage_reference_soc(self, value: float):
        """
        Sets the reference state-of-charge (SOC) of the storage system.

        Args:
            value (float): Reference SOC of the storage system in percentage.
        """
        self.set(self.STORAGE_REFERENCE_SOC, value)

    # @property
    # def soc_prediction_accuracy(self) -> float:
    #     return self.get(self.SOC_PREDICTION_ACCURACY)
    #
    # @soc_prediction_accuracy.setter
    # def soc_prediction_accuracy(self, value: float):
    #     self.set(self.SOC_PREDICTION_ACCURACY, value)

    @property
    def cash_flow_timestep(self) -> float:
        """
        Returns cash flow in current timestep in EUR (pos: revenue, neg: expense)
        :return: cash flow in EUR
        """
        return self.get(self.CASH_FLOW_TIMESTEP)

    @cash_flow_timestep.setter
    def cash_flow_timestep(self, value: float) -> None:
        """
        Sets cash flow in the current timestep in EUR (pos: revenue, neg: expense)
        :param value: value for cash flow in EUR
        :return: None
        """
        self.set(self.CASH_FLOW_TIMESTEP, value)

    @property
    def energy_price_timestep(self) -> float:
        """
        Returns energy price in current timestep in EUR/kWh
        :return: energy price in EUR/kWh as float
        """
        return self.get(self.ENERGY_PRICE_TIMESTEP)

    @energy_price_timestep.setter
    def energy_price_timestep(self, value: float) -> None:
        """
        Sets energy price in the current timestep in EUR/kWh
        :param value: value for energy price in EUR/kWh
        :return: None
        """
        self.set(self.ENERGY_PRICE_TIMESTEP, value)

    @property
    def id(self) -> str:
        """
        Constructs an identifier for the EV home state using the title and energy system ID.

        Return:
            str: The constructed identifier string.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID
