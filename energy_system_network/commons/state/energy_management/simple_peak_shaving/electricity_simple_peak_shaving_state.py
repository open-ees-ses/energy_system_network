from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_management.simple_peak_shaving.simple_peak_shaving_state import \
    SimplePeakShavingState


class ElectricitySimplePeakShavingState(SimplePeakShavingState):
    """
    Represents the state for electricity-based simple peak shaving energy systems.

    Attributes:
        TITLE (str): The title for the electricity-based energy system. It is inherited and extended with the energy form.
        ENERGY_SYSTEM_ID (str): Identifier for the energy system. (Inherited from the parent class)
        energy_system_id (int): ID of the energy system.

    Methods:
        __init__(energy_system_id: int): Initializes the ElectricitySimplePeakShavingState with a given energy system ID.
        id() -> str: Property that returns a unique identifier for the electricity simple peak shaving state.
    """
    def __init__(self, energy_system_id: int):
        """
        Initializes the ElectricitySimplePeakShavingState with a given energy system ID.

        Args:
            energy_system_id (int): The ID for the energy system.
        """
        super().__init__(energy_system_id)
        self.TITLE += EnergyForm.ELECTRICITY + ' '

    @property
    def id(self) -> str:
        """
        Gets a unique identifier for the electricity simple peak shaving state.

        Returns:
            str: The unique identifier.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID + str(self.energy_system_id)
