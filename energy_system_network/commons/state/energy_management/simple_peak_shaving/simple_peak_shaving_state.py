from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState


class SimplePeakShavingState(EnergyManagementState):
    """
    Represents the current state of the simple peak shaving operating strategy.

    Attributes:
        TITLE (str): A static title used to define the state's strategy.
        ENERGY_SYSTEM_ID (str): Identifier for the energy system, inherited from the parent class.

    Methods:
        __init__(energy_system_id): Initializes the SimplePeakShavingState with a given energy system ID.
        energy_system_id() -> float: Property that returns the energy system's ID.
        grid_power() -> float: Property that returns the grid power value.
        grid_power(value: float): Sets the grid power value.
        storage_power() -> float: Property that returns the storage power value.
        storage_power(value: float): Sets the storage power value.
        must_fulfil_load() -> float: Property that returns the must fulfil load value.
        must_fulfil_load(value: float): Sets the must fulfil load value.
        residual_load_final() -> float: Property that returns the residual load final value.
        residual_load_final(value: float): Sets the residual load final value.
        id() -> str: Property that returns a unique identifier for the simple peak shaving state.
    """
    TITLE: str = 'SimplePeakShaving'
    # TODO Need to ensure that values include the scaling factors. Currently values are written directly from the CSVs.

    def __init__(self, energy_system_id):
        """
        Initializes the SimplePeakShavingState with a given energy system ID.

        Args:
            energy_system_id: The ID for the energy system.
        """
        super().__init__()
        self._initialize()
        self.set(self.ENERGY_SYSTEM_ID, energy_system_id)

    @property
    def energy_system_id(self) -> float:
        """
        Returns the energy system's ID.
        """
        return self.get(self.ENERGY_SYSTEM_ID)

    @property
    def grid_power(self) -> float:
        """
        Returns the grid power value.
        """
        return self.get(self.GRID_POWER)

    @grid_power.setter
    def grid_power(self, value: float) -> None:
        """
        Sets the grid power value.

        Args:
            value (float): the grid power value to set.
        """
        self.set(self.GRID_POWER, value)

    @property
    def storage_power(self) -> float:
        """
        Returns the storage power value.
        """
        return self.get(self.STORAGE_POWER)

    @storage_power.setter
    def storage_power(self, value: float) -> None:
        """
        Sets the storage power value.

        Args:
            value (float): the storage power value to set.
        """
        self.set(self.STORAGE_POWER, value)

    @property
    def must_fulfil_load(self) -> float:
        """
        Returns the must fulfil load value.
        """
        return self.get(self.MUST_FULFIL_LOAD)

    @must_fulfil_load.setter
    def must_fulfil_load(self, value: float) -> None:
        """
        Sets the must fulfil load value.

        Args:
            value (float): the must fulfil load value to set.
        """
        self.set(self.MUST_FULFIL_LOAD, value)

    @property
    def residual_load_final(self) -> float:
        """
        Returns the residual load final value.
        """
        return self.get(self.RESIDUAL_LOAD_FINAL)

    @residual_load_final.setter
    def residual_load_final(self, value: float) -> None:
        """
        Sets the residual load final value.

        Args:
            value (float): the residual load final value to set.
        """
        self.set(self.RESIDUAL_LOAD_FINAL, value)

    @property
    def id(self) -> str:
        """
        Returns a unique identifier for the simple peak shaving state.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID
