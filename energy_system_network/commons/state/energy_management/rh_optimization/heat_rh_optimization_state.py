from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_management.rh_optimization.rh_optimization_state import \
    RHOptimizationState


class HeatRHOptimizationState(RHOptimizationState):
    """
    Represents the optimization state specifically for heating energy within an energy system.

    Attributes:
        TITLE (str): The title string representing the optimization type, appended with heat information.

    Methods:
        __init__(energy_system_id: int): Initializes the state with a given energy system ID, with specific handling for heat energy systems.
        id(): Generates an identifier for the heat RH optimization state, incorporating energy system details.
    """
    def __init__(self, energy_system_id: int):
        """
        Initializes the optimization state for heat energy with a given energy system ID.

        Args:
            energy_system_id (int): Identifier for the energy system.
        """
        super().__init__(energy_system_id)
        self.TITLE += EnergyForm.HEAT + ' '

    @property
    def id(self) -> str:
        """
        Generates an identifier for the heat RH optimization state by concatenating the title, static energy system ID and the instance's energy system ID.

        Return:
            str: The constructed identifier string.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID + str(self.energy_system_id)
