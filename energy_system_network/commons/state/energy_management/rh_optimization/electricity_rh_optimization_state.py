from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_management.rh_optimization.rh_optimization_state import \
    RHOptimizationState


class ElectricityRHOptimizationState(RHOptimizationState):
    """
    Represents the state for electricity in the renewable hybrid optimization strategy.

    Inherits the attributes and methods from the RHOptimizationState and specializes them for handling
    electricity-specific operations and properties.

    Attributes:
        TITLE (str): A static title used to define the state's strategy. The title is appended with the energy form, in this case, 'ELECTRICITY'.
        ENERGY_SYSTEM_ID (str): Identifier for the energy system, inherited from the parent class.

    Methods:
        __init__(energy_system_id: int): Initializes the ElectricityRHOptimizationState with a given energy system ID.
        id() -> str: Property that returns a unique identifier for the electricity renewable hybrid optimization state.
    """
    def __init__(self, energy_system_id: int):
        """
        Initializes the ElectricityRHOptimizationState with a given energy system ID and appends 'ELECTRICITY' to the TITLE attribute.

        Args:
            energy_system_id (int): The ID for the energy system.
        """
        super().__init__(energy_system_id)
        self.TITLE += EnergyForm.ELECTRICITY + ' '

    @property
    def id(self) -> str:
        """
        Returns a unique identifier for the electricity renewable hybrid optimization state by
        concatenating the TITLE attribute with the ENERGY_SYSTEM_ID and the instance-specific energy_system_id.

        Returns:
            str: Unique identifier for the state.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID + str(self.energy_system_id)
