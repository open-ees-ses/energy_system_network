from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState


class RHOptimizationState(EnergyManagementState):
    """
    Represents the current state of an energy system for RH Optimization, including parameters such as PV, Load, and other related states.

    Attributes:
        TITLE (str): The title string representing the optimization type.

    Methods:
        __init__(energy_system_id): Initializes the state with a given energy system ID.
        energy_system_id(): Returns the identifier of the energy system.
        must_run_generation_power(): Accessor and mutator for the must-run generation power value.
        can_run_generation_power(): Accessor and mutator for the can-run generation power value.
        grid_power(): Accessor and mutator for grid power.
        storage_power(): Accessor and mutator for storage power in W (pos: dch, neg: ch).
        must_fulfil_load(): Accessor and mutator for must fulfill load.
        can_fulfil_load(): Accessor and mutator for can fulfill load.
        residual_load_must_run_must_fulfil(): Accessor and mutator for residual load (must run must fulfill).
        residual_load_final(): Accessor and mutator for the final residual load.
        generation_reference_power(): Accessor and mutator for generation reference power.
        grid_reference_power(): Accessor and mutator for grid reference power.
        storage_reference_power(): Accessor and mutator for storage reference power.
        storage_power_fulfilment(): Accessor and mutator for storage power fulfillment.
        storage_reference_soc(): Accessor and mutator for storage reference state of charge.
        cash_flow_timestep(): Accessor and mutator for cash flow at a given timestep.
        energy_price_timestep(): Accessor and mutator for energy price at a given timestep.
        id(): Generates an identifier for the RH optimization state.
    """
    TITLE: str = 'RHOptimization'
    # TODO Need to ensure that values include the scaling factors. Currently values are written directly from the CSVs.

    def __init__(self, energy_system_id):
        """
        Initializes the RHOptimizationState with given energy system ID.

        Args:
            energy_system_id (float): Identifier for the energy system.

        Return:
            None
        """
        super().__init__()
        self._initialize()
        self.set(self.ENERGY_SYSTEM_ID, energy_system_id)

    @property
    def energy_system_id(self) -> float:
        """
        Returns the identifier of the energy system.

        Return:
            float: Identifier of the energy system.
        """
        return self.get(self.ENERGY_SYSTEM_ID)

    @property
    def must_run_generation_power(self) -> float:
        """
        Returns the must-run generation power value.
        """
        return self.get(self.MUST_RUN_GENERATION_POWER)

    @must_run_generation_power.setter
    def must_run_generation_power(self, value: float) -> None:
        """
        Sets the must-run generation power value.

        Args:
            value (float): must-run generation power value to set.
        """
        self.set(self.MUST_RUN_GENERATION_POWER, value)

    @property
    def can_run_generation_power(self) -> float:
        """
        Returns the can-run generation power value.
        """
        return self.get(self.CAN_RUN_GENERATION_POWER)

    @can_run_generation_power.setter
    def can_run_generation_power(self, value: float) -> None:
        """
        Sets the can-run generation power value.

        Args:
            value (float): can-run generation power value to set.
        """
        self.set(self.CAN_RUN_GENERATION_POWER, value)

    @property
    def grid_power(self) -> float:
        """
        Returns the grid power value.
        """
        return self.get(self.GRID_POWER)

    @grid_power.setter
    def grid_power(self, value: float) -> None:
        """
        Sets the can-run generation power value.

        Args:
            value (float): can-run generation power value to set.
        """
        self.set(self.GRID_POWER, value)

    @property
    def storage_power(self) -> float:
        """
        Returns storage power in W (pos: dch, neg: ch)
        :return: storage power in W
        """
        return self.get(self.STORAGE_POWER)

    @storage_power.setter
    def storage_power(self, value: float) -> None:
        """
        Sets storage power in W (pos: dch, neg: ch)
        :param value: storage power value in W
        :return: None
        """
        self.set(self.STORAGE_POWER, value)

    @property
    def must_fulfil_load(self) -> float:
        """
        Returns the must-fulfil load power value.
        """
        return self.get(self.MUST_FULFIL_LOAD)

    @must_fulfil_load.setter
    def must_fulfil_load(self, value: float) -> None:
        """
        Sets the must-fulfil load power value.

        Args:
            value (float): must-fulfil load power value to set.
        """
        self.set(self.MUST_FULFIL_LOAD, value)

    @property
    def can_fulfil_load(self) -> float:
        """
        Returns the can-fulfil load power value.
        """
        return self.get(self.CAN_FULFIL_LOAD)

    @can_fulfil_load.setter
    def can_fulfil_load(self, value: float) -> None:
        """
        Sets the can-fulfil load power value.

        Args:
            value (float): can-fulfil load power value to set.
        """
        self.set(self.CAN_FULFIL_LOAD, value)

    @property
    def residual_load_must_run_must_fulfil(self) -> float:
        """
        Returns the residual must_run and must-fulfil load power value.
        """
        return self.get(self.RESIDUAL_LOAD_MUST_RUN_MUST_FULFIL)

    @residual_load_must_run_must_fulfil.setter
    def residual_load_must_run_must_fulfil(self, value: float) -> None:
        """
        Sets the residual must_run and must-fulfil load power value.

        Args:
            value (float): residual must_run and must-fulfil load power value to set.
        """
        self.set(self.RESIDUAL_LOAD_MUST_RUN_MUST_FULFIL, value)

    @property
    def residual_load_final(self) -> float:
        """
        Returns the residual final load power value.
        """
        return self.get(self.RESIDUAL_LOAD_FINAL)

    @residual_load_final.setter
    def residual_load_final(self, value: float) -> None:
        """
        Sets the residual final load power value.

        Args:
            value (float): residual final load power value to set.
        """
        self.set(self.RESIDUAL_LOAD_FINAL, value)

    @property
    def generation_reference_power(self) -> float:
        """
        Returns the generation reference power value.
        """
        return self.get(self.GENERATION_REFERENCE_POWER)

    @generation_reference_power.setter
    def generation_reference_power(self, value: float):
        """
        Sets the generation reference power value.

        Args:
            value (float): generation reference power value to set.
        """
        self.set(self.GENERATION_REFERENCE_POWER, value)

    @property
    def grid_reference_power(self) -> float:
        """
        Returns the grid reference power value.
        """
        return self.get(self.GRID_REFERENCE_POWER)

    @grid_reference_power.setter
    def grid_reference_power(self, value: float):
        """
        Sets the grid reference power value.

        Args:
            value (float): grid reference power value to set.
        """
        self.set(self.GRID_REFERENCE_POWER, value)

    @property
    def storage_reference_power(self) -> float:
        """
        Returns the storage reference power value.
        """
        return self.get(self.STORAGE_REFERENCE_POWER)

    @storage_reference_power.setter
    def storage_reference_power(self, value: float):
        """
        Sets the storage reference power value.

        Args:
            value (float): storage reference power value to set.
        """
        self.set(self.STORAGE_REFERENCE_POWER, value)

    @property
    def storage_power_fulfilment(self) -> float:
        """
        Returns the storage fulfilment power value.
        """
        return self.get(self.STORAGE_POWER_FULFILMENT)

    @storage_power_fulfilment.setter
    def storage_power_fulfilment(self, value: float):
        """
        Sets the storage fulfilment power value.

        Args:
            value (float): storage fulfilment power value to set.
        """
        self.set(self.STORAGE_POWER_FULFILMENT, value)

    @property
    def storage_reference_soc(self) -> float:
        """
        Returns the storage reference soc value.
        """
        return self.get(self.STORAGE_REFERENCE_SOC)

    @storage_reference_soc.setter
    def storage_reference_soc(self, value: float):
        """
        Sets the storage reference soc value.

        Args:
            value (float): storage reference soc value to set.
        """
        self.set(self.STORAGE_REFERENCE_SOC, value)

    # @property
    # def soc_prediction_accuracy(self) -> float:
    #     return self.get(self.SOC_PREDICTION_ACCURACY)
    #
    # @soc_prediction_accuracy.setter
    # def soc_prediction_accuracy(self, value: float):
    #     self.set(self.SOC_PREDICTION_ACCURACY, value)

    @property
    def cash_flow_timestep(self) -> float:
        """
        Returns cash flow in current timestep in EUR (pos: revenue, neg: expense)
        :return: cash flow in EUR
        """
        return self.get(self.CASH_FLOW_TIMESTEP)

    @cash_flow_timestep.setter
    def cash_flow_timestep(self, value: float) -> None:
        """
        Sets cash flow in the current timestep in EUR (pos: revenue, neg: expense)
        :param value: value for cash flow in EUR
        :return: None
        """
        self.set(self.CASH_FLOW_TIMESTEP, value)

    @property
    def energy_price_timestep(self) -> float:
        """
        Returns energy price in current timestep in EUR/kWh
        :return: energy price in EUR/kWh as float
        """
        return self.get(self.ENERGY_PRICE_TIMESTEP)

    @energy_price_timestep.setter
    def energy_price_timestep(self, value: float) -> None:
        """
        Sets energy price in the current timestep in EUR/kWh
        :param value: value for energy price in EUR/kWh
        :return: None
        """
        self.set(self.ENERGY_PRICE_TIMESTEP, value)

    @property
    def id(self) -> str:
        """
        Returns the identifier composed of TITLE and the energy system's ID.

        Returns:
            str: Unique identifier for this state.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID
