from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState


class SimpleDeficitCoverageState(EnergyManagementState):
    """
    Represents the current state of the energy management, detailing parameters like PV, Load, etc.

    Attributes:
        TITLE (str): Title identifier for the state.

    Methods:
        __init__(energy_system_id): Initializes the state.
        energy_system_id() -> float: Gets the energy system ID.
        must_run_generation_power() -> float: Gets the must-run generation power.
        must_run_generation_power(value: float): Sets the must-run generation power.
        can_run_generation_power() -> float: Gets the can-run generation power.
        can_run_generation_power(value: float): Sets the can-run generation power.
        grid_power() -> float: Gets the grid power.
        grid_power(value: float): Sets the grid power.
        storage_power() -> float: Gets the storage power.
        storage_power(value: float): Sets the storage power.
        must_fulfil_load() -> float: Gets the must-fulfil load.
        must_fulfil_load(value: float): Sets the must-fulfil load.
        can_fulfil_load() -> float: Gets the can-fulfil load.
        can_fulfil_load(value: float): Sets the can-fulfil load.
        residual_load_must_run_must_fulfil() -> float: Gets the residual load for must-run and must-fulfil.
        residual_load_must_run_must_fulfil(value: float): Sets the residual load for must-run and must-fulfil.
        residual_load_final() -> float: Gets the final residual load.
        residual_load_final(value: float): Sets the final residual load.
        id() -> str: Returns the unique identifier for the state.
    """
    TITLE: str = 'SimpleDeficitCoverage'
    # TODO Need to ensure that values include the scaling factors. Currently values are written directly from the CSVs.

    def __init__(self, energy_system_id):
        """
        Initializes the SimpleDeficitCoverageState object.

        Args:
            energy_system_id: Identifier for the energy system.
        """
        super().__init__()
        self._initialize()
        self.set(self.ENERGY_SYSTEM_ID, energy_system_id)

    @property
    def energy_system_id(self) -> float:
        """
        Gets the energy system identifier.

        Return:
            float: Identifier for the energy system.
        """
        return self.get(self.ENERGY_SYSTEM_ID)

    @property
    def must_run_generation_power(self) -> float:
        """
        Gets the must-run generation power value.

        Return:
            float: Must-run generation power value.
        """
        return self.get(self.MUST_RUN_GENERATION_POWER)

    @must_run_generation_power.setter
    def must_run_generation_power(self, value: float) -> None:
        """
        Sets the must-run generation power value.

        Args:
            value (float): New must-run generation power value.
        """
        self.set(self.MUST_RUN_GENERATION_POWER, value)

    @property
    def can_run_generation_power(self) -> float:
        """
        Gets the can-run generation power value.

        Return:
            float: Can-run generation power value.
        """
        return self.get(self.CAN_RUN_GENERATION_POWER)

    @can_run_generation_power.setter
    def can_run_generation_power(self, value: float) -> None:
        """
        Sets the can-run generation power value.

        Args:
            value (float): New can-run generation power value.
        """
        self.set(self.CAN_RUN_GENERATION_POWER, value)

    @property
    def grid_power(self) -> float:
        """
        Retrieves the power value sourced from the grid.

        Return:
            float: Power value in watts sourced from the grid.
        """
        return self.get(self.GRID_POWER)

    @grid_power.setter
    def grid_power(self, value: float) -> None:
        """
        Sets the power value sourced from the grid.

        Args:
            value (float): Power value in watts.
        """
        self.set(self.GRID_POWER, value)

    @property
    def storage_power(self) -> float:
        """
        Retrieves the power value from the storage.

        Return:
            float: Power value in watts from storage.
        """
        return self.get(self.STORAGE_POWER)

    @storage_power.setter
    def storage_power(self, value: float) -> None:
        """
        Sets the power value for the storage.

        Args:
            value (float): Power value in watts for storage.
        """
        self.set(self.STORAGE_POWER, value)

    @property
    def must_fulfil_load(self) -> float:
        """
        Retrieves the load that must be fulfilled.

        Return:
            float: Load value in watts that has to be fulfilled.
        """
        return self.get(self.MUST_FULFIL_LOAD)

    @must_fulfil_load.setter
    def must_fulfil_load(self, value: float) -> None:
        """
        Sets the load that must be fulfilled.

        Args:
            value (float): Load value in watts.
        """
        self.set(self.MUST_FULFIL_LOAD, value)

    @property
    def can_fulfil_load(self) -> float:
        """
        Retrieves the load that can be optionally fulfilled.

        Return:
            float: Load value in watts that can be optionally fulfilled.
        """
        return self.get(self.CAN_FULFIL_LOAD)

    @can_fulfil_load.setter
    def can_fulfil_load(self, value: float) -> None:
        """
        Sets the load that can be optionally fulfilled.

        Args:
            value (float): Load value in watts.
        """
        self.set(self.CAN_FULFIL_LOAD, value)

    @property
    def residual_load_must_run_must_fulfil(self) -> float:
        """
        Retrieves the residual load after must-run and must-fulfil operations.

        Return:
            float: Residual load value in watts.
        """
        return self.get(self.RESIDUAL_LOAD_MUST_RUN_MUST_FULFIL)

    @residual_load_must_run_must_fulfil.setter
    def residual_load_must_run_must_fulfil(self, value: float) -> None:
        """
        Sets the residual load value after must-run and must-fulfil operations.

        Args:
            value (float): Residual load value in watts.
        """
        self.set(self.RESIDUAL_LOAD_MUST_RUN_MUST_FULFIL, value)

    @property
    def residual_load_final(self) -> float:
        """
        Retrieves the final residual load.

        Return:
            float: Final residual load value in watts.
        """
        return self.get(self.RESIDUAL_LOAD_FINAL)

    @residual_load_final.setter
    def residual_load_final(self, value: float) -> None:
        """
        Sets the final residual load value.

        Args:
            value (float): Final residual load value in watts.
        """
        self.set(self.RESIDUAL_LOAD_FINAL, value)

    @property
    def id(self) -> str:
        """
        Returns the unique identifier for the state, combining the title and energy system ID.

        Return:
            str: Unique identifier for the state.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID
