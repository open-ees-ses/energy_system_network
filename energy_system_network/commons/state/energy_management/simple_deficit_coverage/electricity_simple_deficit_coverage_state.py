from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_management.simple_deficit_coverage.simple_deficit_coverage_state import \
    SimpleDeficitCoverageState


class ElectricitySimpleDeficitCoverageState(SimpleDeficitCoverageState):
    """
    Represents the current state of the simple deficit coverage strategy specific to the electricity form.

    Attributes:
        TITLE (str): The title identifier for this state, appended with an indication specific to electricity.

    Methods:
        __init__(energy_system_id: int): Constructor to initialize the ElectricitySimpleDeficitCoverageState.
        id() -> str: Returns the unique identifier for the electricity-specific simple deficit coverage state.
    """
    def __init__(self, energy_system_id: int):
        """
        Initializes the ElectricitySimpleDeficitCoverageState, extending the simple deficit coverage state by specifying
        it for electricity.

        Args:
            energy_system_id (int): Identifier for the associated energy system.
        """
        super().__init__(energy_system_id)
        self.TITLE += EnergyForm.ELECTRICITY + ' '

    @property
    def id(self) -> str:
        """
        Retrieves the unique identifier for the state, encompassing details specific to the electricity form.

        Return:
            str: Unique identifier combining the title, energy system ID constant, and the instance's specific energy system ID.
        """
        return self.TITLE + self.ENERGY_SYSTEM_ID + str(self.energy_system_id)