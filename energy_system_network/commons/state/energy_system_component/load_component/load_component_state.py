from energy_system_network.commons.state.state import State


class LoadComponentState(State):
    """
    Represents the state of a LoadComponent within an energy system.

    Attributes:
        POWER (str): Constant string for power in watts.
        POWER_LOSS (str): Constant string for power loss in watts.
        GRID_ENERGY_CONSUMPTION_EMISSIONS (str): Constant string for grid energy consumption emissions in kg.
        DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS (str): Constant string for discharge energy consumption emissions in kg.
        RUN_STATUS (str): Constant string for the run status.
        ENERGY_SYSTEM_ID (str): Constant string for identifying the energy system.
        LOAD_COMPONENT_ID (str): Constant string for identifying the load component.

    Methods:
        __init__(energy_system_id, load_id): Initializes a LoadComponentState with given energy system and load component IDs.
        id(): Returns a combined identifier of the energy system and load component.
        power(): Gets the power in watts.
        power(value: float): Sets the power value in watts.
        power_loss(): Gets the power loss in watts.
        power_loss(value: float): Sets the power loss value in watts.
        run_status(): Gets the run status.
        run_status(value: float): Sets the run status value.
        grid_energy_consumption_emissions(): Gets the emissions from grid energy consumption in kg.
        grid_energy_consumption_emissions(value: float): Sets the emissions from grid energy consumption in kg.
        discharge_energy_consumption_emissions(): Gets the emissions from discharge energy consumption in kg.
        discharge_energy_consumption_emissions(value: float): Sets the emissions from discharge energy consumption in kg.
    """
    POWER = 'Power in W'
    POWER_LOSS = 'Loss power in W'
    GRID_ENERGY_CONSUMPTION_EMISSIONS = 'Emissions Grid Energy Consumption (in kg)'
    DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS = 'Emissions Discharge Energy Consumption (in kg)'
    RUN_STATUS = 'Run Status'
    ENERGY_SYSTEM_ID = 'EnergySystem'
    LOAD_COMPONENT_ID = 'LoadComponent'

    def __init__(self, energy_system_id, load_id):
        """
        Initialize a LoadComponentState with the provided energy system and load component IDs.

        Args:
            energy_system_id: Identifier for the energy system.
            load_id: Identifier for the load component.
        """
        super().__init__()
        self._initialize()
        self.set(self.ENERGY_SYSTEM_ID, energy_system_id)
        self.set(self.LOAD_COMPONENT_ID, load_id)

    @property
    def id(self) -> str:
        """
        Return a combined identifier of the energy system and load component.

        Returns:
            str: Combined identifier.
        """
        return 'ENERGY_SYSTEM' + str(self.get(self.ENERGY_SYSTEM_ID)) + \
               str(self.get(self.LOAD_COMPONENT_ID))

    @property
    def power(self) -> float:
        """
        Get the power in watts.

        Returns:
            float: Power value.
        """
        return self.get(self.POWER)

    @power.setter
    def power(self, value: float) -> None:
        """
        Set the power value.

        Args:
            value (float): Power value in watts.
        """
        self.set(self.POWER, value)

    @property
    def power_loss(self) -> float:
        """
        Get the power loss in watts.

        Returns:
            float: Power loss value.
        """
        return self.get(self.POWER_LOSS)

    @power_loss.setter
    def power_loss(self, value: float) -> None:
        """
        Set the power loss value.

        Args:
            value (float): Power loss value in watts.
        """
        self.set(self.POWER_LOSS, value)

    @property
    def run_status(self) -> float:
        """
        Get the run status.

        Returns:
            float: Run status value.
        """
        return self.get(self.RUN_STATUS)

    @run_status.setter
    def run_status(self, value: float) -> None:
        """
        Set the run status value.

        Args:
            value (float): Run status value.
        """
        self.set(self.RUN_STATUS, value)

    @property
    def grid_energy_consumption_emissions(self) -> float:
        """
        Get the emissions from grid energy consumption in kg.

        Returns:
            float: Emissions value.
        """
        return self.get(self.GRID_ENERGY_CONSUMPTION_EMISSIONS)

    @grid_energy_consumption_emissions.setter
    def grid_energy_consumption_emissions(self, value: float) -> None:
        """
        Set the emissions from grid energy consumption.

        Args:
            value (float): Emissions value.
        """
        self.set(self.GRID_ENERGY_CONSUMPTION_EMISSIONS, value)

    @property
    def discharge_energy_consumption_emissions(self) -> float:
        """
        Get the emissions from discharge energy consumption.

        Returns:
            float: Emissions value.
        """
        return self.get(self.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS)

    @discharge_energy_consumption_emissions.setter
    def discharge_energy_consumption_emissions(self, value: float) -> None:
        """
        Set the emissions from discharge energy consumption.

        Args:
            value (float): Emissions value.
        """
        self.set(self.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS, value)
