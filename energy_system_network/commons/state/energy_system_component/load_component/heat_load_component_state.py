from energy_system_network.commons.state.energy_system_component.load_component.load_component_state import LoadComponentState


class HeatLoadComponentState(LoadComponentState):
    """
    Represents the state of a Heat Load Component within an energy system.

    Attributes:
        Inherits all attributes from LoadComponentState.

    Methods:
        __init__(energy_system_id, load_id): Initializes a HeatLoadComponentState with given energy system and load component IDs.
    """
    def __init__(self, energy_system_id, load_id):
        """
        Initializes the HeatLoadComponentState object.

        Args:
            energy_system_id (str): The ID associated with the energy system.
            load_id (str): The ID associated with the load component.

        Return:
            None
        """
        super().__init__(energy_system_id, load_id)
