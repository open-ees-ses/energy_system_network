from energy_system_network.commons.state.state import State


class GridComponentState(State):
    """
    Represents the state of a Grid Component within an energy system.

    Attributes:
        ENERGY_SYSTEM_ID (str): Identifier for the Energy System.
        GRID_COMPONENT_ID (str): Identifier for the Grid Component.
        POWER (str): Power description in Watts.
        POWER_LOSS (str): Power loss description in Watts.
        RUN_STATUS (str): Description for the run status of the component.
        OPERATION_EMISSIONS (str): Description of operational emissions in kgCO2eq.
        GENERATION_EMISSIONS (str): Description of generation emissions (to be removed after code cleanup).
        ENERGY_IMPORT_EMISSIONS (str): Description of energy import emissions in kgCO2eq.
        CARBON_INTENSITY (str): Grid's carbon intensity in gCO2eq/kWh.
        INSTANTANEOUS_EFFICIENCY (str): Instantaneous efficiency description in p.u.
        CUMULATIVE_EFFICIENCY (str): Cumulative efficiency description in p.u.

    Methods:
        __init__(energy_system_id: str, grid_id: str): Initializes the state with given IDs and sets default values.
        id() -> str: Returns a string identifier combining energy system and grid component IDs.
        power() -> float: Getter for the power attribute.
        power(value: float) -> None: Setter for the power attribute.
        power_loss() -> float: Getter for the power loss attribute.
        power_loss(value: float) -> None: Setter for the power loss attribute.
        run_status() -> float: Getter for the run status attribute.
        run_status(value: float) -> None: Setter for the run status attribute.
        operation_emissions() -> float: Getter for operation emissions attribute.
        operation_emissions(value: float) -> None: Setter for operation emissions attribute.
        operation_emissions() -> float: Getter for operation emissions attribute.
        operation_emissions(value: float) -> None: Setter for operation emissions attribute.
        generation_emissions() -> float: Getter for generation emissions attribute.
        generation_emissions(value: float) -> None: Setter for generation emissions attribute.
        energy_import_emissions() -> float: Getter for the energy import emissions attribute.
        energy_import_emissions(value: float) -> None: Setter for the energy import emissions attribute.
        carbon_intensity() -> float: Getter for the carbon intensity attribute.
        carbon_intensity(value: float) -> None: Setter for the carbon intensity attribute.
        instantaneous_efficiency() -> float: Getter for the instantaneous efficiency attribute.
        instantaneous_efficiency(value: float) -> None: Setter for the instantaneous efficiency attribute.
        cumulative_efficiency() -> float: Getter for the cumulative efficiency attribute.
        cumulative_efficiency(value: float) -> None: Setter for the cumulative efficiency attribute.
    """
    ENERGY_SYSTEM_ID = 'EnergySystem'
    GRID_COMPONENT_ID = 'GridComponent'
    POWER = 'Power in W'
    POWER_LOSS = 'Power Loss in W'
    RUN_STATUS = 'Run Status'
    OPERATION_EMISSIONS = 'Operation Emissions in kgCO2eq'
    GENERATION_EMISSIONS = 'Generation Emissions in kgCO2eq'   # remove in code cleanup after adapting analysis
    ENERGY_IMPORT_EMISSIONS = 'Energy Import Emissions in kgCO2eq'
    CARBON_INTENSITY = 'Grid Carbon Intensity in gCO2eq/kWh'
    INSTANTANEOUS_EFFICIENCY = 'Instantaneous Efficiency in p.u.'
    CUMULATIVE_EFFICIENCY = 'Cumulative Efficiency in p.u.'

    def __init__(self, energy_system_id, grid_id):
        """
        Initializes the GridComponentState object with default efficiency and the provided IDs.

        Args:
            energy_system_id (str): The ID associated with the energy system.
            grid_id (str): The ID associated with the grid component.

        Return:
            None
        """
        super().__init__()
        self._initialize()
        self.set(self.CUMULATIVE_EFFICIENCY, 0.97)
        self.set(self.ENERGY_SYSTEM_ID, energy_system_id)
        self.set(self.GRID_COMPONENT_ID, grid_id)

    @property
    def id(self) -> str:
        """
        Constructs a unique identifier using the energy system and grid component IDs.

        Return:
            str: Unique identifier for the grid component state.
        """
        return 'ENERGY_SYSTEM' + str(self.get(self.ENERGY_SYSTEM_ID)) + str(self.get(self.GRID_COMPONENT_ID))

    @property
    def power(self) -> float:
        """
        Gets the power in Watts.

        Return:
            float: The power of the grid component.
        """
        return self.get(self.POWER)

    @power.setter
    def power(self, value: float) -> None:
        """
        Sets the power in Watts.

        Args:
            value (float): Power value to be set.

        Return:
            None
        """
        self.set(self.POWER, value)

    @property
    def power_loss(self) -> float:
        """
        Gets the power loss in Watts.

        Return:
            float: The power loss of the grid component.
        """
        return self.get(self.POWER_LOSS)

    @power_loss.setter
    def power_loss(self, value: float) -> None:
        """
        Sets the power loss in Watts.

        Args:
            value (float): Power loss value to be set.

        Return:
            None
        """
        self.set(self.POWER_LOSS, value)

    @property
    def run_status(self) -> float:
        """
        Gets the run status.

        Return:
            float: The run status of the grid component.
        """
        return self.get(self.RUN_STATUS)

    @run_status.setter
    def run_status(self, value: float) -> None:
        """
        Sets the run status.

        Args:
            value (float): Run status value to be set.

        Return:
            None
        """
        self.set(self.RUN_STATUS, value)

    @property
    def operation_emissions(self) -> float:
        """
        Gets the operation emissions in kgCO2eq.

        Return:
            float: The operation emissions of the grid component.
        """
        return self.get(self.OPERATION_EMISSIONS)

    @operation_emissions.setter
    def operation_emissions(self, value: float) -> None:
        """
        Sets the operation emissions in kgCO2eq.

        Args:
            value (float): Operation emissions value to be set.

        Return:
            None
        """
        self.set(self.OPERATION_EMISSIONS, value)

    @property
    def generation_emissions(self) -> float:
        """
        Gets the generation emissions in kgCO2eq.

        Return:
            float: The generation emissions of the grid component.
        """
        return self.get(self.GENERATION_EMISSIONS)

    @generation_emissions.setter
    def generation_emissions(self, value: float) -> None:
        """
        Sets the generation emissions in kgCO2eq.

        Args:
            value (float): Generation emissions value to be set.

        Return:
            None
        """
        self.set(self.GENERATION_EMISSIONS, value)

    @property
    def energy_import_emissions(self) -> float:
        """
        Gets the energy import emissions in kgCO2eq.

        Return:
            float: The energy import emissions of the grid component.
        """
        return self.get(self.ENERGY_IMPORT_EMISSIONS)

    @energy_import_emissions.setter
    def energy_import_emissions(self, value: float) -> None:
        """
        Sets the energy import emissions in kgCO2eq.

        Args:
            value (float): Energy import emissions value to be set.

        Return:
            None
        """
        self.set(self.ENERGY_IMPORT_EMISSIONS, value)

    @property
    def carbon_intensity(self) -> float:
        """
        Gets the grid carbon intensity in gCO2eq/kWh.

        Return:
            float: The carbon intensity of the grid component.
        """
        return self.get(self.CARBON_INTENSITY)

    @carbon_intensity.setter
    def carbon_intensity(self, value: float) -> None:
        """
        Sets the grid carbon intensity in gCO2eq/kWh.

        Args:
            value (float): Carbon intensity value to be set.

        Return:
            None
        """
        self.set(self.CARBON_INTENSITY, value)

    @property
    def instantaneous_efficiency(self) -> float:
        """
        Gets the instantaneous efficiency in per unit (p.u.).

        Return:
            float: The instantaneous efficiency of the grid component.
        """
        return self.get(self.INSTANTANEOUS_EFFICIENCY)

    @instantaneous_efficiency.setter
    def instantaneous_efficiency(self, value: float) -> None:
        """
        Sets the instantaneous efficiency in per unit (p.u.).

        Args:
            value (float): Instantaneous efficiency value to be set.

        Return:
            None
        """
        self.set(self.INSTANTANEOUS_EFFICIENCY, value)

    @property
    def cumulative_efficiency(self) -> float:
        """
        Gets the cumulative efficiency in per unit (p.u.).

        Return:
            float: The cumulative efficiency of the grid component.
        """
        return self.get(self.CUMULATIVE_EFFICIENCY)

    @cumulative_efficiency.setter
    def cumulative_efficiency(self, value: float) -> None:
        """
        Sets the cumulative efficiency in per unit (p.u.).

        Args:
            value (float): Cumulative efficiency value to be set.

        Return:
            None
        """
        self.set(self.CUMULATIVE_EFFICIENCY, value)
