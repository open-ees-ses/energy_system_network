from energy_system_network.commons.state.energy_system_component.grid_component.grid_component_state import GridComponentState


class ElectricityGridComponentState(GridComponentState):
    """
    Represents the state of an electricity grid component within an energy system network.

    The ElectricityGridComponentState is a subclass of GridComponentState and extends its functionalities to
    specifically cater to electricity grid components.

    Attributes:
        Inherits all attributes from GridComponentState.

    Methods:
        __init__(energy_system_id, grid_id): Initializes a new instance of ElectricityGridComponentState.
    """
    def __init__(self, energy_system_id, grid_id):
        """
        Initializes a new instance of ElectricityGridComponentState.

        Args:
            energy_system_id (str/int): A unique identifier for the energy system the component belongs to.
            grid_id (str/int): A unique identifier for the grid component within the energy system.
        """
        super().__init__(energy_system_id, grid_id)
