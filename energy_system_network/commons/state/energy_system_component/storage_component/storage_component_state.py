from energy_system_network.commons.state.state import State


class StorageComponentState(State):
    """
    Represents the state of a storage component within an energy system, detailing various parameters
    like power, energy content, efficiencies, and emissions.

    Attributes:
        ENERGY_SYSTEM_ID (str): Identifier for the energy system.
        STORAGE_COMPONENT_ID (str): Identifier for the storage component.
        POWER (str): Description of power in W.
        POWER_LOSS (str): Description of power loss in W.
        ENERGY_CONTENT (str): Description of energy content in Wh.
        SOC (str): Description of SOC in p.u.
        SOH (str): Description of SOH in p.u.
        SOCI (str): Specific CO2 emissions in gCO2eq/kWh.
        CARBON_INTENSITY_CHARGING_ENERGY (str): Carbon intensity for charging energy in gCO2eq/kWh.
        REQUEST_FULFILMENT (str): Status of request fulfilment.
        RUN_STATUS (str): Current run status.
        OPERATION_EMISSIONS (str): Emissions due to operation in kgCO2eq.
        GENERATION_EMISSIONS (str): Emissions due to generation in kgCO2eq.
        INSTANTANEOUS_CH_EFFICIENCY (str): Instantaneous charging efficiency in p.u.
        INSTANTANEOUS_DCH_EFFICIENCY (str): Instantaneous discharging efficiency in p.u.
        CUMULATIVE_CH_EFFICIENCY (str): Cumulative charging efficiency in p.u.
        CUMULATIVE_DCH_EFFICIENCY (str): Cumulative discharging efficiency in p.u.
        AVAILABILITY (str): Availability status.
        DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS (str): Emissions due to discharge energy consumption in kg.
        REFERENCE_DRIVE_POWER (str): Reference for drive power in W.
        _state (dict): Internal state data, presumably inherited from the parent `State` class.

    Methods:
        __init__(energy_system_id, storage_id): Initializes the storage component state.
        id() -> str: Provides a unique identifier combining energy system and storage component IDs.
        power() -> float: Gets/sets storage power.
        power_loss() -> float: Gets/sets power loss.
        reference_drive_power() -> float: Gets/sets the reference drive power.
        run_status() -> float: Gets/sets the current run status.
        energy_content() -> float: Gets/sets the energy content.
        request_fulfilment() -> float: Gets/sets the request fulfilment status.
        soc() -> float: Gets/sets the State of Charge.
        soh() -> float: Gets/sets the State of Health.
        operation_emissions() -> float: Gets/sets the operation emissions.
        soci() -> float: Gets/sets the specific CO2 emissions.
        carbon_intensity_charging_energy() -> float: Gets/sets the carbon intensity of charging energy.
        instantaneous_ch_efficiency() -> float: Gets/sets instantaneous charging efficiency.
        instantaneous_dch_efficiency() -> float: Gets/sets instantaneous discharging efficiency.
        cumulative_ch_efficiency() -> float: Gets/sets cumulative charging efficiency.
        cumulative_dch_efficiency() -> float: Gets/sets cumulative discharging efficiency.
        availability() -> int: Gets/sets the availability status.
        dec_emissions() -> float: Gets/sets the discharge energy consumption emissions.
        _initialize() -> None: Internal method for setting default values.
    """
    ENERGY_SYSTEM_ID = 'EnergySystem'
    STORAGE_COMPONENT_ID = 'StorageComponent'
    POWER = 'Power in W'
    POWER_LOSS = 'Power Loss in W'
    # DCH_SWAP_POWER = 'Discharge Swap Power in W'
    ENERGY_CONTENT = 'Energy Content in Wh'
    SOC = 'SOC in p.u.'
    SOH = 'SOH in p.u.'
    SOCI = 'SOCI in gCO2eq/kWh'
    CARBON_INTENSITY_CHARGING_ENERGY = 'Carbon Intensity Charging Energy in gCO2eq/kWh'
    REQUEST_FULFILMENT = 'Request Fulfilment'
    RUN_STATUS = 'Run Status'
    OPERATION_EMISSIONS = 'Operation Emissions in kgCO2eq'
    GENERATION_EMISSIONS = 'Generation Emissions in kgCO2eq'  # remove in code cleanup after adapting analysis
    INSTANTANEOUS_CH_EFFICIENCY = 'Instantaneous Charging Efficiency in p.u.'
    INSTANTANEOUS_DCH_EFFICIENCY = 'Instantaneous Discharging Efficiency in p.u.'
    CUMULATIVE_CH_EFFICIENCY = 'Cumulative Charging Efficiency in p.u.'
    CUMULATIVE_DCH_EFFICIENCY = 'Cumulative Discharging Efficiency in p.u.'

    # EV relevant states
    AVAILABILITY = 'Availability'
    DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS = 'Emissions Discharge Energy Consumption (in kg)'
    REFERENCE_DRIVE_POWER = 'Reference Drive Power in W'

    def __init__(self, energy_system_id, storage_id):
        """
        Initializes the storage component state with default values.

        Args:
            energy_system_id (str): Identifier for the energy system.
            storage_id (str): Identifier for the storage component.

        Return:
            None
        """
        super().__init__()
        self._initialize()
        self.set(self.CUMULATIVE_DCH_EFFICIENCY, 0.9)
        self.set(self.CUMULATIVE_CH_EFFICIENCY, 0.9)
        self.set(self.SOH, 1.0)
        self.set(self.ENERGY_SYSTEM_ID, energy_system_id)
        self.set(self.STORAGE_COMPONENT_ID, storage_id)

    @property
    def id(self) -> str:
        """
        Returns a combined identifier for the energy system and storage component.

        Return:
            str: Combined identifier for energy system and storage component.
        """
        return 'ENERGY_SYSTEM' + str(self.get(self.ENERGY_SYSTEM_ID)) + str(self.get(self.STORAGE_COMPONENT_ID))

    @property
    def power(self) -> float:
        """
        Get storage power in W.

        Return:
            float: Power value in W.
        """
        return self.get(self.POWER)

    @power.setter
    def power(self, value: float) -> None:
        """
        Set storage power in W.

        Args:
            value (float): Power value in W.

        Return:
            None
        """
        self.set(self.POWER, value)

    # @property
    # def dch_swap_power(self) -> float:
    #     return self.get(self.DCH_SWAP_POWER)
    #
    # @dch_swap_power.setter
    # def dch_swap_power(self, value: float) -> None:
    #     self.set(self.DCH_SWAP_POWER, value)

    @property
    def power_loss(self) -> float:
        """
        Retrieve the power loss of the storage component.

        Return:
            float: Current power loss.
        """
        return self.get(self.POWER_LOSS)

    @power_loss.setter
    def power_loss(self, value: float) -> None:
        """
        Set the power loss for the storage component.

        Args:
            value (float): Power loss value to set.
        """
        self.set(self.POWER_LOSS, value)

    @property
    def reference_drive_power(self) -> float:
        """
        returns the reference drive power in W as float (pos: ch, neg: dch)
        :return: power value as float in W
        """
        return self.get(self.REFERENCE_DRIVE_POWER)

    @reference_drive_power.setter
    def reference_drive_power(self, value: float) -> None:
        """
        sets the reference drive power in W as float (pos: ch, neg: dch)
        :param value: power value as float in W
        :return: None
        """
        self.set(self.REFERENCE_DRIVE_POWER, value)

    @property
    def run_status(self) -> float:
        """
        Retrieve the current run status of the storage component.

        Return:
            float: Current run status.
        """
        return self.get(self.RUN_STATUS)

    @run_status.setter
    def run_status(self, value: float) -> None:
        """
        Set the current run status for the storage component.

        Args:
            value (float): Run status value to set.
        """
        self.set(self.RUN_STATUS, value)

    @property
    def energy_content(self) -> float:
        """
        Retrieve the energy content of the storage component.

        Return:
            float: Current energy content in Wh.
        """
        return self.get(self.ENERGY_CONTENT)

    @energy_content.setter
    def energy_content(self, value: float) -> None:
        """
        Set the energy content for the storage component.

        Args:
            value (float): Energy content value to set in Wh.
        """
        self.set(self.ENERGY_CONTENT, value)

    @property
    def request_fulfilment(self) -> float:
        """
        Retrieve the current request fulfilment status.

        Return:
            float: Current request fulfilment.
        """
        return self.get(self.REQUEST_FULFILMENT)

    @request_fulfilment.setter
    def request_fulfilment(self, value: float) -> None:
        """
        Set the request fulfilment status.

        Args:
            value (float): Request fulfilment value to set.
        """
        self.set(self.REQUEST_FULFILMENT, value)

    @property
    def soc(self) -> float:
        """
        Retrieve the state of charge (SOC) of the storage component.

        Return:
            float: Current state of charge in percentage.
        """
        return self.get(self.SOC)

    @soc.setter
    def soc(self, value: float) -> None:
        """
        Set the state of charge (SOC) for the storage component.

        Args:
            value (float): SOC value to set in percentage.
        """
        self.set(self.SOC, value)

    @property
    def soh(self) -> float:
        """
        Retrieve the state of health (SOH) of the storage component.

        Return:
            float: Current state of health in percentage.
        """
        return self.get(self.SOH)

    @soh.setter
    def soh(self, value: float) -> None:
        """
        Set the state of health (SOH) for the storage component.

        Args:
            value (float): SOH value to set in percentage.
        """
        self.set(self.SOH, value)

    @property
    def operation_emissions(self) -> float:
        """
        Retrieve the emissions generated during the operation of the device.

        Return:
            float: Current operational emissions in grams per kWh.
        """
        return self.get(self.OPERATION_EMISSIONS)

    @operation_emissions.setter
    def operation_emissions(self, value: float) -> None:
        """
        Set the emissions generated during the operation of the device.

        Args:
            value (float): Operational emissions value in grams per kWh.
        """
        self.set(self.OPERATION_EMISSIONS, value)

    @property
    def soci(self) -> float:
        """
        Retrieve the state of charge index (SOCI) of the storage component.

        Return:
            float: Current state of charge index.
        """
        return self.get(self.SOCI)

    @soci.setter
    def soci(self, value: float) -> None:
        """
        Set the state of charge index (SOCI) for the storage component.

        Args:
            value (float): SOCI value to set.
        """
        self.set(self.SOCI, value)

    @property
    def carbon_intensity_charging_energy(self) -> float:
        """
        Retrieve the carbon intensity during the charging phase of the device.

        Return:
            float: Carbon intensity in grams per kWh during charging.
        """
        return self.get(self.CARBON_INTENSITY_CHARGING_ENERGY)

    @carbon_intensity_charging_energy.setter
    def carbon_intensity_charging_energy(self, value: float) -> None:
        """
        Set the carbon intensity value for the charging phase.

        Args:
            value (float): Carbon intensity in grams per kWh during charging.
        """
        self.set(self.CARBON_INTENSITY_CHARGING_ENERGY, value)

    @property
    def instantaneous_ch_efficiency(self) -> float:
        """
        Retrieve the instantaneous charging efficiency of the device.

        Return:
            float: Current charging efficiency as a percentage.
        """
        return self.get(self.INSTANTANEOUS_CH_EFFICIENCY)

    @instantaneous_ch_efficiency.setter
    def instantaneous_ch_efficiency(self, value: float) -> None:
        """
        Set the instantaneous charging efficiency.

        Args:
            value (float): Charging efficiency as a percentage.
        """
        self.set(self.INSTANTANEOUS_CH_EFFICIENCY, value)

    @property
    def instantaneous_dch_efficiency(self) -> float:
        """
        Retrieve the instantaneous discharging efficiency of the device.

        Return:
            float: Current discharging efficiency as a percentage.
        """
        return self.get(self.INSTANTANEOUS_DCH_EFFICIENCY)

    @instantaneous_dch_efficiency.setter
    def instantaneous_dch_efficiency(self, value: float) -> None:
        """
        Set the instantaneous discharging efficiency.

        Args:
            value (float): Discharging efficiency as a percentage.
        """
        self.set(self.INSTANTANEOUS_DCH_EFFICIENCY, value)

    @property
    def cumulative_ch_efficiency(self) -> float:
        """
        Retrieve the cumulative charging efficiency of the device over its lifetime.

        Return:
            float: Cumulative charging efficiency as a percentage.
        """
        return self.get(self.CUMULATIVE_CH_EFFICIENCY)

    @cumulative_ch_efficiency.setter
    def cumulative_ch_efficiency(self, value: float) -> None:
        """
        Set the cumulative charging efficiency.

        Args:
            value (float): Cumulative charging efficiency as a percentage.
        """
        self.set(self.CUMULATIVE_CH_EFFICIENCY, value)

    @property
    def cumulative_dch_efficiency(self) -> float:
        """
        Retrieve the cumulative discharging efficiency of the device over its lifetime.

        Return:
            float: Cumulative discharging efficiency as a percentage.
        """
        return self.get(self.CUMULATIVE_DCH_EFFICIENCY)

    @cumulative_dch_efficiency.setter
    def cumulative_dch_efficiency(self, value: float) -> None:
        """
        Set the cumulative discharging efficiency.

        Args:
            value (float): Cumulative discharging efficiency as a percentage.
        """
        self.set(self.CUMULATIVE_DCH_EFFICIENCY, value)

    @property
    def availability(self) -> int:
        """
        Check the availability status of the device.

        Return:
            bool: True if the device is available, False otherwise.
        """
        return int(self.get(self.AVAILABILITY))

    @availability.setter
    def availability(self, value: int) -> None:
        """
        Set the availability status of the device.

        Args:
            value (bool): Availability status to be set.
        """
        self.set(self.AVAILABILITY, value)

    @property
    def dec_emissions(self) -> float:
        """
        Retrieve the carbon emissions produced during the decommissioning phase of the device.

        Return:
            float: Carbon emissions in grams.
        """
        return float(self.get(self.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS))

    @dec_emissions.setter
    def dec_emissions(self, value: float) -> None:
        """
        Set the carbon emissions value for the decommissioning phase.

        Args:
            value (float): Carbon emissions in grams.
        """
        self.set(self.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS, value)
