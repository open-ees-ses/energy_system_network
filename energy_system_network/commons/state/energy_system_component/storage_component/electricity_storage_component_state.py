from energy_system_network.commons.state.energy_system_component.storage_component.storage_component_state import StorageComponentState


class ElectricityStorageComponentState(StorageComponentState):
    """
    Represents the state of an electricity storage component in an energy system.

    Attributes:
        _energy_system_id (str): Identifier for the energy system this storage belongs to.
        _storage_id (str): Unique identifier for the electricity storage component.

    Methods:
        __init__(energy_system_id: str, storage_id: str): Initializes a new instance of the ElectricityStorageComponentState class.
    """
    def __init__(self, energy_system_id, storage_id):
        """
        Initialize an ElectricityStorageComponentState instance.

        Args:
            energy_system_id (str): Identifier for the energy system this storage belongs to.
            storage_id (str): Unique identifier for the electricity storage component.

        Return:
            None
        """
        super().__init__(energy_system_id, storage_id)
