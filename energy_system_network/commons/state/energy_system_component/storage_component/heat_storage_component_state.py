from energy_system_network.commons.state.energy_system_component.storage_component.storage_component_state import StorageComponentState


class HeatStorageComponentState(StorageComponentState):
    """
    Represents the state of a heat storage component in an energy system.

    Attributes:
        _energy_system_id (str): Identifier for the energy system this storage belongs to.
        _storage_id (str): Unique identifier for the heat storage component.

    Methods:
        __init__(energy_system_id: str, storage_id: str): Initializes a new instance of the HeatStorageComponentState class.
    """
    def __init__(self, energy_system_id, storage_id):
        """
        Initialize a HeatStorageComponentState instance.

        Args:
            energy_system_id (str): Identifier for the energy system this storage belongs to.
            storage_id (str): Unique identifier for the heat storage component.
        """
        super().__init__(energy_system_id, storage_id)
