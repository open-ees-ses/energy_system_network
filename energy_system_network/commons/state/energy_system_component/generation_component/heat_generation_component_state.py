from energy_system_network.commons.state.energy_system_component.generation_component.generation_component_state import \
    GenerationComponentState


class HeatGenerationComponentState(GenerationComponentState):
    """
    Represents the state of a heat generation component within an energy system network.

    The HeatGenerationComponentState is a subclass of GenerationComponentState and extends its
    functionalities to specifically cater to heat generation components.

    Attributes:
        Inherits all attributes from GridComponentState.

    Methods:
        __init__(energy_system_id, generation_id): Initializes a new instance of HeatGenerationComponentState.
    """
    def __init__(self, energy_system_id, generation_id):
        """
        Initializes a new instance of HeatGenerationComponentState.

        Args:
            energy_system_id (str/int): A unique identifier for the energy system the component belongs to.
            generation_id (str/int): A unique identifier for the generation component within the energy system.
        """
        super().__init__(energy_system_id, generation_id)
