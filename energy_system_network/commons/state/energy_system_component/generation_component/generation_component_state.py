from energy_system_network.commons.state.state import State


class GenerationComponentState(State):
    """
    Represents the state of a generation component within an energy system network.

    Attributes:
        POWER (str): Power output description.
        POWER_LOSS (str): Power loss description.
        RUN_STATUS (str): Run status description.
        FUEL_CONSUMPTION (str): Fuel consumption description.
        OPERATION_EMISSIONS (str): Operational emissions description.
        GENERATION_EMISSIONS (str): Generation emissions description.
        CARBON_INTENSITY_GENERATED_ENERGY (str): Carbon intensity of generated energy description.
        POWER_EXPORT (str): Power export description.
        POWER_EXPORT_EMISSIONS (str): Power export emissions description.
        ENERGY_SYSTEM_ID (str): Identifier for the energy system.
        GENERATION_COMPONENT_ID (str): Identifier for the generation component.

    Methods:
        __init__(energy_system_id, generation_id): Initializes the state with provided IDs.
        id(): Returns the unique identifier for the state.
        power(): Gets the power value.
        power(value): Sets the power value.
        power_loss(): Gets the power loss value.
        power_loss(value): Sets the power loss value.
        run_status(): Gets the run status.
        run_status(value): Sets the run status.
        fuel_consumption(): Gets the fuel consumption value.
        fuel_consumption(value): Sets the fuel consumption value.
        operation_emissions(): Gets the operation emissions value.
        operation_emissions(value): Sets the operation emissions value.
        generation_emissions(): Gets the generation emissions value.
        generation_emissions(value): Sets the generation emissions value.
        carbon_intensity_generated_energy(): Gets the carbon intensity of generated energy.
        carbon_intensity_generated_energy(value): Sets the carbon intensity of generated energy.
        power_export(): Gets the power export value.
        power_export(value): Sets the power export value.
        power_export_emissions(): Gets the power export emissions value.
        power_export_emissions(value): Sets the power export emissions value.
    """
    POWER = 'Power in W'
    POWER_LOSS = 'Loss power in W'
    RUN_STATUS = 'Run Status'
    FUEL_CONSUMPTION = 'Fuel consumption in L'
    OPERATION_EMISSIONS = 'Operation Emissions in kgCO2'
    GENERATION_EMISSIONS = 'Generation Emissions in kgCO2'
    CARBON_INTENSITY_GENERATED_ENERGY = 'Carbon Intensity of Generated Energy in g/KWh'
    POWER_EXPORT = 'Export power in W'
    POWER_EXPORT_EMISSIONS = 'Power export emissions in kgCO2'
    ENERGY_SYSTEM_ID = 'EnergySystem'
    GENERATION_COMPONENT_ID = 'GenerationComponent'

    def __init__(self, energy_system_id, generation_id):
        """
        Initializes the GenerationComponentState with identifiers.

        Args:
            energy_system_id (int/str): Identifier for the energy system.
            generation_id (int/str): Identifier for the generation component.

        Return:
            None
        """
        super().__init__()
        self._initialize()
        self.set(self.ENERGY_SYSTEM_ID, energy_system_id)
        self.set(self.GENERATION_COMPONENT_ID, generation_id)

    @property
    def id(self) -> str:
        """
        Generates a unique ID using the energy system and generation component IDs.

        Return:
            str: Unique identifier for this generation component state.
        """
        return 'ENERGY_SYSTEM' + str(self.get(self.ENERGY_SYSTEM_ID)) + str(self.get(self.GENERATION_COMPONENT_ID))

    @property
    def power(self) -> float:
        """
        Return the current power output in W.

        Return:
            float: Power value in watts.
        """
        return self.get(self.POWER)

    @power.setter
    def power(self, value: float) -> None:
        """
        Set the current power output in W.

        Args:
            value (float): The power output value in W.
        """
        self.set(self.POWER, value)

    @property
    def power_loss(self) -> float:
        """
        Returns the power loss value.

        Return:
            float: Power loss value in watts.
        """
        return self.get(self.POWER_LOSS)

    @power_loss.setter
    def power_loss(self, value: float) -> None:
        """
        Sets the power loss value.

        Args:
            value (float): Power loss value in watts.

        Return:
            None
        """
        self.set(self.POWER_LOSS, value)

    @property
    def run_status(self) -> float:
        """
        Returns the run status value.

        Return:
            float: Run status of the component.
        """
        return self.get(self.RUN_STATUS)

    @run_status.setter
    def run_status(self, value: float) -> None:
        """
        Sets the run status value.

        Args:
            value (float): Run status of the component.

        Return:
            None
        """
        self.set(self.RUN_STATUS, value)

    @property
    def fuel_consumption(self) -> float:
        """
        Returns the fuel consumption value.

        Return:
            float: Fuel consumption in liters.
        """

        return self.get(self.FUEL_CONSUMPTION)

    @fuel_consumption.setter
    def fuel_consumption(self, value: float) -> None:
        """
        Sets the fuel consumption value.

        Args:
            value (float): Fuel consumption in liters.

        Return:
            None
        """
        self.set(self.FUEL_CONSUMPTION, value)

    @property
    def operation_emissions(self) -> float:
        """
        Returns the operation emissions value.

        Return:
            float: Operational emissions in kgCO2.
        """

        return self.get(self.OPERATION_EMISSIONS)

    @operation_emissions.setter
    def operation_emissions(self, value: float) -> None:
        """
        Sets the operation emissions value.

        Args:
            value (float): Operational emissions in kgCO2.

        Return:
            None
        """
        self.set(self.OPERATION_EMISSIONS, value)

    @property
    def generation_emissions(self) -> float:
        """
        Returns the generation emissions value.

        Return:
            float: Generation emissions in kgCO2.
        """
        return self.get(self.GENERATION_EMISSIONS)

    @generation_emissions.setter
    def generation_emissions(self, value: float) -> None:
        """
        Sets the generation emissions value.

        Args:
            value (float): Generation emissions in kgCO2.

        Return:
            None
        """
        self.set(self.GENERATION_EMISSIONS, value)

    @property
    def carbon_intensity_generated_energy(self) -> float:
        """
        Returns the carbon intensity of generated energy.

        Return:
            float: Carbon intensity in g/KWh.
        """
        return self.get(self.CARBON_INTENSITY_GENERATED_ENERGY)

    @carbon_intensity_generated_energy.setter
    def carbon_intensity_generated_energy(self, value: float) -> None:
        """
        Sets the carbon intensity of generated energy.

        Args:
            value (float): Carbon intensity in g/KWh.

        Return:
            None
        """
        self.set(self.CARBON_INTENSITY_GENERATED_ENERGY, value)

    @property
    def power_export(self) -> float:
        """
        Returns the power export value.

        Return:
            float: Power exported in watts.
        """
        return self.get(self.POWER_EXPORT)

    @power_export.setter
    def power_export(self, value: float) -> None:
        """
        Sets the power export value.

        Args:
            value (float): Power to be exported in watts.

        Return:
            None
        """
        self.set(self.POWER_EXPORT, value)

    @property
    def power_export_emissions(self) -> float:
        """
        Returns the power export emissions value.

        Return:
            float: Power export emissions in kgCO2.
        """
        return self.get(self.POWER_EXPORT_EMISSIONS)

    @power_export_emissions.setter
    def power_export_emissions(self, value: float) -> None:
        """
        Sets the power export emissions value.

        Args:
            value (float): Power export emissions in kgCO2.

        Return:
            None
        """
        self.set(self.POWER_EXPORT_EMISSIONS, value)
