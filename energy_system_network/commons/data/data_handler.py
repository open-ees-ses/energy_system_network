import csv
import gzip
import threading
import time
from datetime import datetime
from queue import Queue, Empty
import pandas as pd
from energy_system_network.commons.log import Logger
from energy_system_network.commons.state.state import State
from energy_system_network.commons.utils import create_directory_for, copy_all_files, \
    remove_all_files_from
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig


class DataHandler(threading.Thread):
    """
    Export for Energy System data during the simulation into one CSV file_name.
    Energy Systems send parameters for every timestamp to this class, which adds them to the CSV file_name.
    For Multiprocessing a queue is used.

    Attributes:
        USE_GZIP_COMPRESSION (bool): Indicates whether to use GZIP compression.
        EXT_CSV (str): Extension for the CSV file.
        COMP (str): Compression format.
        EXT_COMP (str): Extension for the compressed file.
        __log (Logger): Logger instance for logging.
        __queue (Queue): Queue to hold data.
        __append (bool): Indicates whether to append to file.
        __simulation_running (bool): Indicates if the simulation is running.
        __result_folder (str): Directory for simulation results with a timestamp.
        __working_directory (str): Working directory for temporary storage.
        __classes (list): List of classes for which data should be exported.
        __file_names (list): List of filenames used for export.
        __export_interval (float): Interval for exporting data.
        __timestep (float): Time step for the simulation.

    Methods:
        __init__(result_dir: str, config: GeneralSimulationConfig, export_cls=State): Initializes the DataHandler object.
        run() -> None: Writes data from the queue to the CSV file.
        transfer_data(data: dict) -> None: Adds data to the processing queue.
        simulation_done() -> None: Signals the end of the simulation.
        copy_results_to_destination(): Transfers results to the destination folder.
        close() -> None: Closes all open resources related to data export.
        get_data_from(path: str, state_cls) -> pd.DataFrame: Class method to read data from a path into a DataFrame.
        add_export_class(export_cls: State) -> None: Adds a state class for exporting.
        get_result_folder_name() -> str: Returns the result folder name.
        get_working_dir_name() -> str: Returns the working directory name.
    """

    USE_GZIP_COMPRESSION: bool = True
    EXT_CSV: str = '.csv'
    # Other compression formats: zip, bz2, xz
    COMP: str = 'gzip'
    EXT_COMP: str = '.gz'

    def __init__(self, result_dir: str, config: GeneralSimulationConfig, export_cls=State):  # The default value of 'State' makes this script create a file for each child class of State
        """
        Initializes the DataHandler object.

        Args:
            result_dir (str): Directory for simulation results.
            config (GeneralSimulationConfig): Configuration for the simulation.
            export_cls (State, optional): The state class for which data should be exported. Defaults to State.
        """
        super().__init__()
        self.__log: Logger = Logger(type(self).__name__)
        self.__queue = Queue()
        self.__append = True
        self.__simulation_running = True
        self.__result_folder: str = result_dir + 'ESN_' + datetime.now().strftime('%Y%m%dT%H%M%S')
        create_directory_for(self.__result_folder)
        self.__working_directory = result_dir
        self.__classes: list = list()
        self.__file_names: list = list()
        self.__export_interval: float = config.export_interval
        self.__timestep: float = config.timestep

    def run(self) -> None:
        """
        Function to write data from queue to csv.
        """
        tstmps: dict = dict()
        file_writer: dict = dict()
        file_streams: list = list()
        for cls in self.__classes:
            self.__log.info('Registering ' + type(cls).__name__)
            file_name = self.__working_directory + type(cls).__name__ + self.EXT_CSV
            if self.USE_GZIP_COMPRESSION:
                file_stream = gzip.open(file_name + self.EXT_COMP, 'at' if self.__append else 'wt', newline='')
            else:
                file_stream = open(file_name, 'a' if self.__append else 'w', newline='')
            # file_stream = open(file_name, 'a' if self.__append else 'w', newline='')
            writer = csv.writer(file_stream, delimiter=',')
            writer.writerow(type(cls).header())
            file_writer[type(cls).__name__] = writer
            tstmps[type(cls).__name__] = {}
            file_streams.append(file_stream)
            self.__file_names.append(file_name)
        while self.__simulation_running or not self.__queue.empty():
            try:
                data: dict = self.__queue.get_nowait()
                self.__log.debug('Write data from queue to csv')
                for key in list(data.keys()):
                    # print(data[key])
                    ts = data[key]['TIME']
                    id = data[key]['ID']
                    if id not in tstmps[key]:
                        tstmps[key][id] = 0
                    if ts >= tstmps[key][id] + self.__export_interval * self.__timestep:
                        values: list = data[key]['DATA']
                        file_writer[key].writerow(values)
                        tstmps[key][id] = ts
            except Empty:
                self.__log.debug("Empty Queue")
                time.sleep(0.01)
        self.__log.info('Write to csv finished')
        for stream in file_streams:
            stream.close()
        file_writer.clear()
        file_streams.clear()

    def transfer_data(self, data: dict) -> None:
        """
        Function to place data into the queue. This data is picked up by the run function and written into a CSV file_name.

        Args:
            data (dict): The data to be added to the queue.
        """
        self.__queue.put(data)

    def simulation_done(self) -> None:
        """
        Function to let the DataExport Thread know that the simulation is done and stop after emptying the queue.
        """
        self.__simulation_running = False

    def copy_results_to_destination(self):
        """
        Transfer the resulting csv file_name as well as the configuration file_name to the destination folder defined in the
        class creation.
        """
        copy_all_files(self.__working_directory, self.__result_folder)
        remove_all_files_from(self.__working_directory)

    def close(self) -> None:
        """
        Closing all open resources in data export
        """

        self.__log.debug('Simulation done')
        self.simulation_done()
        self.__log.debug('Waiting for export thread to finish')
        self.join()
        self.copy_results_to_destination()
        self.__log.close()

    @classmethod
    def get_data_from(cls, path: str, state_cls) -> pd.DataFrame:
        """
        Reads data for a given class from the specified path into a pandas DataFrame.

        Args:
            path (str): The path of the simulation results.
            state_cls: The class of the state data to be read.

        Returns:
            pandas.DataFrame: The data from the file at the specified path.
        """
        filename: str = path + '/' + state_cls.__name__ + cls.EXT_CSV
        if cls.USE_GZIP_COMPRESSION:
            data: pd.DataFrame = pd.read_csv(filename + cls.EXT_COMP, compression=cls.COMP, sep=',', header=0)
        else:
            data: pd.DataFrame = pd.read_csv(filename, sep=',', header=0)
        # print('==== ' + state_cls.__name__ + ' ====')
        for key in data.keys():
            data[key] = pd.to_numeric(data[key], errors='coerce')
        # data = data.replace(np.nan, 0, regex=True)
        # print(data.dtypes)
        return data

    def add_export_class(self, export_cls: State) -> None:
        """
        Adds a state class for which data should be exported.

        Args:
            export_cls (State): The state class to be added.
        """
        flag = 0
        for state_class in self.__classes:
            if type(state_class) == type(export_cls):
                flag = 1
        if flag == 0:
            self.__classes.append(export_cls)

    def get_result_folder_name(self) -> str:
        """
        Returns the name of the result folder.

        Returns:
            str: The result folder name.
        """
        return self.__result_folder

    def get_working_dir_name(self) -> str:
        """
        Returns the name of the working directory.

        Returns:
            str: The working directory name.
        """
        return self.__working_directory
