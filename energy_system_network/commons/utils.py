import csv
import inspect
import os
import shutil
import ssl
from urllib.error import URLError
import urllib.request


def remove_file(file: str) -> None:
    """
    Removes the specified file from the system.

    Args:
        file (str): Path to the file to be removed.

    Returns:
        None
    """
    if os.path.isfile(file):
        os.remove(file)

def remove_all_files_from(directory: str):
    """
    Removes all files from the specified directory.

    Args:
        directory (str): Path to the directory from which files should be removed.

    Returns:
        None
    """

    for item in os.listdir(directory):
        path = os.path.join(directory, item)
        if os.path.isfile(path):
            os.remove(path)


def copy_all_files(source: str, target: str):
    """
    Copies all files from the source directory to the target directory.

    Args:
        source (str): Path to the source directory.
        target (str): Path to the target directory.

    Returns:
        None
    """

    for item in os.listdir(source):
        path = os.path.join(source, item)
        if os.path.isfile(path):
            shutil.copy(path, target)


def create_directory_for(path: str) -> None:
    """
    Creates a directory at the specified path.

    Args:
        path (str): Path where the directory should be created.

    Returns:
        None
    """
    if not os.path.exists(path):
        os.makedirs(path)


def format_float(value: float, decimals: int = 2) -> str:
    """
    Formats a float value to the specified number of decimal places.

    Args:
        value (float): Float value to be formatted.
        decimals (int, optional): Number of decimal places. Defaults to 2.

    Returns:
        str: Formatted float value as a string.
    """
    return f"{value:.{decimals}f}"


def write_to_file(filename: str, _list: [str], append: bool = False) -> None:
    """
    Writes a list to a file, with an option to append.

    Args:
        filename (str): Name of the file.
        _list ([str]): List of strings to be written.
        append (bool, optional): Whether to append to the file or overwrite. Defaults to False.

    Returns:
        None
    """
    with open(filename, 'a' if append else 'w', newline='') as writeFile:
        writer = csv.writer(writeFile, delimiter=',')
        writer.writerow(_list)
    writeFile.close()


def all_subclasses(cls) -> list:
    """
    Fetches all the non-abstract subclasses of a class.

    Args:
        cls: Class to fetch the subclasses for.

    Returns:
        list: List containing non-abstract subclasses of the provided class.
    """
    res = list()
    subs = set(cls.__subclasses__()).union([s for c in cls.__subclasses__() for s in all_subclasses(c)])
    for sub in subs:
        if not inspect.isabstract(sub):
            res.append(sub)
    return res


def download_file(url_path: str, file_name: str) -> None:
    """
    Downloads a file from a given URL and saves it to the specified location.

    Args:
        url_path (str): URL to download the file from.
        file_name (str): Path where the downloaded file should be saved.

    Returns:
        None

    Raises:
        FileNotFoundError: If the file at the specified URL is not found.
    """
    try:
        context = ssl._create_unverified_context()
        with urllib.request.urlopen(url_path, context=context) as response:
            print('Downloading ' + os.path.basename(file_name) + ' from ' + url_path + ' ...')
            with open(file_name, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)
    except URLError as err:
        raise FileNotFoundError(err, file_name + ' could not be found in the online repository.')
