class EnergyForm:
    """
    Represents different forms of energy.

    Attributes:
        ELECTRICITY (str): Represents the electricity form of energy.
        HEAT (str): Represents the heat form of energy.
        BATTERY_PACK (str): Represents the energy stored in a battery pack.

    Methods:
        None
    """
    ELECTRICITY: str = 'Electricity'
    HEAT: str = 'Heat'
    BATTERY_PACK: str = 'Battery Pack'
