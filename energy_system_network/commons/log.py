import logging
from energy_system_network.constants_energy_sys_network import ROOT_PATH
from energy_system_network.config.log_config import LogConfig


class Logger:
    """
    Logger class for energy system network.

    Attributes:
        __format (str): Log message format.
        __filename (str): Name of the log file.
        __path (str): Path where the log file should be created.
        __registered_logger (list): List containing all logger instances.
        logger (logging.Logger): The logger instance for logging messages.

    Methods:
        __init__(name: str, log_level: int = 0): Initializes the logger.
        info(msg): Logs informational messages.
        warn(msg): Logs warning messages.
        error(msg): Logs error messages.
        debug(msg): Logs debug messages.
        register(): Registers the logger instance.
        close(): Closes all handlers for registered loggers and clears the list.
        set_path(path: str): Sets the logging path for the logger.
    """
    __format: str = '%(asctime)s %(levelname)s [%(name)s] %(message)s'
    __filename: str = 'energy_System_network.log'
    __path: str = ROOT_PATH
    __registered_logger: list = list()

    def __init__(self, name: str, log_level: int = 0):
        """
        Initializes the logger.

        Args:
            name (str): Name of the logger.
            log_level (int, optional): Logging level. Defaults to 0.

        Returns:
            None
        """
        log_config: LogConfig = LogConfig()
        log_level = log_config.log_level if log_level == 0 else log_level
        # logger and handler
        logger = logging.getLogger(name.split('.')[-1].upper().strip('_'))
        file_handler = logging.FileHandler(self.__path + self.__filename)
        stream_handler = logging.StreamHandler()
        # setting log level
        logger.setLevel(log_level)
        file_handler.setLevel(log_level)
        stream_handler.setLevel(log_level)
        # setting format
        formatter = logging.Formatter(fmt=self.__format)
        file_handler.setFormatter(formatter)
        stream_handler.setFormatter(formatter)
        # add handler
        if log_config.log_to_console:
            logger.addHandler(stream_handler)
        if log_config.log_to_file:
            logger.addHandler(file_handler)
        self.logger: logging.Logger = logger
        self.register()

    def info(self, msg) -> None:
        """
        Logs informational messages.

        Args:
            msg: Message to log.

        Returns:
            None
        """
        self.logger.info(msg)

    def warn(self, msg) -> None:
        """
        Logs warning messages.

        Args:
            msg: Message to log.

        Returns:
            None
        """
        self.logger.warning(msg)

    def error(self, msg) -> None:
        """
        Logs error messages.

        Args:
            msg: Message to log.

        Returns:
            None
        """
        self.logger.error(msg)

    def debug(self, msg) -> None:
        """
        Logs debug messages.

        Args:
            msg: Message to log.

        Returns:
            None
        """
        self.logger.debug(msg)

    def register(self) -> None:
        """
        Registers the logger instance to the class list.
        """
        self.__registered_logger.append(self.logger)

    @classmethod
    def close(cls) -> None:
        """
        Closes all handlers for registered loggers and clears the list.
        """
        for logger in cls.__registered_logger:
            handlers = logger.handlers[:]
            for handler in handlers:
                handler.close()
                logger.removeHandler(handler)
        cls.__registered_logger.clear()

    @classmethod
    def set_path(cls, path: str) -> None:
        """
        Sets the logging path for the logger.

        Args:
            path (str): New logging path.
        """
        cls.__path = path
