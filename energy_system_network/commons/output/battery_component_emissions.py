import pandas as pd


class BatteryComponentEmissionsWriter:
    """
    Writes a component-wise emissions analysis for the LithiumBatterySystem class for the production and end-of-life phases.

    Attributes:
        CELLS (str): Caption for the battery cells.
        ACDC_CONVERTER (str): Caption for the ACDC converter.
        HOUSING (str): Caption for the battery housing.
        HVAC (str): Caption for the HVAC system.
        MISC_ELECTRONICS (str): Caption for miscellaneous electronics.
        TOTAL (str): Caption for the total emissions.
        PRODUCTION (str): Lifecycle phase for production.
        EOL (str): Lifecycle phase for end-of-life.
        FILENAME (str): Name of the file where results will be saved.
        EXTENSION (str): File extension for the results.
        __component_name (str): Name of the battery component. Private attribute.
        __folder_path (str): Path to the results directory combined with the SimSES simulation name. Private attribute.

    Methods:
        __init__(name: str, result_path_simses: str, simses_simulation_name: str): Initializes the writer with the component name and the path details.
        write_to_csv(production_emissions: dict, eol_emissions: dict): Writes the emissions data to a CSV file.
    """
    # Component captions
    CELLS: str = 'Cells'
    ACDC_CONVERTER: str = 'ACDC converter'
    HOUSING: str = 'Housing'
    HVAC: str = 'HVAC'
    MISC_ELECTRONICS: str = 'Misc. electronics'
    TOTAL: str = 'Total'

    # Lifecycle phases
    PRODUCTION: str = 'Production'
    EOL: str = 'EOL'

    # Save file name
    FILENAME: str = 'production_eol_emissions'
    EXTENSION: str = '.csv'

    def __init__(self, name: str, result_path_simses: str, simses_simulation_name: str):
        """
        Initializes the writer with the component name and the path details.

        Args:
            name (str): Name of the battery component.
            result_path_simses (str): Path to the results directory for SimSES.
            simses_simulation_name (str): Name of the SimSES simulation.

        Returns:
            None
        """
        self.__component_name = name
        self.__folder_path = result_path_simses + simses_simulation_name + '/'

    def write_to_csv(self, production_emissions: dict, eol_emissions: dict) -> None:
        """
        Writes the emissions data for production and end-of-life phases to a CSV file.

        Args:
            production_emissions (dict): Dictionary containing the emissions data for the production phase.
            eol_emissions (dict): Dictionary containing the emissions data for the end-of-life phase.

        Returns:
            None
        """
        production: pd.DataFrame = pd.DataFrame.from_dict(production_emissions, orient='index')
        eol: pd.DataFrame = pd.DataFrame.from_dict(eol_emissions, orient='index')
        combined = pd.concat([production, eol], axis=1)
        combined.columns = [self.PRODUCTION, self.EOL]
        combined.to_csv(self.__folder_path + self.FILENAME + self.EXTENSION)
        print('Component-wise emissions for ' + self.__component_name + ' written. Check specified results folder.')
