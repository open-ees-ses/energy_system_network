import sys
import threading
import time
from multiprocessing import Queue
from queue import Empty
from energy_system_network.commons.utils import remove_file
from energy_system_network.constants_energy_sys_network import ROOT_PATH


class ConsolePrinter(threading.Thread):
    """
    Provides real-time console printing capabilities in a threaded environment.

    The class listens to updates in the queue and prints them to the console. It supports registration,
    setting, and stopping of names to be printed to the console. The printed output can also be logged
    into a file.

    Attributes:
        UPDATE (float): The update frequency for the console print.
        CUT_OFF_LENGTH (int): Maximum length for a single print line.
        SPACES (int): Number of spaces between printed items.
        FILE (str): The path to the file for logging printed output.
        STOP_SIGNAL (str): Signal to indicate a stop command.
        REGISTER_SIGNAL (str): Signal to indicate a register command.
        __output (dict): Dictionary to hold the current output values.
        __running (bool): Flag indicating if the thread is running.
        __registered (dict): Dictionary to hold registered names.
        __file_writer (IO): Placeholder for a file writer object (currently unused).
        __queue (Queue): The queue from which the class retrieves updates.

    Methods:
        __init__(queue: Queue):
            Initializes the ConsolePrinter with a given queue.
        register(name: str):
            Registers a new name to be printed.
        not_started_yet() -> bool:
            Checks if the thread is alive and still running.
        set(name: str, line: str):
            Sets the current output value for a given name.
        stop(name):
            Stops printing a given name.
        stop_immediately():
            Stops the printing thread immediately.
        max_key_length() -> int:
            Computes the maximum length among all registered names.
        cut_off(line: str) -> str:
            Shortens a given line if its length exceeds CUT_OFF_LENGTH.
        line() -> str:
            Constructs the current print line.
        clear_screen():
            Clears the console.
        update():
            Retrieves updates from the queue and processes them.
        run():
            Main loop of the thread, which updates and prints to the console.
    """
    UPDATE: float = 1
    CUT_OFF_LENGTH: int = 120
    SPACES: int = 3
    FILE: str = ROOT_PATH + 'progress.log'
    STOP_SIGNAL: str = 'STOP'
    REGISTER_SIGNAL: str = 'REGISTER'

    def __init__(self, queue: Queue):
        """
        Initializes the ConsolePrinter with a given message queue.

        Args:
            queue (Queue): The message queue for console output.
        """
        super().__init__()
        self.__output: dict = dict()
        self.__running: bool = True
        self.__registered: dict = dict()
        self.__file_writer = None
        self.__queue: Queue = queue
        remove_file(self.FILE)

    def register(self, name: str) -> None:
        """
        Registers a new name for output tracking and printing.

        Args:
            name (str): Name to be registered.
        """
        if name not in self.__registered.keys():
            self.__registered[name] = True

    @property
    def not_started_yet(self) -> bool:
        """
        Checks if any names are registered, if the thread is not alive, and if it is still running.

        Returns:
            bool: True if conditions are met, otherwise False.
        """
        return self.__registered.keys() and not self.is_alive() and self.__running

    def set(self, name: str, line: str) -> None:
        """
        Sets the current output value for a given name.

        Args:
            name (str): Name to set the value for.
            line (str): Value to set for the name.
        """
        if name in self.__registered.keys():
            if self.__registered[name]:
                self.__output[name] = line

    def stop(self, name) -> None:
        """
        Stops tracking and printing for a given name.

        Args:
            name (str): Name to stop tracking.
        """
        self.__registered[name] = False
        keys: [str] = list(self.__output.keys())
        if name in keys:
            del self.__output[name]
        stopping: bool = True
        for running in self.__registered.values():
            stopping = stopping and not running

    def stop_immediately(self) -> None:
        """
        Stops the printing thread immediately.
        """
        self.__running = False

    @property
    def max_key_length(self) -> int:
        """
        Computes and returns the maximum length among all registered names.

        Returns:
            int: Maximum length of the registered names.
        """
        max_length: int = 0
        for key in self.__output.keys():
            length: int = len(key)
            if length > max_length:
                max_length = length
        return max_length

    def cut_off(self, line: str) -> str:
        """
        Shortens a given line if its length exceeds the CUT_OFF_LENGTH.

        Args:
            line (str): Line to potentially shorten.

        Returns:
            str: Original or shortened line.
        """
        if len(line) > self.CUT_OFF_LENGTH:
            return line[:self.CUT_OFF_LENGTH] + ' ...'
        else:
            return line

    @property
    def line(self) -> str:
        """
        Constructs and returns the current print line based on registered names and their current values.

        Returns:
            str: Formatted print line.
        """
        line: str = '\r' # '\r'
        string_format: str = '{:'+str(self.max_key_length)+'s}'
        for key in self.__output.keys():
            line += '[' + string_format.format(key) + ': ' + self.__output[key] + ']' + ' ' * self.SPACES
        return self.cut_off(line) + ''

    @staticmethod
    def clear_screen():
        """
        Clears the console screen.
        """
        print('\n' * 10000)

    def update(self) -> None:
        """
        Fetches messages from the queue, processes them, and updates the current output values.
        """
        try:
            while not self.__queue.empty():
                output: dict = self.__queue.get_nowait()
                items = output.items()
                for key, value in items:
                    if value == self.REGISTER_SIGNAL:
                        self.register(key)
                    elif value == self.STOP_SIGNAL:
                        self.stop(key)
                    else:
                        self.set(key, value)
        except Empty:
            return

    def run(self) -> None:
        """
        The main loop of the thread which continuously updates and prints messages to the console.
        """
        while self.__running:
            self.update()
            line: str = self.line
            sys.stdout.write(line)
            sys.stdout.flush()
            time.sleep(self.UPDATE)
