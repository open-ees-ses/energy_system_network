from energy_system_network.commons.utils import download_file
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
import pandas as pd
import numpy as np
import time
import calendar
import os


class ProfileHandler:
    """
    A class to handle profiles related to energy simulations.

    Attributes:
        LEAP_YEAR_HOURS (int): Total hours in a leap year.
        YEAR_HOURS (int): Total hours in a non-leap year.
        PROFILE_REPOSITORY (str): URL repository where profiles are stored.
        __profile (np.ndarray): The profile data.
        __duration (float): Duration of the profile.
        __start_epoch_time (float): Start time of the profile in epoch format.
        __start_human_time (str): Start time of the profile in human-readable format.
        __config_general (GeneralSimulationConfig): General configuration for the simulation.
        __scaling (float): Scaling factor for the profile.
        __initial_offset (float): Initial offset time value for the profile.
        __min_profile_value (float): Minimum value in the profile.
        __average_profile_value (float): Average value of the profile.
        __max_profile_value (float): Maximum value in the profile.
        __value (float): Current profile value.
        __capacity_factor (float): Capacity factor of the profile.

    Methods:
        __init__(filename, general_config: GeneralSimulationConfig, scaling: float): Initialize ProfileHandler with
            given filename, general_config, and scaling.
        next(time: float, adapted_time: float) -> float: Obtain the next profile value based on the provided time.
        profile() -> np.ndarray: Return the profile as an ndarray.
        max_profile_value() -> float: Return the maximum value of the profile.
        min_profile_value() -> float: Return the minimum value of the profile.
        average_profile_value() -> float: Return the average value of the profile.
        capacity_factor() -> float: Return the capacity factor of the profile.
        calculate_capacity_factor() -> float: Calculate the capacity factor of the profile.
        get_initial_offset() -> float: Return the initial offset of the profile.
        close() -> None: Terminate the object.
    """

    LEAP_YEAR_HOURS: int = 8784
    YEAR_HOURS: int = 8760

    PROFILE_REPOSITORY: str = 'https://syncandshare.lrz.de/dl/fiHU5FnpLCmDyftj71RnVz/'

    def __init__(self, filename, general_config: GeneralSimulationConfig, scaling: float):
        """
        Initialize ProfileHandler.

        Args:
            filename (str): The filename of the profile.
            general_config (GeneralSimulationConfig): General simulation configuration object.
            scaling (float): Scaling factor for the profile.
        """
        if filename is None or general_config is None or scaling is None:
            pass
        else:
            try:
                if 'gz' in filename:
                    compression_type = 'gzip'
                else:
                    compression_type = None
                self.__profile = pd.read_csv(filename, decimal=".", header=None, compression=compression_type)
            except FileNotFoundError as err:
                if self.PROFILE_REPOSITORY is None:
                    raise err
                print(filename + ' not found locally. Retrieving file from online repository...')
                try:
                    download_file(self.PROFILE_REPOSITORY + os.path.basename(filename), filename)
                    self.__profile = pd.read_csv(filename, decimal=".", header=None, compression=compression_type)
                except:
                    raise Exception(os.path.basename(filename) + ' is currently not available for download.', err)

            if self.__profile.shape[1]>1:
                self.__profile = self.__profile.loc[:,1]

            sample_time = general_config.timestep
            self.__duration = general_config.duration
            self.__start_epoch_time = general_config.start
            self.__start_human_time = general_config.start_yyyymmdd
            self.__config_general = general_config
            self.__scaling = scaling

            start_year = self.__start_human_time[0:4]
            start_date = self.__start_human_time[0:4] + '-01-01 '
            reset_time = '00:00:00'
            year_first_timestamp = time.strftime(start_date) + reset_time
            is_leap = calendar.isleap(int(start_year))

            if is_leap:
                annual_hours = 8784
            else:
                annual_hours = 8760

            year_start_epoch_time = general_config.extract_utc_timestamp_from(year_first_timestamp)
            self.__initial_offset = self.__start_epoch_time - year_start_epoch_time

            profile = self.__profile.to_numpy().transpose()
            profile = profile[0, :]
            original_profile_length = profile.size

            if original_profile_length % annual_hours == 0:
                pass
            else:
                if is_leap:
                    divisor = self.LEAP_YEAR_HOURS
                else:
                    divisor = self.YEAR_HOURS
                multiple = round(original_profile_length / divisor)
                profile = profile[0 : multiple * divisor]
                original_profile_length = profile.size

            original_seconds_per_timestep = (annual_hours / original_profile_length) * 3600
            original_timesteps = np.arange(1, annual_hours + 1, original_seconds_per_timestep / 3600)
            np.place(original_timesteps, original_timesteps > annual_hours, annual_hours)
            to_interpolate_time_steps = np.arange(1, annual_hours + 1, sample_time / 3600)
            np.place(to_interpolate_time_steps, to_interpolate_time_steps > annual_hours, annual_hours)

            interpolated_profile = np.interp(to_interpolate_time_steps, original_timesteps, profile)
            if 'swap' in filename:
                interpolated_profile *= profile.sum() / interpolated_profile.sum()
                interpolated_profile = np.round_(interpolated_profile)
                print(sum(interpolated_profile))

            self.__profile = interpolated_profile * self.__scaling

            self.__min_profile_value = min(self.__profile)
            self.__average_profile_value = np.mean(self.__profile)
            self.__max_profile_value = max(self.__profile)

            self.__value = 0
            self.__capacity_factor = 0

    def next(self, time: float, adapted_time: float) -> float:
        """
        Obtain the next profile value based on the provided time.

        Args:
            time (float): Current time value.
            adapted_time (float): Adapted time value.

        Returns:
            float: The profile value at the corresponding time.
        """
        adjusted_time = time - adapted_time + self.__initial_offset
        counter = int((adjusted_time - self.__config_general.start) / self.__config_general.timestep)
        try:
            self.__value = self.__profile[counter-1]
        except:
            self.__initial_offset = 0
            self.__value = self.__profile[0]
        return self.__value

    @property
    def profile(self) -> np.ndarray:
        """Return the profile as a ndarray."""
        return self.__profile

    @property
    def max_profile_value(self) -> float:
        """Return the maximum value of the profile."""
        return self.__max_profile_value

    @property
    def min_profile_value(self) -> float:
        """Return the minimum value of the profile."""
        return self.__min_profile_value

    @property
    def average_profile_value(self) -> float:
        """Return the average value of the profile."""
        return self.__average_profile_value

    @property
    def capacity_factor(self) -> float:
        """
        Return the capacity factor of the profile.

        Note:
            This is represented in per unit (p.u.).
        """
        return self.__capacity_factor  # in p.u.

    @capacity_factor.setter
    def capacity_factor(self, value: float) -> None:
        """
        Set the capacity factor for the profile.

        Args:
            value (float): Capacity factor value.
        """
        self.__capacity_factor = value

    def calculate_capacity_factor(self) -> float:
        """
        Calculate the capacity factor of the profile.

        This method is only valid for normalized or discrete binary profiles.

        Returns:
            float: Capacity factor in per unit (p.u.).
        """
        capacity_factor = sum(self.__profile) / (self.__scaling*len(self.__profile))  # in p.u.
        return capacity_factor  # in p.u.

    def get_initial_offset(self) -> float:
        """Return the initial offset of the profile."""
        return self.__initial_offset

    def close(self) -> None:
        """Terminate the object."""
        pass
