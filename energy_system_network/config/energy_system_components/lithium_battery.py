from configparser import ConfigParser
from energy_system_network.config.config import Config
import ast


class LithiumBatteryConfig(Config):
    """
    Configuration handler for lithium battery setups.

    This class provides methods to access various configuration parameters related to lithium batteries
    such as their power settings, converters, and cell attributes.

    Attributes:
        config_name (str): Default name of the configuration.
        __cell_start_soh (float): Represents the initial State of Health (SOH) of the battery cell.


    Methods:
        __init__(path: str, config: ConfigParser, config_name: str): Initialize the battery configuration.
        ac_dc_power(float): AC/DC converter power.
        energy(): Battery energy.
        acdc_converter(): Type of AC/DC converter.
        number_of_converters(): Number of converters.
        switch_value(): Switch value for the AC/DC converter.
        housing(): Type of housing for the battery.
        hvac(): Type of HVAC system.
        hvac_thermal_power(): Thermal power of the HVAC system.
        hvac_set_temperature(): Set temperature for the HVAC system.
        dcdc_converter(): Type of DC/DC converter.
        cell(): Type of battery cell.
        cell_eol(): End of life for the battery cell.
        cell_start_soh(): Start state of health for the battery cell.
        cell_start_rinc(): Initial resistance increase for the battery cell.
        cell_start_soh_share(): Start state of health share for the battery cell.
        enable_thermal_simulation(): Flag to enable thermal simulation.
        ambient_temperature_model(): Type of ambient temperature model.
        solar_irradiation_model(): Type of solar irradiation model.
        operating_strategy(): Operating strategy for the battery.
        power_fcr(): FCR power setting.
        power_idm(): IDM power setting.
        soc_set(): Set state of charge for the battery.
        fcr_reserve(): FCR reserve setting.
        frequency_profile(): Frequency profile for the battery management system.
        use_local_technical_profile_dir(): Flag to use local technical profile directory.
        number_slots(): Number of slots in the battery swapping station.
        number_swap_mechanism(): Number of swapping mechanisms in the swapping station.
        swap_time(): Time required for a battery swap.
        bidirectional(): Flag to indicate if the electric vehicle supports bidirectional charging.
        idle_soc(): Idle state of charge for the electric vehicle.
    """
    config_name: str = 'lithium_battery_setup'

    # Section names
    BATTERY: str = 'BATTERY'
    ELECTRIC_VEHICLE: str = 'ELECTRIC_VEHICLE'
    SWAPPING_STATION: str = 'SWAPPING_STATION'

    ACDC_CONVERTER: str = 'ACDC_CONVERTER'
    HOUSING: str = 'HOUSING'
    HVAC: str = 'HVAC'
    DCDC_CONVERTER: str = 'DCDC_CONVERTER'
    CELL: str = 'CELL'
    THERMAL_SIMULATION = 'THERMAL_SIMULATION'
    AMBIENT_TEMPERATURE_MODEL: str = 'AMBIENT_TEMPERATURE_MODEL'
    SOLAR_IRRADIATION_MODEL: str = 'SOLAR_IRRADIATION_MODEL'
    ENERGY_MANAGEMENT: str = 'ENERGY_MANAGEMENT'

    # Field names
    POWER: str = 'POWER'
    ENERGY: str = 'ENERGY'
    TYPE: str = 'TYPE'

    # ACDC_CONVERTER field names
    NUMBER_OF_CONVERTERS: str = 'NUMBER_OF_CONVERTERS'
    SWITCH_VALUE: str = 'SWITCH_VALUE'

    # HVAC field names
    THERMAL_POWER: str = 'THERMAL_POWER'
    SET_TEMPERATURE: str = 'SET_TEMPERATURE'

    # Cell field names
    EOL: str = 'EOL'
    START_SOH: str = 'START_SOH'
    START_SOH_SHARE: str = 'START_SOH_SHARE'
    START_RINC: str = 'START_RINC'

    # THERMAL_SIMULATION field names
    ENABLE: str = 'ENABLE'

    # EMS parameters
    STRATEGY: str = 'STRATEGY'
    POWER_FCR: str = 'POWER_FCR'
    POWER_IDM: str = 'POWER_IDM'
    SOC_SET: str = 'SOC_SET'
    FCR_RESERVE: str = 'FCR_RESERVE'

    USE_LOCAL_TECHNICAL_PROFILE_DIR: str = 'USE_LOCAL_TECHNICAL_PROFILE_DIR'
    FREQUENCY_PROFILE: str = 'FREQUENCY_PROFILE'

    # BSS parameters
    NUMBER_SLOTS: str = 'NUMBER_SLOTS'
    NUMBER_SWAP_MECHANISM: str = 'NUMBER_SWAP_MECHANISM'
    SWAP_TIME: str = 'SWAP_TIME'

    # EV parameters
    BIDIRECTIONAL: str = 'BIDIRECTIONAL'
    IDLE_SOC: str = 'IDLE_SOC'

    def __init__(self, path: str = None, config: ConfigParser = None, config_name: str = None):
        """
        Initialize the lithium battery configuration.

        Args:
            path (str, optional): Path to the configuration file.
            config (ConfigParser, optional): An instance of a config parser.
            config_name (str, optional): Name of the configuration.
        """
        if config_name is None:
            config_name = self.config_name
        super().__init__(path, config_name, config)
        self.__cell_start_soh = float(self.get_property(self.CELL, self.START_SOH))  # Initialize

    @property
    def ac_dc_power(self) -> float:
        """
        float: AC/DC converter power based on the configuration.
        """
        return float(self.get_property(self.ACDC_CONVERTER, self.POWER))

    @property
    def energy(self) -> float:
        """
        float: Total energy of the battery based on the configuration.
        """
        return float(self.get_property(self.BATTERY, self.ENERGY))

    @property
    def acdc_converter(self) -> str:
        """
        str: Type of AC/DC converter specified in the configuration.
        """
        return self.get_property(self.ACDC_CONVERTER, self.TYPE)

    @property
    def number_of_converters(self) -> int:
        """
        int: Number of converters used in the system based on the configuration.
        """
        return int(self.get_property(self.ACDC_CONVERTER, self.NUMBER_OF_CONVERTERS))

    @property
    def switch_value(self) -> float:
        """
        float: Switch value for the AC/DC converter as per configuration.
        """
        return float(self.get_property(self.ACDC_CONVERTER, self.SWITCH_VALUE))

    @property
    def housing(self) -> str:
        """
        str: The type of housing specified in the configuration.
        """
        return self.get_property(self.HOUSING, self.TYPE)

    @property
    def hvac(self) -> str:
        """
        str: The type of Heating, Ventilation, and Air Conditioning (HVAC) system as per configuration.
        """
        return self.get_property(self.HVAC, self.TYPE)

    @property
    def hvac_thermal_power(self) -> float:
        """
        float: The thermal power of the HVAC system based on the configuration.
        """
        return float(self.get_property(self.HVAC, self.THERMAL_POWER))

    @property
    def hvac_set_temperature(self) -> float:
        """
        float: The set temperature for the HVAC system as per configuration.
        """
        return float(self.get_property(self.HVAC, self.SET_TEMPERATURE))

    @property
    def dcdc_converter(self) -> str:
        """
        str: The type of DC/DC converter specified in the configuration.
        """
        return self.get_property(self.DCDC_CONVERTER, self.TYPE)

    @property
    def cell(self) -> str:
        """
        str: The type of battery cell specified in the configuration.
        """
        return self.get_property(self.CELL, self.TYPE)

    @property
    def cell_eol(self) -> float:
        """
        float: The End Of Life (EOL) value for the battery cell as per configuration.
        """
        return float(self.get_property(self.CELL, self.EOL))

    @property
    def cell_start_soh(self) -> float:
        """
        float: Initial State of Health (SOH) of the battery cell.
        """
        return self.__cell_start_soh

    @cell_start_soh.setter
    def cell_start_soh(self, value: float) -> None:
        """
        Set the initial State of Health (SOH) for the battery cell.

        Args:
            value (float): The initial SOH value to be set for the battery cell. It should be a float
                           between 0 (completely degraded) and 1 (brand new).
        """
        self.__cell_start_soh = value

    @property
    def cell_start_rinc(self) -> (float, None):
        """
        float or None: Initial resistance increase of the battery cell, or None if not set.
        """
        try:
            return float(self.get_property(self.CELL, self.START_RINC))
        except:
            return None

    @property
    def cell_start_soh_share(self) -> (float, None):
        """
        float or None: Initial SOH share of the battery cell, or None if not set.
        """
        try:
            return float(self.get_property(self.CELL, self.START_SOH_SHARE))
        except:
            return None

    @property
    def enable_thermal_simulation(self) -> str:
        """
        str: Indicate if the thermal simulation is enabled based on the configuration.
        """
        return self.get_property(self.THERMAL_SIMULATION, self.ENABLE)

    @property
    def ambient_temperature_model(self) -> str:
        """
        str: The type of ambient temperature model specified in the configuration.
        """
        return self.get_property(self.AMBIENT_TEMPERATURE_MODEL, self.TYPE)

    @property
    def solar_irradiation_model(self) -> str:
        """
        str: The type of solar irradiation model specified in the configuration.
        """
        return self.get_property(self.SOLAR_IRRADIATION_MODEL, self.TYPE)

    @property
    def operating_strategy(self) -> str:
        """
        str: The operating strategy defined in the configuration.
        """
        return self.get_property(self.ENERGY_MANAGEMENT, self.STRATEGY)

    @property
    def power_fcr(self) -> float:
        """
        float: The power for Frequency Containment Reserves (FCR) as per configuration.
        """
        return float(self.get_property(self.ENERGY_MANAGEMENT, self.POWER_FCR))

    @property
    def power_idm(self) -> float:
        """
        float: The Intraday Market (IDM) power specified in the configuration.
        """
        return float(self.get_property(self.ENERGY_MANAGEMENT, self.POWER_IDM))

    @property
    def soc_set(self) -> float:
        """
        float: State of Charge (SOC) set point value as per configuration.
        """
        return float(self.get_property(self.ENERGY_MANAGEMENT, self.SOC_SET))

    @property
    def fcr_reserve(self) -> float:
        """
        float: The reserve power for FCR based on the configuration.
        """
        return float(self.get_property(self.ENERGY_MANAGEMENT, self.FCR_RESERVE))

    @property
    def frequency_profile(self) -> str:
        """
        str: The profile for frequency as defined in the configuration.
        """
        return self.get_property(self.ENERGY_MANAGEMENT, self.FREQUENCY_PROFILE)

    @property
    def use_local_technical_profile_dir(self) -> bool:
        """
        bool: Indicator to determine if a local technical profile directory should be used.
        """
        return ast.literal_eval(self.get_property(self.ENERGY_MANAGEMENT, self.USE_LOCAL_TECHNICAL_PROFILE_DIR))

    # BSS parameters
    @property
    def number_slots(self) -> int:
        """
        int: The number of slots available at the swapping station as per configuration.
        """
        return int(self.get_property(self.SWAPPING_STATION, self.NUMBER_SLOTS))

    @property
    def number_swap_mechanism(self) -> int:
        """
        int: The number of swap mechanisms available at the swapping station as per configuration.
        """
        return int(self.get_property(self.SWAPPING_STATION, self.NUMBER_SWAP_MECHANISM))

    @property
    def swap_time(self) -> float:
        """
        float: The time taken for a battery swap at the swapping station.
        """
        return float(self.get_property(self.SWAPPING_STATION, self.SWAP_TIME))

    # EV parameters
    @property
    def bidirectional(self) -> bool:
        """
        bool: Indicates if the electric vehicle supports bidirectional charging.
        """
        return ast.literal_eval(self.get_property(self.ELECTRIC_VEHICLE, self.BIDIRECTIONAL))

    # EV parameters
    @property
    def idle_soc(self) -> float:
        """
        float: Idle state of charge (SOC) of the electric vehicle based on the configuration.
        """
        return float(self.get_property(self.ELECTRIC_VEHICLE, self.IDLE_SOC))
