import importlib
import os
from configparser import ConfigParser
from energy_system_network.config.energy_system_components.lithium_battery import LithiumBatteryConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.storage_config import StorageConfig
from energy_system_network.constants_energy_sys_network import SIMSES_REFERENCE_CONFIG_PATH
import importlib.resources
import time


class SimSESConfigGenerator:
    """
    A class responsible for generating configurations for SimSES including simulation, analysis, and data.

    Attributes:
        CONFIG_FILES (List[str]): A list containing configuration file types.
        __general_config (GeneralSimulationConfig): General simulation configuration.
        __storage_config (StorageConfig): Configuration for the storage system.
        __lithium_battery_config (LithiumBatteryConfig): Configuration specifics for lithium batteries.
        __peak_power (float): Peak power of the storage system.
        __energy_rating (float): Energy rating of the storage system.
        __dc_storage_flag (bool): A flag indicating if the system is a DC storage system.
        __config_check_simses_passed (bool): A flag indicating if the configuration check passed.
        __simulation_config_generator (SimulationConfigGenerator): Utility for generating simulation configuration.
        __analysis_config_generator (AnalysisConfigGenerator): Utility for generating analysis configuration.
        __temporary_config (dict): A temporary storage for configuration values.

    Methods:
        __init__(general_simulation_config, storage_config, lithium_battery_config, peak_power, energy_rating, dc_storage_system_flag, config_check_passed): Initializes the class with the specified configurations.
        create_simses_simulation_config(): Generates and returns the SimSES simulation configuration.
        create_simses_analysis_config(): Generates and returns the SimSES analysis configuration.
        create_simses_data_config(): Generates the SimSES data configuration.
        update_temporary_config(key, value): Updates the temporary configuration.
        get_temporary_config_value(key): Retrieves a specific value from the temporary configuration.
        __check_reference_configs(): Checks and updates reference configurations if necessary.
    """
    # config file types
    CONFIG_FILES: [str] = ['simulation', 'analysis', 'data']

    def __init__(self, general_simulation_config: GeneralSimulationConfig, storage_config: StorageConfig,
                 lithium_battery_config: LithiumBatteryConfig,
                 peak_power: float, energy_rating: float, dc_storage_system_flag: bool = False, config_check_passed: bool = False):
        """
        Initialize the SimSESConfigGenerator.

        Args:
            general_simulation_config (GeneralSimulationConfig): General simulation configuration.
            storage_config (StorageConfig): Storage configuration.
            lithium_battery_config (LithiumBatteryConfig): Configuration specific to lithium batteries.
            peak_power (float): Peak power of the storage system.
            energy_rating (float): Energy rating of the storage system.
            dc_storage_system_flag (bool, optional): Flag for DC storage system. Default is False.
            config_check_passed (bool, optional): Flag indicating if the config check has passed. Default is False.
        """

        self.__general_config = general_simulation_config
        self.__storage_config = storage_config
        self.__lithium_battery_config = lithium_battery_config
        self.__peak_power = peak_power
        self.__energy_rating = energy_rating
        self.__dc_storage_flag = dc_storage_system_flag

        self.__config_check_simses_passed = config_check_passed

        self.__check_reference_configs()

        from simses.commons.config.generation.simulation import SimulationConfigGenerator
        from simses.commons.config.generation.analysis import AnalysisConfigGenerator

        self.__simulation_config_generator = SimulationConfigGenerator()
        self.__analysis_config_generator = AnalysisConfigGenerator()

        self.__temporary_config = {}

    def create_simses_simulation_config(self) -> ConfigParser:
        """
        Generate and return the SimSES simulation configuration.

        Returns:
            ConfigParser: Generated configuration for the SimSES simulation.
        """
        if self.__temporary_config:
            start_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(self.__temporary_config[GeneralSimulationConfig.START]))
        else:
            start_time = self.__general_config.start_yyyymmdd

        self.__simulation_config_generator.set_simulation_time(start_time,
                                                               self.__general_config.end_yyyymmdd,
                                                               self.__general_config.timestep,
                                                               self.__general_config.loop)

        # Add EMS if specified
        start_soc = self.__storage_config.start_soc
        if self.__lithium_battery_config.operating_strategy:
            if 'Fcr' in self.__lithium_battery_config.operating_strategy:
                self.__simulation_config_generator.set_fcr_operation_strategy(self.__lithium_battery_config.power_fcr,
                                                                              self.__lithium_battery_config.power_idm,
                                                                              self.__lithium_battery_config.fcr_reserve,
                                                                              self.__lithium_battery_config.soc_set)
                start_soc = self.__lithium_battery_config.soc_set

                # Add user-specified profile (if selected)
                if self.__lithium_battery_config.frequency_profile:
                    self.__simulation_config_generator.set_frequency_profile_config(self.__lithium_battery_config.frequency_profile)
            else:
                self.__simulation_config_generator.set_operation_strategy(self.__lithium_battery_config.operating_strategy)
        else:
            pass

        self.__simulation_config_generator.set_battery(start_soc=str(start_soc),
                                                       min_soc=str(self.__storage_config.min_soc),
                                                       max_soc=str(self.__storage_config.max_soc),
                                                       eol=str(self.__lithium_battery_config.cell_eol),
                                                       start_soh=str(self.__lithium_battery_config.cell_start_soh),
                                                       start_soh_share=str(self.__lithium_battery_config.cell_start_soh_share),
                                                       start_rinc=str(self.__lithium_battery_config.cell_start_rinc))

        # Add storage system AC from bottom up
        self.__simulation_config_generator.clear_acdc_converter()
        self.__simulation_config_generator.clear_housing()
        self.__simulation_config_generator.clear_hvac()
        self.__simulation_config_generator.clear_storage_system_ac()

        acdc_converter_key: str = self.__simulation_config_generator.add_acdc_converter(self.__lithium_battery_config.acdc_converter, self.__lithium_battery_config.number_of_converters, self.__lithium_battery_config.switch_value)
        housing_key: str = self.__simulation_config_generator.add_housing(self.__lithium_battery_config.housing)
        hvac_key: str = self.__simulation_config_generator.add_hvac(self.__lithium_battery_config.hvac,
                                                                    self.__lithium_battery_config.hvac_thermal_power,
                                                                    self.__lithium_battery_config.hvac_set_temperature)
        storage_system_ac_key: str = self.__simulation_config_generator.add_storage_system_ac(self.__peak_power, 333,
                                                                                              acdc_converter_key,
                                                                                              housing_key,
                                                                                              hvac_key)

        # Add storage system DC from bottom up
        self.__simulation_config_generator.clear_dcdc_converter()
        self.__simulation_config_generator.clear_storage_technology()
        self.__simulation_config_generator.clear_storage_system_dc()

        dcdc_converter_key: str = self.__simulation_config_generator.add_dcdc_converter(self.__lithium_battery_config.dcdc_converter,
                                                                                        self.__peak_power)
        storage_technology_key: str = self.__simulation_config_generator.add_lithium_ion_battery(self.__energy_rating,
                                                                                                 self.__lithium_battery_config.cell,
                                                                                                 start_soc,
                                                                                                 self.__lithium_battery_config.cell_start_soh)
        self.__simulation_config_generator.add_storage_system_dc(storage_system_ac_key,
                                                                 dcdc_converter_key,
                                                                 storage_technology_key)

        # Enable/disable thermal simulation
        self.__simulation_config_generator.set_enable_thermal_simulation(self.__lithium_battery_config.enable_thermal_simulation)

        # Add ambient conditions
        self.__simulation_config_generator.set_ambient_temperature_model(self.__lithium_battery_config.ambient_temperature_model)
        self.__simulation_config_generator.set_solar_irradiation_model(self.__lithium_battery_config.solar_irradiation_model)

        return self.__simulation_config_generator.get_config()

    def create_simses_analysis_config(self) -> ConfigParser:
        """
        Generate and return the SimSES analysis configuration.

        Returns:
            ConfigParser: Generated configuration for the SimSES analysis.
        """
        self.__analysis_config_generator.print_results(True)
        return self.__analysis_config_generator.get_config()

    def create_simses_data_config(self):
        """
        Generate the SimSES data configuration.
        """
        pass

    def update_temporary_config(self, key: str, value: float) -> None:
        """
        Update the temporary configuration with a given key-value pair.

        Args:
            key (str): Configuration key.
            value (float): Configuration value.
        """
        self.__temporary_config[key] = value

    def get_temporary_config_value(self, key: str) -> float:
        """
        Retrieve a value from the temporary configuration based on the given key.

        Args:
            key (str): Configuration key.

        Returns:
            float: Configuration value for the specified key.
        """
        return self.__temporary_config[key]

    def __check_reference_configs(self) -> None:
        """
        Check the reference configurations against the package versions and update if necessary.
        """
        import simses as simses_config_folder
        for config_type in self.CONFIG_FILES:
            package_reference_config_file = importlib.resources.read_text(simses_config_folder, config_type + '.defaults.ini')
            config_file_name_defaults = 'simses_' + config_type + ".defaults.ini"
            config_file_path_defaults = SIMSES_REFERENCE_CONFIG_PATH + config_file_name_defaults

            if os.path.exists(config_file_path_defaults):
                existing_config_file = open(config_file_path_defaults, encoding='windows-1252')
                existing_config_file = existing_config_file.read()
                if package_reference_config_file == existing_config_file:
                    if self.__config_check_simses_passed:
                        pass
                    else:
                        print('No difference found between the existing ' + config_file_name_defaults + ' file in the project '
                                                                                                        'and the reference file in the package.')
                else:
                    print(
                        'The ' + config_file_name_defaults + ' present in the project and the reference file in the package are diferent. '
                                                             'The copy in the project has been overwritten to reflect the changes. '
                                                             'Please adapt your ' + 'lithium_battery_setup' + ' file accordingly.')
                    text_file = open(config_file_path_defaults, "w")
                    text_file.write(package_reference_config_file)
                    text_file.close()
            else:
                print('No ' + config_file_name_defaults + ' file was found in the project. '
                                                          'The file has been mirrored from the package and stored under ' + SIMSES_REFERENCE_CONFIG_PATH + '.'
                                                                                                                                                           ' Create your own ' + 'lithium_battery_setup.local.ini' + ' to alter LithiumBatterySystem parameters.')
                text_file = open(config_file_path_defaults, "w")
                text_file.write(package_reference_config_file)
                text_file.close()
