from abc import ABC
from configparser import ConfigParser
from energy_system_network.constants_energy_sys_network import CONFIG_PATH


class Config(ABC):
    """
    Configuration handler class for managing configuration options.

    Attributes:
        __used_config (dict): Dictionary containing the used configurations.
        __extensions (list): List of file extensions used for configuration files.
        __config (ConfigParser): ConfigParser object holding the actual configuration data.
        __file_name (str): Name of the file where the configuration is read from.

    Methods:
        __init__(path: str, name: str, config: ConfigParser): Initializes the Config object with a specified path and name.
        get_property(section: str, option: str) -> str: Retrieves a property value given its section and option.
        __overwrite_config_with(config: ConfigParser): Overwrites the existing config with values from another ConfigParser.
        __add_to_used_config(section: str, option: str, value: str): Adds a given configuration option to the used configuration.
        write_config_to(path: str): Writes the current configuration to a file.
        get_config() -> ConfigParser: Retrieves the current configuration.
    """
    __used_config: dict = dict()
    __extensions: [str] = ['.defaults.ini', '.local.ini', '.ini']

    def __init__(self, path: str, name: str, config: ConfigParser = None):
        """
        Initializes the Config object.

        Args:
            path (str): Path to the configuration directory.
            name (str): Name of the configuration file.
            config (ConfigParser, optional): A ConfigParser object with configurations to be used.
        """
        if path is None:
            path = CONFIG_PATH
        self.__config: ConfigParser = ConfigParser()
        for extension in self.__extensions:
            self.__config.read(path + name + extension)
            if 'defaults' not in extension or 'local' not in extension:
                self.__file_name: str = name + extension
        if config:
            self.__overwrite_config_with(config)
        self.__config.read(path + name)

    def get_property(self, section: str, option: str):
        """
        Fetches the value for a given section and option from the configuration.

        Args:
            section (str): The section name.
            option (str): The option name.

        Returns:
            str: The value associated with the given section and option.

        Raises:
            KeyError: If the specified section or option is not found.
        """
        value = None
        try:
            value = self.__config[section][option]
            self.__add_to_used_config(section, option, value)
        except KeyError as err:
            raise err
        finally:
            return value

    def __overwrite_config_with(self, config: ConfigParser):
        """
        Overwrites the current configuration with values from the provided ConfigParser.

        Args:
            config (ConfigParser): The ConfigParser object containing the new values.
        """
        if config is not None:
            for section in config.sections():
                if section in self.__config.sections():
                    for option in config.options(section):
                        if option in self.__config.options(section):
                            value = config[section][option]
                            # print('[' + type(self).__name__ + '] Setting new value in section ' + section +
                            #       ' for option ' + option + ' with ' + value)
                            self.__config[section][option] = value

    def __add_to_used_config(self, section: str, option: str, value: str):
        """
        Adds a configuration option to the used configuration.

        Args:
            section (str): The section name.
            option (str): The option name.
            value (str): The value associated with the section and option.
        """
        key: str = self.__file_name
        if key not in self.__used_config.keys():
            self.__used_config[key] = ConfigParser()
        config: ConfigParser = self.__used_config[key]
        if section not in config.sections():
            config.add_section(section)
        if option not in config.options(section):
            config.set(section, option, value)

    def write_config_to(self, path: str) -> None:
        """
        Writes the current configuration to a file.

        Args:
            path (str): The path where the configuration file should be written.
        """
        with open(path + self.__file_name, 'w') as configfile:
            self.__config.write(configfile)
                # used_config.write(configfile)
    #     if config is not None:
    #         for section in config.sections():
    #             if section in self.__config.sections():
    #                 for option in config.options(section):
    #                     if option in self.__config.options(section):
    #                         value = config[section][option]
    #                         # print('[' + type(self).__name__ + '] Setting new value in section ' + section +
    #                         #       ' for option ' + option + ' with ' + value)
    #                         self.__config[section][option] = value
    #

    def get_config(self) -> ConfigParser:
        """
        Retrieves the current configuration settings.

        Returns:
            ConfigParser: The current configuration.
        """
        return self.__config

    # def get_property(self, section: str, option: str, config: ConfigParser):
    #     value = None
    #     try:
    #         value = self.__config[section][option]
    #         if section not in config.sections():
    #             config.add_section(section)
    #         config.set(section, option, value)
    #     except KeyError as err:
    #         raise err
    #     finally:
    #         return value
    #
    # def write_config_to(self, path: str, config: ConfigParser):
    #     with open(path + self.__file_name, 'w') as configfile:
    #         config.write(configfile)
    #
    # @classmethod
    # @abstractmethod
    # def set_config(cls, config: ConfigParser) -> None:
    #     pass
