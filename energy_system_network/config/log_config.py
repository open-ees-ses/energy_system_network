import logging
from configparser import ConfigParser
from energy_system_network.config.config import Config


class LogConfig(Config):
    """
    Configuration class for logging settings.

    Attributes:
        config_name (str): Specifies the name of the logger configuration.
        __config_parser (ConfigParser): Private attribute for holding the configuration parser.
        __section (str): Private attribute specifying the section name for logging configuration.

    Methods:
        __init__(path: str = None): Initializes the LogConfig with a given path, configuration name, and parser.
        set_config(config: ConfigParser) -> None: Class method to set the configuration parser.
        log_level() -> int: Returns the logging level based on configuration.
        log_to_console() -> bool: Determines if logs should be directed to console based on configuration.
        log_to_file() -> bool: Determines if logs should be saved to a file based on configuration.
    """
    config_name: str = 'logger'
    __config_parser: ConfigParser = None

    def __init__(self, path: str = None):
        """
        Initializes the LogConfig class with the specified configuration path and parser.

        Args:
            path (str, optional): Path to the configuration file.
        """
        super().__init__(path, self.config_name, self.__config_parser)
        self.__section: str = 'LOGGING'

    @classmethod
    def set_config(cls, config: ConfigParser) -> None:
        """
        Set the configuration parser for the class.

        Args:
            config (ConfigParser): The configuration parser object.
        """
        cls.__config_parser = config

    @property
    def log_level(self) -> int:
        """
        Retrieve the logging level based on the configuration setting.

        Return:
            int: The logging level (e.g., logging.DEBUG, logging.ERROR).
        """
        try:
            level: str = self.get_property(self.__section, 'LOG_LEVEL')
            if level == 'DEBUG':
                return logging.DEBUG
            elif level == 'INFO':
                return logging.INFO
            elif level == 'WARNING':
                return logging.WARNING
            elif level == 'ERROR':
                return logging.ERROR
            else:
                return logging.DEBUG
        except:
            return logging.ERROR

    @property
    def log_to_console(self) -> bool:
        """
        Check if logging to console is enabled in the configuration.

        Return:
            bool: True if logging to console is enabled, otherwise False.
        """
        return self.get_property(self.__section, 'LOG_TO_CONSOLE') in ['True']

    @property
    def log_to_file(self) -> bool:
        """
        Check if logging to a file is enabled in the configuration.

        Return:
            bool: True if logging to a file is enabled, otherwise False.
        """
        return self.get_property(self.__section, 'LOG_TO_FILE') in ['True']
