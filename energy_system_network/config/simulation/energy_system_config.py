from configparser import ConfigParser
from energy_system_network.config.simulation.simulation_config import SimulationConfig


class EnergySystemConfig(SimulationConfig):
    """
    Configuration for energy systems in a given network.

    This class extends the SimulationConfig to provide specific functionalities for handling
    and extracting configurations related to energy system components such as generation,
    storage, load, and grid components.

    Attributes:
        SECTION (str): The section identifier in the configuration file for energy system network.
        ENERGY_SYSTEMS (str): Keyword for retrieving energy systems information from the configuration.
        GENERATION_COMPONENTS (str): Keyword for retrieving generation component information.
        STORAGE_COMPONENTS (str): Keyword for retrieving storage component information.
        LOAD_COMPONENTS (str): Keyword for retrieving load component information.
        GRID_COMPONENTS (str): Keyword for retrieving grid component information.

    Methods:
        __init__(config: ConfigParser, path: str = None): Initializes the EnergySystemConfig with given configuration and path.
        energy_systems() -> [[str]]: Returns a list of defined energy systems from the configuration.
        generation_components() -> [[str]]: Returns a list of generation components associated with each energy system.
        storage_components() -> [[str]]: Returns a list of storage components associated with each energy system.
        load_components() -> [[str]]: Returns a list of load components associated with each energy system.
        grid_components() -> [[str]]: Returns a list of grid components associated with each energy system.
    """
    SECTION: str = 'ENERGY_SYSTEM_NETWORK'

    ENERGY_SYSTEMS: str = 'ENERGY_SYSTEMS'
    GENERATION_COMPONENTS: str = 'GENERATION_COMPONENTS'
    STORAGE_COMPONENTS: str = 'STORAGE_COMPONENTS'
    LOAD_COMPONENTS: str = 'LOAD_COMPONENTS'
    GRID_COMPONENTS: str = 'GRID_COMPONENTS'

    def __init__(self, config: ConfigParser, path: str = None):
        """
        Initializes the EnergySystemConfig instance with the given configuration and path.

        Args:
            config (ConfigParser): The configuration parser containing energy system settings.
            path (str, optional): The path to the configuration file. Defaults to None.
        """
        super().__init__(path, config)

    @property
    def energy_systems(self) -> [[str]]:
        """Returns a list of energy systems from simulation config"""
        props: [str] = self.get_property(self.SECTION, self.ENERGY_SYSTEMS).replace(' ', '').replace('\t', '').split(
            '\n')
        del props[0]
        res: [[str]] = list()
        for prop in props:
            res.append(prop.split(','))
        return res

    @property
    def generation_components(self) -> [[str]]:
        """Returns a list of generation components for each energy system from simulation config"""
        props: [str] = self.get_property(self.SECTION, self.GENERATION_COMPONENTS).replace(' ', '').replace('\t',
                                                                                                           '').split(
            '\n')
        del props[0]
        res: [[str]] = list()
        for prop in props:
            res.append(prop.split(','))
        return res

    @property
    def storage_components(self) -> [[str]]:
        """Returns a list of storage components for each energy system from simulation config"""
        props: [str] = self.get_property(self.SECTION, self.STORAGE_COMPONENTS).replace(' ', '').replace('\t', '').split(
            '\n')
        del props[0]
        res: [[str]] = list()
        for prop in props:
            res.append(prop.split(','))
        return res

    @property
    def load_components(self) -> [[str]]:
        """Returns a list of load components for each energy system from simulation config"""
        props: [str] = self.get_property(self.SECTION, self.LOAD_COMPONENTS).replace(' ', '').replace('\t', '').split(
            '\n')
        del props[0]
        res: [[str]] = list()
        for prop in props:
            res.append(prop.split(','))
        return res

    @property
    def grid_components(self) -> [[str]]:
        """Returns a list of grid components for each energy system from simulation config"""
        props: [str] = self.get_property(self.SECTION, self.GRID_COMPONENTS).replace(' ', '').replace('\t', '').split(
            '\n')
        del props[0]
        res: [[str]] = list()
        for prop in props:
            res.append(prop.split(','))
        return res
