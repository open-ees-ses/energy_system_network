from datetime import datetime
from configparser import ConfigParser
from pytz import timezone
from energy_system_network.config.simulation.simulation_config import SimulationConfig


class GeneralSimulationConfig(SimulationConfig):
    """
    This class is a specialized SimulationConfig that extracts general configuration parameters
    for energy system simulations.

    Attributes:
        __UTC (timezone): Universal Coordinated Time (UTC) timezone object.
        SECTION (str): The section in the config file that this class is responsible for.
        TIME_STEP, START, END, LOOP, EXPORT_INTERVAL (str): Constant keys used to fetch the corresponding values from the config file.

    Methods:
        __init__(self, config: ConfigParser = None, path: str = None): Initializes an instance of the GeneralSimulationConfig class.
        timestep(self) -> float: Returns the simulation timestep in seconds.
        start(self) -> float: Returns the simulation start timestamp.
        start_yyyymmdd(self) -> str: Returns the simulation start date as a string without conversion to epoch time.
        end(self) -> float: Returns the simulation end timestamp.
        end_yyyymmdd(self) -> str: Returns the simulation end date as a string without conversion to epoch time.
        duration(self) -> float: Returns the simulation duration in seconds.
        loop(self) -> int: Returns the number of simulation loops.
        export_interval(self) -> float: Returns the interval to write result to file.
        extract_utc_timestamp_from(self, date: str) -> float: Converts a date string into a UTC timestamp.

    """
    __UTC: timezone = timezone('UTC')
    SECTION: str = 'GENERAL'

    TIME_STEP: str = 'TIME_STEP'
    START: str = 'START'
    END: str = 'END'
    LOOP: str = 'LOOP'
    EXPORT_INTERVAL: str = 'EXPORT_INTERVAL'

    def __init__(self, config: ConfigParser = None, path: str = None):
        """
        Initializes an instance of the GeneralSimulationConfig class.

        Args:
            config (ConfigParser, optional): The configuration for the simulation. Defaults to None.
            path (str, optional): The path of the config file. Defaults to None.
        """
        super().__init__(path, config)

    @property
    def timestep(self) -> float:
        """Returns simulation timestep in s"""
        return float(self.get_property(self.SECTION, self.TIME_STEP))

    @property
    def start(self) -> float:
        """Returns simulation start timestamp"""
        date: str = self.get_property(self.SECTION, self.START)
        return self.extract_utc_timestamp_from(date)

    @property
    def start_yyyymmdd(self) -> str:
        """Returns simulation start timestamp as string without conversion to epoch time"""
        return self.get_property(self.SECTION, self.START)

    @property
    def end(self) -> float:
        """Returns simulation end timestamp"""
        date: str = self.get_property(self.SECTION, self.END)
        return self.extract_utc_timestamp_from(date)

    @property
    def end_yyyymmdd(self) -> str:
        """Returns simulation end timestamp as string without conversion to epoch time"""
        return self.get_property(self.SECTION, self.END)

    @property
    def duration(self) -> float:
        """Returns simulation duration in s from __analysis_config file_name"""
        return self.end - self.start

    @property
    def loop(self) -> int:
        """Returns number of simulation loops"""
        return int(self.get_property(self.SECTION, self.LOOP))

    @property
    def export_interval(self) -> float:
        """Returns interval to write result to file"""
        return float(self.get_property(self.SECTION, self.EXPORT_INTERVAL))

    def extract_utc_timestamp_from(self, date: str) -> float:
        """
        Converts a date string into a UTC timestamp.

        Args:
            date (str): The date string in the format of '%Y-%m-%d %H:%M:%S'.

        Returns:
            float: The UTC timestamp corresponding to the provided date string.
        """
        date: datetime = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        return self.__UTC.localize(date).timestamp()
