from configparser import ConfigParser
from energy_system_network.config.config import Config


class SimulationConfig(Config):
    """
    Represents the simulation configuration for the energy system network.

    This class extends the base configuration to provide specific features
    for managing the simulation configuration parameters.

    Attributes:
        config_name (str): Name of the configuration, set to 'simulation'.

    Methods:
        __init__(path: str, config: ConfigParser = None): Initializes the SimulationConfig instance.
    """
    config_name: str = 'simulation'

    def __init__(self, path: str, config: ConfigParser = None):
        """
        Initializes the SimulationConfig instance.

        Args:
            path (str): Path to the configuration file.
            config (ConfigParser, optional): A configuration parser object.
        """
        super().__init__(path, self.config_name, config)


def create_dict_from(properties: [str]) -> dict:
    """
    Creates a dictionary from a list of comma-separated strings.

    Args:
        properties ([str]): List of strings where each string represents a key followed by values separated by commas.

    Returns:
        dict: A dictionary with keys extracted from the input and values as lists.

    Raises:
        Exception: If any of the keys are not unique.
    """
    res: dict = dict()
    for prop in properties:
        items: list = prop.split(',')
        name: str = items.pop(0)
        if name in res.keys():
            raise Exception(name + ' is not unique. Please check your config file.')
        res[name] = items
    return res


def create_list_from(properties: [str]) -> [[str]]:
    """
    Creates a list of lists from a list of comma-separated strings.

    Args:
        properties ([str]): List of strings to split.

    Returns:
        [[str]]: List of lists where each inner list is derived from splitting an input string by commas.
    """
    res: [[str]] = list()
    for prop in properties:
        res.append(prop.split(','))
    return res


def clean_split(properties: str) -> [str]:
    """
    Cleans and splits the given string properties.

    This function removes spaces, tabs, and splits the string by newline characters.
    It also eliminates any empty strings from the resultant list.

    Args:
        properties (str): String to be cleaned and split.

    Returns:
        [str]: List of strings derived from splitting the cleaned input string.
    """
    props: [str] = properties.replace(' ', '').replace('\t', '').split('\n')
    copy = props[:]
    for prop in copy:
        if not prop:
            props.remove(prop)
    return props
