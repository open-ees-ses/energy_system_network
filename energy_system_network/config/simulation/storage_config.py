from configparser import ConfigParser
from energy_system_network.config.simulation.simulation_config import SimulationConfig


class StorageConfig(SimulationConfig):
    """
    Configuration for storage parameters in the energy system network.

    This configuration helps to determine the state of charge (SOC) parameters
    for storage devices within an energy system. It provides attributes to access
    starting, minimum, and maximum states of charge for these devices.

    Attributes:
        SECTION (str): Section name for storage in the configuration.
        START_SOC (str): Configuration key for starting state of charge (SOC).
        MIN_SOC (str): Configuration key for minimum allowable SOC.
        MAX_SOC (str): Configuration key for maximum allowable SOC.
        __start_soc (float): Private attribute to store the starting state of charge.

    Methods:
        __init__(config: ConfigParser, path: str = None): Initializes the StorageConfig instance.
        start_soc(): Starting SOC (0-1) of the storage.
        min_soc(): Minimum allowable SOC (0-1) of the storage.
        max_soc(): Maximum allowable SOC (0-1) of the storage.

    """
    SECTION: str = 'STORAGE'

    START_SOC: str = 'START_SOC'
    MIN_SOC: str = 'MIN_SOC'
    MAX_SOC: str = 'MAX_SOC'

    def __init__(self, config: ConfigParser = None, path: str = None):
        """
        Initializes the StorageConfig instance.

        Args:
            config (ConfigParser, optional): A configuration parser object.
            path (str, optional): Path to the configuration file.
        """
        super().__init__(path, config)
        self.__start_soc = float(self.get_property(self.SECTION, self.START_SOC))

    @property
    def start_soc(self) -> float:
        """
        SOC (0-1)

        Returns
        -------
        float:
            Returns the start soc from simulation config file

        """
        return self.__start_soc

    @start_soc.setter
    def start_soc(self, value: float) -> None:
        """
        sets the value of the start SOC
        :param value: start SOC as float
        """
        self.__start_soc = value

    @property
    def min_soc(self) -> float:
        """
        Minimum SOC (0-1)

        Returns
        -------
        float:
            Returns the minimum soc from simulation config file

        """
        return float(self.get_property(self.SECTION, self.MIN_SOC))

    @property
    def max_soc(self) -> float:
        """
        Maximum SOC (0-1)

        Returns
        -------
        float:
            Returns the maximum soc from simulation config file

        """
        return float(self.get_property(self.SECTION, self.MAX_SOC))
