from configparser import ConfigParser
from energy_system_network.config.simulation.simulation_config import SimulationConfig


class EnergyManagementConfig(SimulationConfig):
    """
    Configuration for energy management strategies and parameters.

    Attributes:
        SECTION (str): Section name for energy management in the configuration.
        STRATEGY (str): Configuration key for operation strategy.
        SIMPLE_DEFICIT_COVERAGE_PRIORITY (str): Configuration key for deficit coverage priority.
        OPTIMIZED_STRATEGY_TIME_HORIZON (str): Configuration key for optimized strategy time horizon.
        RERUN_OPTIMIZATION_TIMESTEPS (str): Configuration key for timesteps to rerun optimization.
        ARBITRAGE_TYPE (str): Configuration key for type of arbitrage algorithm.
        EMISSIONS (str): Identifier for emissions based arbitrage.
        ECONOMIC (str): Identifier for economic based arbitrage.

    Methods:
        __init__(config: ConfigParser, path: str = None): Initializes the EnergyManagementConfig instance.
        operation_strategy() -> [[str]]: Returns the operation strategy from the simulation config file.
        simple_deficit_coverage_priority() -> [str]: Returns the priority of flexible components from config file.
        arbitrage_type() -> str: Returns the type of arbitrage algorithm.
        optimized_strategy_time_horizon() -> int: Returns the length of time horizon in timesteps from config file.
        rerun_optimization_timesteps() -> int: Returns the number of timesteps to rerun the optimization from config file.
    """
    SECTION: str = 'ENERGY_MANAGEMENT'

    STRATEGY: str = 'STRATEGY'
    SIMPLE_DEFICIT_COVERAGE_PRIORITY: str = 'SIMPLE_DEFICIT_COVERAGE_PRIORITY'
    OPTIMIZED_STRATEGY_TIME_HORIZON: str = 'OPTIMIZED_STRATEGY_TIME_HORIZON'
    RERUN_OPTIMIZATION_TIMESTEPS: str = 'RERUN_OPTIMIZATION_TIMESTEPS'
    ARBITRAGE_TYPE:str = 'ARBITRAGE_TYPE'
    EMISSIONS: str = 'Emissions'
    ECONOMIC: str = 'Economic'

    def __init__(self, config: ConfigParser, path: str = None):
        """
        Initializes the EnergyManagementConfig instance.

        Args:
            config (ConfigParser): A configuration parser object.
            path (str, optional): Path to the configuration file.
        """
        super().__init__(path, config)

    @property
    def operation_strategy(self) -> [[str]]:
        """Returns operation strategy from simulation config file"""
        props: [str] = self.get_property(self.SECTION, self.STRATEGY).replace(' ', '').replace('\t', '').split(
            '\n')
        del props[0]
        res: [[str]] = list()
        for prop in props:
            res.append(prop.split(','))
        return res

    @property
    def simple_deficit_coverage_priority(self) -> [str]:
        """Returns priority of coming online for flexible components from configfile"""
        priority = self.get_property(self.SECTION, self.SIMPLE_DEFICIT_COVERAGE_PRIORITY).split('\n')
        return priority

    @property
    def arbitrage_type(self) -> str:
        """Returns type of arbitrage algorithm"""
        type = self.get_property(self.SECTION, self.ARBITRAGE_TYPE)
        if self.ECONOMIC in type or self.EMISSIONS in type:
            return type
        else:
            raise Exception('Only two types of arbitrage algorithms are currently available: ' +
                            self.ECONOMIC + 'and ' +
                            self.EMISSIONS + '.')

    @property
    def optimized_strategy_time_horizon(self) -> int:
        """Returns the length of the time horizon in number of timesteps from configfile"""
        try:
            time_horizon = int(self.get_property(self.SECTION, self.OPTIMIZED_STRATEGY_TIME_HORIZON))
        except:
            raise Exception('Length of optimized strategy time horizon must be specified as int.')
        return time_horizon

    @property
    def rerun_optimization_timesteps(self) -> int:
        """Returns the number of timesteps after which the optimization is to be rerun from configfile"""
        try:
            rerun_timesteps = int(self.get_property(self.SECTION, self.RERUN_OPTIMIZATION_TIMESTEPS))
            if rerun_timesteps > self.optimized_strategy_time_horizon:
                raise Exception('Number of timesteps after which the optimization is to be rerun must be less than or'
                                'equal to the OPTIMIZED_STRATEGY_TIME_HORIZON.')
        except:
            raise Exception('Number of timesteps after which the optimization is to be rerun must be specified as int.')
        return rerun_timesteps
