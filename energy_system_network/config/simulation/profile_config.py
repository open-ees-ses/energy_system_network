from configparser import ConfigParser
from energy_system_network.constants_energy_sys_network import ROOT_PATH
from energy_system_network.commons.log import Logger
from energy_system_network.config.simulation.simulation_config import SimulationConfig


class ProfileConfig(SimulationConfig):
    """
    A class that provides various profile configurations derived from the parent SimulationConfig.

    Attributes:
        SECTION (str): Section name for PROFILE in the configuration.
        GRID_EMISSIONS (str): Configuration key for grid emissions.
        ECONOMIC (str): Configuration key for economic.
        LOAD (str): Configuration key for load.
        POWER_PROFILE_DIR (str): Configuration key for the power profile directory.
        EMISSIONS_PROFILE_DIR (str): Configuration key for the emissions profile directory.
        DISCRETE_EVENTS_PROFILE_DIR (str): Configuration key for the discrete events profile directory.
        ECONOMIC_PROFILE_DIR (str): Configuration key for the economic profile directory.
        LOAD_PROFILE (str): Configuration key for load profile.
        SWAP_EVENTS_PROFILE (str): Configuration key for swap events profile.
        GENERATION_PROFILE (str): Configuration key for generation profile.
        GRID_EMISSIONS_PROFILE (str): Configuration key for grid emissions profile.
        ECONOMIC_PROFILE (str): Configuration key for economic profile.
        GRID_EMISSIONS_SCALING_FACTOR (str): Configuration key for grid emissions scaling factor.
        EV_PARKED_PROFILE (str): Configuration key for EV parked profile.
        EV_DRIVE_POWER_PROFILE (str): Configuration key for EV drive power profile.
        PARKED (str): Tag for parked.
        DRIVE (str): Tag for drive.
        SWAP (str): Tag for swap.
        __log (Logger): Logger instance for the class.

    Methods:
        __init__(config: ConfigParser = None, path: str = None): Initializes the ProfileConfig instance.
        power_profile_dir() -> str: Retrieves the directory of power profiles.
        emissions_profile_dir() -> str: Retrieves the directory of emissions profiles.
        discrete_events_profile_dir() -> str: Retrieves the directory of discrete event profiles.
        economic_profile_dir() -> str: Retrieves the directory of economic profiles.
        load_profile() -> dict: Returns a dictionary of load profiles for specified components.
        generation_profile() -> dict: Returns a dictionary of generation profiles for specified components.
        swap_events_file() -> str: Retrieves the swap events profile filename.
        ev_parked_profile() -> str: Retrieves the EV parked profile name.
        ev_drive_power_profile() -> str: Retrieves the EV drive power profile name.
        grid_emissions_profile_file() -> str: Retrieves the grid emissions profile filename.
        economic_profile_file() -> str: Retrieves the economic profile filename.
        grid_emissions_scaling_factor() -> float: Retrieves the scaling factor for grid emissions.
    """
    SECTION: str = 'PROFILE'

    GRID_EMISSIONS: str = 'grid_emissions'
    ECONOMIC: str = 'economic'
    LOAD: str = 'load'

    POWER_PROFILE_DIR: str = 'POWER_PROFILE_DIR'
    EMISSIONS_PROFILE_DIR: str = 'EMISSIONS_PROFILE_DIR'
    DISCRETE_EVENTS_PROFILE_DIR: str = 'DISCRETE_EVENTS_PROFILE_DIR'
    ECONOMIC_PROFILE_DIR: str = 'ECONOMIC_PROFILE_DIR'
    LOAD_PROFILE: str = 'LOAD_PROFILE'
    SWAP_EVENTS_PROFILE: str = 'SWAP_EVENTS_PROFILE'
    GENERATION_PROFILE: str = 'GENERATION_PROFILE'
    GRID_EMISSIONS_PROFILE: str = 'GRID_EMISSIONS_PROFILE'
    ECONOMIC_PROFILE: str = 'ECONOMIC_PROFILE'
    GRID_EMISSIONS_SCALING_FACTOR: str = 'GRID_EMISSIONS_SCALING_FACTOR'
    EV_PARKED_PROFILE: str = 'EV_PARKED_PROFILE'
    EV_DRIVE_POWER_PROFILE: str = 'EV_DRIVE_POWER_PROFILE'
    PARKED: str = 'parked'
    DRIVE: str = 'drive'
    SWAP: str = 'swap'

    def __init__(self, config: ConfigParser = None, path: str = None):
        """
        Initializes the ProfileConfig instance.

        Args:
            config (ConfigParser, optional): A configuration parser object.
            path (str, optional): Path to the configuration file.
        """
        super().__init__(path, config)
        self.__log: Logger = Logger(type(self).__name__)

    @property
    def power_profile_dir(self) -> str:
        """Returns directory of power profiles from simulation config"""
        return ROOT_PATH + self.get_property(self.SECTION, self.POWER_PROFILE_DIR)

    @property
    def emissions_profile_dir(self) -> str:
        """Returns directory of emissions profiles from simulation config"""
        return ROOT_PATH + self.get_property(self.SECTION, self.EMISSIONS_PROFILE_DIR)

    @property
    def discrete_events_profile_dir(self) -> str:
        """Returns directory of discrete event profiles from simulation config"""
        return ROOT_PATH + self.get_property(self.SECTION, self.DISCRETE_EVENTS_PROFILE_DIR)

    @property
    def economic_profile_dir(self) -> str:
        """Returns directory of economic profiles from simulation config"""
        return ROOT_PATH + self.get_property(self.SECTION, self.ECONOMIC_PROFILE_DIR)

    @property
    def load_profile(self) -> dict:
        """ Return a dict of load profile names for specified load components from simulation config"""
        props: [str] = self.get_property(self.SECTION, self.LOAD_PROFILE).replace(' ', '').replace('\t',
                                                                                                         '').split(
            '\n')
        del props[0]
        res: [[str]] = list()
        for prop in props:
            res.append(prop.split(','))
        profiles = {}
        for profile in res:
            filename = profile[1]
            if self.LOAD in filename:
                profiles[profile[0]] = self.power_profile_dir + filename
            else:
                raise Exception('Please confirm that a suitable load profile has been chosen. The chosen file must contain '
                                + self.LOAD + ' in its filename.')
        return profiles

    @property
    def generation_profile(self) -> dict:
        """ Return a dict of generation profile names for specified generation components from simulation config"""
        props: [str] = self.get_property(self.SECTION, self.GENERATION_PROFILE).replace(' ', '').replace('\t',
                                                                                                            '').split(
            '\n')
        del props[0]
        res: [[str]] = list()
        for prop in props:
            res.append(prop.split(','))
        profiles = {}
        for profile in res:
            profiles[profile[0]] = self.power_profile_dir + profile[1]
        return profiles

    @property
    def swap_events_file(self) -> str:
        """Returns swap events profile file_name name from simulation config"""
        if self.SWAP in self.get_property(self.SECTION, self.SWAP_EVENTS_PROFILE):
            return self.discrete_events_profile_dir + self.get_property(self.SECTION, self.SWAP_EVENTS_PROFILE)
        else:
            raise Exception('Please confirm that a suitable ' + self.SWAP_EVENTS_PROFILE +
                            ' profile has been chosen. The chosen file must contain ' + self.SWAP + ' in its filename.')

    @property
    def ev_parked_profile(self) -> str:
        """Returns name of profile indicating when an EV is parked from simulation config"""
        if self.PARKED in self.get_property(self.SECTION, self.EV_PARKED_PROFILE):
            return self.discrete_events_profile_dir + self.get_property(self.SECTION, self.EV_PARKED_PROFILE)
        else:
            raise Exception('Please confirm that a suitable ' + self.EV_PARKED_PROFILE +
                            ' profile has been chosen. The chosen file must contain ' + self.PARKED + ' in its filename.')

    @property
    def ev_drive_power_profile(self) -> str:
        """Returns name of profile indicating the power demand of an EV during driving from simulation config"""
        if self.DRIVE in self.get_property(self.SECTION, self.EV_DRIVE_POWER_PROFILE):
            return self.power_profile_dir + self.get_property(self.SECTION, self.EV_DRIVE_POWER_PROFILE)
        else:
            raise Exception('Please confirm that a suitable ' + self.EV_DRIVE_POWER_PROFILE +
                            ' profile has been chosen. The chosen file must contain ' + self.DRIVE + ' in its filename.')

    @property
    def grid_emissions_profile_file(self) -> str:
        """ Return grid emissions profile file_name name from simulation config"""
        filename = self.get_property(self.SECTION, self.GRID_EMISSIONS_PROFILE)
        if self.GRID_EMISSIONS in filename:
            return self.emissions_profile_dir + filename
        else:
            raise Exception('Please confirm that a suitable ' + self.GRID_EMISSIONS_PROFILE +
                            ' profile has been chosen. The chosen file must contain ' + self.GRID_EMISSIONS + ' in its filename.')

    @property
    def economic_profile_file(self) -> str:
        """ Return economic profile file_name name from simulation config"""
        filename = self.get_property(self.SECTION, self.ECONOMIC_PROFILE)
        if self.ECONOMIC in filename:
            return self.economic_profile_dir + filename
        else:
            raise Exception('Please confirm that a suitable ' + self.ECONOMIC +
                            ' profile has been chosen. The chosen file must contain ' + self.ECONOMIC + ' in its filename.')

    @property
    def grid_emissions_scaling_factor(self) -> float:
        """ Return scaling factor for grid emissions from simulation config"""
        return float(self.get_property(self.SECTION, self.GRID_EMISSIONS_SCALING_FACTOR))
