import os
from configparser import ConfigParser
from energy_system_network.config.analysis.analysis_config import AnalysisConfig


class GeneralAnalysisConfig(AnalysisConfig):
    """
    Configuration handler for general analysis settings.

    Attributes:
        SECTION (str): Default section name in the configuration file.
        SIMULATION (str): Configuration key for specifying the simulation.
        ENERGY_SYSTEM_NETWORK_ANALYSIS (str): Configuration key for enabling/disabling energy system network analysis.
        ENERGY_SYSTEM_ANALYSIS (str): Configuration key for enabling/disabling energy system analysis.
        PLOTTING (str): Configuration key for enabling/disabling plotting.
        ENERGETIC_ANALYSIS (str): Configuration key for enabling/disabling energetic analysis.
        ENVIRO_ENERGETIC_ANALYSIS (str): Configuration key for enabling/disabling enviro-energetic analysis.
        ENERGY_MANAGEMENT_ANALYSIS (str): Configuration key for enabling/disabling energy management analysis.
        ECONOMIC_ANALYSIS (str): Configuration key for enabling/disabling economic analysis.
        ENVIRONMENTAL_ANALYSIS (str): Configuration key for enabling/disabling environmental analysis.
        EXPORT_ANALYSIS_TO_CSV (str): Configuration key for specifying if analysis results should be exported to CSV.
        PRINT_RESULTS_TO_CONSOLE (str): Configuration key for specifying if analysis results should be printed to console.
        MERGE_ANALYSIS (str): Configuration key for specifying if analysis results are merged.

    Methods:
        __init__(config: ConfigParser, path: str = None): Initializes the GeneralAnalysisConfig with a given configuration and optional path.
        get_result_for(path: str) -> str: Fetches the name of the simulation to analyze based on configuration settings.
        energy_system_network_analysis() -> bool: Determines if energy system network analysis should be performed.
        energy_system_analysis() -> bool: Determines if energy system analysis should be performed.
        plotting() -> bool: Determines if plotting should be done.
        energetic_analysis() -> bool: Determines if energetic analysis should be performed.
        enviro_energetic_analysis() -> bool: Determines if enviro-energetic analysis should be performed.
        energy_management_analysis() -> bool: Determines if energy management analysis should be performed.
        economic_analysis() -> bool: Determines if economic analysis should be performed.
        environmental_analysis() -> bool: Determines if environmental analysis should be performed.
        export_analysis_to_csv() -> bool: Checks if analysis results should be exported to CSV.
        print_result_to_console() -> bool: Checks if analysis results should be printed to the console.
        merge_analysis() -> bool: Checks if analysis results are merged.
    """
    SECTION: str = 'GENERAL'

    SIMULATION: str = 'SIMULATION'
    ENERGY_SYSTEM_NETWORK_ANALYSIS: str = 'ENERGY_SYSTEM_NETWORK_ANALYSIS'
    ENERGY_SYSTEM_ANALYSIS: str = 'ENERGY_SYSTEM_ANALYSIS'
    PLOTTING: str = 'PLOTTING'
    ENERGETIC_ANALYSIS: str = 'ENERGETIC_ANALYSIS'
    ENVIRO_ENERGETIC_ANALYSIS: str = 'ENVIRO_ENERGETIC_ANALYSIS'
    ENERGY_MANAGEMENT_ANALYSIS: str = 'ENERGY_MANAGEMENT_ANALYSIS'
    ECONOMIC_ANALYSIS: str = 'ECONOMIC_ANALYSIS'
    ENVIRONMENTAL_ANALYSIS: str = 'ENVIRONMENTAL_ANALYSIS'
    EXPORT_ANALYSIS_TO_CSV: str = 'EXPORT_ANALYSIS_TO_CSV'
    PRINT_RESULTS_TO_CONSOLE: str = 'PRINT_RESULTS_TO_CONSOLE'
    MERGE_ANALYSIS: str = 'MERGE_ANALYSIS'

    def __init__(self, config: ConfigParser, path: str = None):
        """
        Initializes the GeneralAnalysisConfig with a given configuration and optional path.

        Args:
            config (ConfigParser): Configuration parser containing the analysis settings.
            path (str, optional): Optional path to the directory containing the configuration file. Defaults to None.

        """
        super().__init__(path, config)

    def get_result_for(self, path: str) -> str:
        """
        Fetches the name of the simulation to analyze based on configuration settings.

        Args:
            path (str): Path to the directory containing the simulation results.

        Return:
            str: Name of the simulation to analyze.
        """
        __simulation = self.get_property(self.SECTION, self.SIMULATION)
        if __simulation == 'LATEST':
            result_dirs = list()
            tmp_dirs = os.listdir(path)
            for dir in tmp_dirs:
                if os.path.isdir(path + dir):
                    result_dirs.append(path + dir + '/')
            return max(result_dirs, key=os.path.getmtime)
        else:
            return path + __simulation + '/'

    @property
    def energy_system_network_analysis(self) -> bool:
        """Returns boolean value for energy_system_network_analysis after the simulation"""
        return self.get_property(self.SECTION, self.ENERGY_SYSTEM_NETWORK_ANALYSIS) in ['True']

    @property
    def energy_system_analysis(self) -> bool:
        """Returns boolean value for energy_system_analysis after the simulation"""
        return self.get_property(self.SECTION, self.ENERGY_SYSTEM_ANALYSIS) in ['True']

    @property
    def plotting(self) -> bool:
        """Returns boolean value for matlab_plotting after the simulation"""
        return self.get_property(self.SECTION, self.PLOTTING) in ['True']

    @property
    def energetic_analysis(self) -> bool:
        """Returns boolean value for energetic analysis directly after the simulation"""
        return self.get_property(self.SECTION, self.ENERGETIC_ANALYSIS) in ['True']

    @property
    def enviro_energetic_analysis(self) -> bool:
        """Returns boolean value for enviro-energetic analysis directly after the simulation"""
        return self.get_property(self.SECTION, self.ENVIRO_ENERGETIC_ANALYSIS) in ['True']

    @property
    def energy_management_analysis(self) -> bool:
        """Returns boolean value for energy management analysis directly after the simulation"""
        return self.get_property(self.SECTION, self.ENERGY_MANAGEMENT_ANALYSIS) in ['True']

    @property
    def economic_analysis(self) -> bool:
        """Returns boolean value for economic analysis directly after the simulation"""
        return self.get_property(self.SECTION, self.ECONOMIC_ANALYSIS) in ['True']

    @property
    def environmental_analysis(self) -> bool:
        """Returns boolean value for environmental analysis directly after the simulation"""
        return self.get_property(self.SECTION, self.ENVIRONMENTAL_ANALYSIS) in ['True']

    @property
    def export_analysis_to_csv(self) -> bool:
        """Defines if analysis results are to be exported to csv files"""
        return self.get_property(self.SECTION, self.EXPORT_ANALYSIS_TO_CSV) in ['True']

    @property
    def print_result_to_console(self) -> bool:
        """Defines if analysis results are to be printed to console"""
        return self.get_property(self.SECTION, self.PRINT_RESULTS_TO_CONSOLE) in ['True']

    @property
    def merge_analysis(self) -> bool:
        """Defines if analysis results are merged"""
        return self.get_property(self.SECTION, self.MERGE_ANALYSIS) in ['True']
