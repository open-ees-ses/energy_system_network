from configparser import ConfigParser

from energy_system_network.config.config import Config


class AnalysisConfig(Config):
    """
    Configuration handler for analysis settings.

    Attributes:
        config_file_name (str): The name of the configuration file specific to analysis settings.
        __config_parser (ConfigParser): Internal configuration parser for the analysis settings.
        __used_config (ConfigParser): The configuration parser currently in use.

    Methods:
        __init__(path: str, config: ConfigParser): Initializes the AnalysisConfig with a given path and configuration.
        get_property(section: str, option: str, config: ConfigParser = None) -> str: Fetches a property value from the configuration.
        set_config(config: ConfigParser) -> None: Sets the internal configuration parser for the class.
    """
    config_file_name: str = 'analysis'
    __config_parser: ConfigParser = None
    __used_config: ConfigParser = ConfigParser()

    def __init__(self, path: str, config: ConfigParser):
        """
        Initializes the AnalysisConfig with the provided path and configuration.

        Args:
            path (str): The path to the configuration file.
            config (ConfigParser): The configuration parser containing analysis settings.
        """
        super().__init__(path, self.config_file_name, config)

    def get_property(self, section: str, option: str, config: ConfigParser = None):
        """
        Fetches a property value from the provided section and option in the configuration.

        Args:
            section (str): The section in the configuration.
            option (str): The option within the section to fetch.
            config (ConfigParser, optional): A specific configuration parser to use. Defaults to the used configuration.

        Return:
            str: The value of the specified property in the configuration.
        """
        if config is None:
            config = self.__used_config
        return super().get_property(section, option)

    @classmethod
    def set_config(cls, config: ConfigParser) -> None:
        """
        Sets the internal configuration parser for the class.

        Args:
            config (ConfigParser): The configuration parser to set.
        """
        cls.__config_parser = config
