from configparser import ConfigParser
from energy_system_network.config.analysis.analysis_config import AnalysisConfig
import ast


class EnvironmentalAnalysisConfig(AnalysisConfig):
    """
    Configuration class for handling environmental analysis settings.

    Attributes:
        SECTION (str): Specifies the configuration section for environmental analysis.
        GRID_REINFORCEMENT_ONLY (str): Configuration key for grid reinforcement only setting.
        CELL_PRODUCTION_EMISSIONS (str): Configuration key for cell production emissions setting.
        CELL_EOL_EMISSIONS (str): Configuration key for cell end-of-life emissions setting.
        REMANUFACTURING_EMISSIONS (str): Configuration key for battery remanufacturing emissions setting.
        ALLOCATION_FACTOR (str): Configuration key for second life emissions allocation factor setting.

    Methods:
        __init__(config: ConfigParser, path: str): Initializes the EnvironmentalAnalysisConfig with a given ConfigParser or a path.
        grid_reinforcement_only() -> bool: Fetches user preference for grid inclusion setting.
        cell_production_emissions() -> float: Fetches cell production emissions setting.
        cell_eol_emissions() -> float: Fetches cell end-of-life emissions setting.
        battery_remanufacturing_emissions() -> float: Fetches battery remanufacturing emissions setting.
        second_life_emissions_allocation_factor() -> float: Fetches the second life emissions allocation factor setting.
        power_electronics_production_emissions() -> float: Fetches power electronics production emissions setting.
        power_electronics_eol_emissions() -> float: Fetches power electronics end-of-life emissions setting.
        hvac_production_emissions() -> float: Fetches HVAC production emissions setting.
        hvac_eol_emissions() -> float: Fetches HVAC end-of-life emissions setting.
        misc_electronics_production_emissions() -> float: Fetches miscellaneous electronics production emissions setting.
        misc_electronics_eol_emissions() -> float: Fetches miscellaneous electronics end-of-life emissions setting.
        housing_production_emissions() -> float: Fetches housing production emissions setting.
        housing_eol_emissions() -> float: Fetches housing end-of-life emissions setting.
        pv_production_emissions() -> float: Fetches photovoltaic production emissions setting.
        pv_eol_emissions() -> float: Fetches photovoltaic end-of-life emissions setting.
        wind_turbine_production_emissions() -> float: Fetches wind turbine production emissions setting.
        wind_turbine_eol_emissions() -> float: Fetches wind turbine end-of-life emissions setting.
        diesel_generator_production_emissions() -> float: Fetches diesel generator production emissions setting.
        diesel_generator_eol_emissions() -> float: Fetches diesel generator end-of-life emissions setting.
        diesel_combustion_emissions() -> float: Fetches diesel combustion emissions setting.
        diesel_upstream_emissions() -> float: Fetches diesel upstream emissions setting.
        grid_operations_phase_emissions() -> float: Fetches grid operations phase emissions setting.
        transformer_production_emissions() -> float: Fetches transformer production emissions setting.
        transformer_eol_emissions() -> float: Fetches transformer end-of-life emissions setting.
        lv_grid_cable_production_emissions() -> float: Fetches low voltage grid cable production emissions setting.
        mv_grid_cable_production_emissions() -> float: Fetches medium voltage grid cable production emissions setting.
        lv_grid_cable_eol_emissions() -> float: Fetches low voltage grid cable end-of-life emissions setting.
        mv_grid_cable_eol_emissions() -> float: Fetches medium voltage grid cable end-of-life emissions setting.
    """
    SECTION: str = 'ENVIRONMENTAL_ANALYSIS'

    GRID_REINFORCEMENT_ONLY: str = 'GRID_REINFORCEMENT_ONLY'

    CELL_PRODUCTION_EMISSIONS: str = 'CELL_PRODUCTION_EMISSIONS'
    CELL_EOL_EMISSIONS: str = 'CELL_EOL_EMISSIONS'
    REMANUFACTURING_EMISSIONS: str = 'REMANUFACTURING_EMISSIONS'
    ALLOCATION_FACTOR: str = 'ALLOCATION_FACTOR'

    PE_PRODUCTION_EMISSIONS: str = 'PE_PRODUCTION_EMISSIONS'
    PE_EOL_EMISSIONS: str = 'PE_EOL_EMISSIONS'

    HVAC_PRODUCTION_EMISSIONS: str = 'HVAC_PRODUCTION_EMISSIONS'
    HVAC_EOL_EMISSIONS: str = 'HVAC_EOL_EMISSIONS'

    MISC_ELECTRONICS_PRODUCTION_EMISSIONS: str = 'MISC_ELECTRONICS_PRODUCTION_EMISSIONS'
    MISC_ELECTRONICS_EOL_EMISSIONS: str = 'MISC_ELECTRONICS_EOL_EMISSIONS'

    HOUSING_PRODUCTION_EMISSIONS: str = 'HOUSING_PRODUCTION_EMISSIONS'
    HOUSING_EOL_EMISSIONS: str = 'HOUSING_EOL_EMISSIONS'

    PV_PRODUCTION_EMISSIONS: str = 'PV_PRODUCTION_EMISSIONS'
    PV_EOL_EMISSIONS: str = 'PV_EOL_EMISSIONS'
    WIND_TURBINE_PRODUCTION_EMISSIONS: str = 'WIND_TURBINE_PRODUCTION_EMISSIONS'
    WIND_TURBINE_EOL_EMISSIONS: str = 'WIND_TURBINE_EOL_EMISSIONS'
    DG_PRODUCTION_EMISSIONS: str = 'DG_PRODUCTION_EMISSIONS'
    DG_EOL_EMISSIONS: str = 'DG_EOL_EMISSIONS'
    DIESEL_COMBUSTION_EMISSIONS: str = 'DIESEL_COMBUSTION_EMISSIONS'
    DIESEL_UPSTREAM_EMISSIONS: str = 'DIESEL_UPSTREAM_EMISSIONS'

    OPERATION_PHASE_EMISSIONS: str = 'OPERATION_PHASE_EMISSIONS'
    TRANSFORMER_PRODUCTION_EMISSIONS: str = 'TRANSFORMER_PRODUCTION_EMISSIONS'
    TRANSFORMER_EOL_EMISSIONS: str = 'TRANSFORMER_EOL_EMISSIONS'
    LV_GRID_CABLE_PRODUCTION_EMISSIONS: str = 'LV_GRID_CABLE_PRODUCTION_EMISSIONS'
    MV_GRID_CABLE_PRODUCTION_EMISSIONS: str = 'MV_GRID_CABLE_PRODUCTION_EMISSIONS'
    LV_GRID_CABLE_EOL_EMISSIONS: str = 'LV_GRID_CABLE_EOL_EMISSIONS'
    MV_GRID_CABLE_EOL_EMISSIONS: str = 'MV_GRID_CABLE_EOL_EMISSIONS'

    def __init__(self, config: ConfigParser = None, path: str = None):
        """
        Initializes the EnvironmentalAnalysisConfig with an optional configuration and path.

        Args:
            config (ConfigParser, optional): Configuration parser containing the environmental analysis settings.
            path (str, optional): Optional path to the directory containing the configuration file. Defaults to None.
        """
        super().__init__(path, config)
        # self.__log: Logger = Logger(type(self).__name__)

    @property
    def grid_reinforcement_only(self) -> bool:
        """returns user preference for inclusion of entire grid section or only reinforcement in analysis"""
        return ast.literal_eval(self.get_property(self.SECTION, self.GRID_REINFORCEMENT_ONLY))

    @property
    def cell_production_emissions(self) -> float:
        """cell production emissions"""
        return float(self.get_property(self.SECTION, self.CELL_PRODUCTION_EMISSIONS))

    @property
    def cell_eol_emissions(self) -> float:
        """cell end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.CELL_EOL_EMISSIONS))

    @property
    def battery_remanufacturing_emissions(self) -> float:
        """battery remanufacturing emissions"""
        return float(self.get_property(self.SECTION, self.REMANUFACTURING_EMISSIONS))

    @property
    def second_life_emissions_allocation_factor(self) -> float:
        """second life emissions allocation factor"""
        return float(self.get_property(self.SECTION, self.ALLOCATION_FACTOR))

    @property
    def power_electronics_production_emissions(self) -> str:
        """power electronics production emissions - returns name of model"""
        return self.get_property(self.SECTION, self.PE_PRODUCTION_EMISSIONS)

    @property
    def power_electronics_eol_emissions(self) -> float:
        """power electronics end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.PE_EOL_EMISSIONS))

    @property
    def hvac_production_emissions(self) -> float:
        """hvac production emissions"""
        return float(self.get_property(self.SECTION, self.HVAC_PRODUCTION_EMISSIONS))

    @property
    def hvac_eol_emissions(self) -> float:
        """hvac end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.HVAC_EOL_EMISSIONS))

    @property
    def misc_electronics_production_emissions(self) -> float:
        """misc electronics production emissions"""
        return float(self.get_property(self.SECTION, self.MISC_ELECTRONICS_PRODUCTION_EMISSIONS))

    @property
    def misc_electronics_eol_emissions(self) -> float:
        """misc electronics end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.MISC_ELECTRONICS_EOL_EMISSIONS))

    @property
    def housing_production_emissions(self) -> float:
        """housing production emissions"""
        return float(self.get_property(self.SECTION, self.HOUSING_PRODUCTION_EMISSIONS))

    @property
    def housing_eol_emissions(self) -> float:
        """housing end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.HOUSING_EOL_EMISSIONS))

    @property
    def pv_production_emissions(self) -> float:
        """PV solar panels production emissions"""
        return float(self.get_property(self.SECTION, self.PV_PRODUCTION_EMISSIONS))

    @property
    def pv_eol_emissions(self) -> float:
        """PV solar panels end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.PV_EOL_EMISSIONS))

    @property
    def wind_turbine_production_emissions(self) -> float:
        """wind turbine production emissions"""
        return float(self.get_property(self.SECTION, self.WIND_TURBINE_PRODUCTION_EMISSIONS))

    @property
    def wind_turbine_eol_emissions(self) -> float:
        """wind turbine end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.WIND_TURBINE_EOL_EMISSIONS))

    @property
    def diesel_generator_production_emissions(self) -> float:
        """production emissions"""
        return float(self.get_property(self.SECTION, self.DG_PRODUCTION_EMISSIONS))

    @property
    def diesel_generator_eol_emissions(self) -> float:
        """end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.DG_EOL_EMISSIONS))

    @property
    def diesel_combustion_emissions(self) -> float:
        """Diesel combustion emissions"""
        return float(self.get_property(self.SECTION, self.DIESEL_COMBUSTION_EMISSIONS))

    @property
    def diesel_upstream_emissions(self) -> float:
        """Upstream diesel fuel emissions"""
        return float(self.get_property(self.SECTION, self.DIESEL_UPSTREAM_EMISSIONS))

    @property
    def grid_operations_phase_emissions(self) -> str:
        """Type of model for grid operations phase emissions"""
        return self.get_property(self.SECTION, self.OPERATION_PHASE_EMISSIONS)

    @property
    def transformer_production_emissions(self) -> float:
        """Transformer production emissions"""
        return float(self.get_property(self.SECTION, self.TRANSFORMER_PRODUCTION_EMISSIONS))

    @property
    def transformer_eol_emissions(self) -> float:
        """Transformer end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.TRANSFORMER_EOL_EMISSIONS))

    @property
    def lv_grid_cable_production_emissions(self) -> float:
        """LV Grid cable production emissions"""
        return float(self.get_property(self.SECTION, self.LV_GRID_CABLE_PRODUCTION_EMISSIONS))

    @property
    def mv_grid_cable_production_emissions(self) -> float:
        """MV Grid cable production emissions"""
        return float(self.get_property(self.SECTION, self.MV_GRID_CABLE_PRODUCTION_EMISSIONS))

    @property
    def lv_grid_cable_eol_emissions(self) -> float:
        """Grid cable end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.LV_GRID_CABLE_EOL_EMISSIONS))

    @property
    def mv_grid_cable_eol_emissions(self) -> float:
        """Grid cable end-of-life emissions"""
        return float(self.get_property(self.SECTION, self.MV_GRID_CABLE_EOL_EMISSIONS))
