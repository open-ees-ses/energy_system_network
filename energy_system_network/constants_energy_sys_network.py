import os
from energy_system_network import config

ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__))).replace('\\','/') + '/'
CONFIG_PKG_NAME =  config.__name__.replace('.','/') + '/'
CONFIG_PATH = ROOT_PATH
BATCH_DIR = 'batch/'
SIMSES_REFERENCE_CONFIG_PATH = ROOT_PATH + CONFIG_PKG_NAME + 'energy_system_components/' + 'simses_reference_configs/'
