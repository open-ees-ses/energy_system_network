import time
from configparser import ConfigParser
from multiprocessing import Queue
from queue import Full
import os
import sys
from energy_system_network.commons.console_printer import ConsolePrinter
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.commons.log import Logger
from energy_system_network.commons.utils import format_float
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system_network import EnergySystemNetwork


class Simulation:
    """
    This class represents a simulation of an energy system network.
    The simulation is configured with a path, a configuration file and a printer queue.

    Args:
        path (str): The path where simulation data will be stored.
        config (ConfigParser): The configuration settings for the simulation.
        printer_queue (Queue): The printer queue for outputting messages.

    Attributes:
        __path (str): The path where simulation data will be stored.
        __log (Logger): The logger instance used for logging events.
        __received_config (ConfigParser): The configuration settings for the simulation.
        __config (GeneralSimulationConfig): The parsed configuration settings for the simulation.
        __data_export (DataHandler): The handler for data export.
        __energy_system_network (EnergySystemNetwork): The energy system network to be simulated.
        __name (str): The name of the simulation, derived from the path.
        __printer_queue (Queue): The printer queue for outputting messages.
        __max_loop (int): The maximum number of loops in the simulation.
        __start (int): The start time of the simulation.
        __end (int): The end time of the simulation.
        __duration (int): The duration of the simulation.
        __output_multiplier (int): The multiplier for the output data, based on the simulation duration.
        __timestep (int): The time step for each iteration in the simulation.
        __timesteps_per_hour (int): The number of time steps in an hour.

    Methods:
        __init__(path: str, config: ConfigParser, printer_queue: Queue): Initializes the Simulation instance with given
            path, configuration, and printer queue.
        get_output_multiplier(duration: float): Calculates and returns the output multiplier based on the provided duration.
        run(): Runs the simulation.
        __send_register_signal(): Sends a register signal to the printer queue.
        __send_stop_signal(): Sends a stop signal to the printer queue.
        __print_progress(tstmp: float): Prints the progress of the simulation.
        __put_to_queue(output: dict, blocking: bool = False): Puts the given output to the printer queue.
        __print_end(ts_performance: list, sim_start: float): Prints the end status of the simulation.
        get_energy_system_network(): Returns the energy system network of the simulation.
        close(): Closes the simulation, sends stop signal and writes configuration to path.
    """

    def __init__(self, path: str, config: ConfigParser, printer_queue: Queue):
        """
            Initializes the Simulation instance with given path, configuration, and printer queue.

            Args:
                path (str): The path where simulation data will be stored.
                config (ConfigParser): The configuration settings for the simulation.
                printer_queue (Queue): The printer queue for outputting messages.
        """
        self.__path = path
        self.__log = Logger(__name__)
        self.__received_config = config
        self.__config = GeneralSimulationConfig(config)
        self.__data_export = DataHandler(self.__path, self.__config)
        self.__energy_system_network: EnergySystemNetwork = EnergySystemNetwork(self.__data_export, config)
        self.__name: str = os.path.basename(os.path.dirname(self.__path))
        self.__printer_queue: Queue = printer_queue
        self.__send_register_signal()
        self.__max_loop = self.__config.loop
        self.__start = self.__config.start
        self.__end = self.__config.end
        self.__duration = self.__end - self.__start
        self.__output_multiplier = self.get_output_multiplier(self.__duration * self.__max_loop)
        self.__timestep = self.__config.timestep  # s
        self.__timesteps_per_hour = 3600 / self.__timestep

    @staticmethod
    def get_output_multiplier(duration: float):
        """
        Calculates the multiplier for the output data based on the simulation duration.

        Args:
            duration (float): The duration of the simulation.

        Returns:
            int: The calculated multiplier for the output data.
        """
        output_multiplier = 1
        if duration > 60 * 60 * 10000:
            output_multiplier = 1000
        elif duration > 60 * 60 * 1000:
            output_multiplier = 100
        elif duration > 60 * 60 * 100:
            output_multiplier = 10
        return output_multiplier

    def run(self):
        """
        Executes the simulation.

        Raises:
            TypeError: If an error occurs during the execution of the simulation.
        """
        self.__log.info('start')
        sim_start = time.time()
        ts_performance = []
        self.__data_export.start()
        try:
            loop = 0
            ts = self.__start
            while loop < self.__config.loop:
                self.__log.info('Loop: ' + str(loop))
                ts_adapted = loop * self.__duration
                if loop > 0:
                    ts_adapted += 1 * loop
                while ts <= (ts_adapted + self.__end) - self.__timestep:
                    ts_before = time.time()
                    ts += self.__timestep
                    self.__energy_system_network.update(ts, ts_adapted)
                    # Update each energy system as per its own energy management strategy
                    ts_performance.append(time.time() - ts_before)  # calculates simulation time
                    self.__print_progress(ts)
                loop += 1
                if loop < self.__max_loop:
                    ts_adapted = loop * self.__duration
                    self.__energy_system_network.reset_profiles(ts_adapted)
        except TypeError as err:
            print('Timestamp at error: ' + str(ts))
            raise err
        finally:
            self.__print_end(ts_performance, sim_start)
            self.close()

    def __send_register_signal(self) -> None:
        """
        Sends a register signal to the printer queue.
        """
        self.__put_to_queue({self.__name: ConsolePrinter.REGISTER_SIGNAL}, blocking=True)

    def __send_stop_signal(self) -> None:
        """
        Sends a stop signal to the printer queue.
        """
        self.__put_to_queue({self.__name: ConsolePrinter.STOP_SIGNAL}, blocking=True)

    def __print_progress(self, tstmp: float) -> None:
        """
        Prints the progress of the simulation.

        Args:
            tstmp (float): The timestamp of the current iteration in the simulation.
        """
        progress = (tstmp - self.__start) / (self.__duration * self.__max_loop) * 100
        line: str = '|%-20s| ' % ('#' * round(progress / 5)) + format_float(progress, 1) + '%'
        output: dict = {self.__name: line}
        if self.__printer_queue is None:
            sys.stdout.write('\r' + str(output))
            sys.stdout.flush()
        self.__put_to_queue(output)

    def __put_to_queue(self, output: dict, blocking: bool = False) -> None:
        """
        Puts the given output to the printer queue.

        Args:
            output (dict): The output to put to the printer queue.
            blocking (bool, optional): If True, the function will block if necessary until a free slot is available.
        """
        if self.__printer_queue is not None:
            try:
                if blocking:
                    self.__printer_queue.put(output)
                else:
                    self.__printer_queue.put_nowait(output)
            except Full:
                return

    def __print_end(self, ts_performance: list, sim_start: float) -> None:
        """
        Prints the end status of the simulation, including duration and duration per step.

        Args:
            ts_performance (list): The list of timestamp performances.
            sim_start (float): The start time of the simulation.
        """
        sim_end = time.time()
        duration: str = format_float(sim_end - sim_start)
        duration_per_step: str = format_float(sum(ts_performance) * 1000 / len(ts_performance))
        self.__log.info('100.0% done. Duration in sec: ' + duration)
        self.__log.info('Duration per step in ms:      ' + duration_per_step)
        print('\r[' + self.__name + ': |%-20s| ' % ('#' * 20) + '100.0%]')
        print('Duration in s: ' + duration)
        print('Duration per step in ms: ' + duration_per_step)

    def get_energy_system_network(self) -> EnergySystemNetwork:
        """
        Returns the energy system network of the simulation.

        Returns:
            EnergySystemNetwork: The energy system network of the simulation.
        """
        return self.__energy_system_network

    def close(self) -> None:
        """
        Closes the simulation, sends stop signal and writes configuration to path.
        """
        self.__log.info('closing')
        self.__send_stop_signal()
        self.__config.write_config_to(self.__path)
        self.__log.close()
        self.__energy_system_network.close()
        self.__data_export.close()
