from configparser import ConfigParser
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.energy_system_factory import EnergySystemFactory


class EnergySystemNetwork:
    """
    Represents a network of EnergySystem objects.

    Attributes:
        __data_export (DataHandler): Instance handling data export.
        __energy_systems ([EnergySystem]): List of EnergySystem instances created from factory.
        number_of_systems (int): Total number of EnergySystem instances in the network.

    Methods:
        __init__(data_export: DataHandler, config: ConfigParser): Initializes the EnergySystemNetwork with data handler
            and configurations.
        update(time: float, adapted_time: float): Triggers a run and updates the state of each EnergySystem.
        reset_profiles(adapted_time: float): Resets profile-based modules for each EnergySystem.
        close(): Closes all resources within the network.
        get_energy_systems() -> [EnergySystem]: Retrieves all EnergySystem objects in the network.
    """
    def __init__(self, data_export: DataHandler, config: ConfigParser):
        """
        Initializes the EnergySystemNetwork with data handler and configurations.

        Args:
            data_export (DataHandler): Instance for handling data export.
            config (ConfigParser): Configuration data for the energy system network.

        Return:
            None.
        """
        self.__data_export = data_export
        factory: EnergySystemFactory = EnergySystemFactory(config)
        self.__energy_systems: [EnergySystem] = factory.create_energy_system(data_export)
        self.number_of_systems = len(self.__energy_systems)

    def update(self, time: float, adapted_time: float) -> None:
        """
        Triggers a run and updates the state of each EnergySystem
        :param time: Epoch timestamp as float
        :param adapted_time: Time offset in s as float
        :return:
        """
        for energy_system in self.__energy_systems:  # type: EnergySystem
            energy_system.update(time, adapted_time)

    def reset_profiles(self, adapted_time: float) -> None:
        """
        Resets profile-based modules at the end of each loop in classes using SimSES and SimSESExternalStrategy EMS
        :param adapted_time: Time offset in s as float
        :return: None
        """
        for energy_system in self.__energy_systems:  # type: EnergySystem
            energy_system.reset_profiles(adapted_time)

    def close(self) -> None:
        """Closes all open resources in energy system network"""
        for energy_system in self.__energy_systems:  # type: EnergySystem
            energy_system.close()

    def get_energy_systems(self) -> [EnergySystem]:
        """
        Returns all EnergySystem objects associated with the EnergySystemNetwork object
        :return: List of EnergySystem(s)
        """
        return self.__energy_systems
