from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.profiles.profile_handler import ProfileHandler
from energy_system_network.commons.state.energy_system_component.load_component.electricity_load_component_state import \
    ElectricityLoadComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.profile_config import ProfileConfig
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent


class EVHomeCharger(LoadComponent):
    """
    A class that models an Electric Vehicle Home Charger as a load component.

    Attributes:
        energy_form (str): The form of energy the charger uses (ELECTRICITY).
        peak_power (float): Peak power for the charger in W.
        must_fulfill (bool): Indicates if the load must fulfill its demand.
        name (str): Name of the EV Home Charger based on energy system and load IDs.
        component_ID (str): Unique ID based on energy system and load ID.
        load_profile (ProfileHandler): Profile handler for the charger load.
        state (ElectricityLoadComponentState): Current state of the EV Home Charger.
        __load_power (float): Power load at a given time.
        __time (float): Current time.

    Methods:
        __init__(peak_power, data_export, energy_system_id, load_id, config_general): Initializes the EVHomeCharger.
        next(time, energy_form, adapted_time): Get the load for the given time.
        update(): Update values stored in the state.
        operation_losses(): Calculate operation losses.
        allocate_emissions(load_grid_energy_consumption_emissions, load_discharge_energy_consumption_emissions): Update emission values.
    """
    def __init__(self, peak_power: float, data_export, energy_system_id, load_id, config_general: GeneralSimulationConfig):
        """
        Initializes the EVHomeCharger.

        Args:
            peak_power (float): Peak power of the charger in W.
            data_export: Data export handler.
            energy_system_id: ID of the energy system.
            load_id: ID of the load.
            config_general (GeneralSimulationConfig): General simulation configuration.
        """
        super().__init__(data_export)

        self.energy_form:str = EnergyForm.ELECTRICITY
        self.peak_power = peak_power  # in W
        self.must_fulfill = True

        # Component identification and general
        self.name = str(energy_system_id) + str(load_id) + self.__class__.__name__
        self.component_ID = str(energy_system_id) + '.' + str(load_id)

        profile_config: ProfileConfig = ProfileConfig()
        scaling_factor = self.peak_power
        profile_filename = profile_config.load_profile[self.__class__.__name__]
        self.load_profile = ProfileHandler(profile_filename, config_general, scaling_factor)

        # Update peak power after initialization of profile
        if abs(self.peak_power - 1) < 1e-4:  # Peak Power is 1 when scaling is not required
            self.peak_power = self.load_profile.max_profile_value  # in W

        # Set up logging of states
        self.state = ElectricityLoadComponentState(energy_system_id, load_id)
        self.data_export.add_export_class(self.state)  # Ensures that only those states are logged which are currently used
        self.data_export.transfer_data(self.state.to_export())  # initial timestep

        # Initialize variables
        self.__load_power = 0
        self.__time = 0

    def next(self, time: float, energy_form: str = None, adapted_time: float = 0) -> float:
        """
        Obtain the load for the respective load type at the current time step.

        Args:
            time (float): Current time.
            energy_form (str, optional): Form of energy (default to None).
            adapted_time (float, optional): Adjusted time (default to 0).

        Return:
            float: Load value at the current time step.
        """
        self.__time = time
        if energy_form == EnergyForm.ELECTRICITY:
            # adjusted_time = time - adapted_time
            self.__load_power = self.load_profile.next(time, adapted_time)
            return self.__load_power
        else:
            return 0.0

    def update(self) -> None:
        """
        Update values stored in the state for the load component.
        """
        self.state.time = self.__time
        self.state.power = self.__load_power
        self.state.power_loss = self.operation_losses()
        if abs(self.__load_power) > 0:
            self.state.run_status = 1
        else:
            self.state.run_status = 0
        self.data_export.transfer_data(self.state.to_export())

    def operation_losses(self) -> float:
        """
        Calculate the operation losses in W (disabled for LoadOne).

        Return:
            float: Operation losses (set to 0 for this component).
        """
        return 0.0

    def allocate_emissions(self, load_grid_energy_consumption_emissions: float,
                           load_discharge_energy_consumption_emissions: float) -> None:
        """
        Update the grid_energy_consumption_emissions and discharge_energy_consumption_emissions.

        Args:
            load_grid_energy_consumption_emissions (float): Emissions from grid energy consumption in kg.
            load_discharge_energy_consumption_emissions (float): Emissions from discharge energy consumption in kg.
        """
        self.state.grid_energy_consumption_emissions = load_grid_energy_consumption_emissions  # in kg
        self.state.discharge_energy_consumption_emissions = load_discharge_energy_consumption_emissions  # in kg
