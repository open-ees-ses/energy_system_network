from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.profiles.profile_handler import ProfileHandler
from energy_system_network.commons.state.energy_system_component.load_component.electricity_load_component_state import \
    ElectricityLoadComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.profile_config import ProfileConfig
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent
from energy_system_network.simulation.energy_system.supporting_components import ac_dc_converter_model
from energy_system_network.simulation.energy_system.supporting_components.stacked_dc_dc_converter import \
    DcDcConverterStacked
from energy_system_network.simulation.energy_system.supporting_components.commercial_dc_dc_converter import \
    CommercialDcDcConverter


class EVChargingPointDC(LoadComponent):
    """
    Represents an electrical vehicle charging point DC component within an energy system network.

    Attributes:
        energy_form (str): Specifies the type of energy form the load uses.
        peak_power (float): The peak power value for the load in watts.
        must_fulfill (bool): Flag indicating if the load must be fulfilled.
        name (str): A unique name for the load component.
        component_ID (str): A unique identifier for the load component.
        state (ElectricityLoadComponentState): The current state of the load component.
        load_profile (ProfileHandler): The load profile associated with the component.
        __ac_dc_converter (AcDcConverter, private): The AC/DC converter for the charging point.
        __dc_dc_converter (DcDcConverterStacked, private): The DC/DC converter stack.
        __ac_power (float, private): AC power at converter terminals in watts.
        __dc_power (float, private): DC power at DC/DC converter terminals in watts.
        __dc_power_ev (float, private): DC power at converter terminals in watts for EV charging.
        __load_power (float, private): The current load power.
        __time (float, private): The current time.

    Methods:
        __init__(peak_power, data_export, energy_system_id, load_id, config_general): Initializes the object.
        next(time, energy_form=None, adapted_time=0): Returns the AC power demanded by the EV charging point at each
            timestep.
        get_power_electronics_power(dc_power_ev): Returns the input AC power for AC/DC Converter and input DC power for
            DC/DC converter.
        update(): Updates the state for the load component.
        operation_losses(): Calculates the operation losses in watts.
        allocate_emissions(load_grid_energy_consumption_emissions, load_discharge_energy_consumption_emissions): Updates
            the emissions related to energy consumption and discharge.
    """
    def __init__(self, peak_power: float, data_export: DataHandler, energy_system_id: int, load_id: int,
                 config_general: GeneralSimulationConfig):
        """
        Initializes an EVChargingPointDC object.

        Args:
            peak_power (float): Peak DC power in W.
            data_export (DataHandler): Data export handler.
            energy_system_id (int): Energy system ID.
            load_id (int): Load ID.
            config_general (GeneralSimulationConfig): General simulation config.

        Return:
            None.
        """
        super().__init__(data_export)

        self.energy_form:str = EnergyForm.ELECTRICITY
        self.peak_power = peak_power  # in W
        self.must_fulfill = True

        # Component identification and general
        self.name = str(energy_system_id) + str(load_id) + self.__class__.__name__
        self.component_ID = str(energy_system_id) + '.' + str(load_id)

        from simses.system.power_electronics.acdc_converter.abstract_acdc_converter import AcDcConverter

        # Set up logging of states
        self.state = ElectricityLoadComponentState(energy_system_id, load_id)
        self.data_export.add_export_class(self.state)  # Ensures that only those states are logged which are currently used
        self.data_export.transfer_data(self.state.to_export())  # initial timestep

        profile_config: ProfileConfig = ProfileConfig()
        scaling_factor = self.peak_power
        profile_filename = profile_config.load_profile[self.__class__.__name__]
        self.load_profile = ProfileHandler(profile_filename, config_general, scaling_factor)

        # Update peak power after initialization of profile
        if abs(self.peak_power - 1) < 1e-4:  # Peak Power is 1 when scaling is not required
            self.peak_power = self.load_profile.max_profile_value  # in W

        number_charging_points = 10

        # Set-up AC/DC converter for Charging Point
        ac_dc_converter_type = 'NottonAcDcConverter'
        self.__ac_dc_converter: AcDcConverter = ac_dc_converter_model.create_ac_dc_converter(ac_dc_converter_type,
                                                                                             peak_power * 1.1, number_charging_points, switch_value=1.0)
        # switch value = 1 because this is an integer problem - the next charger is switched ON only when
        # another EV pulls in

        dc_dc_converter = CommercialDcDcConverter(peak_power)
        self.__dc_dc_converter = DcDcConverterStacked(number_charging_points, 1.0, dc_dc_converter)

        # Obtain real peak power
        max_ac_power_in, max_dc_power_in = self.get_power_electronics_power(self.load_profile.max_profile_value)  # W
        self.peak_power = max_ac_power_in

        print(self.name + 'instantiated. Peak power in W: ' + str(self.peak_power))

        # Initialize variables
        self.__ac_power = 0  # AC power at converter terminals in W (requested from Energy System)
        self.__dc_power = 0  # DC power at DC/DC converter terminals in W (intermediate circuit power)
        self.__dc_power_ev = 0  # DC power at converter terminals in W (actual EV charging power)

        # Initialize variables
        self.__load_power = 0
        self.__time = 0

    def next(self, time: float = 0, energy_form: str = None, adapted_time: float = 0) -> float:
        """
        Returns the AC power demanded by the EV charging point at each timestep.

        Args:
            time (float): Timestamp of current timestep.
            energy_form (str, optional): Requested energy form.
            adapted_time (float): Adapted timestamp to account for profile looping.

        Return:
            float: Value of requested AC power at current timestep.
        """
        self.__time = time
        if energy_form == EnergyForm.ELECTRICITY:
            # adjusted_time = time - adapted_time
            self.__dc_power_ev = self.load_profile.next(time, adapted_time)  # in W
            self.__ac_power, self.__dc_power = self.get_power_electronics_power(self.__dc_power_ev)
            self.__load_power = self.__ac_power
            return self.__load_power
        else:
            return 0.0

    def get_power_electronics_power(self, dc_power_ev: float) -> [float]:
        """
        Returns the input AC power for AC/DC Converter and input DC power for DC/DC converter.

        Args:
            dc_power_ev (float): Charging power at EV charging port.

        Return:
            list[float]: AC power and DC power.
        """
        if dc_power_ev / self.peak_power >= 0.0001:
            dc_power = self.__dc_dc_converter.power_in(dc_power_ev)
            ac_power = -1 * self.__ac_dc_converter.to_ac(-1 * dc_power, 999)  # in W
        else:
            dc_power = 0
            ac_power = 0
        return [ac_power, dc_power]

    def update(self) -> None:
        """
        Updates the state for the load component.

        Return:
            None.
        """
        self.state.time = self.__time
        self.state.power = self.__load_power  # in W
        if abs(self.__load_power) > 0:
            self.state.run_status = 1
            self.state.power_loss = self.operation_losses()  # in W
        else:
            self.state.run_status = 0
            self.state.power_loss = 0.0  # in W
        self.data_export.transfer_data(self.state.to_export())

    def operation_losses(self) -> float:
        """
        Calculates the operation losses in W.

        Return:
            float: Operation losses.
        """
        return self.__ac_power - self.__dc_power_ev  # in W

    def allocate_emissions(self, load_grid_energy_consumption_emissions: float,
                           load_discharge_energy_consumption_emissions: float):
        """
        Updates the grid_energy_consumption_emissions and discharge_energy_consumption_emissions.

        Args:
            load_grid_energy_consumption_emissions (float): Grid energy consumption emissions.
            load_discharge_energy_consumption_emissions (float): Discharge energy consumption emissions.

        Return:
            None.
        """
        self.state.grid_energy_consumption_emissions = load_grid_energy_consumption_emissions  # in kg
        self.state.discharge_energy_consumption_emissions = load_discharge_energy_consumption_emissions  # in kg
