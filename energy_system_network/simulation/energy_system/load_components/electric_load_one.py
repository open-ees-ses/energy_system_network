from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.profiles.profile_handler import ProfileHandler
from energy_system_network.commons.state.energy_system_component.load_component.electricity_load_component_state import \
    ElectricityLoadComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.profile_config import ProfileConfig
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent


class ElectricLoadOne(LoadComponent):
    """
    Represents an electrical load component within an energy system network.

    Attributes:
        energy_form (str): Specifies the type of energy form the load uses.
        peak_power (float): The peak power value for the load in watts.
        must_fulfill (bool): Flag indicating if the load must be fulfilled.
        name (str): A unique name for the load component.
        component_ID (str): A unique identifier for the load component.
        load_profile (ProfileHandler): The load profile associated with the component.
        state (ElectricityLoadComponentState): The current state of the load component.
        __load_power (float, private): The current load power.
        __time (float, private): The current time.

    Methods:
        __init__(peak_power, data_export, energy_system_id, load_id, config_general): Initializes the object.
        next(time, energy_form=None, adapted_time=0): Obtain the load for the specified energy form at the current time step.
        update(): Updates values stored in the state for the load component.
        operation_losses(): Calculates operation losses in watts.
        allocate_emissions(load_grid_energy_consumption_emissions, load_discharge_energy_consumption_emissions): Updates
            the emissions related to energy consumption and discharge.
    """
    def __init__(self, peak_power: float, data_export, energy_system_id, load_id, config_general: GeneralSimulationConfig):
        """
        Initializes an ElectricLoadOne object.

        Args:
            peak_power (float): The peak power in watts.
            data_export: Object responsible for exporting data.
            energy_system_id: Identifier for the energy system.
            load_id: Identifier for the load within the energy system.
            config_general (GeneralSimulationConfig): Configuration data for the simulation.

        Return:
            None.
        """
        super().__init__(data_export)

        self.energy_form:str = EnergyForm.ELECTRICITY
        self.peak_power = peak_power  # in W
        self.must_fulfill = True

        # Component identification and general
        self.name = str(energy_system_id) + str(load_id) + self.__class__.__name__
        self.component_ID = str(energy_system_id) + '.' + str(load_id)

        profile_config: ProfileConfig = ProfileConfig()
        scaling_factor = self.peak_power
        profile_filename = profile_config.load_profile[self.__class__.__name__]
        self.load_profile = ProfileHandler(profile_filename, config_general, scaling_factor)

        # Update peak power after initialization of profile
        if abs(self.peak_power - 1) < 1e-4:  # Peak Power is 1 when scaling is not required
            self.peak_power = self.load_profile.max_profile_value  # in W

        # Set up logging of states
        self.state = ElectricityLoadComponentState(energy_system_id, load_id)
        self.data_export.add_export_class(self.state)  # Ensures that only those states are logged which are currently used
        self.data_export.transfer_data(self.state.to_export())  # initial timestep

        # Initialize variables
        self.__load_power = 0
        self.__time = 0

    def next(self, time: float, energy_form: str = None, adapted_time: float = 0) -> float:
        """
        Obtain the load for the specified energy form at the current time step.

        Args:
            time (float): The current time.
            energy_form (str, optional): The type of energy form. Defaults to None.
            adapted_time (float, optional): Adjusted time value. Defaults to 0.

        Return:
            float: Load value at the current time step.
        """
        self.__time = time
        if energy_form == EnergyForm.ELECTRICITY:
            # adjusted_time = time - adapted_time
            self.__load_power = self.load_profile.next(time, adapted_time)
            return self.__load_power
        else:
            return 0.0

    def update(self) -> None:
        """
        Updates a set of values stored in the state for the load component.

        Args:
            None.

        Return:
            None.
        """
        self.state.time = self.__time
        self.state.power = self.__load_power
        self.state.power_loss = self.operation_losses()
        if abs(self.__load_power) > 0:
            self.state.run_status = 1
        else:
            self.state.run_status = 0
        self.data_export.transfer_data(self.state.to_export())

    def operation_losses(self) -> float:
        """
        Calculate the operation losses in watts.

        Args:
            None.

        Return:
            float: Operation losses in watts.
        """
        return 0.0

    def allocate_emissions(self, load_grid_energy_consumption_emissions: float,
                           load_discharge_energy_consumption_emissions: float) -> None:
        """
        Update the emissions related to grid energy consumption and energy discharge.

        Args:
            load_grid_energy_consumption_emissions (float): Emissions due to grid energy consumption in kg.
            load_discharge_energy_consumption_emissions (float): Emissions due to energy discharge in kg.

        Return:
            None.
        """
        self.state.grid_energy_consumption_emissions = load_grid_energy_consumption_emissions  # in kg
        self.state.discharge_energy_consumption_emissions = load_discharge_energy_consumption_emissions  # in kg
