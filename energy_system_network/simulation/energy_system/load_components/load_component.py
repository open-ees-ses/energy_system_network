from abc import abstractmethod
from energy_system_network.profiles.profile_handler import ProfileHandler
from energy_system_network.commons.state.energy_system_component.load_component.load_component_state import LoadComponentState
from energy_system_network.simulation.energy_system.energy_system_component import EnergySystemComponent


class LoadComponent(EnergySystemComponent):
    """
    Abstract base class for a LoadComponent in the energy system.

    Attributes:
        DESCRIPTION (str): Static description of the LoadComponent.
        __load_profile (ProfileHandler): Profile handler for the load.
        __must_fulfill (bool): Flag indicating the must-fulfil status.
        __discrete_load (bool): Flag indicating if load is discrete.
        __states (LoadComponentState): State of the load component.

    Methods:
        __init__(self, data_export=None): Initializes the LoadComponent.
        must_fulfill(): Get the must-fulfil status.
        discrete_load(): Get the discrete load status.
        load_profile(): Get the load profile handler.
        state(): Get the current state.
        close(): Close and release resources.
        next(): Determine the next state or action.
        allocate_emissions(): Allocate emissions based on consumption.
    """
    DESCRIPTION: str = 'Load Component '

    def __init__(self, data_export=None):
        """
        Initializes the LoadComponent.

        Args:
            data_export: Data export handler.
        """
        super().__init__(data_export)
        self.__load_profile = ProfileHandler(None, None, None)
        self.__must_fulfill = False
        self.__discrete_load = False
        self.__states = LoadComponentState(0, 0)

    @property
    def must_fulfill(self) -> bool:
        """
        Get the must-fulfil status of the load component.

        Return:
            bool: Must-fulfil status.
        """
        return self.__must_fulfill

    @must_fulfill.setter
    def must_fulfill(self, value: bool) -> None:
        """
        Set the must-fulfil status of the load component.

        Args:
            value (bool): Must-fulfil status to set.
        """
        self.__must_fulfill = value

    @property
    def discrete_load(self) -> bool:
        """
        Get the discrete load status.

        Return:
            bool: Discrete load status.
        """
        return self.__discrete_load

    @discrete_load.setter
    def discrete_load(self, value: bool) -> None:
        """
        Set the discrete load status.

        Args:
            value (bool): Discrete load status to set.
        """
        self.__discrete_load = value

    @property
    def load_profile(self) -> ProfileHandler:
        """
        Get the load profile handler.

        Return:
            ProfileHandler: Load profile handler.
        """
        return self.__load_profile

    @load_profile.setter
    def load_profile(self, value: ProfileHandler) -> None:
        """
        Set the load profile.

        Args:
            value (ProfileHandler): Load profile to set.
        """
        self.__load_profile = value

    @property
    def state(self) -> LoadComponentState:
        """
        Get the current state.

        Return:
            LoadComponentState: Current state.
        """
        return self.__states

    @state.setter
    def state(self, value: LoadComponentState) -> None:
        """
        Set the state.

        Args:
            value (LoadComponentState): State to set.
        """
        self.__states = value

    def close(self) -> None:
        """
        Close and release resources.
        """
        self.load_profile.close()
        self.data_export.transfer_data(self.__states.to_export())

    @abstractmethod
    def next(self, time: float, energy_form: str = None, adapted_time: float = 0) -> float:
        """
        Determine the next state or action.

        Args:
            time (float): Time to determine next state.
            energy_form (str, optional): Form of the energy. Defaults to None.
            adapted_time (float, optional): Adjusted time. Defaults to 0.

        Return:
            float: Next state or action value.
        """
        pass

    @abstractmethod
    def allocate_emissions(self, load_grid_energy_consumption_emissions: float,
                           load_discharge_energy_consumption_emissions: float):
        """
        Allocate emissions based on consumption.

        Args:
            load_grid_energy_consumption_emissions (float): Emissions from grid energy consumption.
            load_discharge_energy_consumption_emissions (float): Emissions from discharge energy consumption.
        """
        pass
