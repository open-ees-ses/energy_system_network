import pytest
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.load_components.electric_load_one import ElectricLoadOne
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.constants_energy_sys_network import ROOT_PATH
from random import randint
from energy_system_network.commons.state.energy_system_component.load_component.load_component_state \
    import LoadComponentState


class TestLoadOne:
    """
    Test suite for the ElectricLoadOne class of the energy_system_network.

    Attributes:
        config (): Configuration for the energy system network.
        general_config (GeneralSimulationConfig): General configuration for the energy system simulation.
        start_time (int): Start time for the simulation.
        duration (int): Duration of the simulation.
        sample_time (int): Time interval for each sample in the simulation.
        time_step_range (list): Range of time steps used for testing.
        peak_power (int): Maximum power for the electric load.
        energy_form (str): Form of energy being tested. Initially set to 'Electricity'.
        data_export (DataHandler): Object responsible for exporting simulation data.
        energy_system_id (int): ID for the energy system being tested.
        load_id (int): ID for the load component being tested.

    Methods:
        create_model() -> ElectricLoadOne:
            Create an instance of the ElectricLoadOne model with preset attributes.

        test_next():
            Test the next method of ElectricLoadOne for both electricity and heat energy forms.

        test_state():
            Test the state property of the ElectricLoadOne model to ensure it returns an instance of LoadComponentState.

    """
    config = None
    general_config: GeneralSimulationConfig = GeneralSimulationConfig(config)
    start_time = int(general_config.start)
    duration = int(general_config.duration)
    sample_time = int(general_config.timestep)
    time_step_range = list(range(start_time, start_time + duration, 4 * sample_time))
    peak_power = randint(0, 5000)
    energy_form = 'Electricity'
    data_export = DataHandler(result_dir=ROOT_PATH + "results/energy_system_simulation/", config=general_config)
    energy_system_id = 1
    load_id = 1

    def create_model(self) -> ElectricLoadOne:
        """
        Create an instance of the ElectricLoadOne model with preset attributes.

        Returns:
            ElectricLoadOne - Instance of the ElectricLoadOne model.
        """
        return ElectricLoadOne(self.peak_power, self.data_export, self.energy_system_id, self.load_id,
                               self.general_config)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_next(self, time_step):
        """
        Test the next method of ElectricLoadOne for both electricity and heat energy forms.

        Args:
            time_step (int):
                Time step for which the next method is tested.
        """
        uut = self.create_model()
        assert (self.peak_power > uut.next(time_step, self.energy_form) >= 0)

        self.energy_form = 'Heat'  # changing the local energy form to heat after instantiating as electricity
        assert uut.next(time_step, self.energy_form) == 0.0

    def test_state(self):
        """
        Test the state property of the ElectricLoadOne model to ensure it returns an instance of LoadComponentState.
        """
        uut = self.create_model()
        assert isinstance(uut.state, LoadComponentState)
