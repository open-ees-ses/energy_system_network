from energy_system_network.commons import unit_converter
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_system_component.grid_component.electricity_grid_component_state import \
    ElectricityGridComponentState
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent


class PandapowerElectricityGridConnection(GridComponent):
    """
    Represents a grid connection using the Pandapower framework.

    The class captures the connection to the larger electricity grid. It manages the state of the connection,
    computational aspects related to power exchange, and environmental impact.

    Attributes:
        peak_power (float): Maximum power supported by the connection.
        feed_in_enabled (bool): True if energy can be fed back into the grid, False otherwise.
        energy_form (str): Form of energy being exchanged, e.g., ELECTRICITY.
        name (str): Unique name for the grid connection.
        component_ID (str): Unique identifier for the grid connection.
        __environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.
        __sample_time (float): Time interval for sampling.
        __grid_computation_model (GridComputationModel): Instance of GridComputationModel for calculations.
        state (ElectricityGridComponentState): State of the grid connection.
        production_emissions (float): Emissions during the production phase of the grid section.
        end_of_life_emissions (float): Emissions during the end of life (EOL) phase of the grid section.
        __electricity_grid_power (float): Power exchanged with the larger grid at the interface.
        __ac_power (float): Power entering/exiting the energy system.
        __grid_carbon_intensity (float): Carbon intensity of the grid in g/kWh.
        __instantaneous_energy_throughput (float): Instantaneous energy throughput in Watt-seconds.
        __cumulative_energy_throughput (float): Cumulative energy throughput in Watt-seconds.
        __instantaneous_losses (float): Instantaneous losses in power during transmission.
        __cumulative_losses (float): Cumulative losses in power during transmission.
        __instantaneous_efficiency (float): Instantaneous efficiency in percentage (p.u.).
        __cumulative_efficiency (float): Cumulative efficiency in percentage (p.u.).
        __time (float): Current simulation time.
    Methods:
        __init__(peak_power: float, feed_in_enabled: bool, data_export: DataHandler,
                 energy_system_id: int, grid_id: int, config_general: GeneralSimulationConfig,
                 environmental_analysis_config: EnvironmentalAnalysisConfig):
            Initializes a PandapowerElectricityGridConnection object.

        next(time: float = 0, energy_form: str = None, power: float = 0, adapted_time: float = 0) -> float:
            Fetches the next output from the grid connection.

        update():
            Updates values stored in the state of the grid connection object.

        compute_efficiency():
            Computes and updates both the instantaneous and cumulative efficiency values for the grid.

        operation_emissions() -> float:
            Calculates the operation emissions for the current timestep.

        energy_import_emissions() -> float:
            Calculates the energy import emissions for the current timestep.

        loss_adjusted_carbon_intensity() -> float:
            Retrieves the loss adjusted carbon intensity for the grid section.

        operation_losses() -> float:
            Calculates the grid operation losses for the current timestep.

        calculate_production_emissions() -> float:
            Calculates the production phase emissions for the grid section.

        calculate_end_of_life_emissions() -> float:
            Calculates the end of life (EOL) emissions for the grid section.

        close():
            Closes all open resources.
    """
    def __init__(self, peak_power: float, feed_in_enabled: bool, data_export: DataHandler,
                 energy_system_id: int, grid_id: int, config_general: GeneralSimulationConfig,
                 environmental_analysis_config: EnvironmentalAnalysisConfig):
        """
        Initializes a PandapowerElectricityGridConnection object.

        Args:
            peak_power (float): Maximum power supported by the connection in W.
            feed_in_enabled (bool): Whether energy can be fed back into the grid.
            data_export (DataHandler): An object for handling data exports.
            energy_system_id (int): Identifier for the energy system.
            grid_id (int): Identifier for the grid.
            config_general (GeneralSimulationConfig): General configuration settings for the simulation.
            environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration related to environmental analysis.
        """
        from grid_computation_model.main import GridComputationModel  # the project is imported here to prevent
        # errors for users who don't use this component.
        print('Info: You have selected the PandapowerElectricityGridConnection as a grid component, please check the '
              'documentation on how to configure the GCM-based PandapowerElectricityGridConnection '
              'component from within GCM.')
        super().__init__(data_export, config_general)
        self.energy_form: str = EnergyForm.ELECTRICITY
        self.peak_power = peak_power  # in W
        self.feed_in_enabled = feed_in_enabled

        # Component identification and general
        self.name = str(energy_system_id) + str(grid_id) + self.__class__.__name__
        self.component_ID = str(energy_system_id) + '.' + str(grid_id)

        # Configs
        self.__environmental_analysis_config: EnvironmentalAnalysisConfig = environmental_analysis_config
        self.__sample_time = config_general.timestep

        # GCM instantiation
        esn_result_folder = data_export.get_result_folder_name() + '/GCM_' + self.name
        self.__grid_computation_model = GridComputationModel(esn_result_folder, unit_converter.W_to_MW(peak_power), self.name)  # GCM takes the peak power input in MW

        # revised peak power based on GCM estimate
        self.peak_power = unit_converter.MW_to_W(self.__grid_computation_model.get_max_load_power())  # W

        # Set up logging of state
        self.state = ElectricityGridComponentState(energy_system_id, grid_id)

        self.data_export.add_export_class(self.state)  # Ensures that only those states are logged which are currently used
        self.data_export.transfer_data(self.state.to_export())  # initial timestep

        # Calculate emissions for production and EOL
        self.production_emissions = self.calculate_production_emissions()
        self.end_of_life_emissions = self.calculate_end_of_life_emissions()

        # Initialize variables
        self.__electricity_grid_power = 0  # Power that is exchanged with the larger grid at the interface in W
        self.__ac_power = 0  # Power actually entering/exiting the energy system in W
        self.__grid_carbon_intensity = 0  # in g/kWh

        self.__instantaneous_energy_throughput = 0
        self.__cumulative_energy_throughput = 0
        self.__instantaneous_losses = 0
        self.__cumulative_losses = 0
        self.__instantaneous_efficiency = 0
        self.__cumulative_efficiency = 0.97

        self.__time = 0

    def next(self, time: float = 0, energy_form: str = None, power: float = 0, adapted_time: float = 0) -> float:
        """
        Fetches the next output from the grid connection.

        Args:
            time (float): Current simulation time.
            energy_form (str): The form of energy being exchanged (e.g., ELECTRICITY).
            power (float): Power being exchanged with the grid.
            adapted_time (float): Adjusted time value.

        Returns:
            float: Net energy from the grid connection at the current time step.
        """
        self.__time = time
        if energy_form == EnergyForm.ELECTRICITY:
            if power < 0:  # energy is drawn from grid
                power = unit_converter.W_to_MW(power)  # in MW, GCM takes input in MW
                self.__ac_power = self.__grid_computation_model.run_step(power * -1, time)
                self.__ac_power = unit_converter.MW_to_W(self.__ac_power)  # in W, GCM output in MW
            elif power > 0 and self.feed_in_enabled is True:  # energy is fed into grid
                power = unit_converter.W_to_MW(power)  # in MW, GCM takes input in MW
                self.__ac_power = self.__grid_computation_model.run_step(power * -1, time)
                self.__ac_power = unit_converter.MW_to_W(self.__ac_power)  # in W, GCM output in MW
            else:  # idle state, losses occur in transformers
                self.__grid_computation_model.run_step(0, time)
                self.__ac_power = 0.0
            if time != 0:
                self.__grid_carbon_intensity = self.carbon_intensity_time_step(time, adapted_time)  # in g/kWh of CO2
            return self.__ac_power
        else:
            return 0.0

    def update(self) -> None:
        """
        Updates values stored in the state of the grid connection object.
        """
        self.state.time = self.__time
        self.state.power = self.__ac_power  # AC power exchanged with the energy system in W
        self.state.power_loss = self.operation_losses()  # Losses encountered during transmission from energy system to larger grid in W
        self.state.carbon_intensity = self.__grid_carbon_intensity  # in g/kWh
        self.compute_efficiency()
        self.state.cumulative_efficiency = self.__cumulative_efficiency
        self.state.energy_import_emissions = self.energy_import_emissions()  # in kg per timestep
        self.state.operation_emissions = self.operation_emissions()  # in kg per timestep
        self.state.instantaneous_efficiency = self.__instantaneous_efficiency
        if abs(self.__ac_power) > 1e-5:
            self.state.run_status = 1
        else:
            self.state.run_status = 0
        self.data_export.transfer_data(self.state.to_export())

    def compute_efficiency(self) -> None:
        """
        Computes and stores instantaneous and cumulative efficiency values for the grid section in p.u.
        """
        self.__instantaneous_energy_throughput = abs(self.state.power * self.__sample_time)
        self.__cumulative_energy_throughput += self.__instantaneous_energy_throughput
        self.__instantaneous_losses = abs(self.state.power_loss) * self.__sample_time
        self.__cumulative_losses += self.__instantaneous_losses
        try:
            self.__instantaneous_efficiency = 1 - self.__instantaneous_losses / self.__instantaneous_energy_throughput  # in p.u.
        except ZeroDivisionError:
            self.__instantaneous_efficiency = 0
        self.__cumulative_efficiency = 1 - self.__cumulative_losses / self.__cumulative_energy_throughput  # in p.u.

    def operation_emissions(self) -> float:
        """
        Calculates and returns the operation emissions in the current timestep in kgCO2eq
        :return: operation emissions in current timestep as float
        """
        if self.__ac_power >= 0:
            # __ac_power is positive at the end of the next method in case of grid import
            # losses take place in transformers at all times, hence zero included in condition
            loss_energy_at_timestep = unit_converter.J_to_kWh(self.state.power_loss * self.__sample_time)  # in kWh
            emissions = unit_converter.g_to_kg(loss_energy_at_timestep * self.__grid_carbon_intensity)  # in kg
            return emissions  # in kgCO2eq
        else:
            return 0.0

    def energy_import_emissions(self) -> float:
        """
        Calculates and returns the energy import emissions in the current timestep in kgCO2eq
        """
        if self.__ac_power > 0:
            # __ac_power is positive at the end of the next method in case of grid import
            energy_at_timestep = unit_converter.J_to_kWh(self.__ac_power * self.__sample_time)  # in kWh
            return unit_converter.g_to_kg(energy_at_timestep * self.__grid_carbon_intensity)  # in kgCO2eq
        else:
            return 0.0

    def loss_adjusted_carbon_intensity(self) -> float:
        """
        Returns the loss adjusted carbon intensity for the grid section in gCO2eq/kWh
        :return: loss adjusted carbon intensity for the grid section in gCO2eq/kWh as float
        """
        if self.__ac_power > 0:
            # __ac_power is positive at the end of the 'next' method in case of grid import
            loss_adjusted_carbon_intensity = unit_converter.kg_to_g(self.state.energy_import_emissions + self.state.operation_emissions) / \
                                             unit_converter.J_to_kWh(abs(self.__ac_power) * self.__sample_time)  # in gCO2eq/kWh
            return loss_adjusted_carbon_intensity  # in gCO2eq/kWh
        else:
            return 0.0

    def operation_losses(self) -> float:
        """
        Calculates and returns the grid section operation losses in the current timestep in W
        """
        grid_losses = unit_converter.MW_to_W(self.__grid_computation_model.get_losses())  # in W, GCM returns losses in MW
        return grid_losses  # in W

    def calculate_production_emissions(self) -> float:
        """
        calculates the production phase emissions for the grid section in kgCO2eq
        :return: production_emissions in kgCO2eq as float
        """
        line_rating_reinforcement_mv = self.__grid_computation_model.get_reinforcement_line_ratings()[0]  # GCM returns line length in km  TODO check for dependencies of emissions other than on line length.
        line_rating_reinforcement_lv = self.__grid_computation_model.get_reinforcement_line_ratings()[1]
        transformer_rating_reinforcement = unit_converter.MW_to_kW(sum(self.__grid_computation_model.get_reinforcement_transformer_ratings()))  # in kVA, GCM returns rating in MVA

        line_rating_existing_mv = self.__grid_computation_model.get_line_ratings()[0] - line_rating_reinforcement_mv
        line_rating_existing_lv = self.__grid_computation_model.get_line_ratings()[1] - line_rating_reinforcement_lv
        transformer_rating_existing = unit_converter.MW_to_kW(sum(self.__grid_computation_model.get_transformer_ratings())) - transformer_rating_reinforcement

        line_emissions_lv = self.__environmental_analysis_config.lv_grid_cable_production_emissions  # in kgCO2eq
        line_emissions_mv = self.__environmental_analysis_config.mv_grid_cable_production_emissions  # in kgCO2eq
        transformer_emissions = self.__environmental_analysis_config.transformer_production_emissions  # in kgCO2eq

        if self.__environmental_analysis_config.grid_reinforcement_only:
            production_emissions_existing = 0
        else:
            production_emissions_existing = line_rating_existing_mv * line_emissions_mv + \
                                            line_rating_existing_lv * line_emissions_lv + \
                                            transformer_rating_existing * transformer_emissions

        production_emissions_reinforcement = line_emissions_lv * line_rating_reinforcement_lv + \
                                             line_emissions_mv * line_rating_reinforcement_mv + \
                                             transformer_emissions * transformer_rating_reinforcement  # in kgCO2eq

        production_emissions = production_emissions_existing + production_emissions_reinforcement
        return production_emissions  # in kgCO2eq

    def calculate_end_of_life_emissions(self) -> float:
        """
        calculates the EOL phase emissions for the grid section in kgCO2eq
        :return: end_of_life_emissions in kgCO2eq as float
        """
        line_rating_reinforcement_mv = self.__grid_computation_model.get_reinforcement_line_ratings()[0]  # GCM returns line length in km  TODO check for dependencies of emissions other than on line length.
        line_rating_reinforcement_lv = self.__grid_computation_model.get_reinforcement_line_ratings()[1]
        transformer_rating_reinforcement = unit_converter.MW_to_kW(sum(self.__grid_computation_model.get_reinforcement_transformer_ratings()))  # in kVA, GCM returns rating in MVA

        line_rating_existing_mv = self.__grid_computation_model.get_line_ratings()[0] - line_rating_reinforcement_mv
        line_rating_existing_lv = self.__grid_computation_model.get_line_ratings()[1] - line_rating_reinforcement_lv
        transformer_rating_existing =  unit_converter.MW_to_kW(sum(self.__grid_computation_model.get_transformer_ratings())) - transformer_rating_reinforcement

        line_emissions_lv = self.__environmental_analysis_config.lv_grid_cable_eol_emissions  # in kgCO2eq
        line_emissions_mv = self.__environmental_analysis_config.mv_grid_cable_eol_emissions  # in kgCO2eq
        transformer_emissions = self.__environmental_analysis_config.transformer_eol_emissions  # in kgCO2eq

        if self.__environmental_analysis_config.grid_reinforcement_only:
            eol_emissions_existing = 0
        else:
            eol_emissions_existing = line_rating_existing_mv * line_emissions_mv + \
                                     line_rating_existing_lv * line_emissions_lv + \
                                     transformer_rating_existing * transformer_emissions

        eol_emissions_reinforcement = line_emissions_mv * line_rating_reinforcement_mv + \
                                      line_emissions_lv * line_rating_reinforcement_lv + \
                                      transformer_emissions * transformer_rating_reinforcement  # in kgCO2eq

        end_of_life_emissions = eol_emissions_existing + eol_emissions_reinforcement
        return end_of_life_emissions  # in kgCO2eq

    def close(self) -> None:
        """
        closes all open resources
        """
        self.carbon_intensity_profile.close()
        self.__grid_computation_model.close(save_plots=False, merge_analysis=True)
