import pytest
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.grid_components.simple_electricity_grid_connection import SimpleElectricityGridConnection
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.constants_energy_sys_network import ROOT_PATH
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from random import randint
from configparser import ConfigParser
from energy_system_network.commons.state.energy_system_component.grid_component.grid_component_state \
    import GridComponentState


class TestSimpleElectricityGridConnection:
    """
    Test suite for SimpleElectricityGridConnection.

    Attributes:
        config (type): Configuration for the test, type is not specified in the given code.
        general_config (GeneralSimulationConfig): General configuration for the simulation.
        start_time (int): Starting time for the simulation.
        duration (int): Duration of the simulation.
        sample_time (int): Sampling time for the simulation.
        time_step_range (list): Range of time steps.
        peak_power (int): Maximum power value.
        energy_form (str): Form of energy, set to 'Electricity'.
        feed_in_enabled (bool): Flag indicating if feeding into the grid is enabled.
        data_export (DataHandler): Handler for data export.
        energy_system_id (int): ID for the energy system.
        grid_id (int): ID for the grid.
        environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.

    Methods:
        create_model(): Returns a SimpleElectricityGridConnection instance.
        test_next(time_step): Tests the 'next' method of the model.
        test_energy_import_emissions(time_step): Tests the energy import emissions method.
        test_operation_emissions(time_step): Tests the operation emissions method.
        test_operation_losses(time_step): Tests the operation losses method.
        test_state(): Checks the state type of the model.
        test_calculate_production_emissions(): Tests the calculation of production emissions.
        test_calculate_end_of_life_emissions(): Tests the end-of-life emissions calculation.
        test_get_carbon_intensity_time_step(time_step): Tests the carbon intensity retrieval.
        test_get_production_emissions(): Tests fetching the production emissions value.
        test_get_end_of_life_emissions(): Tests fetching the end-of-life emissions value.
    """
    config = None
    general_config: GeneralSimulationConfig = GeneralSimulationConfig(config)
    start_time = int(general_config.start)
    duration = int(general_config.duration)
    sample_time = int(general_config.timestep)
    time_step_range = list(range(start_time, start_time + duration, 20*sample_time))  # amend 20 to change no. of tests
    peak_power = randint(0, 1000)
    energy_form = 'Electricity'
    feed_in_enabled = True
    data_export = DataHandler(result_dir=ROOT_PATH + "results/energy_system_simulation/", config=general_config)
    energy_system_id = 1
    grid_id = 1
    environmental_analysis_config = EnvironmentalAnalysisConfig(config=ConfigParser())

    def create_model(self) -> SimpleElectricityGridConnection:
        """
        Creates and returns a SimpleElectricityGridConnection model instance with predefined parameters.

        Returns:
            SimpleElectricityGridConnection: Model instance for testing.
        """
        return SimpleElectricityGridConnection(self.peak_power, self.feed_in_enabled, self.data_export,
                                               self.energy_system_id, self.grid_id, self.general_config,
                                               self.environmental_analysis_config)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_next(self, time_step):
        """
        Tests the 'next' method of the model for various conditions.

        Args:
            time_step (int): Time step for which to test the model.
        """
        uut = self.create_model()

        # energy is requested from the grid. energy output will be less than or equal to peak power
        power = randint(-self.peak_power*2, -1)
        assert (self.peak_power >= uut.next(time=time_step, energy_form='Electricity', power=power) > 0)

        # output is zero if energy form is not electricity
        power = randint(-self.peak_power, self.peak_power)
        assert uut.next(time=time_step, energy_form='Heat', power=power) == 0

        # energy can be sold to the grid. energy output will be less than or equal to peak power
        power = randint(self.peak_power+20, self.peak_power*2)
        assert self.peak_power >= abs(uut.next(time=time_step, energy_form='Electricity', power=power))*uut.get_efficiency()
        assert uut.next(time=time_step, energy_form='Electricity', power=power) < 0
        self.feed_in_enabled = False  # energy output is zero if feed_in is disabled
        uut = self.create_model()
        assert uut.next(time=time_step, energy_form='Electricity', power=power) == 0

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_energy_import_emissions(self, time_step):
        """
        Tests the 'energy_import_emissions' method of the model to ensure it returns a float.

        Args:
            time_step (int): Time step for which to test the model.
        """
        uut = self.create_model()
        uut.next(time=time_step, energy_form='Electricity', power=randint(-self.peak_power, self.peak_power))
        assert isinstance(uut.energy_import_emissions(), float)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_operation_emissions(self, time_step):
        """
        Tests the 'operation_emissions' method of the model to ensure it returns a float.

        Args:
            time_step (int): Time step for which to test the model.
        """
        uut = self.create_model()
        uut.next(time=time_step, energy_form='Electricity', power=randint(-self.peak_power, self.peak_power))
        assert isinstance(uut.operation_emissions(), float)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_operation_losses(self, time_step):
        """
        Tests the 'operation_losses' method of the model to ensure it returns a float.

        Args:
            time_step (int): Time step for which to test the model.
        """
        uut = self.create_model()
        uut.next(time=time_step, energy_form='Electricity', power=randint(-self.peak_power, self.peak_power))
        assert isinstance(uut.operation_losses(), float)

    def test_state(self):
        """
        Tests that the 'state' attribute of the model is of the type GridComponentState.
        """
        uut = self.create_model()
        assert isinstance(uut.state, GridComponentState)

    def test_calculate_production_emissions(self):
        """
        Tests the 'calculate_production_emissions' method to ensure it returns a float.
        """
        uut = self.create_model()
        assert isinstance(uut.calculate_production_emissions(), float)

    def test_calculate_end_of_life_emissions(self):
        """
        Tests the 'calculate_end_of_life_emissions' method to ensure it returns a float.
        """
        uut = self.create_model()
        assert isinstance(uut.calculate_end_of_life_emissions(), float)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_get_carbon_intensity_time_step(self, time_step):
        """
        Tests the 'carbon_intensity_time_step' method of the model to ensure it returns a float.

        Args:
            time_step (int): Time step for which to test the model.
        """
        uut = self.create_model()
        assert isinstance(uut.carbon_intensity_time_step(time=time_step), float)

    def test_get_production_emissions(self):
        """
        Tests that the 'production_emissions' attribute of the model returns a positive float.
        """
        uut = self.create_model()
        assert isinstance(uut.production_emissions, float)
        assert uut.production_emissions > 0

    def test_get_end_of_life_emissions(self):
        """
        Tests that the 'end_of_life_emissions' attribute of the model returns a float.
        """
        uut = self.create_model()
        assert isinstance(uut.end_of_life_emissions, float)
