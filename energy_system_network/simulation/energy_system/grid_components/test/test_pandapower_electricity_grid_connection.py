import pytest
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.grid_components.pandapower_electricity_grid_connection import PandapowerElectricityGridConnection
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.constants_energy_sys_network import ROOT_PATH
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from random import randint
from configparser import ConfigParser
from energy_system_network.commons.state.energy_system_component.grid_component.grid_component_state \
    import GridComponentState


class TestPandapowerElectricityGridConnection:
    """
    Test suite for PandapowerElectricityGridConnection.

    Attributes:
        config: Configuration for the simulation.
        general_config (GeneralSimulationConfig): General simulation configuration.
        start_time (int): Starting time of the simulation.
        duration (int): Duration of the simulation.
        sample_time (int): Sampling time for the simulation.
        time_step_range (list[int]): Time steps for the simulation.
        peak_power (int): Maximum allowable power.
        energy_form (str): Form of energy ('Electricity').
        feed_in_enabled (bool): Flag indicating if energy feed-in is enabled.
        data_export (DataHandler): Handler for exporting data.
        energy_system_id (int): ID for the energy system.
        grid_id (int): ID for the grid component.
        environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.

    Methods:
        create_model(): Creates an instance of the PandapowerElectricityGridConnection for testing.
        test_next(time_step): Tests the next method for various conditions including energy request, form checks, and feed-in checks.
        test_energy_import_emissions(time_step): Tests the calculation of energy import emissions.
        test_operation_emissions(time_step): Tests the calculation of operational emissions.
        test_operation_losses(time_step): Tests the calculation of operational losses.
        test_loss_adjusted_carbon_intensity(time_step): Tests the calculation of carbon intensity adjusted for losses.
        test_state(): Tests fetching the state of the grid component.
        test_calculate_production_emissions(): Tests the calculation of production emissions.
        test_calculate_end_of_life_emissions(): Tests the calculation of end-of-life emissions.
        test_get_carbon_intensity_time_step(time_step): Tests fetching the carbon intensity for a given time step.
        test_get_production_emissions(): Tests fetching the production emissions value.
        test_get_end_of_life_emissions(): Tests fetching the end-of-life emissions value.
    """
    config = None
    general_config: GeneralSimulationConfig = GeneralSimulationConfig(config)
    start_time = int(general_config.start)
    duration = int(general_config.duration)
    sample_time = int(general_config.timestep)
    time_step_range = list(range(start_time, start_time + duration, 20*sample_time))  # amend 20 to change no. of tests
    peak_power = randint(9000, 10000)
    energy_form = 'Electricity'
    feed_in_enabled = True
    data_export = DataHandler(result_dir=ROOT_PATH + "results/energy_system_simulation/", config=general_config)
    energy_system_id = 1
    grid_id = 1
    environmental_analysis_config = EnvironmentalAnalysisConfig(config=ConfigParser())

    def create_model(self) -> PandapowerElectricityGridConnection:
        """
        Creates an instance of PandapowerElectricityGridConnection.

        Return:
            PandapowerElectricityGridConnection: New instance of the model.
        """
        return PandapowerElectricityGridConnection(self.peak_power, self.feed_in_enabled, self.data_export,
                                                   self.energy_system_id, self.grid_id, self.general_config,
                                                   self.environmental_analysis_config)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_next(self, time_step):
        """
        Test the 'next' method for different conditions.

        Args:
            time_step (int): Time step for the simulation.
        """
        uut = self.create_model()

        # energy is requested from the grid. energy output will be less than or equal to peak power
        power = randint(-self.peak_power*2, -1)
        assert (self.peak_power >= uut.next(time=time_step, energy_form='Electricity', power=power) > 0)

        # output is zero if energy form is not electricity
        power = randint(-self.peak_power, self.peak_power)
        assert uut.next(time=time_step, energy_form='Heat', power=power) == 0

        # energy can be sold to the grid. energy output will be less than or equal to peak power
        power = randint(1, self.peak_power*2)
        assert self.peak_power >= abs(uut.next(time=time_step, energy_form='Electricity', power=power))
        assert uut.next(time=time_step, energy_form='Electricity', power=power) < 0
        self.feed_in_enabled = False  # energy output is zero if feed_in is disabled
        uut = self.create_model()
        assert uut.next(time=time_step, energy_form='Electricity', power=power) == 0

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_energy_import_emissions(self, time_step):
        """
        Test the 'energy_import_emissions' method.

        Args:
            time_step (int): Time step for the simulation.
        """
        uut = self.create_model()
        uut.next(time=time_step, energy_form='Electricity', power=randint(-self.peak_power, self.peak_power))
        assert isinstance(uut.energy_import_emissions(), float)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_operation_emissions(self, time_step):
        """
        Test the 'operation_emissions' method.

        Args:
            time_step (int): Time step for the simulation.
        """
        uut = self.create_model()
        uut.next(time=time_step, energy_form='Electricity', power=randint(-self.peak_power, self.peak_power))
        assert isinstance(uut.operation_emissions(), float)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_operation_losses(self, time_step):
        """
        Test the 'operation_losses' method.

        Args:
            time_step (int): Time step for the simulation.
        """
        uut = self.create_model()
        uut.next(time=time_step, energy_form='Electricity', power=randint(-self.peak_power, self.peak_power))
        assert isinstance(uut.operation_losses(), float)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_loss_adjusted_carbon_intensity(self, time_step):
        """
        Test the 'loss_adjusted_carbon_intensity' method.

        Args:
            time_step (int): Time step for the simulation.
        """
        uut = self.create_model()
        uut.next(time=time_step, energy_form='Electricity', power=randint(-self.peak_power, self.peak_power))
        assert isinstance(uut.loss_adjusted_carbon_intensity(), float)

    def test_state(self):
        """
        Test if the 'state' method returns an instance of GridComponentState.
        """
        uut = self.create_model()
        assert isinstance(uut.state, GridComponentState)

    def test_calculate_production_emissions(self):
        """
        Test the 'calculate_production_emissions' method.
        """
        uut = self.create_model()
        assert isinstance(uut.calculate_production_emissions(), float)

    def test_calculate_end_of_life_emissions(self):
        """
        Test the 'calculate_end_of_life_emissions' method.
        """
        uut = self.create_model()
        assert isinstance(uut.calculate_end_of_life_emissions(), float)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_get_carbon_intensity_time_step(self, time_step):
        """
        Test the 'carbon_intensity_time_step' method.

        Args:
            time_step (int): Time step for the simulation.
        """
        uut = self.create_model()
        assert isinstance(uut.carbon_intensity_time_step(time=time_step), float)

    def test_get_production_emissions(self):
        """
        Test the 'production_emissions' property.
        """
        uut = self.create_model()
        assert isinstance(uut.production_emissions, float)

    def test_get_end_of_life_emissions(self):
        """
        Test the 'end_of_life_emissions' property.
        """
        uut = self.create_model()
        assert isinstance(uut.end_of_life_emissions, float)
