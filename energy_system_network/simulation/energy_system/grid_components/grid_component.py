import statistics
from abc import abstractmethod
from energy_system_network.commons import unit_converter
from energy_system_network.profiles.profile_handler import ProfileHandler
from energy_system_network.commons.state.energy_system_component.grid_component.grid_component_state import GridComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.profile_config import ProfileConfig
from energy_system_network.simulation.energy_system.energy_system_component import EnergySystemComponent


class GridComponent(EnergySystemComponent):
    """
    A component representing the energy grid in an energy system.

    Attributes:
        DESCRIPTION (str): A description for grid components.
        __carbon_intensity_profile (ProfileHandler): The carbon intensity profile of the grid component.
        __average_carbon_intensity (float): Average carbon intensity of the grid.
        __energy_costs_profile (ProfileHandler): The energy cost profile of the grid component.
        __feed_in_enabled (bool): Flag indicating whether feed-in is enabled.
        __states (GridComponentState): The state of the grid component.

    Methods:
        __init__(data_export=None, config_general: GeneralSimulationConfig=None): Initializes a new GridComponent.
        carbon_intensity_profile() -> ProfileHandler: Getter for carbon intensity profile.
        energy_costs_profile() -> ProfileHandler: Getter for energy costs profile.
        energy_cost_time_step(time: float, adapted_time: float = 0) -> float: Gets the energy cost at a specific time step.
        carbon_intensity_time_step(time: float, adapted_time: float = 0) -> float: Gets the carbon intensity at a specific time step.
        get_emissions_per_joule(time: float, adapted_time: float = 0) -> float: Calculates specific emissions per joule of import energy.
        average_carbon_intensity() -> float: Getter for the average carbon intensity of the grid.
        feed_in_enabled() -> bool: Getter for the feed-in capability.
        feed_in_enabled(value: bool): Setter for the feed-in capability.
        state() -> GridComponentState: Getter for the state of the grid component.
        state(value: GridComponentState): Setter for the state of the grid component.
        close() -> None: Closes all open resources associated with the grid component.
        next(time: float, energy_form: str = None, power: float = 0, adapted_time: float = 0) -> float: Abstract method to be implemented by subclasses, represents the next state of the component.
    """
    DESCRIPTION: str = 'Grid Component '

    def __init__(self, data_export=None, config_general: GeneralSimulationConfig=None):
        """
        Initializes the GridComponent.

        Args:
            data_export: The data export handler.
            config_general: GeneralSimulationConfig: The configuration object for the general simulation.
        """
        super().__init__(data_export)
        profile_config: ProfileConfig = ProfileConfig()
        scaling_factor = float(profile_config.grid_emissions_scaling_factor)
        self.__carbon_intensity_profile = ProfileHandler(profile_config.grid_emissions_profile_file, config_general, scaling_factor)
        self.__average_carbon_intensity = statistics.mean(self.carbon_intensity_profile.profile)  # in gCO2eq/kWh
        try:
            self.__energy_costs_profile = ProfileHandler(profile_config.economic_profile_file, config_general, 1)
        except:
            pass
        self.__feed_in_enabled = False
        self.__states: GridComponentState = GridComponentState(0, 0)

    @property
    def carbon_intensity_profile(self) -> ProfileHandler:
        """
        returns carbon intensity profile
        :return: __carbon_intensity_profile as ProfileHandler
        """
        return self.__carbon_intensity_profile

    @property
    def energy_costs_profile(self) -> ProfileHandler:
        """
        returns energy costs profile, if it exists
        :return: __energy_costs_profile as ProfileHandler
        """
        try:
            return self.__energy_costs_profile
        except:
            print('No energy costs profile associated with this GridComponent.')

    def energy_cost_time_step(self, time: float, adapted_time: float = 0) -> float:
        """
        returns energy cost at current timestep in EUR/kWh
        :param time: current timestep as float
        :param adapted_time:
        :return: energy cost in EUR/kWh as float
        """
        return self.energy_costs_profile.next(time, adapted_time)  # in EUR/kWh

    def carbon_intensity_time_step(self, time: float, adapted_time: float = 0) -> float:
        """
        returns carbon intensity of grid at current timestep in gCO2eq/kWh
        :param time: current timestep as float
        :param adapted_time:
        :return: grid carbon intensity as float
        """
        grid_carbon_intensity = self.carbon_intensity_profile.next(time, adapted_time)  # in gCO2eq/kWh
        return grid_carbon_intensity  # in gCO2eq/kWh

    def get_emissions_per_joule(self, time: float, adapted_time: float = 0) -> float:
        """
        Calculates and returns specific emissions per joule of import energy in kgCO2eq/J.

        Args:
            time (float): The current time step.
            adapted_time (float, optional): Adjusted time value. Defaults to 0.

        Returns:
            float: Specific emissions per joule of import energy.
        """
        grid_carbon_intensity = self.carbon_intensity_time_step(time, adapted_time)  # in gCO2eq/kWh
        emissions_per_joule = grid_carbon_intensity * (unit_converter.g_to_kg(1)/unit_converter.kWh_to_J(1))  # in kgCO2eq/J
        return emissions_per_joule  # in kgCO2eq/J

    @property
    def average_carbon_intensity(self) -> float:
        """
        returns average grid carbon intensity for selected profile in gCO2eq/kWh
        :return: __average_carbon_intensity as float
        """
        return self.__average_carbon_intensity  # in gCO2eq/kWh

    @property
    def feed_in_enabled(self) -> bool:
        """
        returns feed-in capability of grid section as True/False
        :return: __feed_in_enabled as bool
        """
        return self.__feed_in_enabled

    @feed_in_enabled.setter
    def feed_in_enabled(self, value: bool) -> None:
        """
        Setter for feed-in capability.

        Args:
            value (bool): True if feeding into the grid is enabled; otherwise False.
        """
        self.__feed_in_enabled = value

    @property
    def state(self) -> GridComponentState:
        """
        returns the state for the grid section
        :return: __states as GridComponentState
        """
        return self.__states

    @state.setter
    def state(self, value: GridComponentState) -> None:
        """
        Sets the state for the grid section.

        Args:
            value (GridComponentState): The new state for the grid section.
        """
        self.__states = value

    def close(self) -> None:
        """
        closes all open resources
        """
        self.carbon_intensity_profile.close()
        self.data_export.transfer_data(self.__states.to_export())

    @abstractmethod
    def next(self, time: float, energy_form: str = None, power: float = 0, adapted_time: float = 0) -> float:
        """
        Abstract method to determine the next state of the grid component based on input parameters.

        Args:
            time (float): The current time step.
            energy_form (str, optional): The form of energy, e.g., "Electricity". Defaults to None.
            power (float, optional): The power value. Defaults to 0.
            adapted_time (float, optional): Adjusted time value. Defaults to 0.

        Returns:
            float: The next state of the component.
        """
        pass
