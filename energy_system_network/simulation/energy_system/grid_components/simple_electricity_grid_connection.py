from numpy.core._multiarray_umath import sign
from energy_system_network.commons import unit_converter
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_system_component.grid_component.electricity_grid_component_state import \
    ElectricityGridComponentState
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent


class SimpleElectricityGridConnection(GridComponent):
    """
    A representation of a basic electricity grid connection, modeling interactions
    with the larger grid over time.

    Attributes:
        energy_form (str): Type of energy, set to `EnergyForm.ELECTRICITY`.
        peak_power (float): Peak power in Watts.
        feed_in_enabled (bool): Indicates if feeding energy into the grid is allowed.
        name (str): Concatenated identifier based on energy system ID, grid ID, and class name.
        component_ID (str): Identifier for the component.
        state (ElectricityGridComponentState): The current state of the electricity grid component.
        production_emissions (float): Emissions during the production phase in kgCO2eq.
        end_of_life_emissions (float): Emissions at the end of life in kgCO2eq.
        __start_time (float): Start time from the general config.
        __sample_time (float): Sample time from the general config.
        __environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.
        __cable_length (float): Length of the cable (currently a placeholder value).
        __instantaneous_efficiency (float): Instantaneous efficiency in per unit (p.u.).
        __cumulative_efficiency (float): Cumulative efficiency in per unit (p.u.).
        __electricity_grid_power (float): Power exchanged with the larger grid at the interface in W.
        __ac_power (float): Power actually entering/exiting the energy system in W.
        __grid_carbon_intensity (float): Carbon intensity in g/kWh.
        __time (float): Current time.

    Methods:
        __init__(peak_power, feed_in_enabled, ...): Initializes a SimpleElectricityGridConnection object.
        next(time, energy_form, power, adapted_time): Simulates the grid connection for a given time step.
        update(): Updates the state of the grid connection object.
        energy_import_emissions(): Calculates emissions attributed to energy import.
        operation_emissions(): Calculates emissions due to operation losses.
        operation_losses(): Computes the operational power losses.
        get_efficiency(): Returns efficiency value of the grid section.
        calculate_production_emissions(): Calculates production phase emissions.
        calculate_end_of_life_emissions(): Calculates end-of-life phase emissions.
    """
    def __init__(self, peak_power: float, feed_in_enabled: bool, data_export: DataHandler,
                 energy_system_id: int, grid_id: int, config_general: GeneralSimulationConfig,
                 environmental_analysis_config: EnvironmentalAnalysisConfig):
        """
        Initializes a SimpleElectricityGridConnection object.

        Args:
            peak_power (float): Peak power in Watts.
            feed_in_enabled (bool): Indicates if feeding energy into the grid is allowed.
            data_export (DataHandler): Object for data export operations.
            energy_system_id (int): ID for the energy system.
            grid_id (int): ID for the grid.
            config_general (GeneralSimulationConfig): General simulation configuration.
            environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.

        Return:
            SimpleElectricityGridConnection: An instance of SimpleElectricityGridConnection.
        """
        super().__init__(data_export, config_general)
        self.energy_form: str = EnergyForm.ELECTRICITY
        self.peak_power = peak_power  # in W
        self.feed_in_enabled = feed_in_enabled

        # Component identification and general
        self.name = str(energy_system_id) + str(grid_id) + self.__class__.__name__
        self.component_ID = str(energy_system_id) + '.' + str(grid_id)

        # Configs
        self.__start_time = config_general.start
        self.__sample_time = config_general.timestep
        self.__environmental_analysis_config = environmental_analysis_config

        # Setting up logging of states
        self.state = ElectricityGridComponentState(energy_system_id, grid_id)

        self.data_export.add_export_class(self.state)  # Ensures that only those states are logged which are currently used
        self.data_export.transfer_data(self.state.to_export())  # initial timestep

        # Specify constants
        self.__cable_length = 0.01  # TODO add real values here, and pass this parameter from config
        self.__instantaneous_efficiency = 0.98  # in p.u.
        self.__cumulative_efficiency = 0.98  # in p.u.

        # Calculate emissions
        self.production_emissions = self.calculate_production_emissions()  # in kgCO2eq
        self.end_of_life_emissions = self.calculate_end_of_life_emissions()  # in kgCO2eq

        # Initialize variables
        self.__electricity_grid_power = 0  # Power that is exchanged with the larger grid at the interface in W
        self.__ac_power = 0  # Power actually entering/exiting the energy system in W
        self.__grid_carbon_intensity = 0  # in g/kWh

        self.__time = 0

    def next(self, time: float = 0, energy_form: str = None, power: float = 0, adapted_time: float = 0) -> float:
        """
        Simulates the grid connection for a given time step.

        Args:
            time (float): Current time step.
            energy_form (str): Type of energy.
            power (float): Power value for this time step.
            adapted_time (float): Adjusted time value.

        Return:
            float: Net energy from the grid connection at the current time step.
        """
        # A negative value implies power requested from grid, a positive value must be returned, and vice-versa
        self.__time = time
        if energy_form == EnergyForm.ELECTRICITY:
            self.__ac_power = power
            if power < 0:  # energy is requested from grid
                self.__electricity_grid_power = self.__ac_power / self.__cumulative_efficiency
                if abs(self.__electricity_grid_power) > self.peak_power:
                    self.__electricity_grid_power = self.peak_power * sign(power)
                    self.__ac_power = self.__electricity_grid_power * self.__cumulative_efficiency
            elif power > 0 and self.feed_in_enabled is True:  # energy is fed into grid
                self.__electricity_grid_power = self.__ac_power * self.__cumulative_efficiency
                if abs(self.__electricity_grid_power) > self.peak_power:
                    self.__electricity_grid_power = self.peak_power * sign(power)
                    self.__ac_power = self.__electricity_grid_power / self.__cumulative_efficiency
            else:
                self.__ac_power = 0.0
                self.__electricity_grid_power = 0

            self.__ac_power = self.__ac_power * -1
            if time != 0:
                self.__grid_carbon_intensity = self.carbon_intensity_time_step(time, adapted_time)
            return self.__ac_power
        else:
            return 0.0

    def update(self) -> None:
        """
        updates the state of the grid section object
        --
        """
        self.state.time = self.__time
        self.state.power = self.__ac_power  # AC power exchanged with the energy system in W
        self.state.power_loss = self.operation_losses()  # Losses encountered during transmission from energy system to larger grid in W
        self.state.carbon_intensity = self.__grid_carbon_intensity  # in gCO2eq/kWh
        self.state.cumulative_efficiency = self.__cumulative_efficiency
        self.state.energy_import_emissions = self.energy_import_emissions()  # # in kgCO2eq per timestep
        self.state.operation_emissions = self.operation_emissions()  # # in kgCO2eq per timestep
        if abs(self.__ac_power) > 1e-5:
            self.state.run_status = 1
            self.state.instantaneous_efficiency = self.__instantaneous_efficiency
        else:
            self.state.run_status = 0
            self.state.instantaneous_efficiency = 0
        self.data_export.transfer_data(self.state.to_export())

    def energy_import_emissions(self) -> float:
        """
        Calculates the emissions attributed to the energy imported at the specified grid emission factor in kgCO2eq
        """
        # __ac_power is positive at the end of the next method in case of grid import
        if self.__ac_power > 0:
            energy_at_timestep = unit_converter.J_to_kWh(self.__ac_power * self.__sample_time)  # in kWh
            return unit_converter.g_to_kg(energy_at_timestep * self.__grid_carbon_intensity)  # in kgCO2eq
        else:
            return 0.0

    def operation_emissions(self) -> float:
        """
        Calculates the emissions attributed to the losses in the GridComponent in kgCO2eq
        """
        if self.__ac_power > 0:  # i.e. grid import
            loss_energy_at_timestep = unit_converter.J_to_kWh(self.state.power_loss * self.__sample_time)  # in kWh
            emissions = unit_converter.g_to_kg(loss_energy_at_timestep * self.__grid_carbon_intensity)  # in kg
            return emissions
        else:
            return 0.0  # in kgCO2eq

    def operation_losses(self) -> float:
        """
        Calculates the operation losses in W
        """
        losses = abs(self.__electricity_grid_power) - abs(self.__ac_power)  # in W
        return losses  # in W

    def get_efficiency(self) -> float:
        """
        returns value of efficiency for the grid section in p.u.
        :return: __efficiency as float
        """
        return self.__cumulative_efficiency  # in p.u.

    def calculate_production_emissions(self) -> float:
        """
        calculates the production phase emissions for the grid section in kgCO2eq
        :return: production_emissions as float
        """
        if self.__environmental_analysis_config.grid_reinforcement_only:
            production_emissions_existing = 0
        else:
            transformer_production_emissions = unit_converter.W_to_kW(self.peak_power) * self.__environmental_analysis_config.transformer_production_emissions  # in kgCO2eq
            grid_cable_production_emissions = self.__cable_length * self.__environmental_analysis_config.lv_grid_cable_production_emissions  # in kgCO2eq
            production_emissions_existing = transformer_production_emissions + grid_cable_production_emissions  # in kgCO2eq
        production_emissions_reinforcement = 0  # This class does not feature reinforcement

        return production_emissions_existing + production_emissions_reinforcement  # in kgCO2eq

    def calculate_end_of_life_emissions(self) -> float:
        """
        calculates the EOL phase emissions for the grid section in kgCO2eq
        :return: end_of_life_emissions as float
        """
        if self.__environmental_analysis_config.grid_reinforcement_only:
            eol_emissions_existing = 0
        else:
            transformer_end_of_life_emissions = unit_converter.W_to_kW(self.peak_power) * self.__environmental_analysis_config.transformer_eol_emissions  # in kgCO2eq
            grid_cable_end_of_life_emissions = self.__cable_length * self.__environmental_analysis_config.lv_grid_cable_eol_emissions  # in kgCO2eq
            eol_emissions_existing = transformer_end_of_life_emissions + grid_cable_end_of_life_emissions  # in kgCO2eq
        eol_emissions_reinforcement = 0  # This class does not feature reinforcement

        return eol_emissions_existing + eol_emissions_reinforcement  # in kgCO2eq
