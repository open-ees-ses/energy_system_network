import pandas as pd
import scipy.interpolate
import os
from energy_system_network.simulation.energy_system.supporting_components.dc_dc_converter import DcDcConverter


class CommercialDcDcConverter(DcDcConverter):
    """
    DC/DC Converter class to model the efficiency of a commercial converter based on an efficiency curve.

    Attributes:
        POWER_IDX (int): Index for power values in the data file.
        EFFICIENCY_IDX (int): Index for efficiency values in the data file.
        DATA_FOLDER (str): Folder containing the data files.
        EFFICIENCY_CURVE_FILE (str): File containing the efficiency curve data.
        __efficiency_interpolator (scipy.interpolate.interp1d): Interpolation function for efficiency based on power.

    Methods:
        __init__(peak_power: float): Constructor. Initializes the converter with peak power and loads the efficiency curve.
        power_out(power_in: float) -> float: Computes the output power for a given input, based on the efficiency curve.
        power_in(power_out: float) -> float: Computes the input power required to achieve a specified output.
        check_power_value(value: float) -> float: Validates and ensures the input power value is within permissible limits.
        create_instance(peak_power: float): Factory method. Creates and returns an instance of CommercialDcDcConverter.

    Reference:
        https://toshiba.semicon-storage.com/us/semiconductor/design-development/referencedesign/articles/300w-Isolated-dc-dc_powermanagement_rd024.html
    """
    POWER_IDX: int = 0
    EFFICIENCY_IDX: int = 1
    DATA_FOLDER = 'data'
    EFFICIENCY_CURVE_FILE = 'commercial_dc_dc_converter_efficiency.csv'

    def __init__(self, peak_power: float):
        """
        Initialize the CommercialDcDcConverter instance.

        Args:
            peak_power (float): Peak power for the converter.
        """
        super().__init__(peak_power)

        file_name = os.path.join(self.DATA_FOLDER, self.EFFICIENCY_CURVE_FILE)
        file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), file_name)
        efficiency_curve = pd.read_csv(file_path, header=None)

        power = efficiency_curve.iloc[0:, self.POWER_IDX] * peak_power
        efficiencies = efficiency_curve.iloc[0:, self.EFFICIENCY_IDX] / 100
        self.__efficiency_interpolator = scipy.interpolate.interp1d(power, efficiencies, kind='linear')

    def power_out(self, power_in: float) -> float:
        """
        Get the output power based on the input power and efficiency curve.

        Args:
            power_in (float): Input power.

        Returns:
            float: Output power after considering efficiency.
        """
        power_in = self.check_power_value(power_in)
        efficiency = float(self.__efficiency_interpolator(power_in))
        return power_in * efficiency

    def power_in(self, power_out: float) -> float:
        """
        Get the input power based on the output power and efficiency curve.

        Args:
            power_out (float): Output power.

        Returns:
            float: Required input power to achieve the given output.
        """
        power_out = self.check_power_value(power_out)
        efficiency = float(self.__efficiency_interpolator(power_out))
        if abs(power_out) > 1e-5:
            power_in = power_out / efficiency
        else:
            power_in = 0
        return power_in

    def check_power_value(self, value: float) -> float:
        """
        Check and ensure the given power value is within permissible limits.

        Args:
            value (float): Input power value.

        Returns:
            float: Clamped power value.
        """
        power = min(value, self.peak_power)
        return power

    @classmethod
    def create_instance(cls, peak_power: float):
        """
        Class method to create an instance of the ToshibaDcDcConverter.

        Args:
            peak_power (float): Peak power for the converter.

        Returns:
            CommercialDcDcConverter: A new instance of the ToshibaDcDcConverter class.
        """
        return CommercialDcDcConverter(peak_power)

if __name__ == '__main__':  # Enables isolated debugging and execution of test cases
    print("This only executes when %s is executed rather than imported" % __file__)

    conv = CommercialDcDcConverter(3000)
    power = conv.power_out(3200)
    print(conv)
