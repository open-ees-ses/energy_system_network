from energy_system_network.simulation.energy_system.supporting_components.dc_dc_converter import DcDcConverter
import math
from numpy import sign


class DcDcConverterStacked(DcDcConverter):
    """
    Represents a stacked configuration of DC-DC converters.

    Attributes:
        __NUMBER_CONVERTERS (int): Number of individual DC-DC converters in the stack.
        __SWITCH_VALUE (float): Ratio of power over the rated power to decide switching behavior.
        __MAX_POWER_INDIVIDUAL (float): Maximum power that can be handled by each individual converter.
        __converters (list[DcDcConverter]): List containing individual DC-DC converter instances.

    Methods:
        __init__(number_converters: int, switch_value: float, dcdc_converter: DcDcConverter):
            Initialize the DcDcConverterStacked with given parameters.
        power_out(power_in: float) -> float: Calculate the output power based on the input power.
        power_in(power_out: float) -> float: Calculate the input power required to achieve the desired output power.
        __power_splitter(power: float) -> list: Distribute the power among individual converters based on certain rules.
        create_instance(peak_power: float): Create an instance of the DcDcConverterStacked with the specified peak
            power.
    """
    def __init__(self, number_converters: int, switch_value: float, dcdc_converter: DcDcConverter):
        """
        Initialize the DcDcConverterStacked with given parameters.

        Args:
            number_converters (int): Number of individual converters.
            switch_value (float): Switching value to determine power distribution.
            dcdc_converter (DcDcConverter): A reference DC-DC converter instance.
        """
        super().__init__(dcdc_converter.peak_power)
        self.__NUMBER_CONVERTERS: int = number_converters
        self.__SWITCH_VALUE: float = switch_value  # ratio power over rated power
        self.__MAX_POWER_INDIVIDUAL: float = dcdc_converter.peak_power / number_converters
        self.__converters: [DcDcConverter] = list()
        for converter in range(number_converters):
            self.__converters.append(dcdc_converter.create_instance(self.__MAX_POWER_INDIVIDUAL))

    def power_out(self, power_in: float) -> float:
        """
        Calculate the output power based on the input power.

        Args:
            power_in (float): Input power.

        Return:
            float: Output power from the stack.
        """
        power_individual = iter(self.__power_splitter(power_in))
        power_individual_dc: [float] = list()
        for converter in self.__converters:
            converter: DcDcConverter = converter
            power_individual_dc.append(converter.power_out(next(power_individual)))
        return sum(power_individual_dc)

    def power_in(self, power_out: float) -> float:
        """
        Calculate the input power required to achieve the desired output power.

        Args:
            power_out (float): Desired output power.

        Return:
            float: Input power for the stack.
        """
        power_individual = iter(self.__power_splitter(power_out))
        power_individual_dc: [float] = list()
        for converter in self.__converters:
            converter: DcDcConverter = converter
            power_individual_dc.append(converter.power_in(next(power_individual)))
        return sum(power_individual_dc)

    def __power_splitter(self, power: float) -> list:
        """
        Distribute the power among individual converters based on certain rules.

        Args:
            power (float): Power to be distributed.

        Return:
            list: List of power values for individual converters.
        """
        switch_value = self.__SWITCH_VALUE
        power_individual: [float] = [0.0] * self.__NUMBER_CONVERTERS
        if abs(power) > self.peak_power:
            for converter in range(self.__NUMBER_CONVERTERS):
                power_individual[converter] = self.__MAX_POWER_INDIVIDUAL * sign(power)
        elif abs(power) > self.peak_power * switch_value:
            equal_distributed_power: float = abs(power) / self.__NUMBER_CONVERTERS
            for converter in range(self.__NUMBER_CONVERTERS):
                power_individual[converter] = max(0.0, min(equal_distributed_power, self.__MAX_POWER_INDIVIDUAL)) * sign(power)
        else:
            remaining_power: float = abs(power)
            max_individual_power: float = self.__MAX_POWER_INDIVIDUAL * switch_value
            number_converters_activated: int = min(math.ceil(remaining_power / max_individual_power), self.__NUMBER_CONVERTERS)
            equal_distributed_power: float = remaining_power / number_converters_activated
            for converter in range(self.__NUMBER_CONVERTERS):
                individual_power: float = max(0.0, min(equal_distributed_power, max_individual_power, remaining_power))
                power_individual[converter] = individual_power * sign(power)
                remaining_power -= individual_power
        return power_individual

    @classmethod
    def create_instance(cls, peak_power: float):
        """
        Create an instance of the DcDcConverterStacked with the specified peak power.

        Args:
            peak_power (float): Peak power for the stack.

        Return:
            DcDcConverterStacked: An instance of the DcDcConverterStacked.
        """
        pass
