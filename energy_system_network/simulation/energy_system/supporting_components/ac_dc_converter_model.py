import math


def create_ac_dc_converter(ac_dc_converter_name: str, max_power: float, number_stacked: int = 1, switch_value: float = 1):
    """
    Create an AC-DC converter based on the provided name and other parameters.

    Args:
        ac_dc_converter_name (str): Name of the desired AC-DC converter.
        max_power (float): Maximum power for the converter.
        number_stacked (int, optional): Number of stacked converters. Defaults to 1.
        switch_value (float, optional): Switching value to determine power distribution. Defaults to 1.

    Returns:
        AcDcConverter: An instance of the desired AC-DC converter.

    Raises:
        Exception: If the provided ac_dc_converter_name is not supported.
    """
    from simses.system.power_electronics.acdc_converter.abstract_acdc_converter import AcDcConverter
    from simses.system.power_electronics.acdc_converter.stacked import AcDcConverterIdenticalStacked
    from simses.system.power_electronics.acdc_converter.fix_efficiency import FixEfficiencyAcDcConverter
    from simses.system.power_electronics.acdc_converter.notton import NottonAcDcConverter

    if ac_dc_converter_name == FixEfficiencyAcDcConverter.__name__:
        ac_dc_converter: AcDcConverter = FixEfficiencyAcDcConverter(max_power)
    elif ac_dc_converter_name == NottonAcDcConverter.__name__:
        ac_dc_converter: AcDcConverter = NottonAcDcConverter(max_power)
    else:
        supported_ac_dc_converters = get_supported_ac_dc_converters()
        raise Exception('ACDC converter ' + ac_dc_converter_name + ' is unknown. '
                        'Following options are available: ' + str(supported_ac_dc_converters))
    if number_stacked > 1:
        ac_dc_converter: AcDcConverter = AcDcConverterIdenticalStacked(number_stacked, switch_value, ac_dc_converter)
    return ac_dc_converter


def calculate_production_emissions(max_power: float, number_of_units: int) -> float:
    """
    Calculate the production emissions based on the provided maximum power and number of units.

    Args:
        max_power (float): Maximum power of the converter.
        number_of_units (int): Number of converter units.

    Returns:
        float: Calculated production emissions in kg CO2eq/kW.
    """
    # Power electronics production emissions, as per regression model from Nam Truong
    b_parameter = 11.23  # kg CO2eq/kW
    c_parameter = 97.96  # kg CO2eq/kW

    specific_production_emissions = -b_parameter * math.log(max_power/number_of_units) + c_parameter  # kg CO2eq/kW
    production_emissions = max_power * specific_production_emissions  # max_power required in kW
    return production_emissions


# def calculate_eol_emissions (max_power: float) -> float:
#     pe_eol_emissions = max_power * self.__environmental_analysis_config.power_electronics_eol_emissions
def get_supported_ac_dc_converters() -> list:
    """
    Get a list of supported AC-DC converters.

    Returns:
        list[str]: A list containing the names of the supported AC-DC converters.
    """
    from simses.system.power_electronics.acdc_converter.fix_efficiency import FixEfficiencyAcDcConverter
    from simses.system.power_electronics.acdc_converter.notton import NottonAcDcConverter
    options: [str] = list()
    options.append(FixEfficiencyAcDcConverter.__name__)
    options.append(NottonAcDcConverter.__name__)
    return options


class AcDcConverterModel:  # This enables the above methods to be called directly from other parts of the program
    """
    Model for the AC-DC converter. This acts as a bridge and allows the above methods
    to be called directly from other parts of the program.
    """
    pass
