from abc import abstractmethod


class DcDcConverter:
    """
    Abstract base class for DC/DC converters.

    The DcDcConverter class provides a blueprint for DC/DC converter implementations. Each DC/DC converter
    should be able to calculate the output power given an input and vice versa. The peak power of the converter
    is provided during instantiation.

    Attributes:
        peak_power (float): The maximum power the converter can handle.

    Methods:
        __init__(peak_power: float): Initializes a DcDcConverter with a specified peak power.
        power_out(power_in: float) -> float: Abstract method. Computes the output power for a given input power.
        power_in(power_out: float) -> float: Abstract method. Computes the input power required for a given output power.
        create_instance(peak_power: float): Abstract class method. Factory method to create an instance of a DC/DC converter.
    """
    def __init__(self, peak_power: float):
        """
        Initializes a DcDcConverter with a specified peak power.

        Args:
            peak_power (float): The peak power capacity of the converter.
        """
        self.peak_power = peak_power

    @abstractmethod
    def power_out(self, power_in: float) -> float:
        """
        Computes the output power for a given input power.

        Args:
            power_in (float): Input power.

        Returns:
            float: Output power.
        """
        pass

    @abstractmethod
    def power_in(self, power_out: float) -> float:
        """
        Computes the input power required for a given output power.

        Args:
            power_out (float): Output power.

        Returns:
            float: Input power required.
        """
        pass

    @classmethod
    @abstractmethod
    def create_instance(cls, peak_power: float):
        """
        Factory method to create an instance of a DC/DC converter.

        Args:
            peak_power (float): The peak power capacity of the converter.

        Returns:
            DcDcConverter: An instance of a DC/DC converter with the specified peak power.
        """
        pass
