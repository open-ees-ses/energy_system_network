from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.simulation.energy_management.energy_management import EnergyManagement
from energy_system_network.simulation.energy_system.energy_system_component import EnergySystemComponent
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent


class EnergySystem:
    """
    Represents an energy system comprised of different energy system components.

    Attributes:
        __name (str): The name of the energy system.
        __id (str): The unique identifier of the energy system.
        __energy_form ([str]): List of energy forms the system supports.
        __data_export (DataHandler): Data handler for exporting system data.
        __energy_management (EnergyManagement): The energy management system for the EnergySystem.
        __generation_components ([GenerationComponent]): List of generation components in the system.
        __grid_components ([GridComponent]): List of grid components in the system.
        __storage_components ([StorageComponent]): List of storage components in the system.
        __load_components ([LoadComponent]): List of load components in the system.

    Methods:
        __init__(name, id_number, energy_forms, data_export, energy_management, generation_components, storage_components, load_components, grid_components):
            Initializes the energy system.
        update(time, adapted_time): Updates the states of all components in the system.
        reset_profiles(adapted_time): Resets profiles of components at the end of each loop.
        close(): Closes all resources associated with the energy system.
        get_generation_components() -> [GenerationComponent]: Retrieves the generation components.
        get_load_components() -> [LoadComponent]: Retrieves the load components.
        get_storage_components() -> [StorageComponent]: Retrieves the storage components.
        get_grid_components() -> [GridComponent]: Retrieves the grid components.
        get_name() -> str: Retrieves the name of the energy system.
        get_id() -> str: Retrieves the ID of the energy system.
        get_energy_form() -> [str]: Retrieves the energy forms supported by the system.
        get_energy_management() -> EnergyManagement: Retrieves the energy management system instance.
    """
    def __init__(self, name: str, id_number: int, energy_forms: [str], data_export: DataHandler, energy_management,
                 generation_components: [GenerationComponent], storage_components: [StorageComponent],
                 load_components: [LoadComponent], grid_components: [GridComponent]):
        """
        Initializes the EnergySystem with specified attributes.

        Args:
            name (str): Name of the energy system.
            id_number (int): Identifier for the energy system.
            energy_forms ([str]): Supported energy forms.
            data_export (DataHandler): Data export handler.
            energy_management: Instance for managing energy within the system.
            generation_components ([GenerationComponent]): Generation components list.
            storage_components ([StorageComponent]): Storage components list.
            load_components ([LoadComponent]): Load components list.
            grid_components ([GridComponent]): Grid components list.

        Return:
            None.
        """
        self.__name = name
        self.__id = str(id_number)
        self.__energy_form = energy_forms
        self.__data_export = data_export
        self.__energy_management: EnergyManagement = energy_management
        self.__generation_components: [GenerationComponent] = generation_components
        self.__grid_components: list[GridComponent] = grid_components
        self.__storage_components: [StorageComponent] = storage_components
        self.__load_components: [LoadComponent] = load_components

    def update(self, time: float, adapted_time: float) -> None:
        """
        updates the states of all components in the EnergySystem.

        Args:
            time (float): Epoch timestamp.
            adapted_time (float): Time offset in seconds.
        """
        self.__energy_management.next(time, adapted_time)
        for component_group in self.__generation_components, \
                               self.__storage_components, \
                               self.__load_components, \
                               self.__grid_components:
            if component_group:
                for component in component_group:  # type: EnergySystemComponent
                    component.update()

    def reset_profiles(self, adapted_time: float) -> None:
        """
        Resets profile-based modules at the end of each loop in classes using SimSES and SimSESExternalStrategy EMS
        :param adapted_time: Time offset in s
        :return: None
        """
        if self.__storage_components:
            for component in self.__storage_components:  # type: StorageComponent
                    component.reset_profiles(adapted_time)

    def close(self) -> None:
        """
        closes all open resources in the EnergySystem
        """
        for component_group in self.__generation_components, \
                               self.__storage_components, \
                               self.__load_components, \
                               self.__grid_components:
            if component_group:
                for component in component_group:  # type: EnergySystemComponent
                    component.close()
        self.__energy_management.close()

    def get_generation_components(self) -> [GenerationComponent]:
        """
        Retrieves the list of generation components within the energy system.

        Return:
            [GenerationComponent]: List of generation components.
        """
        return self.__generation_components

    def get_load_components(self) -> [LoadComponent]:
        """
        Retrieves the list of load components within the energy system.

        Return:
            [LoadComponent]: List of load components.
        """
        return self.__load_components

    def get_storage_components(self) -> [StorageComponent]:
        """
        Retrieves the list of storage components within the energy system.

        Return:
            [StorageComponent]: List of storage components.
        """
        return self.__storage_components

    def get_grid_components(self) -> [GridComponent]:
        """
        Retrieves the list of grid components within the energy system.

        Return:
            [GridComponent]: List of grid components.
        """
        return self.__grid_components

    def get_name(self) -> str:
        """
        Retrieves the name of the energy system.

        Return:
            str: Name of the energy system.
        """
        return self.__name

    def get_id(self) -> str:
        """
        Retrieves the unique identifier of the energy system.

        Return:
            str: ID of the energy system.
        """
        return self.__id

    def get_energy_form(self) -> [str]:
        """
        Retrieves the supported energy forms of the energy system.

        Return:
            [str]: List of supported energy forms.
        """
        return self.__energy_form

    def get_energy_management(self) -> EnergyManagement:
        """
        Retrieves the energy management system instance associated with the energy system.

        Return:
            EnergyManagement: The energy management system instance.
        """
        return self.__energy_management
