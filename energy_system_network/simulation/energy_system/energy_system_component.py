from abc import ABC, abstractmethod
from energy_system_network.commons.state.state import State


class EnergySystemComponent(ABC):
    """
    A base abstract class for energy system components. This class provides common attributes and abstract methods
    which must be implemented by any concrete energy system component.

    Attributes:
        data_export (): An optional object to handle data exporting for the component.
        __energy_form (list): A list indicating supported energy forms for the component.
        __peak_power (float): The peak power rating of the component in Watts.
        __name (str): The name of the energy system component.
        __component_ID (str): A unique identifier for the energy system component.
        __production_emissions (float): Emissions related to the production phase of the component, in kgCO2eq.
        __end_of_life_emissions (float): Emissions related to the end of life phase of the component, in kgCO2eq.

    Methods:
        __init__(data_export=None): Constructor method for EnergySystemComponent.
        energy_form() -> list: Getter for the energy form.
        energy_form(value: list) -> None: Setter for the energy form.
        peak_power() -> float: Getter for the peak power.
        peak_power(value: float) -> None: Setter for the peak power.
        name() -> str: Getter for the name.
        name(value: str) -> None: Setter for the name.
        component_ID() -> str: Getter for the component ID.
        component_ID(value: str) -> None: Setter for the component ID.
        production_emissions() -> float: Getter for the production phase emissions.
        production_emissions(value: float) -> None: Setter for the production phase emissions.
        end_of_life_emissions() -> float: Getter for the end of life phase emissions.
        end_of_life_emissions(value: float) -> None: Setter for the end of life phase emissions.
        update(): Abstract method for updating the state of the component.
        state() -> State: Abstract method to retrieve the state of the component.
        operation_losses() -> float: Abstract method to get the operational losses of the component.
        close() -> None: Abstract method to handle any cleanup or finalization actions for the component.
    """
    def __init__(self, data_export=None):
        """
        Initializes an instance of the EnergySystemComponent with the provided configuration.

        Args:
            data_export: An optional object to handle data exporting for the component.

        Return:
            None.
        """
        self.data_export = data_export
        self.__energy_form = []
        self.__peak_power = 0.0  # in W
        self.__name = ''
        self.__component_ID = ''

        self.__production_emissions = 0.0
        self.__end_of_life_emissions = 0.0

    @property
    def energy_form(self) -> list:
        """
        returns supported energy forms for EnergySystemComponent
        :return: __energy_form as list
        """
        return self.__energy_form

    @energy_form.setter
    def energy_form(self, value: list) -> None:
        """
        sets supported energy forms for EnergySystemComponent

        Args:
            value (list): The list of supported energy forms to set.

        """
        self.__energy_form = value

    @property
    def peak_power(self) -> float:
        """
        returns peak power rating of EnergySystemComponent in W
        :return: peak_power in W as float
        """
        return self.__peak_power

    @peak_power.setter
    def peak_power(self, value: float) -> None:
        """
        sets peak power rating of EnergySystemComponent in W

        Args:
            value (float): peak power value to set.
        """
        self.__peak_power = value

    @property
    def name(self) -> str:
        """
        returns name of EnergySystemComponent
        :return: __name as str
        """
        return self.__name

    @name.setter
    def name(self, value: str) -> None:
        """
        sets name of EnergySystemComponent

        Args:
            value (str): name for the energy system to set.
        """
        self.__name = value

    @property
    def component_ID(self) -> str:
        """
        returns component ID of EnergySystemComponent
        :return: __component_ID as str
        """
        return self.__component_ID

    @component_ID.setter
    def component_ID(self, value: str) -> None:
        """
        sets component ID of EnergySystemComponent

        Args:
            value (str): component ID for the energy system to set.
        """
        self.__component_ID = value

    @property
    def production_emissions(self) -> float:
        """
        returns production phase emissions for the grid section in kgCo2eq
        :return: __production_emissions as float
        """
        return self.__production_emissions  # in kgCO2eq

    @production_emissions.setter
    def production_emissions(self, value: float) -> None:
        """
        sets production phase emissions for the grid section in kgCo2eq.

        Args:
            value (float): production phase emissions for the grid section to set.
        """
        self.__production_emissions = value # in kgCO2eq

    @property
    def end_of_life_emissions(self) -> float:
        """
        returns EOL phase emissions for the grid section in kgCO2eq
        :return: __end_of_life_emissions as float
        """
        return self.__end_of_life_emissions  # in kgCO2eq

    @end_of_life_emissions.setter
    def end_of_life_emissions(self, value: float) -> None:
        """
        sets EOL phase emissions for the grid section in kgCO2eq

        Args:
            value (float): EOL phase emissions for the grid section to set.

        Return:
            __end_of_life_emissions as float.
        """
        self.__end_of_life_emissions = value  # in kgCO2eq

    @abstractmethod
    def update(self):
        """
        Abstract method to update the state of the energy system component.
        """
        pass

    @property
    @abstractmethod
    def state(self) -> State:
        """
        Retrieves the current state of the EnergySystemComponent.

        Return:
            State: An instance of State representing the current state of the component.
        """
        pass

    @abstractmethod
    def operation_losses(self) -> float:
        """
        Calculates the operation losses for the EnergySystemComponent.

        Return:
            float: The operational losses for the component.
        """
        pass

    @abstractmethod
    def close(self) -> None:
        """
        Handles any cleanup or finalization actions required for the EnergySystemComponent.
        """
        pass
