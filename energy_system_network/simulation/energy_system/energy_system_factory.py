import warnings
from configparser import ConfigParser
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.commons.log import Logger
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from energy_system_network.config.simulation.energy_system_config import EnergySystemConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.profile_config import ProfileConfig
from energy_system_network.simulation.energy_management.energy_management import EnergyManagement
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.generation_components.diesel_generator import \
    DieselGenerator
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent
from energy_system_network.simulation.energy_system.generation_components.photovoltaic_solar import \
    PhotovoltaicSolar
from energy_system_network.simulation.energy_system.generation_components.wind_turbine import WindTurbine
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.grid_components.pandapower_electricity_grid_connection import \
    PandapowerElectricityGridConnection
from energy_system_network.simulation.energy_system.grid_components.simple_electricity_grid_connection import \
    SimpleElectricityGridConnection
from energy_system_network.simulation.energy_system.load_components.ev_charging_station_dc import EVChargingPointDC
from energy_system_network.simulation.energy_system.load_components.ev_home_charger import EVHomeCharger
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent
from energy_system_network.simulation.energy_system.load_components.electric_load_one import ElectricLoadOne
from energy_system_network.simulation.energy_system.storage_components.electric_vehicle import \
    ElectricVehicle
from energy_system_network.simulation.energy_system.storage_components.lithium_battery_system import \
    LithiumBatterySystem
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent
from energy_system_network.simulation.energy_system.supporting_components import ac_dc_converter_model


class EnergySystemFactory:
    """
    A factory class to coordinate and create energy systems and their components based on the simulation.ini configuration.

    Attributes:
        ENERGY_SYSTEM_NAME (int): Index for the name of the energy system.
        ENERGY_SYSTEM_ENERGY_FORMS (int): Index for the energy forms of the energy system.
        COMPONENT_NAME (int): Index for the name of the component.
        COMPONENT_POWER_RATING (int): Index for the power rating of the component.
        STORAGE_COMPONENT_ENERGY_RATING (int): Index for the energy rating of storage components.
        STORAGE_COMPONENT_DC_SYSTEM_FLAG (int): Index for the DC system flag of storage components.
        GRID_COMPONENT_FEED_IN_ENABLED (int): Index for checking if feed-in is enabled in grid components.
        GRID_COMPONENT_ACDC_CONVERTER_NAME (int): Index for the AC/DC converter name of grid components.
        __log (Logger): Logger for the factory operations.
        __simulation_config (ConfigParser): Configuration parser for the simulation.
        __system_config (EnergySystemConfig): Configuration parser for the energy system.
        __general_config (GeneralSimulationConfig): Configuration parser for the general simulation.
        __profile_config (ProfileConfig): Configuration parser for the profile.
        __environmental_config (EnvironmentalAnalysisConfig): Configuration parser for the environmental analysis.

    Methods:
        __init__(config: ConfigParser): Initializes the EnergySystemFactory.
        create_energy_system(data_export: DataHandler) -> [EnergySystem]: Creates and returns energy systems based on
            the configuration.
        grid_components(name: str, data_export: DataHandler, energy_system_id: int) -> [GridComponent]: Creates and
            returns grid components based on the configuration.
        generation_components(energy_system_name: str, data_export: DataHandler, energy_system_id: int)
            -> [GenerationComponent]: Creates and returns generation components based on the configuration.
        storage_components(name: str, data_export: DataHandler, energy_system_id: int) -> [StorageComponent]:
            Creates and returns storage components based on the configuration.
        load_components(name: str, data_export: DataHandler, energy_system_id: int) -> [LoadComponent]: Creates and
            returns load components based on the configuration.
        create_generation_component(generation_component_type: str, data_export: DataHandler, peak_power: float,
            energy_system_id: int, generation_id: int) -> GenerationComponent: Factory method to create specific
                generation components.
        create_storage_component(storage_component_type: str, data_export: DataHandler, peak_power: float,
            energy_capacity: float, energy_system_id: int, storage_id: int, dc_storage_system_flag: bool)
                -> StorageComponent: Factory method to create specific storage components.
        create_load_component(load_component_type: str, data_export: DataHandler, peak_power: float, energy_system_id:
            int, load_id: int) -> LoadComponent: Factory method to create specific load components.
        create_grid_component(self, grid_component_type: str, data_export: DataHandler, peak_power: float,
            feed_in_enabled: bool, energy_system_id: int, grid_id: int, ac_dc_converter_type: str) -> GridComponent:
                Creates instance of specified GridComponent
    """
    ENERGY_SYSTEM_NAME: int = 0
    ENERGY_SYSTEM_ENERGY_FORMS: int = 1

    COMPONENT_NAME: int = 1
    COMPONENT_POWER_RATING: int = 2

    STORAGE_COMPONENT_ENERGY_RATING: int = 3
    STORAGE_COMPONENT_DC_SYSTEM_FLAG: int = 4

    GRID_COMPONENT_FEED_IN_ENABLED: int = 3
    GRID_COMPONENT_ACDC_CONVERTER_NAME: int = 4

    def __init__(self, config: ConfigParser):
        """
        Initializes the EnergySystemFactory with the provided configuration.

        Args:
            config (ConfigParser): Configuration data for the energy systems and components.

        Return:
            None.
        """
        self.__log: Logger = Logger(type(self).__name__)
        self.__simulation_config: ConfigParser = config
        self.__system_config: EnergySystemConfig = EnergySystemConfig(config)
        self.__general_config: GeneralSimulationConfig = GeneralSimulationConfig(config)
        self.__profile_config: ProfileConfig = ProfileConfig(config)
        self.__environmental_config: EnvironmentalAnalysisConfig = EnvironmentalAnalysisConfig(config)

    def create_energy_system(self, data_export: DataHandler) -> [EnergySystem]:
        """
        Coordinates the creation of energy systems specified in simulation.ini and returns created EnergySystem(s)
        :param data_export: DataHandler object
        :return: List of EnergySystem object(s)
        """
        energy_systems: [[str]] = self.__system_config.energy_systems
        res: [EnergySystem] = list()
        names: [str] = list()
        id_number: int = 0
        for system in energy_systems:
            id_number += 1
            name = system[self.ENERGY_SYSTEM_NAME]
            energy_forms: [str] = system[self.ENERGY_SYSTEM_ENERGY_FORMS:]
            if name in names:
                raise Exception('Energy system name ' + name + ' is not unique.')
            names.append(name)
            generation_components: [GenerationComponent] = self.generation_components(name, data_export, id_number)
            storage_components: [StorageComponent] = self.storage_components(name, data_export,
                                                                             id_number)
            load_components: [LoadComponent] = self.load_components(name, data_export,
                                                                    id_number)
            grid_components: [GridComponent] = self.grid_components(name, data_export, id_number)
            energy_management: [EnergyManagement] = EnergyManagement(data_export, self.__simulation_config, id_number,
                                                                     energy_forms, generation_components,
                                                                     storage_components, load_components,
                                                                     grid_components)
            res.append(
                EnergySystem(name, id_number, energy_forms, data_export, energy_management, generation_components,
                             storage_components, load_components, grid_components))
        return res

    def grid_components(self, name: str, data_export: DataHandler, energy_system_id: int) -> [GridComponent]:
        """
        Coordinates the creation of grid components specified in simulation.ini and returns created GridComponent(s)
        :param name: name of energy system to which specified components are associated with as str
        :param data_export: DataHandler object
        :param energy_system_id: ID of energy system to which specified components are associated with as int
        :return: List of GridComponent object(s)
        """
        grid_components: [[str]] = self.__system_config.grid_components
        res: [] = list()
        grid_id = 0
        for component in grid_components:
            system = component[self.ENERGY_SYSTEM_NAME]
            if system == name:
                grid_id += 1
                grid_component_type: str = component[self.COMPONENT_NAME]
                peak_power: float = float(component[self.COMPONENT_POWER_RATING])
                feed_in_enabled: bool = eval(component[self.GRID_COMPONENT_FEED_IN_ENABLED])
                try:
                    acdc_converter_name: str = component[self.GRID_COMPONENT_ACDC_CONVERTER_NAME]
                    if acdc_converter_name in ac_dc_converter_model.get_supported_ac_dc_converters():
                        ac_dc_converter_type = acdc_converter_name
                    else:
                        raise Exception('Specified AC/DC converted not supported. '
                                        'The following types are currently supported:'
                                        + str(ac_dc_converter_model.get_supported_ac_dc_converters()))
                except IndexError:
                    ac_dc_converter_type = None

                grid_component: GridComponent = self.create_grid_component(grid_component_type,
                                                                           data_export, peak_power,
                                                                           feed_in_enabled,
                                                                           energy_system_id,
                                                                           grid_id, ac_dc_converter_type)
                res.append(grid_component)
        return res

    def generation_components(self, energy_system_name: str, data_export: DataHandler, energy_system_id: int) -> [GenerationComponent]:
        """
        Coordinates the creation of generation components specified in simulation.ini and returns created GenerationComponent(s)
        :param energy_system_name: name of energy system to which specified components are associated with as str
        :param data_export: DataHandler object
        :param energy_system_id: ID of energy system to which specified components are associated with as int
        :return: List of GenerationComponent object(s)
        """
        generation_components: [[str]] = self.__system_config.generation_components
        res: [] = list()
        generation_id = 0
        for component in generation_components:
            system = component[self.ENERGY_SYSTEM_NAME]
            if system == energy_system_name:
                generation_id += 1
                generation_component_type: str = component[self.COMPONENT_NAME]
                peak_power: float = float(component[self.COMPONENT_POWER_RATING])
                generation_component: GenerationComponent = self.create_generation_component(generation_component_type,
                                                                                             data_export, peak_power,
                                                                                             energy_system_id,
                                                                                             generation_id)
                res.append(generation_component)
        return res

    def storage_components(self, name: str, data_export: DataHandler, energy_system_id: int) -> [StorageComponent]:
        """
        Coordinates the creation of generation components specified in simulation.ini and returns created StorageComponent(s)
        :param name: name of energy system to which specified components are associated with as str
        :param data_export: DataHandler object
        :param energy_system_id: ID of energy system to which specified components are associated with as int
        :return: List of StorageComponent object(s)
        """
        storage_components: [[str]] = self.__system_config.storage_components
        res: [] = list()
        storage_id = 0
        for component in storage_components:
            system = component[self.ENERGY_SYSTEM_NAME]
            if system == name:
                storage_id += 1
                storage_component_type: str = component[self.COMPONENT_NAME]
                peak_power: float = float(component[self.COMPONENT_POWER_RATING])
                energy_capacity: float = float(component[self.STORAGE_COMPONENT_ENERGY_RATING])
                try:
                    if component[self.STORAGE_COMPONENT_DC_SYSTEM_FLAG] == 'DC':
                        dc_storage_system_flag = True
                    elif component[self.STORAGE_COMPONENT_DC_SYSTEM_FLAG] == '':
                        dc_storage_system_flag = False
                    else:
                        dc_storage_system_flag = False
                        warning_msg = 'Optional argument for storage component not recognized. '
                        '"DC" is the only valid optional argument.'
                        warnings.warn(warning_msg)
                except IndexError:
                    dc_storage_system_flag = False
                storage_component: StorageComponent = self.create_storage_component(storage_component_type, data_export,
                                                                                    peak_power, energy_capacity,
                                                                                    energy_system_id,
                                                                                    storage_id, dc_storage_system_flag)
                res.append(storage_component)
        return res

    def load_components(self, name: str, data_export: DataHandler, energy_system_id: int) -> [LoadComponent]:
        """
        Coordinates the creation of generation components specified in simulation.ini and returns created LoadComponent(s)
        :param name: name of energy system to which specified components are associated with as str
        :param data_export: DataHandler object
        :param energy_system_id: ID of energy system to which specified components are associated with as int
        :return: List of LoadComponent object(s)
        """
        load_components: [[str]] = self.__system_config.load_components
        res: [] = list()
        load_id = 0
        for component in load_components:
            system = component[self.ENERGY_SYSTEM_NAME]
            if system == name:
                load_id += 1
                load_component_type: str = component[self.COMPONENT_NAME]
                try:
                    peak_power: float = float(component[self.COMPONENT_POWER_RATING])
                except IndexError:
                    peak_power = 0
                load_component: LoadComponent = self.create_load_component(load_component_type, data_export, peak_power,
                                                                           energy_system_id, load_id)
                res.append(load_component)
        return res

    def create_generation_component(self, generation_component_type: str, data_export: DataHandler, peak_power: float,
                                    energy_system_id: int, generation_id: int) -> GenerationComponent:
        """
        Creates instance of specified GenerationComponent
        :param generation_component_type: Name of specified component as str
        :param data_export: DataHandler object
        :param peak_power: Peak power of specified component in W as float
        :param energy_system_id: ID of energy system to which specified component is associated with as int
        :param generation_id: ID of specified component as int
        :return: GenerationComponent object
        """
        if generation_component_type == DieselGenerator.__name__:
            self.__log.debug('Creating ' + GenerationComponent.DESCRIPTION + ': ' + generation_component_type)
            return DieselGenerator(peak_power, data_export, energy_system_id, generation_id,
                                   self.__general_config, self.__environmental_config)
        elif generation_component_type == WindTurbine.__name__:
            self.__log.debug('Creating ' + GenerationComponent.DESCRIPTION + ': ' + generation_component_type)
            return WindTurbine(peak_power, data_export, energy_system_id, generation_id,
                               self.__general_config, self.__environmental_config)
        elif generation_component_type == PhotovoltaicSolar.__name__:
            self.__log.debug('Creating ' + GenerationComponent.DESCRIPTION + ': ' + generation_component_type)
            return PhotovoltaicSolar(peak_power, data_export, energy_system_id, generation_id,
                                     self.__general_config, self.__environmental_config)
        else:
            options: [str] = list()
            options.append(DieselGenerator.__name__)
            options.append(WindTurbine.__name__)
            options.append(PhotovoltaicSolar.__name__)
            raise Exception(GenerationComponent.DESCRIPTION + ' ' + generation_component_type
                            + ' is unknown. Following options are available: ' + str(options))

    def create_storage_component(self, storage_component_type: str, data_export: DataHandler, peak_power: float,
                                 energy_capacity: float, energy_system_id: int, storage_id: int,
                                 dc_storage_system_flag: bool) -> StorageComponent:
        """
        Creates instance of specified StorageComponent
        :param storage_component_type: Name of specified component as str
        :param data_export: DataHandler object
        :param peak_power: Peak power of specified component in W as float
        :param energy_capacity: Energy capacity of specified component in Wh as float
        :param energy_system_id: ID of energy system to which specified component is associated with as int
        :param storage_id: ID of specified component as int
        :param dc_storage_system_flag: Specifies whether the storage system operates in a DC network only
        :return: StorageComponent object
        """
        if storage_component_type == LithiumBatterySystem.__name__:
            self.__log.debug('Creating ' + StorageComponent.DESCRIPTION + ': ' + storage_component_type)
            return LithiumBatterySystem(peak_power, energy_capacity, data_export, energy_system_id,
                                        storage_id, self.__general_config, self.__environmental_config,
                                        self.__simulation_config, dc_storage_system_flag)
        elif storage_component_type == ElectricVehicle.__name__:
            self.__log.debug('Creating ' + StorageComponent.DESCRIPTION + ': ' + storage_component_type)
            return ElectricVehicle(peak_power, energy_capacity, data_export, energy_system_id,
                                   storage_id, self.__general_config, self.__environmental_config,
                                   self.__simulation_config, dc_storage_system_flag)
        else:
            options: [str] = list()
            options.append(LithiumBatterySystem.__name__)
            options.append(ElectricVehicle.__name__)
            raise Exception(StorageComponent.DESCRIPTION + storage_component_type
                            + ' is unknown. Following options are available: ' + str(options))

    def create_load_component(self, load_component_type: str, data_export: DataHandler, peak_power: float,
                              energy_system_id: int, load_id: int) -> LoadComponent:
        """
        Creates instance of specified LoadComponent
        :param load_component_type: Name of specified component as str
        :param data_export: DataHandler object
        :param peak_power: Peak power of specified component in W as float
        :param number_slots: Specifies the number of slots (in case of BSSLoadImplementation) as int
        :param energy_system_id: ID of energy system to which specified component is associated with as int
        :param load_id: ID of specified component as int
        :return: LoadComponent object
        """
        if load_component_type == ElectricLoadOne.__name__:
            self.__log.debug('Creating ' + LoadComponent.DESCRIPTION + ': ' + load_component_type)
            return ElectricLoadOne(peak_power, data_export, energy_system_id, load_id, self.__general_config)
        elif load_component_type == EVChargingPointDC.__name__:
            self.__log.debug('Creating ' + LoadComponent.DESCRIPTION + ': ' + load_component_type)
            return EVChargingPointDC(peak_power, data_export, energy_system_id, load_id, self.__general_config)
        elif load_component_type == EVHomeCharger.__name__:
            self.__log.debug('Creating ' + LoadComponent.DESCRIPTION + ': ' + load_component_type)
            return EVHomeCharger(peak_power, data_export, energy_system_id, load_id, self.__general_config)
        else:
            options: [str] = list()
            options.append(ElectricLoadOne.__name__)
            options.append(EVChargingPointDC.__name__)
            raise Exception(LoadComponent.DESCRIPTION + load_component_type
                            + ' is unknown. Following options are available: ' + str(options))

    def create_grid_component(self, grid_component_type: str, data_export: DataHandler, peak_power: float,
                              feed_in_enabled: bool, energy_system_id: int, grid_id: int,
                              ac_dc_converter_type: str) -> GridComponent:
        """
        Creates instance of specified GridComponent
        :param grid_component_type: Name of specified component as str
        :param data_export: DataHandler object
        :param peak_power: Peak power of specified component in W as float
        :param feed_in_enabled: Specifies if the grid is permitted to allow power feed-in as bool
        :param energy_system_id: ID of energy system to which specified component is associated with as int
        :param grid_id: ID of specified component as int
        :param ac_dc_converter_type: Type of AcDcConverter object (in case of AcDcConverterElectricityGridConnection) as str
        :return: GridComponent object
        """
        if grid_component_type == SimpleElectricityGridConnection.__name__ and ac_dc_converter_type is None:
            self.__log.debug('Creating ' + GridComponent.DESCRIPTION + ': ' + grid_component_type)
            return SimpleElectricityGridConnection(peak_power, feed_in_enabled, data_export,
                                                   energy_system_id, grid_id, self.__general_config,
                                                   self.__environmental_config)
        elif grid_component_type == PandapowerElectricityGridConnection.__name__ and ac_dc_converter_type is None:
            self.__log.debug('Creating ' + GridComponent.DESCRIPTION + ': ' + grid_component_type)
            return PandapowerElectricityGridConnection(peak_power, feed_in_enabled, data_export,
                                                       energy_system_id, grid_id, self.__general_config,
                                                       self.__environmental_config)
        else:
            options: [str] = list()
            options.append(SimpleElectricityGridConnection.__name__)
            options.append(PandapowerElectricityGridConnection.__name__)
            raise Exception(GridComponent.DESCRIPTION + grid_component_type
                            + ' is unknown. Following options are available: ' + str(options))
