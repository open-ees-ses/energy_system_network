import pytest
from energy_system_network.commons.state.energy_system_component.storage_component.storage_component_state import \
    StorageComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.storage_components.lithium_battery_system import \
    LithiumBatterySystem
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.constants_energy_sys_network import ROOT_PATH
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from random import randint
from random import random
from configparser import ConfigParser


class TestLithiumBatterySystem:
    """
    Test class for verifying the functionality of the LithiumBatterySystem.

    Attributes:
        config (): Configuration object.
        general_config (GeneralSimulationConfig): General simulation configuration.
        start_time (int): Start time for the tests.
        duration (int): Total duration for the tests.
        sample_time (int): Sampling time for the tests.
        time_step_range (list): Time step ranges for the tests.
        peak_power (float): Peak power setting.
        energy_form (str): Energy form setting ('Electricity').
        energy_capacity (float): Energy capacity setting.
        data_export (DataHandler): Data export handler.
        energy_system_id (int): ID for the energy system.
        storage_id (int): ID for the storage system.
        environmental_analysis_config (EnvironmentalAnalysisConfig): Environmental analysis configuration.
        dc_storage_system_flag (bool): Flag indicating if the system is a DC storage system.
        uut (LithiumBatterySystem): Unit under test.

    Methods:
        test_next(time_step): Test the `next` method.
        test_calculate_production_emissions(): Test the production emissions calculation.
        test_calculate_eol_emissions(): Test the end of life emissions calculation.
        test_calculate_operation_emissions(): Test the operation emissions calculation.
        test_get_operation_losses(): Test getting the operation losses.
        test_write_component_wise_lca_results(): Test writing the LCA results.
        test_get_production_emissions(): Test getting production emissions.
        test_get_end_of_life_emissions(): Test getting the end of life emissions.
        test_get_name(): Test getting the name of the system.
        test_get_storage_state(): Test getting the state of the storage.
        test_get_component_ID(): Test getting the component ID.
        test_state(): Test getting the state.
    """
    config = None
    general_config: GeneralSimulationConfig = GeneralSimulationConfig(config)
    start_time = int(general_config.start)
    duration = int(general_config.duration)
    sample_time = int(general_config.timestep)
    time_step_range = list(range(start_time, start_time + duration, 20*sample_time))  # amend 20 to change no. of tests
    peak_power = 1e6 #randint(1, 1e6)
    energy_form = 'Electricity'
    energy_capacity = 1370e3 #randint(1, 1370e3)
    data_export = DataHandler(result_dir=ROOT_PATH + "results/energy_system_simulation/", config=general_config)
    energy_system_id = 1
    storage_id = 1
    environmental_analysis_config = EnvironmentalAnalysisConfig(config=ConfigParser())
    dc_storage_system_flag = False

    uut = LithiumBatterySystem(peak_power, energy_capacity, data_export, energy_system_id, storage_id, general_config,
                               environmental_analysis_config, config, dc_storage_system_flag)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_next(self, time_step):
        """
        Test the `next` method of the LithiumBatterySystem.

        Args:
            time_step (int): The timestep for which the method is tested.

        Return:
            None: Asserts ensure expected behavior.
        """
        power = randint(-self.peak_power*10, self.peak_power*10)
        assert self.peak_power > self.uut.next(time=time_step+1, energy_form='Electricity', target_electrical_power=power, charging_energy_carbon_intensity=random())
        self.uut.update()

        #tests for calculate_stored_energy_carbon_intensity & calculate_stored_energy_carbon_intensity tested below
        assert isinstance(self.uut.calculate_carbon_intensity_stored_energy(), float) or \
               isinstance(self.uut.calculate_carbon_intensity_stored_energy(), int)

        assert isinstance(self.uut.calculate_carbon_intensity_stored_energy(), float) or \
               isinstance(self.uut.calculate_carbon_intensity_stored_energy(), int)

        # output is zero if energy form is not electricity
        # power = randint(-self.peak_power, self.peak_power)
        assert self.uut.next(time=time_step, energy_form='Heat', target_electrical_power=power) == 0

    def test_calculate_production_emissions(self):
        """
        Test the `calculate_production_emissions` method of the LithiumBatterySystem.

        Return:
            None: Asserts ensure expected behavior.
        """
        assert isinstance(self.uut.calculate_production_emissions(), float)

    # def test_get_hvac_rating(self): # removed from lithium_battery_system.py
    #     assert isinstance(self.uut.get_hvac_rating(), float)

    def test_calculate_eol_emissions(self):
        """
        Test the `calculate_eol_emissions` method of the LithiumBatterySystem.

        Return:
            None: Asserts ensure expected behavior.
        """
        assert isinstance(self.uut.calculate_eol_emissions(),float)

    def test_calculate_operation_emissions(self):
        """
        Test the `calculate_operation_emissions` method of the LithiumBatterySystem.

        Return:
            None: Asserts ensure expected behavior.
        """
        assert isinstance(self.uut.calculate_operation_emissions(), float)

    def test_get_operation_losses(self):
        """
        Test the `operation_losses` of the LithiumBatterySystem.

        Return:
            None: Asserts ensure expected behavior.
        """
        assert isinstance(self.uut.operation_losses(), float)

    # @pytest.mark.parametrize("time_step", time_step_range)
    # def test_calculate_stored_energy_carbon_intensity(self, time_step):
    #     power = randint(-self.peak_power, self.peak_power)
    #     carbon_intensity = random()
    #     self.uut.next(time=time_step, energy_form='Electricity', target_electrical_power=power,
    #                   charging_energy_carbon_intensity=carbon_intensity)
    #     self.uut.update()
    #     a = 1
    #     assert isinstance(self.uut.calculate_stored_energy_carbon_intensity(), float) or \
    #            isinstance(self.uut.calculate_stored_energy_carbon_intensity(), int)
    #
    # @pytest.mark.parametrize("time_step", time_step_range)
    # def test_get_stored_energy_carbon_intensity(self, time_step):
    #     power = randint(-self.peak_power, self.peak_power)
    #     carbon_intensity = random()
    #     self.uut.next(time=time_step, energy_form='Electricity', target_electrical_power=power,
    #                   charging_energy_carbon_intensity=carbon_intensity)
    #     self.uut.update()
    #     assert isinstance(self.uut.calculate_stored_energy_carbon_intensity(), float) or \
    #            isinstance(self.uut.calculate_stored_energy_carbon_intensity(), int)

    def test_write_component_wise_lca_results(self):
        """
        Test the `write_component_wise_lca_results` method of the LithiumBatterySystem.

        Return:
            None: Functionality needs to be checked for file writing.
        """
        pass  # check if the correct file written in relevant folder (check exists or indirectory....)

    def test_get_production_emissions(self):
        """
        Test the `production_emissions` method of the LithiumBatterySystem.

        Return:
            None: Asserts ensure expected behavior.
        """
        assert isinstance(self.uut.production_emissions, float)

    def test_get_end_of_life_emissions(self):
        """
        Test the `end_of_life_emissions` attribute of the LithiumBatterySystem.

        Return:
            None: Asserts ensure expected behavior.
        """

        assert isinstance(self.uut.end_of_life_emissions, float)

    def test_get_name(self):
        """
        Test the `name` attribute of the LithiumBatterySystem.

        Return:
            None: Asserts ensure expected behavior.
        """

        assert isinstance(self.uut.name, str)

    def test_get_storage_state(self):
        """
        Test the `state` attribute of the LithiumBatterySystem.

        Return:
            None: Asserts ensure expected behavior.
        """

        assert isinstance(self.uut.state, StorageComponentState)

    def test_get_component_ID(self):
        """
        Test the `component_ID` attribute of the LithiumBatterySystem.

        Return:
            None: Asserts ensure expected behavior.
        """

        assert isinstance(self.uut.component_ID, str)

    def test_state(self):
        """
        Test the `state` attribute and `close` method of the LithiumBatterySystem.

        Return:
            None: Asserts ensure expected behavior.
        """
        assert isinstance(self.uut.state, StorageComponentState)
        self.uut.close()
