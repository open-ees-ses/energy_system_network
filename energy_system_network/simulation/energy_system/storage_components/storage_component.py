from abc import abstractmethod
from typing import Union
from energy_system_network.commons import unit_converter
from energy_system_network.commons.state.energy_system_component.storage_component.storage_component_state import StorageComponentState
from energy_system_network.simulation.energy_system.energy_system_component import EnergySystemComponent


class StorageComponent(EnergySystemComponent):
    """
    Abstract base class representing a storage component in an energy system.

    Attributes:
        DESCRIPTION (str): Static description of the storage component.
        __energy_capacity (float): The energy capacity of the storage component in Wh.
        __states (StorageComponentState): Current state of the storage component.

    Methods:
        __init__(data_export = None): Initialize a storage component with optional data export.
        reset_profiles(adapted_time: float): Abstract method to reset profiles.
        energy_capacity(): Property to get/set the energy capacity in Wh.
        state(): Property to get/set the storage component state.
        get_emissions_per_joule(): Calculate and return specific emissions per joule of discharged energy in kgCO2eq/J.
        carbon_intensity_stored_energy(): Abstract method to get carbon intensity of stored energy.
        set_initial_carbon_intensity_stored_energy(average_grid_carbon_intensity: float): Abstract method to set initial carbon intensity of stored energy.
        set_carbon_intensity_charging_energy(carbon_intensity_charging_energy: float): Abstract method to set carbon intensity of charging energy.
        next(time: float, energy_form: str = None, power: Union[float, None] = 0,
            charging_energy_carbon_intensity: float = 0, adapted_time: float = 0) -> float:
            Abstract method for the next step in the simulation.
        close(): Close all open resources.
    """
    DESCRIPTION: str = 'Storage Component '

    def __init__(self, data_export = None):
        """
        Initialize a storage component with optional data export.

        Args:
            data_export: Optional data export handler.
        """
        super().__init__(data_export)
        self.__energy_capacity: float = 0.0  # in Wh
        self.__states = StorageComponentState(0, 0)

    @abstractmethod
    def reset_profiles(self, adapted_time: float) -> None:
        """
        Abstract method to reset profiles for the storage component.

        Args:
            adapted_time (float): Time after adjustment.
        """
        pass

    @property
    def energy_capacity(self) -> float:
        """
        returns energy capacity of StorageComponent in Wh
        :return: energy_capacity as float
        """
        return self.__energy_capacity

    @energy_capacity.setter
    def energy_capacity(self, value: float) -> None:
        """
        Set the energy capacity of the StorageComponent in Wh.

        Args:
            value (float): New energy capacity value in Wh to be set.
        """
        self.__energy_capacity = value

    @property
    def state(self) -> StorageComponentState:
        """
        Get the current state of the storage component.

        Return:
            StorageComponentState: The current state of the storage component.
        """
        return self.__states

    @state.setter
    def state(self, value: StorageComponentState) -> None:
        """
        Set the current state of the storage component.

        Args:
            value (StorageComponentState): The new state to set for the storage component.
        """
        self.__states = value

    def get_emissions_per_joule(self) -> float:
        """
        Calculate and return specific emissions per joule of discharged energy in kgCO2eq/J.

        Return:
            float: Specific emissions per joule of discharged energy.
        """
        return self.state.soci * (unit_converter.g_to_kg(1)/unit_converter.kWh_to_J(1))  # in kgCO2eq/J

    @abstractmethod
    def carbon_intensity_stored_energy(self) -> float:
        """
        Abstract method to get the carbon intensity of the stored energy.

        Return:
            float: Carbon intensity of the stored energy.
        """
        pass

    @abstractmethod
    def set_initial_carbon_intensity_stored_energy(self, average_grid_carbon_intensity: float) -> None:
        """
        Abstract method to set the initial carbon intensity of the stored energy based on the average grid carbon intensity.

        Args:
            average_grid_carbon_intensity (float): Average carbon intensity from the grid.
        """
        pass

    @abstractmethod
    def set_carbon_intensity_charging_energy(self, carbon_intensity_charging_energy: float) -> None:
        """
        Abstract method to set the carbon intensity of the charging energy.

        Args:
            carbon_intensity_charging_energy (float): Carbon intensity value for the charging energy.
        """
        pass

    @abstractmethod
    def next(self, time: float, energy_form: str = None, power: Union[float, None] = 0, charging_energy_carbon_intensity: float = 0, adapted_time: float = 0) -> float:
        """
        Abstract method for the next step in the simulation.

        Args:
            time (float): Current time.
            energy_form (str, optional): Type of energy form. Default is None.
            power (Union[float, None], optional): Power value. Default is 0.
            charging_energy_carbon_intensity (float, optional): Carbon intensity of the charging energy. Default is 0.
            adapted_time (float, optional): Time after adjustment. Default is 0.

        Return:
            float: Resultant value after the next step.
        """
        pass

    def close(self) -> None:
        """
        closes all open resources
        """
        self.data_export.transfer_data(self.__states.to_export())
