from configparser import ConfigParser
from energy_system_network.commons import unit_converter
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.output.battery_component_emissions import BatteryComponentEmissionsWriter
from energy_system_network.profiles.profile_handler import ProfileHandler
from energy_system_network.commons.state.energy_system_component.storage_component.electricity_storage_component_state import \
    ElectricityStorageComponentState
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from energy_system_network.config.energy_system_components.lithium_battery import LithiumBatteryConfig
from energy_system_network.config.energy_system_components.simses_config_generator import SimSESConfigGenerator
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.profile_config import ProfileConfig
from energy_system_network.config.simulation.storage_config import StorageConfig
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent
from energy_system_network.simulation.energy_system.supporting_components import ac_dc_converter_model


class ElectricVehicle(StorageComponent):
    """
    Represents an Electric Vehicle as a storage component within an energy system.

    Attributes:
        ELECTRIC_VEHICLE_BATTERY_SETUP (str): Setup configuration for the EV battery.
        energy_form (List[str]): Form of energy used, defaulting to electricity.
        peak_power (float): Maximum power for home charging in Watts.
        energy_capacity (float): Energy capacity of the battery in Watt-hours.
        name (str): Identifier for the energy system concatenated with storage ID and class name.
        component_ID (str): Identifier constructed from energy system ID and storage ID.
        __config_profile (ProfileConfig): Configuration for profile settings.
        __general_simulation_config (GeneralSimulationConfig): General simulation configuration.
        __environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.
        __config_storage (StorageConfig): Configuration for storage settings.
        __lithium_battery_config (LithiumBatteryConfig): Configuration for lithium battery settings.
        __bidirectional (bool): Indicates whether the EV battery is bidirectional.
        __idle_soc (float): Idle state of charge for the battery.
        ev_parked_profile (ProfileHandler): Profile handler for when EV is parked.
        ev_drive_profile (ProfileHandler): Profile handler for when EV is driving.
        __sample_time (float): Time interval for each sample in simulation.
        __dc_storage_system_flag (bool): Flag indicating if the storage system is purely DC.
        state (ElectricityStorageComponentState): State object to hold various states for the EV.
        __simses (SimSES): Simulation object for running various simulations.
        __simses_system_parameters (SystemParametersReader): Parameter reader for the SimSES simulation.
        __number_containers (int): Total number of containers in the SimSES simulation.
        __hvac_capacity (float): HVAC capacity in Kilowatts.
        __initial_capacity (float): Initial capacity of the battery in Watt-hours.
        __carbon_intensity_stored_energy (float): Carbon intensity of stored energy in g/kWh.
        __carbon_intensity_charging_energy (float): Carbon intensity of charging energy in g/kWh.
        __terminal_power (float): Terminal power, can be AC or DC.
        __instantaneous_ch_energy_throughput (float): Instantaneous charging energy throughput.
        __cumulative_ch_energy_throughput (float): Cumulative charging energy throughput.
        __instantaneous_ch_losses (float): Instantaneous charging losses.
        __cumulative_ch_losses (float): Cumulative charging losses.
        __instantaneous_ch_efficiency (float): Instantaneous charging efficiency.
        __cumulative_ch_efficiency (float): Cumulative charging efficiency.
        __instantaneous_dch_energy_throughput (float): Instantaneous discharging energy throughput.
        __cumulative_dch_energy_throughput (float): Cumulative discharging energy throughput.
        __instantaneous_dch_losses (float): Instantaneous discharging losses.
        __cumulative_dch_losses (float): Cumulative discharging losses.
        __instantaneous_dch_efficiency (float): Instantaneous discharging efficiency.
        __cumulative_dch_efficiency (float): Cumulative discharging efficiency.
        __storage_power (float): Current power of storage.
        __reference_drive_power (float): Reference power for driving.
        __time (float): Current time in the simulation.
        __adapted_time (float): Adapted time in the simulation.
        __available (int): Indicator of availability (binary).
        __deferred_drive_power (list[float]): List of deferred drive power values.
        __fast_charging_power (float): Power level for fast charging.
        __max_fast_charging_duration (float): Maximum duration for fast charging.
        __max_fast_charging_timesteps (int): Maximum number of timesteps for fast charging.
        __fast_charging_active_since (int): Number of timesteps since fast charging started.
        __production_emissions_cells (float): Production emissions from cells.
        __production_emissions_pe (float): Production emissions from PE.
        __production_emissions_housing (float): Production emissions from housing.
        __production_emissions_hvac (float): Production emissions from HVAC.
        __production_emissions_miscellaneous_electronics (float): Production emissions from miscellaneous electronics.
        __eol_emissions_cells (float): End-of-life emissions from cells.
        __eol_emissions_pe (float): End-of-life emissions from PE.
        __eol_emissions_housing (float): End-of-life emissions from housing.
        __eol_emissions_hvac (float): End-of-life emissions from HVAC.
        __eol_miscellaneous_electronics (float): End-of-life emissions from miscellaneous electronics.
        production_emissions (float): The amount of CO2 emissions (in kilograms) produced during the manufacturing of
            the vehicle.
        end_of_life_emissions (float): The amount of CO2 emissions (in kilograms) produced at the end of the vehicle's
            lifecycle, typically during disposal or recycling.


    Methods:
        __init__(peak_power, energy_capacity, data_export, energy_system_id, storage_id, config_general,
                environmental_analysis_config, config, dc_storage_system_flag): Initializes the ElectricVehicle object.

        next(time, energy_form, target_electrical_power, charging_energy_carbon_intensity, adapted_time)  -> float:
                Computes and returns the actual power based on target power at each timestep.

        fast_charging_handler(power_reference: float) -> float:
            Checks if fast charging is necessary, and if previous unfulfilled drive powers can be executed.

        check_deferred_power(power_reference: float) -> float:
            Checks if any deferred drive power phases are present, and attempts to execute them.

        update() -> None:
            Updates various attributes of the battery's state based on the current conditions.

        compute_efficiency() -> None:
            Computes and updates the efficiency at the current timestep and average efficiency up to this timestep.

        calculate_operation_emissions() -> float:
            Calculates the emissions due to inefficiencies in the Li-on BESS for each timestep.

        operation_losses() -> float:
            Returns the total operational losses for the battery component in the current timestep.

        set_initial_carbon_intensity_stored_energy(average_grid_carbon_intensity: float) -> None:
            Sets the initial carbon intensity of the stored energy based on the provided grid carbon intensity.

        set_carbon_intensity_charging_energy(carbon_intensity_charging_energy: float) -> None:
            Sets the carbon intensity of the charging energy (method implementation missing in the provided code).

        calculate_carbon_intensity_stored_energy() -> float:
            Calculates the carbon intensity of the energy stored in the StorageComponent at the current timestep.

        carbon_intensity_stored_energy() -> float:
            Placeholder method (implementation missing in the provided code).

        calculate_production_emissions() -> float:
            Calculates the total production emissions of all components in the Lithium-ion battery.

        calculate_eol_emissions() -> float:
            Calculates the total end-of-life emissions of all components in the Lithium-ion BESS.

        write_component_wise_emissions(result_path_simses: str, simses_simulation_name: str) -> None:
            Writes component-wise emissions for the production and EOL phases for the Li-ion BESS and saves it.

        reset_profiles(adapted_time: float) -> None:
            Resets the vehicle's profiles to their default values.

        close() -> None:
            Performs necessary cleanup actions, possibly including shutting down connections or freeing up resources.

    """
    ELECTRIC_VEHICLE_BATTERY_SETUP: str = 'electric_vehicle_battery_setup'

    def __init__(self, peak_power: float, energy_capacity: float, data_export, energy_system_id, storage_id,
                 config_general: GeneralSimulationConfig,
                 environmental_analysis_config: EnvironmentalAnalysisConfig, config: ConfigParser,
                 dc_storage_system_flag: bool):
        """
        Initializes an ElectricVehicle object.

        Args:
            peak_power (float): Maximum power for home charging.
            energy_capacity (float): Energy capacity of the battery in Watt-hours.
            data_export: Object to handle data exporting.
            energy_system_id: Identifier for the energy system.
            storage_id: Identifier for the storage.
            config_general (GeneralSimulationConfig): General simulation configuration.
            environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.
            config (ConfigParser): Configuration parser object.
            dc_storage_system_flag (bool): Flag indicating if the storage system is purely DC.

        Return:
            None
        """
        from simses.main import SimSES
        from simses.commons.state.parameters_reader import SystemParametersReader

        super().__init__(data_export)
        self.energy_form: [str] = [EnergyForm.ELECTRICITY]
        self.peak_power = peak_power  # in W, this only represents the home charger power
        self.energy_capacity = energy_capacity  # in Wh

        # Instance identification details
        self.name = str(energy_system_id) + str(storage_id) + self.__class__.__name__
        self.component_ID = str(energy_system_id) + '.' + str(storage_id)

        # Configs
        self.__config_profile: ProfileConfig = ProfileConfig()
        self.__general_simulation_config: GeneralSimulationConfig = config_general
        self.__environmental_analysis_config = environmental_analysis_config
        self.__config_storage = StorageConfig(config)
        self.__lithium_battery_config = LithiumBatteryConfig(config_name=self.ELECTRIC_VEHICLE_BATTERY_SETUP)
        self.__bidirectional: bool = self.__lithium_battery_config.bidirectional
        self.__idle_soc: float = self.__lithium_battery_config.idle_soc
        battery_drive_power = self.__lithium_battery_config.ac_dc_power  # in W, represents drivetrain power capability

        self.ev_parked_profile = ProfileHandler(self.__config_profile.ev_parked_profile, config_general, scaling=1)
        self.ev_parked_profile.capacity_factor = self.ev_parked_profile.calculate_capacity_factor()
        self.ev_drive_profile = ProfileHandler(self.__config_profile.ev_drive_power_profile, config_general, scaling=1)

        # General variables
        self.__sample_time = config_general.timestep
        self.__dc_storage_system_flag = dc_storage_system_flag  # True: if the storage system is a purely DC system

        # Set up logging of state
        self.state = ElectricityStorageComponentState(energy_system_id, storage_id)
        self.data_export.add_export_class(
            self.state)  # Ensures that only those states are logged which are currently used
        self.data_export.transfer_data(self.state.to_export())  # initial timestep

        # SimSES Instantiation, overwrite SimSES Configs to match Energy System Network Configs
        config_creator = SimSESConfigGenerator(config_general, self.__config_storage, self.__lithium_battery_config,
                                               battery_drive_power, self.energy_capacity, self.__dc_storage_system_flag)
        sim_config: ConfigParser = config_creator.create_simses_simulation_config()
        analysis_config: ConfigParser = config_creator.create_simses_analysis_config()

        result_path_simses = data_export.get_result_folder_name() + '/'
        simulation_name: str = 'SimSES_' + self.name

        self.__simses: SimSES = SimSES(result_path_simses, simulation_name,
                                       do_simulation=True, do_analysis=True,
                                       simulation_config=sim_config,
                                       analysis_config=analysis_config)

        # Read SystemParameters from instantiated SimSES object
        self.__simses_system_parameters: SystemParametersReader = SystemParametersReader(result_path_simses,
                                                                                         simulation_name)

        # Initialize variables
        self.__number_containers = self.__simses_system_parameters.get_total_containers()
        self.__hvac_capacity = unit_converter.W_to_kW(self.__lithium_battery_config.hvac_thermal_power)  # in KW
        self.__initial_capacity = self.__simses.state.capacity  # in Wh
        self.__carbon_intensity_stored_energy = 0  # in g/kWh
        self.__carbon_intensity_charging_energy = 0  # in g/kWh

        self.__terminal_power = 0  # as the storage system can be a DC as as well as an AC coupled system
        self.state.energy_content = self.__simses.state.capacity * self.__simses.state.soc

        self.__instantaneous_ch_energy_throughput = 0
        self.__cumulative_ch_energy_throughput = 0
        self.__instantaneous_ch_losses = 0
        self.__cumulative_ch_losses = 0
        self.__instantaneous_ch_efficiency = 0
        self.__cumulative_ch_efficiency = 0.9

        self.__instantaneous_dch_energy_throughput = 0
        self.__cumulative_dch_energy_throughput = 0
        self.__instantaneous_dch_losses = 0
        self.__cumulative_dch_losses = 0
        self.__instantaneous_dch_efficiency = 0
        self.__cumulative_dch_efficiency = 0.9

        # Initialize variables
        self.__storage_power = 0
        self.__reference_drive_power = 0
        self.__time = 0
        self.__adapted_time = 0
        self.__available = 0

        self.__deferred_drive_power: list[float] = list()
        self.__fast_charging_power = 150e3  # W
        self.__max_fast_charging_duration = 2700  # s
        self.__max_fast_charging_timesteps = round(self.__max_fast_charging_duration/self.__sample_time)  # in number
        self.__fast_charging_active_since = 0  # in number of timesteps

        # Calculate emissions for production and EOL
        self.__production_emissions_cells = 0
        self.__production_emissions_pe = 0
        self.__production_emissions_housing = 0
        self.__production_emissions_hvac = 0
        self.__production_emissions_miscellaneous_electronics = 0

        self.__eol_emissions_cells = 0
        self.__eol_emissions_pe = 0
        self.__eol_emissions_housing = 0
        self.__eol_emissions_hvac = 0
        self.__eol_miscellaneous_electronics = 0

        self.__time = 0
        self.__adapted_time = 0

        self.production_emissions = self.calculate_production_emissions()
        self.end_of_life_emissions = self.calculate_eol_emissions()
        self.write_component_wise_emissions(result_path_simses, simulation_name)

    def next(self, time: float, energy_form: str = None, target_electrical_power: float = 0,
             charging_energy_carbon_intensity: float = 0,
             adapted_time: float = 0) -> float:
        """
        Computes and returns the actual power for the battery model based on target power at each timestep.

        Args:
            time (float): Epoch timestamp indicating the current time.
            energy_form (str, optional): Type of energy form. Defaults to None.
            target_electrical_power (float, optional): Target power in Watts. Defaults to 0.
            charging_energy_carbon_intensity (float, optional): Carbon intensity of charging energy. Defaults to 0.
            adapted_time (float, optional): Epoch timestamp for adapted time. Defaults to 0.

        Return:
            float: Actual power in Watts. Positive for discharge, negative for charge.
        """
        if adapted_time > self.__adapted_time:
            self.__adapted_time = adapted_time
            self.__simses.reset_profiles(adapted_time)
        self.__time = time

        if energy_form == EnergyForm.ELECTRICITY:
            self.__available = self.ev_parked_profile.next(time, adapted_time)  # binary, 1 if available
            drive_power = self.ev_drive_profile.next(time, adapted_time)  # in W
            self.__reference_drive_power = drive_power
            self.__carbon_intensity_charging_energy = 0.0

            if abs(self.__available - 1) < 1e-5:  # If available
                # Limit charging power to Home Charger power
                charging_power_limit = min(self.peak_power, abs(target_electrical_power))
                # Follow V2H power targets only when SOC > IDLE_SOC
                if self.__bidirectional and self.state.soc > self.__idle_soc:
                    # neg: dch, pos: ch
                    power_reference = -charging_power_limit if target_electrical_power < 0 else charging_power_limit
                else:
                    # Otherwise continue to attempt charging the EV
                    power_reference = self.peak_power  # Limit charging power to Home Charger power
            else:
                power_reference = drive_power  # neg: dch, pos: ch

            deferred_power = self.check_deferred_power(power_reference)  # Check for deferred power
            if deferred_power < 0:  # If a drive has been deferred
                power_reference = deferred_power  # new reference if the deferred drive power
            fast_charging_power = self.fast_charging_handler(power_reference)

            fast_charge = 0
            if fast_charging_power > 0:
                fast_charge = 1
                power_reference = 0  # Get fast charging done first
                self.__available = 0  # EV is not at home if being fast charged

            if abs(fast_charging_power) < 1e-5 and deferred_power < 0:
                self.__available = 0  # Deferred drive power is executed, EV is not at home

            self.__simses.run_one_simulation_step(time, power_reference + fast_charging_power)
            ac_power = self.__simses.state.ac_power_delivered * -1
            if ac_power < 0:  # ch
                # regenerative charging is excluded by multiplying with the availability
                self.__carbon_intensity_charging_energy = charging_energy_carbon_intensity * abs((fast_charge - self.__available))  # in gCO2eq/kWh
            return ac_power * abs((fast_charge - self.__available))  # Returns power value to home EMS only if available
        else:
            return 0.0

    def fast_charging_handler(self, power_reference: float) -> float:
        """
        Checks if fast charging is necessary, and if previous unfulfilled drive powers can be executed
        :param power_reference: power reference in W as float
        :return: effective power (either fast charging, or deferred drive power) in W as float
        """
        # Check if SOC sufficient to execute reference power
        # Maintain 10% reserve and fast charge until SOC = 0.8
        if power_reference < 0:
            energy_required = unit_converter.J_to_Wh(abs(power_reference * self.__sample_time))/\
                              self.__cumulative_dch_efficiency  # in Wh
            is_low_soc = self.state.soc < 0.1
            is_mid_soc = 0.1 < self.state.soc < 0.8
            is_fast_charging_active = 0 < self.__fast_charging_active_since < self.__max_fast_charging_timesteps
            insufficient_energy = self.state.energy_content < energy_required

            if is_low_soc or is_mid_soc and is_fast_charging_active or insufficient_energy:
                self.__deferred_drive_power.insert(0, power_reference)  # Defer this load until fast charging is complete
                self.__fast_charging_active_since += 1
                return self.__fast_charging_power  # W
            else:
                self.__fast_charging_active_since = 0
                return 0.0

        else:
            self.__fast_charging_active_since = 0
            return 0.0

    def check_deferred_power(self, power_reference: float) -> float:
        """
        Checks if any deferred drive power phases are present, and attempts to execute them
        :param power_reference: power reference in current timestep in W as float
        :return: deferred power in W as float
        """
        if self.__deferred_drive_power and abs(self.__available - 1) < 1e-5 or \
                self.__deferred_drive_power and abs(power_reference) < 1e-5:
            deferred_power = self.__deferred_drive_power.pop(0)  # Get and remove the first element
            return deferred_power
        else:
            return 0.0

    def update(self):
        """
        Updates the state attributes of the storage component based on the current power status, energy content, and
        emissions.
        :return: None
        """
        self.state.time = self.__time
        self.state.power = self.__simses.state.ac_power_delivered  # in W
        self.state.availability = self.__available  # binary
        self.state.reference_drive_power = self.__reference_drive_power  # in W
        self.state.power_loss = self.operation_losses()  # in W
        self.state.soc = self.__simses.state.soc  # in p.u.
        self.state.soh = self.__simses.state.capacity / self.__initial_capacity  # in p.u.
        # self.state.request_fulfilment = self.__simses.state.fulfillment  # in p.u.
        self.state.carbon_intensity_charging_energy = self.__carbon_intensity_charging_energy  # gCO2eq/kWh
        self.compute_efficiency()
        if self.state.power < 0:  # if storage is discharging
            self.state.soci = self.__carbon_intensity_stored_energy  # in g/kWh
            self.state.instantaneous_dch_efficiency = self.__instantaneous_ch_efficiency
            self.state.cumulative_dch_efficiency = self.__cumulative_dch_efficiency
            self.state.instantaneous_ch_efficiency = 0
            self.state.cumulative_ch_efficiency = self.__cumulative_ch_efficiency
            self.state.dec_emissions = abs(unit_converter.g_to_kg(self.state.soci) \
                                       * unit_converter.J_to_kWh(self.state.power * self.__sample_time) \
                                       * (1 - self.state.availability))  # in kgCO2
        else:  # if storage is charging
            self.state.soci = self.calculate_carbon_intensity_stored_energy()  # gCO2eq/kWh
            self.state.instantaneous_ch_efficiency = self.__instantaneous_ch_efficiency
            self.state.cumulative_ch_efficiency = self.__cumulative_ch_efficiency
            self.state.instantaneous_dch_efficiency = 0
            self.state.cumulative_dch_efficiency = self.__cumulative_dch_efficiency
            self.state.dec_emissions = 0
        self.state.operation_emissions = self.calculate_operation_emissions()  # kgCO2eq of CO2
        self.state.energy_content = self.__simses.state.capacity * self.__simses.state.soc
        # Important: energy content in Wh, state.energy_content updated last as
        # calculate_carbon_intensity_stored_energy() requires previous value of state.energy_content

        if abs(self.__simses.state.ac_power_delivered) > 1e-6:
            self.state.run_status = 1
        else:
            self.state.run_status = 0
        self.data_export.transfer_data(self.state.to_export())

    def compute_efficiency(self) -> None:
        """
        computes and updates the efficiency at current timestep and average efficiency till timestep
        :return: None
        """
        if self.state.power < 0:  # i.e. a discharge request took place
            self.__instantaneous_dch_energy_throughput = abs(self.state.power * self.__sample_time)  # in J
            self.__cumulative_dch_energy_throughput += self.__instantaneous_dch_energy_throughput
            self.__instantaneous_dch_losses = abs(self.state.power_loss) * self.__sample_time  # in J
            self.__cumulative_dch_losses += self.__instantaneous_dch_losses
            try:
                self.__instantaneous_dch_efficiency = 1 - self.__instantaneous_dch_losses / \
                                                      (self.__instantaneous_dch_losses + self.__instantaneous_dch_energy_throughput)  # in p.u.
            except ZeroDivisionError:
                self.__instantaneous_dch_efficiency = 0
            cumulative_dch_efficiency = 1 - self.__cumulative_dch_losses / (self.__cumulative_dch_losses + self.__cumulative_dch_energy_throughput)  # in p.u.
            if cumulative_dch_efficiency > 0:
                self.__cumulative_dch_efficiency = cumulative_dch_efficiency

        elif self.state.power > 0:  # i.e. a charge request took place
            self.__instantaneous_ch_energy_throughput = abs(self.state.power * self.__sample_time)  # in J
            self.__cumulative_ch_energy_throughput += self.__instantaneous_ch_energy_throughput
            self.__instantaneous_ch_losses = abs(self.state.power_loss) * self.__sample_time  # in J
            self.__cumulative_ch_losses += self.__instantaneous_ch_losses
            try:
                self.__instantaneous_ch_efficiency = 1 - self.__instantaneous_ch_losses / self.__instantaneous_ch_energy_throughput
            except ZeroDivisionError:
                self.__instantaneous_ch_efficiency = 0
            cumulative_ch_efficiency = 1 - self.__cumulative_ch_losses / self.__cumulative_ch_energy_throughput
            if cumulative_ch_efficiency > 0:
                self.__cumulative_ch_efficiency = cumulative_ch_efficiency
        else:
            pass

    def calculate_operation_emissions(self) -> float:
        """
        Calculates the emissions due to inefficiencies in the Li-on BESS for each timestep in kgCO2eq
        :return: returns the value of emissions in current timestep in kgCO2 as float
        """
        loss_energy_at_timestep = unit_converter.J_to_kWh(self.state.power_loss * self.__sample_time)  # in kWh
        if self.state.power < 0:  # i.e. a discharge request took place
            return unit_converter.g_to_kg(loss_energy_at_timestep * self.__carbon_intensity_stored_energy)  # in kg
        elif self.state.power > 0:  # i.e. a charge request took place
            return unit_converter.g_to_kg(loss_energy_at_timestep * self.__carbon_intensity_charging_energy)  # in kg
        else:
            return 0.0

    def operation_losses(self) -> float:
        """
        Computes the sum of various operation losses in the storage system.
        :return: Total operational losses in W as float.
        """
        total_operation_losses: float = self.__simses.state.pe_losses + self.__simses.state.aux_losses + \
                                        self.__simses.state.dc_power_loss + self.__simses.state.storage_power_loss
        return total_operation_losses  # in W

    def set_initial_carbon_intensity_stored_energy(self, average_grid_carbon_intensity: float) -> None:
        """
        Sets the initial carbon intensity of the energy stored in the system.
        :param average_grid_carbon_intensity: Average carbon intensity of the grid in g/kWh.
        :return: None
        """

        self.__carbon_intensity_stored_energy = average_grid_carbon_intensity  # in g/kWh

    def set_carbon_intensity_charging_energy(self, carbon_intensity_charging_energy: float) -> None:
        """
        Sets the carbon intensity of the energy during charging.
        :param carbon_intensity_charging_energy: Carbon intensity during charging in g/kWh.
        :return: None
        """

        pass

    def calculate_carbon_intensity_stored_energy(self) -> float:
        """
        Calculates the carbon intensity of the energy stored in the StorageComponent at current timestep in gCO2eq/kWh
        :return: the carbon intensity of the energy stored in the StorageComponent at the current timestep as float
        """
        # return existing value if not changed, or return new value
        if self.state.energy_content < 1e-5:  # Reset value to zero, if storage is empty
            self.__carbon_intensity_stored_energy = 0
        if self.state.power > 0:  # +ve implies a charging request
            effective_charging_energy = unit_converter.J_to_kWh(self.__simses.state.dc_power_storage
                                                                * self.__sample_time)  # in kWh
            if effective_charging_energy > 0:
                self.__carbon_intensity_stored_energy = (unit_converter.Wh_to_kWh(
                    self.state.energy_content) * self.__carbon_intensity_stored_energy +
                                                         effective_charging_energy * self.__carbon_intensity_charging_energy) / (
                                                                    unit_converter.Wh_to_kWh(
                                                                        self.state.energy_content) + effective_charging_energy)  # in g / kWh
        return self.__carbon_intensity_stored_energy  # in gCO2eq/kWh

    def carbon_intensity_stored_energy(self) -> float:
        """
        Retrieves the carbon intensity of the stored energy.
        :return: Carbon intensity of the stored energy in gCO2eq/kWh as float.
        """
        pass

    def calculate_production_emissions(self) -> float:
        """
        Calculates the total production emissions of all components in the Lithium-ion battery in kgCO2eq
        :return: returns the total calculated production emissions as float
        """
        miscellaneous_electronics_share = unit_converter.pc_to_pu(self.__environmental_analysis_config.misc_electronics_production_emissions)  # % of total emissions

        self.__production_emissions_cells = self.energy_capacity * self.__environmental_analysis_config.cell_production_emissions
        number_pe_units = self.__lithium_battery_config.number_of_converters
        self.__production_emissions_pe = ac_dc_converter_model.calculate_production_emissions(unit_converter.W_to_kW(self.peak_power), number_pe_units)  # power is to be passed in kW
        self.__production_emissions_housing = 0  # Assumed EV housing to be negligible
        self.__production_emissions_hvac = 0  # Assumed passive cooling
        self.__production_emissions_miscellaneous_electronics = miscellaneous_electronics_share * (
                self.__production_emissions_cells + self.__production_emissions_pe + self.__production_emissions_housing + self.__production_emissions_hvac) / (1 - miscellaneous_electronics_share)
        total_production_emissions = self.__production_emissions_cells + self.__production_emissions_pe + self.__production_emissions_housing \
                                     + self.__production_emissions_hvac + self.__production_emissions_miscellaneous_electronics
        return total_production_emissions  # in kgCO2eq

    def calculate_eol_emissions(self) -> float:
        """
        Calculates the total end-of-life emissions of all components in the Lithium-ion BESS in kgCO2eq
        :return: returns the total calculated end-of-life emissions as float
        """
        miscellaneous_electronics_eol_share = unit_converter.pc_to_pu(self.__environmental_analysis_config.misc_electronics_eol_emissions)  # % of misc. electronics production emissions
        self.__eol_emissions_cells = self.energy_capacity * self.__environmental_analysis_config.cell_eol_emissions
        self.__eol_emissions_pe = unit_converter.W_to_kW(self.peak_power) * self.__environmental_analysis_config.power_electronics_eol_emissions
        self.__eol_emissions_housing = self.__number_containers * self.__environmental_analysis_config.housing_eol_emissions
        self.__eol_emissions_hvac = self.__number_containers * self.__environmental_analysis_config.hvac_eol_emissions
        self.__eol_miscellaneous_electronics = - miscellaneous_electronics_eol_share * self.__production_emissions_miscellaneous_electronics
        total_eol_emissions = self.__eol_emissions_cells + self.__eol_emissions_pe + self.__eol_emissions_housing + self.__eol_emissions_hvac + self.__eol_miscellaneous_electronics
        return total_eol_emissions  # in kgCO2eq

    def write_component_wise_emissions(self, result_path_simses: str, simses_simulation_name: str) -> None:
        """
        Writes component-wise emissions for the production and EOL phases for the Li-ion BESS and saves it
        :param result_path_simses: simses result path as str
        :param simses_simulation_name: simses simulation name as str
        :return: None
        """
        emissions_writer: BatteryComponentEmissionsWriter = BatteryComponentEmissionsWriter(self.name,
                                                                                            result_path_simses,
                                                                                            simses_simulation_name)
        production = dict()
        production[BatteryComponentEmissionsWriter.CELLS] = self.__production_emissions_cells
        production[BatteryComponentEmissionsWriter.ACDC_CONVERTER] = self.__production_emissions_pe
        production[BatteryComponentEmissionsWriter.HOUSING] = self.__production_emissions_housing
        production[BatteryComponentEmissionsWriter.HVAC] = self.__production_emissions_hvac
        production[
            BatteryComponentEmissionsWriter.MISC_ELECTRONICS] = self.__production_emissions_miscellaneous_electronics
        production[BatteryComponentEmissionsWriter.TOTAL] = self.production_emissions

        end_of_life = dict()
        end_of_life[BatteryComponentEmissionsWriter.CELLS] = self.__eol_emissions_cells
        end_of_life[BatteryComponentEmissionsWriter.ACDC_CONVERTER] = self.__eol_emissions_pe
        end_of_life[BatteryComponentEmissionsWriter.HOUSING] = self.__eol_emissions_housing
        end_of_life[BatteryComponentEmissionsWriter.HVAC] = self.__eol_emissions_hvac
        end_of_life[BatteryComponentEmissionsWriter.MISC_ELECTRONICS] = self.__eol_miscellaneous_electronics
        end_of_life[BatteryComponentEmissionsWriter.TOTAL] = self.end_of_life_emissions

        emissions_writer.write_to_csv(production, end_of_life)

    def reset_profiles(self, adapted_time: float) -> None:
        """
        Resets the profiles associated with the object to their default values.
        :param adapted_time: Adapted time in the simulation.
        :return: None
        """
        pass

    def close(self) -> None:
        """
        Closes all open resources and saves the used config file
        :return: None
        """
        self.__simses.close_simulation()
        self.__simses.run_analysis()
        self.__simses.close_analysis()
        self.__lithium_battery_config.write_config_to(self.data_export.get_working_dir_name())
