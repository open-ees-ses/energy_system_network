from typing import Union
from energy_system_network.commons import unit_converter
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.output.battery_component_emissions import BatteryComponentEmissionsWriter
from energy_system_network.config.energy_system_components.lithium_battery import LithiumBatteryConfig
from energy_system_network.config.energy_system_components.simses_config_generator import SimSESConfigGenerator
from configparser import ConfigParser
from energy_system_network.commons.state.energy_system_component.storage_component.electricity_storage_component_state import \
    ElectricityStorageComponentState
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from energy_system_network.config.simulation.storage_config import StorageConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.profile_config import ProfileConfig
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent
from energy_system_network.simulation.energy_system.supporting_components import ac_dc_converter_model
print('Info: If you have selected the LithiumBatterySystem as a storage component, please check the documentation on how to configure the SimSES-based LithiumBatterySystem '
      'component locally from within ESN.')


class LithiumBatterySystem(StorageComponent):
    """
    A class to represent a Lithium Battery System for storage with SimSES at its core.

    Attributes:
        energy_form (List[str]): The type of energy form, i.e., electricity.
        peak_power (float): Peak power of the battery system in W.
        energy_capacity (float): The total energy capacity of the battery system in Wh.
        name (str): Unique identifier for the battery system.
        component_ID (str): Identification details for the battery system.
        __config_profile (dict): Configuration details for the system profile.
        __general_simulation_config (dict): General simulation configuration details.
        __environmental_analysis_config (dict): Environmental analysis configuration details.
        __config_storage (dict): Storage configuration details.
        __lithium_battery_config (dict): Lithium battery specific configuration.
        __sample_time (float): The sample time for the system.
        __dc_storage_system_flag (bool): A flag indicating if DC storage system is used.
        state (dict): The current state of the battery system.
        __simses (object): The SimSES core model object.
        __simses_system_parameters (dict): Parameters for the SimSES system.
        __number_containers (int): Number of container units.
        __hvac_capacity (float): HVAC capacity for the battery.
        __initial_capacity (float): Initial capacity for the battery system.
        __carbon_intensity_stored_energy (float): Carbon intensity for stored energy.
        __carbon_intensity_charging_energy (float): Carbon intensity during charging.
        __instantaneous_ch_energy_throughput (float): Instantaneous charging energy throughput.
        __cumulative_ch_energy_throughput (float): Cumulative charging energy throughput.
        __instantaneous_ch_losses (float): Instantaneous charging losses.
        __cumulative_ch_losses (float): Cumulative charging losses.
        __instantaneous_ch_efficiency (float): Instantaneous charging efficiency.
        __cumulative_ch_efficiency (float): Cumulative charging efficiency.
        __instantaneous_dch_energy_throughput (float): Instantaneous discharging energy throughput.
        __cumulative_dch_energy_throughput (float): Cumulative discharging energy throughput.
        __instantaneous_dch_losses (float): Instantaneous discharging losses.
        __cumulative_dch_losses (float): Cumulative discharging losses.
        __instantaneous_dch_efficiency (float): Instantaneous discharging efficiency.
        __cumulative_dch_efficiency (float): Cumulative discharging efficiency.
        __production_emissions_cells (float): Production emissions for the battery cells.
        __production_emissions_pe (float): Production emissions for the power electronics.
        __production_emissions_housing (float): Production emissions for the battery housing.
        __production_emissions_hvac (float): Production emissions for the HVAC.
        __production_emissions_miscellaneous_electronics (float): Production emissions for miscellaneous electronics.
        __eol_emissions_cells (float): End-of-life emissions for the battery cells.
        __eol_emissions_pe (float): End-of-life emissions for the power electronics.
        __eol_emissions_housing (float): End-of-life emissions for the battery housing.
        __eol_emissions_hvac (float): End-of-life emissions for the HVAC.
        __eol_miscellaneous_electronics (float): End-of-life emissions for miscellaneous electronics.
        __time (float): Current timestamp for the battery system.
        __adapted_time (float): Adapted timestamp for synchronization with other systems.
        production_emissions (float): Overall production emissions.
        end_of_life_emissions (float): Overall end-of-life emissions.


    Methods:
        __init__(peak_power, energy_capacity, ...): Initializes a LithiumBatterySystem object.
        next(time, energy_form, target_electrical_power, charging_energy_carbon_intensity, adapted_time): Runs the battery model at each timestep.
        update(): Updates the state for the storage component.
        compute_efficiency(): Computes and updates the efficiency.
        calculate_operation_emissions(): Calculates emissions due to inefficiencies.
        compute_efficiency(): Computes and updates the efficiency at the current timestep and average efficiency till
            the timestep.
        calculate_operation_emissions(): Calculates the emissions due to inefficiencies in the Li-on BESS for each
            timestep in kgCO2eq.
        operation_losses(): Calculates and returns the operation losses in the Li-ion BESS for each timestep in W.
        set_initial_carbon_intensity_stored_energy(average_grid_carbon_intensity: float): Updates the value of initial
            carbon_intensity_stored_energy for the StorageComponent in g/kWh.
        set_carbon_intensity_charging_energy(carbon_intensity_charging_energy: float): Updates the value of
            carbon_intensity_charging_energy for the StorageComponent in g/kWh.
        calculate_carbon_intensity_stored_energy(): Calculates the carbon intensity of the energy stored in the
            StorageComponent at the current timestep in gCO2eq/kWh.
        carbon_intensity_stored_energy(): Returns the carbon intensity of stored energy in gCO2eq/kWh.
        calculate_production_emissions(): Calculates the total production emissions of all components in the
            Lithium-ion BESS in kgCO2eq.
        calculate_eol_emissions(): Calculates the total end-of-life emissions of all components in the Lithium-ion BESS
            in kgCO2eq.
        write_component_wise_emissions(result_path_simses: str, simses_simulation_name: str): Writes component-wise
            emissions for the production and EOL phases for the Li-ion BESS and saves it.
        reset_profiles(adapted_time: float): Resets EMS and other profile-based modules in SimSES at the end of each
            loop if using SimSESExternalStrategy EMS.
        close(): Closes all open resources and saves the used config file.

    """
    def __init__(self, peak_power: float, energy_capacity: float, data_export, energy_system_id, storage_id,
                 config_general: GeneralSimulationConfig,
                 environmental_analysis_config: EnvironmentalAnalysisConfig, config: ConfigParser,
                 dc_storage_system_flag: bool):
        """
        Initializes a LithiumBatterySystem object with SimSES at its core.

        Args:
            peak_power (float): Peak power of the battery system in W.
            energy_capacity (float): The total energy capacity of the battery system in Wh.
            data_export: Data export handler responsible for output data management.
            energy_system_id: Identifier for the energy system to which this battery system belongs.
            storage_id: Unique identifier for this storage system within the energy system.
            config_general (GeneralSimulationConfig): General configuration for the simulation.
            environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration related to environmental analysis.
            config (ConfigParser): Config parser containing additional battery-related settings.
            dc_storage_system_flag (bool): True if the storage system is purely DC, otherwise False.

        Returns:
            LithiumBatterySystem object.
        """
        from simses.main import SimSES
        from simses.commons.state.parameters_reader import SystemParametersReader

        super().__init__(data_export)
        self.energy_form: [str] = [EnergyForm.ELECTRICITY]
        self.peak_power = peak_power  # in W
        self.energy_capacity = energy_capacity  # in Wh

        # Instance identification details
        self.name = str(energy_system_id) + str(storage_id) + self.__class__.__name__
        self.component_ID = str(energy_system_id) + '.' + str(storage_id)

        # Configs
        self.__config_profile: ProfileConfig = ProfileConfig()
        self.__general_simulation_config: GeneralSimulationConfig = config_general
        self.__environmental_analysis_config = environmental_analysis_config
        self.__config_storage = StorageConfig(config)
        self.__lithium_battery_config = LithiumBatteryConfig()

        # General variables
        self.__sample_time = config_general.timestep
        self.__dc_storage_system_flag = dc_storage_system_flag  # True: if the storage system is a purely DC system

        # Set up logging of state
        self.state = ElectricityStorageComponentState(energy_system_id, storage_id)
        self.data_export.add_export_class(self.state)  # Ensures that only those states are logged which are currently used
        self.data_export.transfer_data(self.state.to_export())  # initial timestep

        # SimSES Instantiation, overwrite SimSES Configs to match Energy System Network Configs
        config_creator = SimSESConfigGenerator(config_general, self.__config_storage, self.__lithium_battery_config,
                                               self.peak_power, self.energy_capacity, self.__dc_storage_system_flag)
        sim_config: ConfigParser = config_creator.create_simses_simulation_config()
        analysis_config: ConfigParser = config_creator.create_simses_analysis_config()

        result_path_simses = data_export.get_result_folder_name() + '/'
        simulation_name: str = 'SimSES_' + self.name

        self.__simses: SimSES = SimSES(result_path_simses, simulation_name,
                                       do_simulation=True, do_analysis=True,
                                       simulation_config=sim_config,
                                       analysis_config=analysis_config)

        # Update energy capacity to reflect actual capacity after SimSES initialization
        self.energy_capacity = self.__simses.state.capacity  # in Wh

        # Read SystemParameters from instantiated SimSES object
        self.__simses_system_parameters: SystemParametersReader = SystemParametersReader(result_path_simses,
                                                                                         simulation_name)

        # Initialize variables
        self.__number_containers = self.__simses_system_parameters.get_total_containers()
        self.__hvac_capacity = unit_converter.W_to_kW(self.__lithium_battery_config.hvac_thermal_power)  # in KW
        self.__initial_capacity = self.__simses.state.capacity  # in Wh
        self.__carbon_intensity_stored_energy = 0  # in g/kWh
        self.__carbon_intensity_charging_energy = 0  # in g/kWh

        self.state.energy_content = self.__simses.state.capacity * self.__simses.state.soc

        self.__instantaneous_ch_energy_throughput = 0
        self.__cumulative_ch_energy_throughput = 0
        self.__instantaneous_ch_losses = 0
        self.__cumulative_ch_losses = 0
        self.__instantaneous_ch_efficiency = 0
        self.__cumulative_ch_efficiency = 0.9

        self.__instantaneous_dch_energy_throughput = 0
        self.__cumulative_dch_energy_throughput = 0
        self.__instantaneous_dch_losses = 0
        self.__cumulative_dch_losses = 0
        self.__instantaneous_dch_efficiency = 0
        self.__cumulative_dch_efficiency = 0.9

        # Calculate emissions for production and EOL
        self.__production_emissions_cells = 0
        self.__production_emissions_pe = 0
        self.__production_emissions_housing = 0
        self.__production_emissions_hvac = 0
        self.__production_emissions_miscellaneous_electronics = 0

        self.__eol_emissions_cells = 0
        self.__eol_emissions_pe = 0
        self.__eol_emissions_housing = 0
        self.__eol_emissions_hvac = 0
        self.__eol_miscellaneous_electronics = 0

        self.__time = 0
        self.__adapted_time = 0

        self.production_emissions = self.calculate_production_emissions()
        self.end_of_life_emissions = self.calculate_eol_emissions()
        self.write_component_wise_emissions(result_path_simses, simulation_name)

    def next(self, time: float, energy_form: str = None, target_electrical_power: Union[float, None] = 0,
             charging_energy_carbon_intensity: float = 0, adapted_time: float = 0) -> float:
        """
        Receives a target power (ch/dch) and runs the battery model at each timestep.

        Args:
            time (float): Epoch timestamp.
            energy_form (str): Energy form as str.
            target_electrical_power (float): Target power in W. Negative for discharge, positive for charge.
            charging_energy_carbon_intensity (float): Value of the charging energy carbon intensity in gCO2/kWh.
            adapted_time (float): Epoch timestamp.
        Return:
            float: Actual power in W. Positive for discharge, negative for charge.
        """
        self.__time = time
        if energy_form == EnergyForm.ELECTRICITY:
            self.__simses.run_one_simulation_step(time=time, adapted_time=adapted_time, power=target_electrical_power)
            ac_power = self.__simses.state.ac_power_delivered * -1  # in W
            if ac_power < 0:
                # ie. ch
                self.__carbon_intensity_charging_energy = charging_energy_carbon_intensity  # in gCO2eq/kWh
            else:
                # i.e. dch
                self.__carbon_intensity_charging_energy = 0.0
            return ac_power  # in W
        else:
            return 0.0

    def update(self) -> None:
        """
        Updates the state for the storage component.

        Return:
            None.
        """
        self.state.time = self.__time
        self.state.power = self.__simses.state.ac_power_delivered  # in W
        # TODO think about how the aux systems get their power supply in a DC sub-system
        self.state.power_loss = self.operation_losses()  # in W
        self.state.soc = self.__simses.state.soc  # in p.u.
        self.state.soh = self.__simses.state.capacity / self.__initial_capacity  # in p.u.
        # self.state.request_fulfilment = self.__simses.state.fulfillment  # in p.u.
        self.state.carbon_intensity_charging_energy = self.__carbon_intensity_charging_energy  # gCO2eq/kWh
        self.compute_efficiency()
        if self.state.power < 0:  # if storage is discharging
            self.state.soci = self.__carbon_intensity_stored_energy  # in g/kWh
            self.state.instantaneous_dch_efficiency = self.__instantaneous_ch_efficiency
            self.state.cumulative_dch_efficiency = self.__cumulative_dch_efficiency
            self.state.instantaneous_ch_efficiency = 0
            self.state.cumulative_ch_efficiency = self.__cumulative_ch_efficiency
        else:  # if storage is charging
            self.state.soci = self.calculate_carbon_intensity_stored_energy()  # gCO2eq/kWh
            self.state.instantaneous_ch_efficiency = self.__instantaneous_ch_efficiency
            self.state.cumulative_ch_efficiency = self.__cumulative_ch_efficiency
            self.state.instantaneous_dch_efficiency = 0
            self.state.cumulative_dch_efficiency = self.__cumulative_dch_efficiency
        self.state.operation_emissions = self.calculate_operation_emissions()  # kgCO2eq of CO2
        self.state.energy_content = self.__simses.state.capacity * self.__simses.state.soc
        # Important: energy content in Wh, state.energy_content updated last as
        # calculate_carbon_intensity_stored_energy() requires previous value of state.energy_content

        if abs(self.__simses.state.ac_power_delivered) > 1e-6:
            self.state.run_status = 1
        else:
            self.state.run_status = 0
        self.data_export.transfer_data(self.state.to_export())

    def compute_efficiency(self) -> None:
        """
        computes and updates the efficiency at current timestep and average efficiency till timestep
        :return: None
        """
        if self.state.power < 0:  # i.e. a discharge request took place
            self.__instantaneous_dch_energy_throughput = abs(self.state.power * self.__sample_time)
            self.__cumulative_dch_energy_throughput += self.__instantaneous_dch_energy_throughput
            self.__instantaneous_dch_losses = abs(self.state.power_loss) * self.__sample_time
            self.__cumulative_dch_losses += self.__instantaneous_dch_losses
            try:
                self.__instantaneous_dch_efficiency = 1 - self.__instantaneous_dch_losses /\
                                                      (self.__instantaneous_dch_losses + self.__instantaneous_dch_energy_throughput)
            except ZeroDivisionError:
                self.__instantaneous_dch_efficiency = 0
            cumulative_dch_efficiency = 1 - self.__cumulative_dch_losses / \
                                        (self.__cumulative_dch_losses + self.__cumulative_dch_energy_throughput)
            if cumulative_dch_efficiency > 0:
                self.__cumulative_dch_efficiency = cumulative_dch_efficiency

        elif self.state.power > 0:  # i.e. a charge request took place
            self.__instantaneous_ch_energy_throughput = abs(self.state.power * self.__sample_time)
            self.__cumulative_ch_energy_throughput += self.__instantaneous_ch_energy_throughput
            self.__instantaneous_ch_losses = abs(self.state.power_loss) * self.__sample_time
            self.__cumulative_ch_losses += self.__instantaneous_ch_losses
            try:
                self.__instantaneous_ch_efficiency = 1 - self.__instantaneous_ch_losses / self.__instantaneous_ch_energy_throughput
            except ZeroDivisionError:
                self.__instantaneous_ch_efficiency = 0
            cumulative_ch_efficiency = 1 - self.__cumulative_ch_losses / self.__cumulative_ch_energy_throughput
            if cumulative_ch_efficiency > 0:
                self.__cumulative_ch_efficiency = cumulative_ch_efficiency
        else:
            pass

    def calculate_operation_emissions(self) -> float:
        """
        Calculates the emissions due to inefficiencies in the Li-on BESS for each timestep in kgCO2eq
        :return: returns the value of emissions in current timestep in kgCO2 as float
        """
        loss_energy_at_timestep = unit_converter.J_to_kWh(self.state.power_loss * self.__sample_time)  # in kWh
        if self.state.power < 0:  # i.e. a discharge request took place
            return unit_converter.g_to_kg(loss_energy_at_timestep * self.__carbon_intensity_stored_energy)  # in kg
        elif self.state.power > 0:  # i.e. a charge request took place
            return unit_converter.g_to_kg(loss_energy_at_timestep * self.__carbon_intensity_charging_energy)  # in kg
        else:
            return 0.0

    def operation_losses(self) -> float:
        """
        Calculates and returns the operation losses in the Li-ion BESS for each timestep in W
        :return: operation losses in W as float
        """
        total_operation_losses: float = self.__simses.state.pe_losses + self.__simses.state.aux_losses + \
                                        self.__simses.state.dc_power_loss + self.__simses.state.storage_power_loss
        return total_operation_losses  # in W

    def set_initial_carbon_intensity_stored_energy(self, average_grid_carbon_intensity: float) -> None:
        """
        Updates the value of initial carbon_intensity_stored_energy for the StorageComponent in g/kWh
        :param average_grid_carbon_intensity: average carbon intensity of energy used to charge the StorageComponent
        as float
        :return: None
        """
        self.__carbon_intensity_stored_energy = average_grid_carbon_intensity  # in g/kWh

    def set_carbon_intensity_charging_energy(self, carbon_intensity_charging_energy: float) -> None:
        """
        Updates the value of carbon_intensity_charging_energy for the StorageComponent in g/kWh (used in case of
        iterative circular calculation problems such as MultiSourcePeakShaving)
        :param average_grid_carbon_intensity: average carbon intensity of energy used to charge the StorageComponent
        as float
        :return: None
        """
        self.__carbon_intensity_charging_energy = carbon_intensity_charging_energy  # in g/kWh

    def calculate_carbon_intensity_stored_energy(self) -> float:
        """
        Calculates the carbon intensity of the energy stored in the StorageComponent at current timestep in gCO2eq/kWh
        :return: the carbon intensity of the energy stored in the StorageComponent at the current timestep as float
        """
        # return existing value if not changed, or return new value
        if self.state.energy_content < 1e-5:  # Reset value to zero, if storage is empty
            self.__carbon_intensity_stored_energy = 0
        if self.state.power > 0:  # +ve implies a charging request
            effective_charging_energy = unit_converter.J_to_kWh(self.__simses.state.dc_power_storage
                                                                * self.__sample_time)  # in kWh
            if effective_charging_energy > 0:
                self.__carbon_intensity_stored_energy = (unit_converter.Wh_to_kWh(self.state.energy_content) * self.__carbon_intensity_stored_energy +
                                                         effective_charging_energy * self.__carbon_intensity_charging_energy) / (unit_converter.Wh_to_kWh(self.state.energy_content) + effective_charging_energy)  # in g / kWh
        return self.__carbon_intensity_stored_energy  # in gCO2eq/kWh

    def carbon_intensity_stored_energy(self) -> float:
        """
        returns the carbon intensity of stored energy in gCO2eq/kWh
        :return: __carbon_intensity_stored_energy as float
        """
        return self.__carbon_intensity_stored_energy  # in gCO2eq/kWh

    def calculate_production_emissions(self) -> float:
        """
        Calculates the total production emissions of all components in the Lithium-ion BESS in kgCO2eq
        :return: returns the total calculated production emissions as float
        """
        miscellaneous_electronics_share = unit_converter.pc_to_pu(self.__environmental_analysis_config.misc_electronics_production_emissions)  # % of total emissions

        if self.__lithium_battery_config.cell_start_soh < 1.0:  # 2nd Life Battery
            if self.__environmental_analysis_config.second_life_emissions_allocation_factor == -1:  # cut-off allocation
                self.__production_emissions_cells = self.energy_capacity * self.__environmental_analysis_config.battery_remanufacturing_emissions
            else:
                self.__production_emissions_cells = self.energy_capacity \
                                                    * (self.__environmental_analysis_config.cell_production_emissions
                                                       * unit_converter.pc_to_pu(self.__environmental_analysis_config.second_life_emissions_allocation_factor)
                                                       + self.__environmental_analysis_config.battery_remanufacturing_emissions)
        else:
            self.__production_emissions_cells = self.energy_capacity * self.__environmental_analysis_config.cell_production_emissions
        number_pe_units = self.__lithium_battery_config.number_of_converters
        self.__production_emissions_pe = ac_dc_converter_model.calculate_production_emissions(unit_converter.W_to_kW(self.peak_power), number_pe_units)  # power is to be passed in kW
        self.__production_emissions_housing = self.__number_containers * self.__environmental_analysis_config.housing_production_emissions
        self.__production_emissions_hvac = self.__number_containers * self.__environmental_analysis_config.hvac_production_emissions
        self.__production_emissions_miscellaneous_electronics = miscellaneous_electronics_share * (
                self.__production_emissions_cells + self.__production_emissions_pe + self.__production_emissions_housing + self.__production_emissions_hvac) / (1 - miscellaneous_electronics_share)
        total_production_emissions = self.__production_emissions_cells + self.__production_emissions_pe + self.__production_emissions_housing \
                                     + self.__production_emissions_hvac + self.__production_emissions_miscellaneous_electronics
        return total_production_emissions  # in kgCO2eq

    def calculate_eol_emissions(self) -> float:
        """
        Calculates the total end-of-life emissions of all components in the Lithium-ion BESS in kgCO2eq
        :return: returns the total calculated end-of-life emissions as float
        """
        miscellaneous_electronics_eol_share = unit_converter.pc_to_pu(self.__environmental_analysis_config.misc_electronics_eol_emissions)  # % of misc. electronics production emissions
        if self.__lithium_battery_config.cell_start_soh < 1.0:  # 2nd Life Battery
            if self.__environmental_analysis_config.second_life_emissions_allocation_factor == -1:  # cut-off allocation
                self.__eol_emissions_cells = self.energy_capacity * self.__environmental_analysis_config.cell_eol_emissions
            else:
                self.__eol_emissions_cells = self.energy_capacity * \
                                             (self.__environmental_analysis_config.cell_eol_emissions *
                                              (unit_converter.pc_to_pu(self.__environmental_analysis_config.second_life_emissions_allocation_factor)))
        else:
            self.__eol_emissions_cells = self.energy_capacity * self.__environmental_analysis_config.cell_eol_emissions
        self.__eol_emissions_pe = unit_converter.W_to_kW(self.peak_power) * self.__environmental_analysis_config.power_electronics_eol_emissions
        self.__eol_emissions_housing = self.__number_containers * self.__environmental_analysis_config.housing_eol_emissions
        self.__eol_emissions_hvac = self.__number_containers * self.__environmental_analysis_config.hvac_eol_emissions
        self.__eol_miscellaneous_electronics = - miscellaneous_electronics_eol_share * self.__production_emissions_miscellaneous_electronics
        total_eol_emissions = self.__eol_emissions_cells + self.__eol_emissions_pe + self.__eol_emissions_housing + self.__eol_emissions_hvac + self.__eol_miscellaneous_electronics
        return total_eol_emissions  # in kgCO2eq

    def write_component_wise_emissions(self, result_path_simses: str, simses_simulation_name: str) -> None:
        """
        Writes component-wise emissions for the production and EOL phases for the Li-ion BESS and saves it
        :param result_path_simses: simses result path as str
        :param simses_simulation_name: simses simulation name as str
        """
        emissions_writer: BatteryComponentEmissionsWriter = BatteryComponentEmissionsWriter(self.name,
                                                                                            result_path_simses,
                                                                                            simses_simulation_name)
        production = dict()
        production[BatteryComponentEmissionsWriter.CELLS] = self.__production_emissions_cells
        production[BatteryComponentEmissionsWriter.ACDC_CONVERTER] = self.__production_emissions_pe
        production[BatteryComponentEmissionsWriter.HOUSING] = self.__production_emissions_housing
        production[BatteryComponentEmissionsWriter.HVAC] = self.__production_emissions_hvac
        production[
            BatteryComponentEmissionsWriter.MISC_ELECTRONICS] = self.__production_emissions_miscellaneous_electronics
        production[BatteryComponentEmissionsWriter.TOTAL] = self.production_emissions

        end_of_life = dict()
        end_of_life[BatteryComponentEmissionsWriter.CELLS] = self.__eol_emissions_cells
        end_of_life[BatteryComponentEmissionsWriter.ACDC_CONVERTER] = self.__eol_emissions_pe
        end_of_life[BatteryComponentEmissionsWriter.HOUSING] = self.__eol_emissions_housing
        end_of_life[BatteryComponentEmissionsWriter.HVAC] = self.__eol_emissions_hvac
        end_of_life[BatteryComponentEmissionsWriter.MISC_ELECTRONICS] = self.__eol_miscellaneous_electronics
        end_of_life[BatteryComponentEmissionsWriter.TOTAL] = self.end_of_life_emissions

        emissions_writer.write_to_csv(production, end_of_life)

    def reset_profiles(self, adapted_time: float) -> None:
        """
        Resets EMS and other profile-based modules in SimSES at the end of each loop if using SimSESExternalStrategy EMS
        :param adapted_time: Time offset in s
        :return: None
        """
        self.__adapted_time = adapted_time
        self.__simses.reset_profiles(adapted_time)

    def close(self) -> None:
        """
        Closes all open resources and saves the used config file
        """
        self.__simses.close_simulation()
        self.__simses.run_analysis()
        self.__simses.close_analysis()
        self.__lithium_battery_config.write_config_to(self.data_export.get_working_dir_name())
