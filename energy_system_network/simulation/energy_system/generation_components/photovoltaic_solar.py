from energy_system_network.commons import unit_converter
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.profiles.profile_handler import ProfileHandler
from energy_system_network.commons.state.energy_system_component.generation_component.electricity_generation_component_state import \
    ElectricityGenerationComponentState
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.profile_config import ProfileConfig
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent
from energy_system_network.simulation.energy_system.supporting_components import ac_dc_converter_model


class PhotovoltaicSolar(GenerationComponent):
    """
    Represents a photovoltaic solar energy generation component.

    Attributes:
        SOLAR (str): Identifier for solar energy.
        energy_form (list[str]): List of energy forms, typically [EnergyForm.ELECTRICITY].
        peak_power (float): Peak power of the solar component in Watts.
        must_run (bool): Indicates if the component must run.
        name (str): Name of the photovoltaic solar component.
        component_ID (str): Unique identification for the component.
        generation_profile (ProfileHandler): The generation profile handler for the component.
        capacity_factor (float): Capacity factor of the generation component.
        state (ElectricityGenerationComponentState): Current state of the component.
        minimum_carbon_intensity_energy (float): Minimum carbon intensity of energy in kgCO2eq/kWh.
        __general_config (GeneralSimulationConfig): General configuration for the simulation.
        __estimated_lifetime (float): Estimated lifetime of the photovoltaic solar component in years.
        __lifetime_energy_production (float): Expected total energy production over the component's lifetime in kWh.
        production_emissions (float): Carbon emissions produced during the component's production in kgCO2eq.
        end_of_life_emissions (float): Carbon emissions at the end of the component's lifecycle in kgCO2eq.
        __generation_power (float): Current generation power in Watts.
        __export_power (float): Current export power in Watts.
        __time (float): Current simulation time.

    Methods:
        __init__(peak_power: float, data_export, energy_system_id, generation_id,
                 config_general: GeneralSimulationConfig, environmental_analysis_config: EnvironmentalAnalysisConfig):
            Initializes a PhotovoltaicSolar object.
        next(time: float, energy_form: str = None, power: float = 0, adapted_time: float = 0) -> float:
            Simulates the solar component for the next time step.
        update() -> None:
            Updates the state of the PhotovoltaicSolar component.
        operation_losses() -> float:
            Calculates the operation losses for the component.
        get_emissions_per_joule() -> float:
            Calculates the specific emissions per joule of energy.
        allocate_export_power(export_power: float) -> None:
            Allocates the export power as calculated by the EMS.
        fuel_consumption() -> float:
            Returns the fuel consumption for the current timestep.
    """
    SOLAR: str = 'solar'

    def __init__(self, peak_power: float, data_export, energy_system_id, generation_id,
                 config_general: GeneralSimulationConfig, environmental_analysis_config: EnvironmentalAnalysisConfig):
        """
        Initializes a PhotovoltaicSolar object.

        Args:
            peak_power (float): Peak power of the solar component in Watts.
            data_export: Data export handler.
            energy_system_id: Identification for the energy system.
            generation_id: Identification for the generation.
            config_general (GeneralSimulationConfig): General configuration for the simulation.
            environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.
        """
        super().__init__(data_export)

        self.energy_form: [str] = [EnergyForm.ELECTRICITY]
        self.peak_power = peak_power  # in W
        self.must_run = True

        self.__general_config = config_general

        # Component identification and general
        self.name = str(energy_system_id) + str(generation_id) + self.__class__.__name__
        self.component_ID = str(energy_system_id) + '.' + str(generation_id)

        # Generation Profile
        profile_config: ProfileConfig = ProfileConfig()
        scaling_factor = peak_power
        profile_filename = profile_config.generation_profile[self.__class__.__name__]
        if self.SOLAR in profile_filename:
            self.generation_profile = ProfileHandler(profile_filename, config_general, scaling_factor)
            self.generation_profile.capacity_factor = self.generation_profile.calculate_capacity_factor()
            self.capacity_factor = self.generation_profile.capacity_factor
        else:
            raise Exception('Please confirm that a suitable PV solar generation profile has been chosen. The chosen file must contain ' + self.SOLAR + 'in its filename.')

        # Setting up of logging of state
        self.state = ElectricityGenerationComponentState(energy_system_id, generation_id)
        self.data_export.add_export_class(self.state)  # Ensures that only those states are logged which are currently used
        self.data_export.transfer_data(self.state.to_export())  # initial timestep

        # Calculate emissions
        self.__estimated_lifetime: float = 20  # in years
        self.__lifetime_energy_production: float = unit_converter.Wh_to_kWh(self.peak_power * unit_converter.years_to_hours(1) * self.__estimated_lifetime * self.capacity_factor)  # in kWh

        pv_panel_production_emissions = unit_converter.W_to_kW(self.peak_power) * environmental_analysis_config.pv_production_emissions  # in kgCO2eq
        ac_dc_converter_production_emissions = ac_dc_converter_model.calculate_production_emissions(unit_converter.W_to_kW(self.peak_power), 5)  # Assuming 5 subunits
        self.production_emissions = pv_panel_production_emissions + ac_dc_converter_production_emissions  # in kgCO2eq

        pv_panel_eol_emissions = unit_converter.W_to_kW(self.peak_power) * environmental_analysis_config.pv_eol_emissions  # in kgCO2eq
        ac_dc_converter_eol_emissions = (unit_converter.W_to_kW(self.peak_power)) * environmental_analysis_config.power_electronics_eol_emissions  # in kgCO2eq
        self.end_of_life_emissions = pv_panel_eol_emissions + ac_dc_converter_eol_emissions  # in kgCO2eq

        self.minimum_carbon_intensity_energy: float = (self.production_emissions + self.end_of_life_emissions) / self.__lifetime_energy_production  # in kgCO2eq/kWh

        # Initialize variables
        self.__generation_power = 0
        self.__export_power = 0

        self.__time = 0

    def next(self, time: float, energy_form: str = None, power: float = 0, adapted_time: float = 0) -> float:
        """
        Simulates the solar component for the next time step.

        Args:
            time (float): Current simulation time.
            energy_form (str, optional): Form of energy. Defaults to None.
            power (float, optional): Power input. Defaults to 0.
            adapted_time (float, optional): Adapted time for the simulation. Defaults to 0.

        Return:
            float: Output power from the PV module at the current time step.
        """
        self.__time = time
        if energy_form == EnergyForm.ELECTRICITY:
            self.__generation_power = self.generation_profile.next(time, adapted_time)
            self.__export_power = 0  # reset export power, updated later by the EMS using method allocate_export_power
            return self.__generation_power
        else:
            return 0.0

    def update(self) -> None:
        """
        Updates the state of the PhotovoltaicSolar component.

        Args:
            None

        Return:
            None
        """
        self.state.time = self.__time
        self.state.power = self.__generation_power
        self.state.power_loss = self.operation_losses()
        if self.__generation_power > 0:
            self.state.run_status = 1
        else:
            self.state.run_status = 0
        self.state.power_export = self.__export_power
        self.state.power_export_emissions = -1 * self.__export_power * self.get_emissions_per_joule() \
                                            * self.__general_config.timestep  # kgCO2
        self.data_export.transfer_data(self.state.to_export())

    def operation_losses(self) -> float:
        """
        Calculates the operation losses for the component.

        Return:
            float: Operation losses in Watts.
        """
        return 0.0

    def get_emissions_per_joule(self) -> float:
        """
        Calculates the specific emissions per joule of energy.

        Return:
            float: Specific emissions per joule of energy in kgCO2eq/J.
        """
        emissions_per_joule = self.minimum_carbon_intensity_energy / unit_converter.kWh_to_J(1)  # in kgCO2eq/J
        return emissions_per_joule  # in kgCO2eq/J

    def allocate_export_power(self, export_power: float) -> None:
        """
        Allocates the export power as calculated by the EMS.

        Args:
            export_power (float): Power to be exported.

        Return:
            None
        """
        self.__export_power = export_power

    def fuel_consumption(self) -> float:
        """
        Returns the fuel consumption for the current timestep.

        Return:
            float: Fuel consumption in liters. (always returns 0 for PhotovoltaicSolar)
        """
        return 0.0
