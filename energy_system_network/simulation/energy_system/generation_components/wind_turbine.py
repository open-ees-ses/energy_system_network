from energy_system_network.commons import unit_converter
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.profiles.profile_handler import ProfileHandler
from energy_system_network.commons.state.energy_system_component.generation_component.electricity_generation_component_state import \
    ElectricityGenerationComponentState
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.profile_config import ProfileConfig
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent


class WindTurbine(GenerationComponent):
    """
    Model representing a Wind Turbine as a Generation Component in an energy system network.

    Attributes:
        WIND (str): Identifier for wind.
        energy_form (list): List containing energy forms applicable to the wind turbine.
        peak_power (float): The peak power of the wind turbine in watts (W).
        must_run (bool): Flag indicating if the wind turbine must always run.
        __general_config (GeneralSimulationConfig): General configuration settings for the simulation.
        name (str): Name identifier for the wind turbine.
        component_ID (str): Component identifier for the wind turbine.
        capacity_factor (float): Capacity factor for the wind turbine, representing the ratio of its actual output to
            its potential output.
        generation_profile (ProfileHandler): Generation profile of the wind turbine.
        state (ElectricityGenerationComponentState): State of the wind turbine.
        __estimated_lifetime (float): Estimated lifetime of the wind turbine in years.
        __lifetime_energy_production (float): Expected total energy production of the wind turbine over its lifetime in kWh.
        production_emissions (float): Carbon emissions associated with the production of the wind turbine in kgCO2eq/kW.
        end_of_life_emissions (float): Carbon emissions associated with the end of life of the wind turbine in kgCO2eq/kW.
        minimum_carbon_intensity_energy (float): Minimum carbon intensity of the energy produced by the wind turbine in kgCO2eq/kWh.
        __generation_power (float): Power being generated by the wind turbine in W.
        __export_power (float): Power being exported from the wind turbine in W.
        __time (float): Current simulation time.

    Methods:
        __init__(peak_power, data_export, energy_system_id, generation_id, config_general, environmental_analysis_config): Initialize a WindTurbine instance.
        next(time, energy_form, power, adapted_time): Compute the power output for a given time step.
        update(): Update the state variables based on current operational status.
        operation_losses(): Compute operational losses.
        get_emissions_per_joule(): Calculate specific emissions per joule of energy.
        allocate_export_power(export_power): Assign a specified export power.
        fuel_consumption(): Compute fuel consumption.
    """
    WIND: str = 'wind'

    def __init__(self, peak_power: float, data_export, energy_system_id, generation_id,
                 config_general: GeneralSimulationConfig, environmental_analysis_config: EnvironmentalAnalysisConfig):
        """
        Initialize a WindTurbine instance.

        Args:
            peak_power (float): Peak power of the wind turbine in W.
            data_export: Object for exporting data.
            energy_system_id: Identifier for the energy system.
            generation_id: Identifier for the generation component.
            config_general (GeneralSimulationConfig): General configuration settings for the simulation.
            environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.

        Return:
            None.
        """
        super().__init__(data_export)

        self.energy_form = [EnergyForm.ELECTRICITY]
        self.peak_power = peak_power  # in W
        self.must_run = True

        self.__general_config = config_general

        # Component identification and general
        self.name = str(energy_system_id) + str(generation_id) + self.__class__.__name__
        self.component_ID = str(energy_system_id) + '.' + str(generation_id)

        # Generation Profile
        profile_config: ProfileConfig = ProfileConfig()
        scaling_factor = peak_power
        profile_filename = profile_config.generation_profile[self.__class__.__name__]
        if self.WIND in profile_filename:
            self.generation_profile = ProfileHandler(profile_filename, config_general, scaling_factor)
            self.generation_profile.capacity_factor = self.generation_profile.calculate_capacity_factor()
            self.capacity_factor = self.generation_profile.capacity_factor
        else:
            raise Exception('Please confirm that a suitable wind turbine generation profile has been chosen. '
                            'The chosen file must contain ' + self.WIND + 'in its filename.')

        # Set up logging of state
        self.state = ElectricityGenerationComponentState(energy_system_id, generation_id)
        self.data_export.add_export_class(self.state)  # Ensures that only those states are logged which are currently used
        self.data_export.transfer_data(self.state.to_export())  # initial timestep

        # Calculate emissions
        self.__estimated_lifetime: float = 20  # in years
        self.__lifetime_energy_production: float = unit_converter.Wh_to_kWh(self.peak_power * unit_converter.years_to_hours(1) * self.__estimated_lifetime * self.capacity_factor)  # in kWh

        self.production_emissions = unit_converter.W_to_kW(self.peak_power) * environmental_analysis_config.wind_turbine_production_emissions  # in kgCO2eq/kW
        self.end_of_life_emissions = unit_converter.W_to_kW(self.peak_power) * environmental_analysis_config.wind_turbine_eol_emissions  # in kgCO2eq/kW

        self.minimum_carbon_intensity_energy: float = (self.production_emissions + self.end_of_life_emissions) / self.__lifetime_energy_production  # in kgCO2eq/kWh

        # Initialize variables
        self.__generation_power = 0
        self.__export_power = 0

        self.__time = 0

    def next(self, time: float, energy_form: str = None, power: float = 0, adapted_time: float = 0) -> float:
        """
        Compute the power output from the wind turbine for a given time step.

        Args:
            time (float): Current time step.
            energy_form (str, optional): Form of energy (e.g., electricity). Defaults to None.
            power (float, optional): Power value. Defaults to 0.
            adapted_time (float, optional): Adapted time value. Defaults to 0.

        Return:
            float: Power output from the wind turbine at the current time step.
        """
        self.__time = time
        if energy_form == EnergyForm.ELECTRICITY:
            self.__generation_power = self.generation_profile.next(time, adapted_time)
            self.__export_power = 0  # reset export power, updated later by the EMS using method allocate_export_power
            return self.__generation_power
        else:
            return 0.0

    def update(self) -> None:
        """
        Update the state variables based on current operational status.

        Args:
            None.

        Return:
            None.
        """
        self.state.time = self.__time
        self.state.power = self.__generation_power
        self.state.power_loss = self.operation_losses()
        if self.__generation_power > 0:
            self.state.run_status = 1
        else:
            self.state.run_status = 0
        self.state.power_export = self.__export_power
        self.state.power_export_emissions = -1 * self.__export_power * self.get_emissions_per_joule() \
                                            * self.__general_config.timestep  # kgCO2
        self.data_export.transfer_data(self.state.to_export())

    def operation_losses(self) -> float:
        """
        Compute operational losses.

        Args:
            None.

        Return:
            float: Operational losses (always 0 for wind turbine).
        """
        return 0.0

    def get_emissions_per_joule(self) -> float:
        """
        Calculate specific emissions per joule of energy.

        Args:
            None.

        Return:
            float: Specific emissions per joule of energy in kgCO2eq/J.
        """
        emissions_per_joule = self.minimum_carbon_intensity_energy / unit_converter.kWh_to_J(1)
        return emissions_per_joule  # in kgCO2eq/J

    def allocate_export_power(self, export_power: float) -> None:
        """
        Assign a specified amount of export power.

        Args:
            export_power (float): Amount of power to be exported.

        Return:
            None.
        """
        self.__export_power = export_power

    def fuel_consumption(self) -> float:
        """
        Compute fuel consumption.

        Args:
            None.

        Return:
            float: Fuel consumption (always 0 for wind turbine).
        """
        return 0.0
