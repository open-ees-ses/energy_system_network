from abc import abstractmethod
from typing import Union
from energy_system_network.profiles.profile_handler import ProfileHandler
from energy_system_network.commons.state.energy_system_component.generation_component.generation_component_state import \
    GenerationComponentState
from energy_system_network.simulation.energy_system.energy_system_component import EnergySystemComponent


class GenerationComponent(EnergySystemComponent):
    """
    Represents a generic generation component within the energy system.

    Attributes:
        DESCRIPTION (str): A basic description of the Generation Component.
        __generation_profile (ProfileHandler): The energy generation profile for this component.
        __must_run (bool): Indicates if this generation component must continuously run.
        __capacity_factor (float): Represents the capacity factor for this generation component.
        __states (GenerationComponentState): Holds the state information for this generation component.
        __minimum_carbon_intensity_energy (float): Minimum carbon intensity for the generation component in gCO2/kWh.

    Methods:
        __init__(data_export=None): Initializes a GenerationComponent instance.
        generation_profile(): Getter and setter for __generation_profile.
        must_run(): Getter and setter for __must_run.
        capacity_factor(): Getter and setter for __capacity_factor.
        state(): Getter and setter for __states.
        minimum_carbon_intensity_energy(): Getter and setter for __minimum_carbon_intensity_energy.
        close(): Closes all open resources related to this generation component.
        get_emissions_per_joule(): Abstract method to calculate and retrieve emissions per joule of energy generated.
        allocate_export_power(export_power): Abstract method to allocate exported power.
        fuel_consumption(): Abstract method to retrieve the fuel consumption.
        next(time, energy_form, power, adapted_time): Abstract method to move to the next state or timestep.
    """
    DESCRIPTION: str = 'Generation Component '

    def __init__(self, data_export=None):
        """
        Initializes a GenerationComponent instance.

        Args:
            data_export (Optional): Data export handler for this component.
        """
        self.__generation_profile: ProfileHandler = ProfileHandler(None, None, None)
        self.__must_run = False
        self.__capacity_factor = 1
        self.__states = GenerationComponentState(0, 0)
        self.__minimum_carbon_intensity_energy = 0.0  # in kgCO2eq/kWh
        super().__init__(data_export)

    @property
    def generation_profile(self) -> ProfileHandler:
        """
        Returns the generation profile for the generation component.

        Return:
            ProfileHandler representing the generation profile.
        """
        return self.__generation_profile

    @generation_profile.setter
    def generation_profile(self, value: ProfileHandler) -> None:
        """
        Sets the generation profile for the generation component.

        Args:
            value (ProfileHandler): Object representing the generation profile.
        """
        self.__generation_profile = value

    @property
    def must_run(self) -> bool:
        """
        Returns the must-run status of the generation component.

        Return:
            bool: True if the component must run, else False.
        """
        return self.__must_run

    @must_run.setter
    def must_run(self, value: bool) -> None:
        """
        Sets the must-run status of the generation component.

        Args:
            value (bool): Value indicating if the component must run.
        """
        self.__must_run = value

    @property
    def capacity_factor(self) -> float:
        """
        Retrieves the capacity factor of the generation component.

        Return:
            float: Capacity factor of the component.
        """

        return self.__capacity_factor

    @capacity_factor.setter
    def capacity_factor(self, value: float) -> None:
        """
        Sets the capacity factor of the generation component.

        Args:
            value (float): Value for the capacity factor.
        """
        self.__capacity_factor = value

    @property
    def state(self) -> Union[GenerationComponentState, dict]:
        """
        Retrieves the state of the generation component.

        Return:
            GenerationComponentState or dict: Current state of the generation component.
        """
        return self.__states

    @state.setter
    def state(self, value: Union[GenerationComponentState, dict]) -> None:
        """
        Updates the state of the generation component.

        Args:
            value (GenerationComponentState or dict): New state of the generation component.
        """
        self.__states = value

    @property
    def minimum_carbon_intensity_energy(self) -> float:
        """
        Retrieves the minimum carbon intensity for the generation component.

        Return:
            float: Minimum carbon intensity in gCO2/kWh.
        """
        return self.__minimum_carbon_intensity_energy

    @minimum_carbon_intensity_energy.setter
    def minimum_carbon_intensity_energy(self, value: float) -> None:
        """
        Sets the minimum carbon intensity for the generation component.

        Args:
            value (float): Minimum carbon intensity in gCO2/kWh.
        """
        self.__minimum_carbon_intensity_energy = value

    def close(self) -> None:
        """
        closes all open resources
        """
        self.generation_profile.close()
        self.data_export.transfer_data(self.__states.to_export())

    @abstractmethod
    def get_emissions_per_joule(self) -> float:
        """
        Computes and returns the emissions per joule for the generation component.

        Returns:
            float: Emissions per joule.
        """
        pass

    @abstractmethod
    def allocate_export_power(self, export_power: float) -> None:
        """
        Allocates the specified export power to the generation component.

        Args:
            export_power (float): The power to be exported.
        """
        pass

    @abstractmethod
    def fuel_consumption(self) -> float:
        """
        Computes and returns the fuel consumption for the generation component.

        Returns:
            float: Fuel consumption value.
        """

        pass

    @abstractmethod
    def next(self, time: float, energy_form: str = None, power: float = 0, adapted_time: float = 0) -> float:
        """
        Computes and returns the next state or timestep for the generation component based on the provided parameters.

        Args:
            time (float): The current time or timestep.
            energy_form (str, optional): The form of energy for this step.
            power (float, optional): Power value for this step.
            adapted_time (float, optional): Adjusted time for this step.

        Returns:
            float: The next value or state.
        """

        pass
