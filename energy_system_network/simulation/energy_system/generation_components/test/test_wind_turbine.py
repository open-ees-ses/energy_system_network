import pytest
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.generation_components.wind_turbine import WindTurbine
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.constants_energy_sys_network import ROOT_PATH
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from random import randint
from configparser import ConfigParser
from energy_system_network.commons.state.energy_system_component.generation_component.generation_component_state \
    import GenerationComponentState

class TestWindTurbine:
    """
    Test suite for the WindTurbine generation component.

    Attributes:
        config: Configuration for the test, initialized as None.
        general_config (GeneralSimulationConfig): General simulation configurations.
        start_time (int): Start time of the simulation.
        duration (int): Duration of the simulation.
        sample_time (int): Sample time step of the simulation.
        time_step_range (list): Range of time steps for simulation.
        peak_power (int): Random peak power value.
        energy_form (dict): Mapping of energy form type to its description.
        data_export (DataHandler): Data handler for exporting results.
        energy_system_id (int): ID for the energy system being simulated.
        generation_id (int): ID for the generation component.
        environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.

    Methods:
        create_model() -> WindTurbine: Creates and returns an instance of the WindTurbine class.
        test_next(time_step): Tests the next() method of the WindTurbine component.
        test_state(): Tests the state attribute of the WindTurbine component.
    """
    config = None
    general_config: GeneralSimulationConfig = GeneralSimulationConfig(config)
    start_time = int(general_config.start)
    duration = int(general_config.duration)
    sample_time = int(general_config.timestep)
    time_step_range = list(range(start_time, start_time + duration, 4 * sample_time))
    peak_power = randint(0, 6000)
    energy_form = {3: 'Electricity'}
    data_export = DataHandler(result_dir=ROOT_PATH + "results/energy_system_simulation/", config=general_config)
    energy_system_id = 1
    generation_id = 1
    environmental_analysis_config = EnvironmentalAnalysisConfig(config=ConfigParser())

    def create_model(self) -> WindTurbine:
        """
        Create and return an instance of the WindTurbine class.

        Return:
            WindTurbine: An instance of the WindTurbine class with the test configurations.
        """
        return WindTurbine(self.peak_power, self.data_export, self.energy_system_id, self.generation_id,
                           self.general_config, self.environmental_analysis_config)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_next(self, time_step):
        """
        Test the next() method of the WindTurbine component.

        Args:
            time_step: The current time step for the simulation.

        Assertions:
            - The power output is within the specified range.
            - The energy form 'Heat' outputs 0 power for the wind turbine system.
        """
        uut = self.create_model()
        assert (self.peak_power > uut.next(time=time_step, energy_form=self.energy_form[3]) >= 0)
        assert uut.next(time=time_step, energy_form='Heat') == 0.0  # Wind turbine only outputs electricity

    def test_state(self):
        """
        Test the state attribute of the WindTurbine component.

        Assertions:
            - The state attribute is an instance of the GenerationComponentState class.
        """
        uut = self.create_model()
        assert isinstance(uut.state, GenerationComponentState)
