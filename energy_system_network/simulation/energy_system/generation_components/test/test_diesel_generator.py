import pytest
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.generation_components.diesel_generator \
    import DieselGenerator
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.constants_energy_sys_network import ROOT_PATH
from energy_system_network.config.analysis.environmental_analysis_config import EnvironmentalAnalysisConfig
from random import randint
from configparser import ConfigParser
from energy_system_network.commons.state.energy_system_component.generation_component.generation_component_state \
    import GenerationComponentState


class TestDieselGenerator:
    """
    Test suite for the DieselGenerator generation component.

    Attributes:
        config: Configuration for the test, initialized as None.
        general_config (GeneralSimulationConfig): General simulation configurations.
        start_time (int): Start time of the simulation.
        duration (int): Duration of the simulation.
        sample_time (int): Sample time step of the simulation.
        time_step_range (list): Range of time steps for simulation.
        peak_power (int): Random peak power value.
        energy_form (dict): Mapping of energy form types to their descriptions.
        data_export (DataHandler): Data handler for exporting results.
        energy_system_id (int): ID for the energy system being simulated.
        generation_id (int): ID for the generation component.
        environmental_analysis_config (EnvironmentalAnalysisConfig): Configuration for environmental analysis.

    Methods:
        create_model() -> DieselGenerator: Creates and returns a DieselGenerator instance.
        test_next(time_step): Tests the next() method of the DieselGenerator.
        test_calculate_fuel_consumption(): Tests the fuel consumption calculation.
        test_generation_emissions(): Tests the emission calculation based on fuel volume.
        test_state(): Validates the type of each state attribute in the DieselGenerator.
        test_get_production_emissions(): Validates the type of production emissions attribute.
        test_get_end_of_life_emissions(): Validates the type of end-of-life emissions attribute.
    """
    config = None
    general_config: GeneralSimulationConfig = GeneralSimulationConfig(config)
    start_time = int(general_config.start)
    duration = int(general_config.duration)
    sample_time = int(general_config.timestep)
    time_step_range = list(range(start_time, start_time + duration, 4 * sample_time))
    peak_power = randint(0, 3000)
    energy_form = {1: 'Electricity', 2: 'Heat'}
    data_export = DataHandler(result_dir=ROOT_PATH + "results/energy_system_simulation/", config=general_config)
    energy_system_id = 1
    generation_id = 1
    environmental_analysis_config = EnvironmentalAnalysisConfig(config=ConfigParser())

    def create_model(self) -> DieselGenerator:
        """
        Create and return an instance of the DieselGenerator class.

        Return:
            DieselGenerator: An instance of the DieselGenerator class with the test configurations.
        """
        return DieselGenerator(self.peak_power, self.data_export, self.energy_system_id, self.generation_id,
                               self.general_config, self.environmental_analysis_config)

    @pytest.mark.parametrize("time_step", time_step_range)
    def test_next(self, time_step):
        """
        Test the next() method of the DieselGenerator for various demand scenarios.

        Args:
            time_step: The current time step for the simulation.

        Assertions:
            - Validates the generated electrical power based on various target power demands.
            - Ensures heat generation is non-negative.
        """
        uut = self.create_model()

        # in each case heat produced is >= 0
        # the generated power will be equal to the demand if the demand is less than peak power
        target_electrical_power = randint(-self.peak_power, -1)
        assert uut.next(time=time_step, energy_form='Electricity',
                        target_electrical_power=target_electrical_power) == -target_electrical_power
        assert uut.next(time=time_step, energy_form='Heat') >= 0

        #  the generated power will be equal to the peak power if the demand is more than or equal to peak power
        assert uut.next(time=time_step, energy_form='Electricity',
                        target_electrical_power=randint(-self.peak_power * 100, -self.peak_power)) == self.peak_power
        assert uut.next(time=time_step, energy_form='Heat') >= 0

        #  the generated power will be zero if the demand is positive
        assert uut.next(time=time_step, energy_form='Electricity',
                        target_electrical_power=randint(0, 1000)) == 0.0
        assert uut.next(time=time_step, energy_form='Heat') >= 0

    def test_calculate_fuel_consumption(self):
        """
        Test the fuel consumption calculation based on the generation power.

        Assertions:
            - Validates that the fuel consumption is non-negative.
        """
        uut = self.create_model()
        uut.calculate_fuel_consumption(generation_power=randint(0, self.peak_power))
        assert uut.fuel_consumption() >= 0.0

    def test_generation_emissions(self):
        """
        Test the emission calculation based on fuel volume.

        Assertions:
            - Ensures the generated emission value is a float.
        """
        uut = self.create_model()
        assert isinstance(uut.generation_emissions(fuel_volume=randint(0, 50000)), float)

    def test_state(self):
        """
        Validate the type of each state attribute in the DieselGenerator.

        Assertions:
            - Ensures each state attribute is an instance of the GenerationComponentState class.
        """
        uut = self.create_model()
        for key in uut.state:
            assert isinstance(uut.state[key], GenerationComponentState)

    def test_get_production_emissions(self): # TODO check for other tests
        """
        Validate the type of the production emissions attribute.

        Assertions:
            - Ensures the production emissions attribute is a float.
        """
        uut = self.create_model()
        assert isinstance(uut.production_emissions, float)

    def test_get_end_of_life_emissions(self):
        """
        Validate the type of the end-of-life emissions attribute.

        Assertions:
            - Ensures the end-of-life emissions attribute is a float.
        """
        uut = self.create_model()
        assert isinstance(uut.end_of_life_emissions, float)
