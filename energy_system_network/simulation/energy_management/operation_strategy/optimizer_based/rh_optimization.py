import statistics
import numpy as np
from energy_system_network.commons import unit_converter
from energy_system_network.commons.state.energy_management.rh_optimization.electricity_rh_optimization_state import \
    ElectricityRHOptimizationState
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_management.operation_strategy.optimizer_based.optimizer_based_operation_strategy import \
    OptimizerBasedOperationStrategy
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent
import pyomo.environ as pyo
import datetime
import warnings


class RHOptimization(OptimizerBasedOperationStrategy):
    """
    RHOptimization is an energy management strategy based on optimization. It dictates target power values
    for all components of the energy system and aims to minimize the carbon emissions of the system.

    Attributes:
        __soc_reference_after_last_ts (float): Reference state of charge after the last time step.
        __print_values_for_ts (int): Counter for printing values for a time step.
        __must_run_generation (dict): Power values for must-run generation components.
        __grid_power (dict): Power values for grid components.
        __can_run_generation (dict): Power values for can-run generation components.
        __storage_power (dict): Power values for storage components.
        __must_fulfil_load (dict): Power values for must-fulfil load components.
        __can_fulfil_load (dict): Power values for can-fulfil load components.
        __residual_load_must_run_must_fulfil (dict): Residual load after considering must-run and must-fulfil components.
        __residual_load_final (dict): Final residual load after optimization.
        __generation_reference_power (dict): Reference power values for generation components.
        __grid_reference_power (dict): Reference power values for grid components.
        __storage_reference_power (dict): Reference power values for storage components.
        __storage_reference_soc (dict): Reference state of charge values for storage components.
        __export_power_generator (dict): Power values for exported power from generators.

    Methods:
        __init__(energy_forms, generation_components, storage_components, load_components, grid_components, general_config, ems_config):
            Initializes the RHOptimization class.

        next(time, adapted_time):
            Generates and dictates the target power values for all components of the energy system.

        create_variables_constraints(energy_form, time, adapted_time):
            Creates required subclass-specific constraints and objective.

        print_results(time):
            Prints the optimization results for each timestep.

        update(energy_management_state):
            Updates the Energy Management State for this operation strategy.

        allocate_export_power_generator():
            Allocates the calculated export power values to each must-run generation component.

        allocate_energy_consumption_emissions(grid_power, must_fulfil_load, grid_carbon_intensity, storage_power=None):
            Allocates Discharge Energy Consumption (DEC) and Grid Energy Consumption (GEC) emissions to load components.
    """
    def __init__(self, energy_forms: [str], generation_components: [GenerationComponent],
                 storage_components: [StorageComponent], load_components: [LoadComponent],
                 grid_components: [GridComponent],
                 general_config: GeneralSimulationConfig, ems_config: EnergyManagementConfig):
        """
        Instantiates a RHOptimization EMS class
        :param energy_forms: list of energy forms
        :param generation_components: list of generation components to control
        :param storage_components: list of generation components to control
        :param load_components: list of generation components to control
        :param grid_components: list of generation components to control
        :param general_config: general configuration
        :param ems_config: energy management system configuration
        """
        super().__init__(energy_forms,
                         general_config, ems_config,
                         generation_components, storage_components, load_components, grid_components,)

        # EmissionsDrivenSizing(generation_components, storage_components, load_components, grid_components, general_config, ems_config)

        self.__soc_reference_after_last_ts = 0
        self.__print_values_for_ts = 2

        # Initialize SOCI of energy storage to zero and obtain total peak power of storage components
        storage_peak_power = list()
        if self.storage_components:
            for storage_component in self.storage_components:
                storage_component.set_initial_carbon_intensity_stored_energy(0)
                storage_peak_power.append(storage_component.peak_power)
            total_storage_peak_power = sum(storage_peak_power)  # W
        else:
            raise Exception('No storage components specified. '
                  'EMS strategy RHOptimization currently needs at least one storage component.')

        # Obtain total peak power of grid component
        grid_component = self.grid_components[0]
        grid_peak_power = grid_component.peak_power
        total_grid_peak_power = grid_peak_power  # W

        # Obtain total peak power of load components
        load_peak_power = list()
        for load_component in self.load_components:
            load_peak_power.append(load_component.peak_power)
        total_load_peak_power = sum(load_peak_power)

        # Peak power check for user convenience
        buffer_pc = 0.025  # p.u.
        firm_power_difference = (total_grid_peak_power + total_storage_peak_power) - total_load_peak_power
        if 0 <= firm_power_difference <= total_load_peak_power * buffer_pc:  # Buffer of 2.5%
            print('\n')
            warning_msg = '\n \nPeak power of firm power sources is less than or within ' + str(buffer_pc * 100) + \
                          ' % of peak load power. ' \
                          '\nOptimization may possibly be infeasible in some periods. ' \
                          '\nDifference in W = ' + str(firm_power_difference) + \
                          '\nTry to increase peak powers of firm power sources if optimization fails.'
            warnings.warn(warning_msg)

        # Initialize variables to enable logging of state
        self.__must_run_generation = {}
        self.__grid_power = {}
        self.__can_run_generation = {}
        self.__storage_power = {}
        self.__must_fulfil_load = {}
        self.__can_fulfil_load = {}
        self.__residual_load_must_run_must_fulfil = {}
        self.__residual_load_final = {}

        self.__generation_reference_power = {}
        self.__grid_reference_power = {}
        self.__storage_reference_power = {}
        self.__storage_reference_soc = {}

        self.__export_power_generator = {}

    def next(self, time: float, adapted_time: float) -> None:
        """
        Generates and dictates the target power values for all components of the energy system
        :param time: timestamp of current timestep as float
        :param adapted_time: delta time to adjust for looping of simulations
        :return: None
        """
        for energy_form in self.energy_forms:  # type: str
            target_powers: dict = self.get_target_powers(energy_form, time, adapted_time)
            self.check_synchronization(target_powers, time)

            # Obtain load
            total_must_fulfil_load = 0
            if self.load_components:
                must_fulfil_load = list()
                for load_component in self.load_components:  # type: LoadComponent
                    if load_component.must_fulfill is True:
                        must_fulfil_load.append(load_component.next(time, energy_form, adapted_time))
                total_must_fulfil_load = sum(must_fulfil_load)
            self.__must_fulfil_load[energy_form] = total_must_fulfil_load  # in W

            # Obtain  powers for must-run Generation Components
            total_must_run_generation = 0
            if self.generation_components:
                must_run_generation = list()
                carbon_intensity_generation = list()
                for generation_component in self.generation_components:  # type: GenerationComponent
                    if generation_component.must_run is True:
                        must_run_generation.append(generation_component.next(time, energy_form, 0, adapted_time))
                        carbon_intensity_generation.append(generation_component.get_emissions_per_joule())
                total_must_run_generation = sum(must_run_generation)
            self.__must_run_generation[energy_form] = total_must_run_generation  # in W
            self.__can_run_generation[energy_form] = 0  # in W

            # Obtain reference powers
            try:
                self.__storage_reference_power[energy_form] = sum(target_powers[self.STORAGE])
                self.__storage_reference_soc[energy_form] = statistics.mean(target_powers[self.SOC])
                storage_reference_power = -1 * sum(target_powers[self.STORAGE])  # +ve -> ch, -ve -> dch
            except:
                self.__storage_reference_power[energy_form] = 0
                self.__storage_reference_soc[energy_form] = 0
                storage_reference_power = 0
            try:
                self.__grid_reference_power[energy_form] = target_powers[self.GRID]
                reference_grid_power = -1 * target_powers[self.GRID]  # -ve implies import request
            except:
                self.__grid_reference_power[energy_form] = 0
                reference_grid_power = 0
            try:
                self.__generation_reference_power[energy_form] = sum(target_powers[self.GENERATION])
                reference_must_run_generation = sum(target_powers[self.GENERATION])
            except:
                self.__generation_reference_power[energy_form] = 0
                reference_must_run_generation = 0

            # Obtain residual load
            # residual_load = reference_must_run_generation - total_must_fulfil_load  # in W
            residual_load = total_must_run_generation - total_must_fulfil_load  # in W
            self.__residual_load_must_run_must_fulfil[energy_form] = residual_load  # in W

            # Carbon intensity charging energy
            # Grid carbon intensities at present time step
            if self.grid_components:
                grid_component = self.grid_components[0]
                grid_carbon_intensity = grid_component.carbon_intensity_time_step(time, adapted_time)
            else:
                grid_carbon_intensity = 0.0

            if target_powers[self.GRID] < 1e-6:
                carbon_intensity_charging_energy = 0
            else:
                # Only carbon intensity of grid in the numerator,as all other generation components are fully within the system boundaries
                carbon_intensity_charging_energy = (- reference_grid_power * grid_carbon_intensity) / \
                                                   (- reference_grid_power + reference_must_run_generation)

            # Storage Components
            soc_after_last_ts = list()
            total_storage_power = 0
            power_st = storage_reference_power
            if self.storage_components:
                storage_power = list()
                if residual_load != 0.0:
                    for storage_component in self.storage_components:  # type: StorageComponent
                        storage_power.append(storage_component.next(time, energy_form, power_st,
                                                                    carbon_intensity_charging_energy, adapted_time))
                        soc_after_last_ts.append(storage_component.state.soc)
                        # storage power is +ve when discharging and -ve when charging
                        power_st += storage_power[-1]
                        # +ve at end implies partial fulfilment of ch request,
                        # -ve at end implies partial fulfilment of dch request
                total_storage_power = sum(storage_power)
                self.__storage_power[energy_form] = total_storage_power

            # intermediate_residual_power = reference_must_run_generation - total_must_fulfil_load + total_storage_power
            intermediate_residual_power = total_must_run_generation - total_must_fulfil_load + total_storage_power

            # Grid Components
            revised_grid_reference_power = intermediate_residual_power

            power_gr = revised_grid_reference_power
            total_grid_power = 0
            if self.grid_components:
                # if residual_load is not 0.0:
                total_grid_power = grid_component.next(time, energy_form, power_gr, adapted_time)
                # grid power is +ve if energy drawn, -ve if energy fed in
            self.__grid_power[energy_form] = total_grid_power

            # Allocate power exports to Must-Run generation based on highest carbon intensity first
            if self.generation_components and total_grid_power < -1e-3:
                exported_energy_generator = list(np.zeros(len(carbon_intensity_generation)))
                carbon_intensity_generation_ascending = sorted(
                    carbon_intensity_generation)  # ascending order

                unallocated_export = -1 * total_grid_power

                for gen in reversed(carbon_intensity_generation_ascending):
                    idx = carbon_intensity_generation.index(gen)
                    max_gen = must_run_generation[idx]
                    if unallocated_export <= max_gen:
                        exported_energy_generator[idx] = unallocated_export
                        unallocated_export = 0
                    else:
                        exported_energy_generator[idx] = max_gen
                        unallocated_export -= max_gen
                self.__export_power_generator[energy_form] = exported_energy_generator
                self.allocate_export_power_generator()

            final_residual_load = total_must_run_generation + total_grid_power + total_storage_power - total_must_fulfil_load

            revised_final_residual_load = final_residual_load

            # Allocate emissions to load # TODO verify if we have the same problem as the Sankey diagram here
            try:
                if self.storage_components and self.grid_components:
                    self.allocate_energy_consumption_emissions(total_grid_power, must_fulfil_load,
                                                               grid_carbon_intensity, storage_power)
                elif not self.storage_components:
                    self.allocate_energy_consumption_emissions(total_grid_power, must_fulfil_load,
                                                               grid_carbon_intensity)
            except:
                raise Exception('Check if Grid, Load, and Storage Components '
                                'are present in the energy system for the energy form ' + energy_form)

            self.__residual_load_final[energy_form] = revised_final_residual_load

            if revised_final_residual_load < 0 and abs(revised_final_residual_load) > 1e-3:
                self.__print_values_for_ts = 2

            if self.__print_values_for_ts > 0:
                print('\n')
                print('Epoch timestamp: ', time)
                print('Time: ', datetime.datetime.utcfromtimestamp(time))
                print('Load Reference in W: ', target_powers[self.LOAD])
                print('Load in W: ', must_fulfil_load)
                print('Final residual load in W: ', revised_final_residual_load)
                print('\n')
                print('Storage actual SOC (last timestep) after operation in p.u.: ', soc_after_last_ts)
                print('Storage reference SOC (last timestep) after operation in p.u.: ', self.__soc_reference_after_last_ts)
                print('\n')
                print('Storage reference power in W: ', storage_reference_power)
                print('Storage actual power in W: ', total_storage_power)

                print('Grid reference power in W: ', reference_grid_power)
                print('Grid actual power in W: ', total_grid_power)
                self.__print_values_for_ts -= 1

            self.__soc_reference_after_last_ts = target_powers[self.SOC]

    def create_variables_constraints(self, energy_form: str, time: float, adapted_time: float) -> None:
        """
        creates required subclass-specific constraints and objective and adds them to the pyomo model
        :param energy_form: energy form as str
        :param time: current epoch timestamp as float
        :param adapted_time: current adapted epoch timestamp as float
        :return: None
        """
        self.generation_variables_constraints(energy_form, time, adapted_time)
        self.grid_variables_constraints(time, adapted_time)
        self.create_grid_specific_emissions_series(time, adapted_time)
        self.storage_variables_constraints()
        self.energy_balance_constraint(energy_form, time, adapted_time)

        # add a model objective (kgCO2eq)
        self.model.objective = pyo.Objective(expr=(sum(self.specific_emissions_gen[i-1][j-1] * self.model.power_values_gen[i,j] for i in self.model.timesteps for j in self.model.number_generation_components) + sum(self.specific_emissions_grid[i-1] * self.model.power_values_grid[i] for i in self.model.timesteps)) * unit_converter.s_to_h(self.simulation_timestep), sense=pyo.minimize)

    def print_results(self, time: float) -> None:
        """
        prints the optimization results for each timestep
        :param time: current epoch timestamp as float
        :return: None
        """
        print('Objective function value in kgCO2: ', [pyo.value(self.model.objective)])
        print('\n')
        for i in range(1, self.timesteps + 1):
            print('Timestep: ', i)
            epoch_timestamp = time + (i - 1) * self.simulation_timestep
            print('Epoch timestamp: ', epoch_timestamp)
            print('Time: ', datetime.datetime.utcfromtimestamp(epoch_timestamp))
            print('Load in kW: ', round(self.must_fulfil_load[i - 1],2))
            print('Max generation power in kW: ', [round(x,2) for x in self.max_gen_power[i-1]])
            print('Specific emissions generators in kgCO2/kWh: ', [round(x,4) for x in self.specific_emissions_gen[i-1]])
            print('Specific emissions grid in kgCO2/kWh: ', round(self.specific_emissions_grid[i-1],4))
            try:
                print('Supplied power generators in kW: ', [round(pyo.value(self.model.power_values_gen[i,j]),2) for j in self.model.number_generation_components])
                print('Supplied power grid in kW: ', round(pyo.value(self.model.power_values_grid[i]),2))
                print('Storage charging power in kW: ', [round(pyo.value(self.model.storage_charging_power[i,k]),2) for k in self.model.number_storage_components])
                print('Storage discharging power in kW: ', [round(pyo.value(self.model.storage_discharging_power[i,k]),2) for k in self.model.number_storage_components])

                soc = [round(pyo.value(self.model.storage_energy_content[i, j]) / self.storage_energy_capacity[j - 1], 2) for j in self.model.number_storage_components]
                print('Storage SOC in p.u.: ', soc)
            except:
                print('Optimization did not converge.')
                pass
            print('\n')

    def update(self, energy_management_state: dict[str, ElectricityRHOptimizationState]) -> None:
        """
        updates the Energy Management State for this operation strategy
        :param energy_management_state: energy management state as dict
        :return: None
        """
        for energy_form in self.energy_forms:
            energy_management_state[energy_form].must_run_generation_power = self.__must_run_generation[energy_form]
            energy_management_state[energy_form].grid_power = self.__grid_power[energy_form]
            energy_management_state[energy_form].can_run_generation_power = self.__can_run_generation[energy_form]
            energy_management_state[energy_form].storage_power = self.__storage_power[energy_form]
            energy_management_state[energy_form].must_fulfil_load = self.__must_fulfil_load[energy_form]
            energy_management_state[energy_form].can_fulfil_load = 0.0 if not self.__can_fulfil_load else \
                self.__can_fulfil_load[energy_form]
            energy_management_state[energy_form].residual_load_must_run_must_fulfil = \
                self.__residual_load_must_run_must_fulfil[energy_form]
            energy_management_state[energy_form].residual_load_final = self.__residual_load_final[energy_form]

            energy_management_state[energy_form].storage_reference_power = self.__storage_reference_power[energy_form]
            energy_management_state[energy_form].grid_reference_power = self.__grid_reference_power[energy_form]
            energy_management_state[energy_form].storage_reference_soc = self.__storage_reference_soc[energy_form]
            energy_management_state[energy_form].generation_reference_power = self.__generation_reference_power[energy_form]

            if self.__storage_reference_power[energy_form] == 0:
                fulfilment = 1
            else:
                fulfilment = self.__storage_power[energy_form] / self.__storage_reference_power[energy_form]
            energy_management_state[energy_form].storage_power_fulfilment = fulfilment

    def allocate_export_power_generator(self) -> None:
        """
        Allocates the calculated export power values to each must-run generation component
        :return: None
        """
        for energy_form in self.energy_forms:
            # Allocate determined export power to Must-run Generation Components
            if self.generation_components:
                for generation_component, export_power in zip(self.generation_components, self.__export_power_generator[energy_form]):  # type: GenerationComponent, float
                    if generation_component.must_run is True:
                        generation_component.allocate_export_power(export_power)

    def allocate_energy_consumption_emissions(self, grid_power: float, must_fulfil_load: [float],
                                              grid_carbon_intensity: float, storage_power: [float] = None) -> None:
        """
        allocates Discharge Energy Consumption (DEC) and Grid Energy COnsumption (GEC) emissions to load components
        :param grid_power: grid power in W as float
        :param must_fulfil_load: must fulfill load powers in W as list of floats
        :param grid_carbon_intensity: grid carbon intensity in gCO2/kWh as float
        :param storage_power: storage powers in W as list of floats
        :return: None
        """
        discharge_energy_consumption_emissions = 0
        if storage_power is not None:
            if sum(storage_power) > 0:  # i.e. storage system discharging
                mean_stored_energy_carbon_intensity = list()
                for storage_component in self.storage_components:  # type: StorageComponent
                    idx = self.storage_components.index(storage_component)
                    mean_stored_energy_carbon_intensity.append(storage_component.carbon_intensity_stored_energy())
                    discharge_energy_consumption_emissions = unit_converter.J_to_kWh(abs(storage_power[idx]) * self.simulation_timestep) * \
                                                             unit_converter.g_to_kg(mean_stored_energy_carbon_intensity[-1])  # in kgCO2eq

        # Direct energy consumption from grid
        grid_energy_consumption_emissions = 0
        if grid_power > 0:  # i.e. energy import
            direct_grid_consumption = 0
            if storage_power is not None:
                if sum(storage_power) > 0:  # i.e. storage system discharging
                    direct_grid_consumption = grid_power  # in W
                elif sum(storage_power) <= 0:  # i.e. storage system charging
                    direct_grid_consumption = grid_power - abs(sum(storage_power))  # in W
            else:
                direct_grid_consumption = grid_power  # in W

            grid_energy_consumption_emissions = unit_converter.J_to_kWh(direct_grid_consumption * self.simulation_timestep) * \
                                                unit_converter.g_to_kg(grid_carbon_intensity)  # in kgCO2eq

        for load_component in self.load_components:  # type: LoadComponent
            idx = self.load_components.index(load_component)
            if must_fulfil_load[idx] < 1e-6:
                load_fraction = 0
            else:
                load_fraction = must_fulfil_load[idx] / sum(must_fulfil_load)
            if load_fraction > 0:
                load_grid_energy_consumption_emissions = load_fraction * grid_energy_consumption_emissions  # in kgCO2eq
                load_discharge_energy_consumption_emissions = load_fraction * discharge_energy_consumption_emissions  # in kgCO2eq
            else:
                load_grid_energy_consumption_emissions = 0
                load_discharge_energy_consumption_emissions = 0
            load_component.allocate_emissions(load_grid_energy_consumption_emissions,
                                              load_discharge_energy_consumption_emissions)  # in kgCO2eq
