from abc import abstractmethod
from pyomo.opt import TerminationCondition
from energy_system_network.commons import unit_converter
from energy_system_network.commons.state.energy_management.rh_optimization.rh_optimization_state import \
    RHOptimizationState
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_management.operation_strategy.operation_strategy import OperationStrategy
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent
import pyomo.environ as pyo


class OptimizerBasedOperationStrategy(OperationStrategy):
    """
    Optimizer-Based Operation Strategy for managing energy components and optimizing strategies.

    The class acts as a superclass that integrates and optimizes various energy components,
    such as generation, load, storage, and grid elements. It uses the pyomo library
    for formulating and solving optimization models.

    Attributes:
        energy_forms (list): Different forms of energy considered in the model.
        __simulation_duration (int): Duration of simulation.
        __simulation_timestep (int): Simulation timestep in seconds.
        __simulation_start (datetime): Starting time of the simulation.
        __simulation_end (datetime): Ending time of the simulation.
        adapted_time (int): adapted epoch timestamp
        optimization_time_horizon (int): Number of timesteps for the optimized strategy time horizon.
        rerun_optimization_after (int): Number of timesteps after which to rerun optimization.
        remaining_timesteps_end (int): Remaining timesteps to the end of the simulation.
        ts_since_last_optimization (int): Timesteps since the last optimization run.
        timesteps (int): number of timesteps
        generation_components ([GenerationComponent]): List of generation components.
        load_components ([LoadComponent]): List of load components.
        storage_components ([StorageComponent]): List of storage components.
        grid_components ([GridComponent]): List of grid components.
        LOAD (str): Load type.
        GENERATION (str): Generation type.
        STORAGE (str): Storage type.
        GRID (str): Grid type.
        SOC (str): State of Charge type.
        TIME (float): Time type.
        __must_fulfil_load (float): List of the loads that must be fulfilled.
        __max_gen_power (List): List of aximum generation power.
        __specific_emissions_gen (List): List of specific generator emissions in kg/kWh.
        __specific_emissions_grid (List): List of specific grid emissions in kg/kWh.
        __energy_prices (List): List of energy prices in EUR/kWh.
        __target_powers (List): List of target powers.
        __model (ConcreteModel): Pyomo optimization concrete model.
        __storage_energy_capacity (List): List of storage energy capacities in kWh.


    Methods:
        __init__(self, energy_forms: list,
                 general_config: GeneralSimulationConfig, ems_config: EnergyManagementConfig,
                 generation_components: [GenerationComponent]=None,
                 storage_components: [StorageComponent]=None,
                 load_components: [LoadComponent]=None,
                 grid_components: [GridComponent]=None):
            Initialize the optimization model with given configurations and components.
        simulation_timestep(): Returns the simulation timestep in seconds.
        must_fulfil_load(): Returns a list of must-fulfil load values in kW.
        max_gen_power(): Returns a list of maximum generation power values in kW.
        specific_emissions_gen(): Returns a list of specific generator emissions in kg/kWh.
        specific_emissions_grid(): Returns a list of specific grid emissions in kg/kWh.
        storage_energy_capacity(): Returns a list of storage energy capacities in kWh.
        energy_prices(): Returns a list of energy prices in EUR/kWh.
        model(): Returns the created pyomo optimization concrete model.
        get_target_powers(): Returns target values at the current timestep from the larger optimized solution.
        run_optimization(): Creates an optimization model and returns optimal values.
        create_concrete_model(): Creates a pyomo concrete model and initializes model time dimensions.
        generation_variables_constraints(): Creates constraints related to the selected generation components.
        grid_variables_constraints(): Creates constraints related to the selected grid component.
        create_grid_specific_emissions_series(): Creates a list of grid specific emissions over the optimization horizon.
        create_energy_prices_series(): Creates a list of energy prices over the optimization horizon.
        storage_variables_constraints(ch_efficiency, dch_efficiency): Sets up the constraints related to energy storage
        in the optimization model.
        energy_balance_constraint(energy_form, time, adapted_time): Creates the energy balance constraints to ensure
        that the system meets its energy requirements.
        print_results(time): Abstract method expected to be implemented in subclasses to display or save the results of
        the optimization.
        aggregate_target_values(time): Aggregates the target power values for all components over the optimization
        horizon.
        check_synchronization(targets, time): Validates if the timestamps used in the optimization model are
        synchronized with those in the input profiles.
        next(time, adapted_time): Abstract method to be defined in subclasses, dictating how the model should proceed to
        the next timestep.
        update(energy_management_state): Abstract method to be defined in subclasses, outlining how the model state
        should be updated.
        create_variables_constraints(energy_form, time, adapted_time): Abstract method that guides how variables and
        constraints should be created in the subclasses.

    Args:
        energy_forms (list): List of energy forms being considered.
        general_config (GeneralSimulationConfig): Configuration for the general simulation.
        ems_config (EnergyManagementConfig): Configuration for the energy management system.
        generation_components ([GenerationComponent], optional): List of generation components. Defaults to None.
        storage_components ([StorageComponent], optional): List of storage components. Defaults to None.
        load_components ([LoadComponent], optional): List of load components. Defaults to None.
        grid_components ([GridComponent], optional): List of grid components. Defaults to None.
    """
    LOAD: str = 'LOAD'
    GENERATION: str = 'GENERATION'
    STORAGE: str = 'STORAGE'
    GRID: str = 'GRID'
    SOC: str = 'SOC'
    TIME: float = 'TIME'

    def __init__(self, energy_forms: list,
                 general_config: GeneralSimulationConfig, ems_config: EnergyManagementConfig,
                 generation_components: [GenerationComponent] = None,
                 storage_components: [StorageComponent] = None,
                 load_components: [LoadComponent] = None,
                 grid_components: [GridComponent] = None):
        """
        Initializes the class with configurations for energy simulation and management.

        Parameters:
            energy_forms (list): List specifying different forms of energy being considered in the simulation.
            general_config (GeneralSimulationConfig): Configuration object containing general settings for the
                simulation, e.g., duration, timestep, start, and end.
            ems_config (EnergyManagementConfig): Configuration object containing settings for the energy management
                system, e.g., optimization time horizon and frequency of optimization reruns.
            generation_components ([GenerationComponent], optional): List of generation components to be included in
                the simulation. Default is None.
            storage_components ([StorageComponent], optional): List of storage components to be included in the
                simulation. Default is None.
            load_components ([LoadComponent], optional): List of load components to be included in the simulation.
                Default is None.
            grid_components ([GridComponent], optional): List of grid components to be included in the simulation.
                Default is None.
        """
        super().__init__()
        self.energy_forms = energy_forms
        self.__simulation_duration = general_config.duration  # will change based on how far into the future we look
        self.__simulation_timestep = general_config.timestep  # in seconds
        self.__simulation_start = general_config.start
        self.__simulation_end = general_config.end
        self.adapted_time = 0

        self.optimization_time_horizon = ems_config.optimized_strategy_time_horizon  # number of timesteps
        self.rerun_optimization_after = ems_config.rerun_optimization_timesteps  # number of timesteps

        # Initialize
        self.remaining_timesteps_end = 0
        self.ts_since_last_optimization: int = 0
        self.timesteps = 0

        self.generation_components: [GenerationComponent] = generation_components
        self.load_components: [LoadComponent] = load_components
        self.storage_components: [StorageComponent] = storage_components
        self.grid_components: [GridComponent] = grid_components

        self.__must_fulfil_load: [float] = list()
        self.__max_gen_power: [list] = list()
        self.__specific_emissions_gen: [list] = list()
        self.__specific_emissions_grid: list = list()

        self.__energy_prices: list = list()

        self.__target_powers: [dict] = []

        self.__model = pyo.ConcreteModel()

        self.__storage_energy_capacity: list = list()

    @property
    def simulation_timestep(self) -> float:
        """
        returns simulation timestep in s
        :return: __simulation_timestep as float
        """
        return self.__simulation_timestep

    @property
    def must_fulfil_load(self) -> [float]:
        """
        returns a list of must-fulfil load values in kW
        :return: __must_fulfil_load as list of floats
        """
        return self.__must_fulfil_load

    @property
    def max_gen_power(self) -> [float]:
        """
        returns a list of max generation power values in kW
        :return: __max_gen_power as list of floats
        """
        return self.__max_gen_power

    @property
    def specific_emissions_gen(self) -> [[float]]:
        """
        returns a list of specific generator emissions in kg/kWh
        :return: __specific_emissions_gen as list of floats
        """
        return self.__specific_emissions_gen

    @property
    def specific_emissions_grid(self) -> [float]:
        """
        returns a list of specific grid emissions in kg/kWh
        :return: __specific_emissions_grid as list of floats
        """
        return self.__specific_emissions_grid

    @property
    def storage_energy_capacity(self) -> [float]:
        """
        returns a list of storage energy capacities in kWh
        :return: __storage_energy_capacity as list of floats
        """
        return self.__storage_energy_capacity

    @property
    def energy_prices(self) -> [float]:
        """
        returns a list of energy prices in EUR/kWh
        :return: __energy_prices as list of floats
        """
        return self.__energy_prices

    @property
    def model(self) -> pyo.ConcreteModel:
        """
        returns the created pyomo optimization concrete model
        :return: __model as instance of ConcreteModel
        """
        return self.__model

    def get_target_powers(self, energy_form: str, time: float, adapted_time: float) -> dict:
        """
        returns target values at current timestep from the larger optimized solution
        :param energy_form: energy form as str
        :param time: current epoch timestamp as float
        :param adapted_time: current adapted epoch timestamp as float
        :return: dict of target values
        """
        if adapted_time > self.adapted_time:  # Reset for a new loop
            self.adapted_time = adapted_time
            self.__target_powers = []
        rerun_after = min(self.remaining_timesteps_end, self.rerun_optimization_after)
        if self.ts_since_last_optimization > rerun_after or not self.__target_powers:
            target_powers: [dict] = self.run_optimization(energy_form, time, adapted_time)
            # Falls back on the available target powers from the last optimization if current optimization run fails
            if target_powers:
                self.__target_powers: [dict] = target_powers
                self.ts_since_last_optimization = 0
            else:
                print('Current optimization run infeasible. ' + '\n' +
                      'Using available target power values from the previous optimization run... ' + '\n' +
                      'Timesteps since last optimization: ' + str(self.ts_since_last_optimization))

        self.ts_since_last_optimization += 1
        try:
            return self.__target_powers[self.ts_since_last_optimization - 1]  # power in W and storage SOC in p.u.
        except IndexError:
            print('Optimization failed to converge after ' + str(self.ts_since_last_optimization - 1) + ' attempts.')
            raise Exception('Optimization failed')

    def run_optimization(self, energy_form: str, time: float, adapted_time: float) -> [dict]:
        """
        creates an optimization model based on specific methods in subclasses and returns optimal values
        :param energy_form: energy form as str
        :param time: current epoch timestamp as float
        :param adapted_time: current adapted epoch timestamp as float
        :return: returns a list containing dicts of optimally solved target values at each timestep
        """

        self.create_concrete_model(time, adapted_time)
        self.create_variables_constraints(energy_form, time, adapted_time)

        # create a solver
        solver = pyo.SolverFactory("gurobi", solver_io="python")

        # solve
        solution = solver.solve(self.model, tee=False)
        print_optimizer_results = False

        if solution.solver.termination_condition == TerminationCondition.infeasible:
            print_optimizer_results = True
            print('Solver Status:' + solution.solver.status)
        elif 'ok' not in solution.solver.status:
            print('Solver Status:' + solution.solver.status)

        if print_optimizer_results:
            solution.write()
            self.print_results(time)

        target_powers: [dict] = self.aggregate_target_values(time)  # Powers in W and SOC in p.u.

        return target_powers

    def create_concrete_model(self, time: float, adapted_time: float) -> None:
        """
        creates a pyomo concrete model and initializes variables and time dimensions of the model
        :param time: current epoch timestamp as float
        :param adapted_time: current adapted epoch timestamp as float
        :return: None
        """
        self.__model = pyo.ConcreteModel()  # Reinitialize the model
        self.remaining_timesteps_end = round(
            (self.__simulation_end - (time - adapted_time)) / self.__simulation_timestep)
        self.timesteps = min(self.optimization_time_horizon,
                             self.remaining_timesteps_end)  # avoid error at last optimization
        special_timesteps = self.timesteps - 1  # for storage soc loop, consider only t-1 timesteps

        # create time variables in the model
        self.__model.timesteps = pyo.RangeSet(self.timesteps)
        self.__model.special_timesteps = pyo.RangeSet(special_timesteps)

    def generation_variables_constraints(self, energy_form: str, time: float, adapted_time: float) -> None:
        """
        creates required constraints related to the selected generation components and adds them to the pyomo model
        :param energy_form: energy form as str
        :param time: current epoch timestamp as float
        :param adapted_time: current adapted epoch timestamp as float
        :return: None
        """
        # Variables
        number_generation_components = len(self.generation_components)  # number of generation components
        self.__model.number_generation_components = pyo.RangeSet(number_generation_components)
        self.__model.power_values_gen = pyo.Var(self.__model.timesteps, self.__model.number_generation_components,
                                                within=pyo.NonNegativeReals)  # in kW

        # Constraints
        # Calculate: Specific Emissions - kg/kWh
        specific_emissions_gen = list()
        # Max power in kW
        max_gen_power = list()
        for i in range(self.timesteps):
            timestep_peak_power_gen = list()
            timestep_emissions_gen = list()

            for generation_component in self.generation_components:  # type: GenerationComponent
                if generation_component.must_run:
                    timestep_peak_power_gen.append(unit_converter.W_to_kW(
                        generation_component.next(i * self.__simulation_timestep + time, energy_form, 0.0,
                                                  adapted_time)))  # in kW
                    timestep_emissions_gen.append(0.0)
                else:
                    timestep_peak_power_gen.append(unit_converter.W_to_kW(generation_component.peak_power))  # in kW
                    timestep_emissions_gen.append(
                        unit_converter.kWh_to_J(generation_component.get_emissions_per_joule()))  # in kg/kWh

            max_gen_power.append(timestep_peak_power_gen)
            specific_emissions_gen.append(timestep_emissions_gen)

        self.__max_gen_power = max_gen_power
        self.__specific_emissions_gen = specific_emissions_gen

        def peak_power_gen_constraint(model, i, j):  # kW <= kW (Units on each side)
            return model.power_values_gen[i, j] <= max_gen_power[i - 1][j - 1]

        self.__model.peak_power_gen_constraint = pyo.Constraint(self.__model.timesteps,
                                                                self.__model.number_generation_components,
                                                                rule=peak_power_gen_constraint)

    def grid_variables_constraints(self, time: float, adapted_time: float) -> None:
        """
        creates required constraints related to the selected grid component and adds them to the pyomo model
        :param time: current epoch timestamp as float
        :param adapted_time: current adapted epoch timestamp as float
        :return: None
        """
        # Variables
        grid_component: GridComponent = self.grid_components[0]
        self.__model.power_values_grid = pyo.Var(self.__model.timesteps, within=pyo.NonNegativeReals)  # in kW

        # Constraints
        def peak_power_grid_constraint(model, i):  # kW <= kW (Units on each side)
            return model.power_values_grid[i] <= unit_converter.W_to_kW(grid_component.peak_power)

        self.__model.peak_power_grid_constraint = pyo.Constraint(self.__model.timesteps,
                                                                 rule=peak_power_grid_constraint)

    def create_grid_specific_emissions_series(self, time: float, adapted_time: float) -> None:
        """
        Creates and saves a list of grid specific emissions over the optimization horizon
        :param time: current epoch timestamp as float
        :param adapted_time: current adapted epoch timestamp as float
        :return: None
        """
        grid_component: GridComponent = self.grid_components[0]
        # Calculate: Specific Emissions - kg/kWh
        specific_emissions_grid = list()
        for i in range(self.timesteps):
            specific_emissions_grid.append(unit_converter.kWh_to_J(
                grid_component.get_emissions_per_joule(i * self.__simulation_timestep + time,
                                                       adapted_time)))  # in kgCO2/kWh
        self.__specific_emissions_grid = specific_emissions_grid

    def create_energy_prices_series(self, time: float, adapted_time: float) -> None:
        """
        Creates and saves a list of energy prices over the optimization horizon
        :param time:
        :param adapted_time:
        :return: None
        """
        grid_component: GridComponent = self.grid_components[0]
        # Calculate: Energy prices
        timestep_energy_prices = list()
        for i in range(self.timesteps):
            timestep_energy_prices.append(
                (grid_component.energy_cost_time_step(i * self.__simulation_timestep + time, adapted_time)))  # in €/kWh
        self.__energy_prices = timestep_energy_prices

    def storage_variables_constraints(self, ch_efficiency: float = None, dch_efficiency: float = None) -> None:
        """
        Creates and adds constraints related to the selected storage components to the pyomo model.

        The method focuses on charging and discharging constraints of storage components.
        It defines variables, such as number of storage components, charging/discharging power, and storage energy content.
        It also sets constraints for the storage components considering efficiency, peak power, and state of charge.

        Parameters:
            ch_efficiency (float, optional): Charging efficiency. If not provided, it will be deduced from the component's state.
            dch_efficiency (float, optional): Discharging efficiency. If not provided, it will be deduced from the component's state.

        Returns:
            None

        Internal Functions:
            - storage_charging_constraint: Defines the constraints for the charging power of storage.
            - storage_discharging_constraint: Defines the constraints for the discharging power of storage.
            - initial_charging_energy_constraint: Sets the initial energy constraint for charging.
            - charging_energy_constraint: Sets the energy constraint for charging from the second timestep onwards.
            - initial_discharging_energy_constraint: Sets the initial energy constraint for discharging.
            - discharging_energy_constraint: Sets the energy constraint for discharging from the second timestep onwards.
            - initial_soc_constraint: Sets the constraint for the state of charge at the first timestep.
            - delta_soc_constraint: Adjusts the state of charge based on the charging/discharging at the next timestep.

        """
        simulation_timestep_h = unit_converter.s_to_h(self.__simulation_timestep)  # in h
        # Variables
        self.__model.number_storage_components = pyo.RangeSet(len(self.storage_components))
        # Binary variable to exclude charging during discharging and vice-versa
        self.__model.binary_storage_states = pyo.Var(self.__model.timesteps, self.__model.number_storage_components,
                                                     domain=pyo.Binary, initialize=0)

        # Charging and discharging power in kW
        # storage_charging_power -ve
        # storage_discharging_power +ve
        self.__model.storage_charging_power = pyo.Var(self.__model.timesteps, self.__model.number_storage_components,
                                                      within=pyo.NonPositiveReals)  # in kW
        self.__model.storage_discharging_power = pyo.Var(self.__model.timesteps, self.__model.number_storage_components,
                                                         within=pyo.NonNegativeReals)  # in kW
        self.__model.storage_energy_content = pyo.Var(self.__model.timesteps, self.__model.number_storage_components,
                                                      within=pyo.NonNegativeReals)  # in kWh

        # Storage SOC - between 0 and 1
        soc_lower_bound = 0.05  # buffer to account for deviations in the optimizer and simulation model
        soc_upper_bound = 1
        # lower bound not zero to enable some buffer to take care of differences between target and actual powers from SimSES

        # Constraints
        # Adjust as per storage and grid configuration to minimize deviations between optimizer and simulation model
        # Constraint: Storage State, Charging, Discharging Power
        energy_capacity = list()
        storage_peak_power = list()
        initial_soc = list()
        for storage_component in self.storage_components:  # type: StorageComponent
            if ch_efficiency is None and dch_efficiency is None:
                storage_component_ch_efficiency_buffer = 0.05
                storage_component_dch_efficiency_buffer = 0.05
                storage_ch_efficiency = storage_component.state.cumulative_ch_efficiency - storage_component_ch_efficiency_buffer  # in p.u.
                storage_dch_efficiency = storage_component.state.cumulative_dch_efficiency - storage_component_dch_efficiency_buffer  # in p.u.
            else:
                storage_ch_efficiency = ch_efficiency
                storage_dch_efficiency = dch_efficiency
            energy_capacity.append(unit_converter.Wh_to_kWh(storage_component.energy_capacity) *
                                   (soc_upper_bound - soc_lower_bound) * storage_component.state.soh)  # in kWh
            storage_peak_power.append(unit_converter.W_to_kW(storage_component.peak_power))  # in kW
            initial_soc.append(storage_component.state.soc)  # in p.u.

        self.__storage_energy_capacity = energy_capacity.copy()

        def storage_charging_constraint(model, i, j):
            return -1 * model.storage_charging_power[i, j] \
                   - model.binary_storage_states[i, j] * storage_peak_power[j - 1] <= 0  # in kW

        self.__model.charging_power = pyo.Constraint(self.__model.timesteps, self.__model.number_storage_components,
                                                     rule=storage_charging_constraint)

        def storage_discharging_constraint(model, i, j):
            return model.storage_discharging_power[i, j] \
                   + (model.binary_storage_states[i, j] - 1) * storage_peak_power[j - 1] <= 0  # in kW

        self.__model.discharging_power = pyo.Constraint(self.__model.timesteps, self.__model.number_storage_components,
                                                        rule=storage_discharging_constraint)

        # Constraint: Storage Energy Content # Watt-second(=Joule) <= Joule --> all four constraints below
        def initial_charging_energy_constraint(model, j):  # for first timestep
            return -1 * model.storage_charging_power[1, j] \
                   * simulation_timestep_h * storage_ch_efficiency <= (1 - initial_soc[j - 1]) * energy_capacity[
                       j - 1]  # in kWh

        self.__model.initial_charging_energy_constraint = pyo.Constraint(self.__model.number_storage_components,
                                                                         rule=initial_charging_energy_constraint)

        def charging_energy_constraint(model, i, j):  # for timesteps 2:end
            return -1 * model.storage_charging_power[i + 1, j] \
                   * simulation_timestep_h * storage_ch_efficiency <= energy_capacity[0] - model.storage_energy_content[
                       i, j]  # in kWh

        self.__model.charging_energy_constraint = pyo.Constraint(self.__model.special_timesteps,
                                                                 self.__model.number_storage_components,
                                                                 rule=charging_energy_constraint)

        def initial_discharging_energy_constraint(model, j):  # for first timestep
            return model.storage_discharging_power[1, j] * simulation_timestep_h / storage_dch_efficiency \
                   <= initial_soc[j - 1] * energy_capacity[j - 1]  # in kWh

        self.__model.initial_discharging_energy_constraint = pyo.Constraint(self.__model.number_storage_components,
                                                                            rule=initial_discharging_energy_constraint)

        def discharging_energy_constraint(model, i, j):  # for timesteps 2:end
            return (model.storage_discharging_power[i + 1, j]) * simulation_timestep_h / storage_dch_efficiency \
                   <= model.storage_energy_content[i, j]  # in kWh

        self.__model.discharging_energy_constraint = pyo.Constraint(self.__model.special_timesteps,
                                                                    self.__model.number_storage_components,
                                                                    rule=discharging_energy_constraint)

        # Constraint: soc
        def initial_soc_constraint(model, j):  # for first timestep
            return initial_soc[j - 1] * energy_capacity[j - 1] \
                   - (model.storage_charging_power[1, j] * storage_ch_efficiency
                      + model.storage_discharging_power[1, j] / storage_dch_efficiency) * simulation_timestep_h \
                   == model.storage_energy_content[1, j]  # in kWh  # in kWh

        self.__model.intial_soc_constraint = pyo.Constraint(self.__model.number_storage_components,
                                                            rule=initial_soc_constraint)

        def delta_soc_constraint(model, i, j):  # for timesteps 2:end
            return model.storage_energy_content[i, j] \
                   - (model.storage_charging_power[i + 1, j] * storage_ch_efficiency
                      + model.storage_discharging_power[i + 1, j] / storage_dch_efficiency) * simulation_timestep_h \
                   == model.storage_energy_content[i + 1, j]  # in kWh

        self.__model.delta_soc_constraint = pyo.Constraint(self.__model.special_timesteps,
                                                           self.__model.number_storage_components,
                                                           rule=delta_soc_constraint)

    def energy_balance_constraint(self, energy_form: str, time: float, adapted_time: float) -> None:
        """
        creates required energy balance constraints and adds them to the pyomo model
        :param energy_form: energy form as str
        :param time: current epoch timestamp as float
        :param adapted_time: current adapted epoch timestamp as float
        :return: None
        """
        # Max load in kW
        load_efficiency = 1
        must_fulfil_load = list()
        for i in range(self.timesteps):
            loads = list()
            for load_component in self.load_components:  # type: LoadComponent
                if load_component.must_fulfill is True:
                    loads.append(unit_converter.W_to_kW(
                        load_component.next(i * self.__simulation_timestep + time, energy_form,
                                            adapted_time) / load_efficiency))  # in kW
            must_fulfil_load.append(sum(loads))  # in kW

        self.__must_fulfil_load = must_fulfil_load

        # Constraint: sum of powers (kW) >= power required by load (kW)
        def energy_balance_1(model, i):
            a = sum(model.power_values_gen[i, j] for j in model.number_generation_components)
            b = model.power_values_grid[i]
            c = sum(model.storage_charging_power[i, j] + model.storage_discharging_power[i, j] for j in
                    model.number_storage_components)
            return (a + b + c) - must_fulfil_load[i - 1] <= 1e-6  # in kW

        self.__model.energy_balance_1 = pyo.Constraint(self.__model.timesteps, rule=energy_balance_1)

        def energy_balance_2(model, i):
            a = sum(model.power_values_gen[i, j] for j in model.number_generation_components)
            b = model.power_values_grid[i]
            c = sum(model.storage_charging_power[i, j] + model.storage_discharging_power[i, j] for j in
                    model.number_storage_components)
            return - (a + b + c) + must_fulfil_load[i - 1] <= 1e-6  # in kW

        self.__model.energy_balance_2 = pyo.Constraint(self.__model.timesteps, rule=energy_balance_2)

    @abstractmethod
    def print_results(self, time: float) -> None:
        """
        Prints the optimization results for the given time step.

        Args:
            time (float): The current epoch timestamp for which the results should be printed.

        Returns:
            None
        """
        pass

    def aggregate_target_values(self, time: float) -> [dict]:
        """
        aggregates and create a list of dicts containing target values for all components at each timestep
        :return: target_powers as list of dicts
        """
        target_powers: [dict] = list()
        for i in range(1, self.timesteps + 1):
            powers = {}
            powers[self.TIME] = time + (i - 1) * self.simulation_timestep
            # Convert back to W to maintain compliance with the rest of the program
            if self.load_components:
                try:
                    powers[self.LOAD] = unit_converter.kW_to_W(self.__must_fulfil_load[i - 1])  # in kW
                except:
                    pass
            if self.generation_components:
                try:
                    powers[self.GENERATION] = [unit_converter.kW_to_W(pyo.value(self.__model.power_values_gen[i, j]))
                                               for j in self.__model.number_generation_components]  # in kW
                except:
                    pass
            if self.grid_components:
                try:
                    powers[self.GRID] = unit_converter.kW_to_W(pyo.value(self.__model.power_values_grid[i]))  # in kW
                except:
                    pass
            if self.storage_components:
                try:
                    ch_power = [unit_converter.kW_to_W(pyo.value(self.__model.storage_charging_power[i, k])) for k in
                                self.__model.number_storage_components]  # in kW
                    dch_power = [unit_converter.kW_to_W(pyo.value(self.__model.storage_discharging_power[i, k])) for k
                                 in self.__model.number_storage_components]  # in kW
                    storage_power = [0.0]
                    if abs(sum(ch_power)) > 1e-5:
                        storage_power = ch_power
                    elif abs(sum(dch_power)) > 1e-5:
                        storage_power = dch_power
                    powers[self.STORAGE] = storage_power  # in W
                    powers[self.SOC] = [
                        round(pyo.value(self.__model.storage_energy_content[i, k]), 4) / self.storage_energy_capacity[
                            k - 1] for k in self.__model.number_storage_components]  # in p.u.
                except:
                    pass
            target_powers.append(powers)  # powers in W and soc in p.u.
        return target_powers

    def check_synchronization(self, targets: dict, time: float):
        """
        Checks the synchronization between the optimization timestamps and the provided time.

        This method ensures that the time from the optimization model and the provided
        time argument are synchronized within a tolerance. If the time difference exceeds
        the threshold (1e-3), it raises an exception indicating potential discrepancies
        in timesteps used in the optimization model and the profiles.

        Parameters:
            targets (dict): A dictionary containing key-value pairs of various targets, including the time from the optimization model.
            time (float): The time value to be checked for synchronization against the optimization model's timestamp.

        Raises:
            Exception: If the optimization timestamps are found to be out of synchronization.

        """
        try:
            if abs(time - targets[self.TIME]) < 1e-3:
                pass
        except:
            raise Exception('Optimization timestamps out of sync. '
                            'Check the timesteps used in the optimization model and in the profiles.')

    def next(self, time: float, adapted_time: float) -> None:
        """
        specific implementation in each subclass
        :param time: current epoch timestamp as float
        :param adapted_time: current adapted epoch timestamp as float
        :return: None
        """
        pass

    def update(self, energy_management_state: dict[str, RHOptimizationState]) -> None:
        """
        specific implementation in each subclass
        :param energy_management_state:
        :return: None
        """
        pass

    def create_variables_constraints(self, energy_form: str, time: float, adapted_time: float):
        """
        specific implementation in each subclass
        :param energy_form: energy form as str
        :param time: current epoch timestamp as float
        :param adapted_time: current adapted epoch timestamp as float
        :return: None
        """
        pass
