import datetime
from energy_system_network.simulation.energy_management.operation_strategy.optimizer_based.optimizer_based_operation_strategy import \
    OptimizerBasedOperationStrategy
from energy_system_network.commons import unit_converter
from energy_system_network.commons.state.energy_management.rh_optimization.electricity_rh_optimization_state import \
    ElectricityRHOptimizationState
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent
import pyomo.environ as pyo


class ArbitrageOptimization(OptimizerBasedOperationStrategy):
    """
    ArbitrageOptimization is a strategy for optimizing energy arbitrage operations.

    Attributes:
        STORAGE (str): Constant representing storage.
        SOC (str): Constant representing state of charge.
        __emissions_driven (bool): Flag indicating if the optimization is driven by emissions.
        __soc_reference_after_last_ts (float): State of charge reference after the last time step.
        __print_values_for_ts (int): Values for time step printing.
        __storage_power (dict): Storage power values.
        __grid_power (dict): Grid power values.
        __residual_load_final (dict): Final residual load values.
        __grid_reference_power (dict): Grid reference power values.
        __storage_reference_power (dict): Storage reference power values.
        __storage_reference_soc (dict): Storage reference state of charge values.

    Methods:
        __init__(self, energy_forms, storage_components, grid_components, general_config, ems_config):
            Initializes the ArbitrageOptimization strategy.
        next(self, time, adapted_time):
            Processes the next time step for the energy management strategy.
        create_variables_constraints(self, energy_form, time, adapted_time):
            Creates subclass-specific constraints and objective for the pyomo model.
        print_results(self, time):
            Prints the optimization results for each timestep.
        update(self, energy_management_state):
            Updates the Energy Management State of the operation strategy.
        allocate_energy_consumption_emissions(self, grid_power, must_fulfil_load, average_grid_carbon_intensity, storage_power=None):
            Allocates energy consumption and emissions for the given parameters.
    """
    STORAGE: str = 'STORAGE'
    SOC: str = 'SOC'

    def __init__(self, energy_forms: list, storage_components: [StorageComponent],
                 grid_components: [GridComponent],
                 general_config: GeneralSimulationConfig, ems_config: EnergyManagementConfig):
        """
        Initializes the ArbitrageOptimization strategy with the provided configurations and components.

        Args:
            energy_forms (list): List of energy forms to be managed.
            storage_components ([StorageComponent]): List of storage components.
            grid_components ([GridComponent]): List of grid components.
            general_config (GeneralSimulationConfig): General configuration for the simulation.
            ems_config (EnergyManagementConfig): Configuration for the energy management system.
        """
        super().__init__(energy_forms, general_config, ems_config,
                         generation_components=None,
                         storage_components=storage_components,
                         load_components=None,
                         grid_components=grid_components,
                         )
        if EnergyManagementConfig.EMISSIONS in ems_config.arbitrage_type:
            self.__emissions_driven = True  # Driven by emissions
        else:
            self.__emissions_driven = False

        # Initialize
        self.__soc_reference_after_last_ts = 0
        self.__print_values_for_ts = 2

        # Initialize variables
        self.__storage_power = {}
        self.__grid_power = {}
        self.__residual_load_final = {}

        self.__grid_reference_power = {}
        self.__storage_reference_power = {}
        self.__storage_reference_soc = {}

    def next(self, time: float, adapted_time: float) -> None:
        """
        Processes the next time step for the energy management strategy.

        Args:
            time (float): Current epoch timestamp.
            adapted_time (float): Current adapted epoch timestamp.
        """
        for energy_form in self.energy_forms:
            target_powers: dict = self.get_target_powers(energy_form, time, adapted_time)  # Power in W, SOC in p.u.

            self.check_synchronization(target_powers, time)

            # Obtain reference powers
            self.__storage_reference_power[energy_form] = target_powers[self.STORAGE][0]  # in W
            self.__storage_reference_soc[energy_form] = target_powers[self.SOC][0]

            storage_reference_power = -1 * target_powers[self.STORAGE][0]  # in W
            # +ve -> ch, -ve -> dch

            # Carbon intensity charging energy
            # Grid carbon intensities at present time step
            grid_component = self.grid_components[0]
            grid_carbon_intensity = grid_component.carbon_intensity_time_step(time, adapted_time)

            carbon_intensity_charging_energy = grid_carbon_intensity

            # Storage Components
            soc_after_last_ts = list()
            storage_component: StorageComponent = self.storage_components[0]
            storage_power = storage_component.next(time, energy_form, storage_reference_power,
                                                        carbon_intensity_charging_energy, adapted_time)
            soc_after_last_ts.append(storage_component.state.soc)
            # storage power is +ve when discharging and -ve when charging
            # +ve at end implies partial fulfilment of ch request,
            # -ve at end implies partial fulfilment of dch request
            self.__storage_power[energy_form] = storage_power

            # Grid Components
            # if residual_load is not 0.0:
            grid_reference = storage_power
            grid_power = grid_component.next(time, energy_form, grid_reference, adapted_time)
            # grid power is +ve if energy drawn, -ve if energy fed in
            self.__grid_power[energy_form] = grid_power

            final_residual_load = grid_power + storage_power
            self.__residual_load_final[energy_form] = final_residual_load
            self.__energy_costs_timestep = grid_component.energy_cost_time_step(time, adapted_time)

    def create_variables_constraints(self, energy_form: str, time: float, adapted_time: float) -> None:
        """
        Creates required subclass-specific constraints and objective and adds them to the pyomo model.

        Args:
            energy_form (str): Energy form to be considered.
            time (float): Current epoch timestamp.
            adapted_time (float): Current adapted epoch timestamp.
        """
        if len(self.storage_components) > 1:
            raise Exception('EMS strategy ArbitrageOptimization only supports one energy storage system.')

        storage_ch_efficiency = 0.88
        storage_dch_efficiency = 0.88
        self.storage_variables_constraints(storage_ch_efficiency, storage_dch_efficiency)
        self.create_grid_specific_emissions_series(time, adapted_time)
        self.create_energy_prices_series(time, adapted_time)

        # add a model objective (kgCO2eq)
        if self.__emissions_driven:
            self.model.objective = pyo.Objective(expr=sum(
                (self.model.storage_discharging_power[i,j] + self.model.storage_charging_power[i,j]) * unit_converter.s_to_h(
                    self.simulation_timestep) * self.specific_emissions_grid[i - 1] for i in self.model.timesteps for j in self.model.number_storage_components)
                                            , sense=pyo.maximize)
        # add a model objective (Euro)
        else:
            self.model.objective = pyo.Objective(expr=sum(
                (self.model.storage_discharging_power[i,j] + self.model.storage_charging_power[i,j]) * unit_converter.s_to_h(
                    self.simulation_timestep) * self.energy_prices[i - 1] for i in self.model.timesteps for j in self.model.number_storage_components)
                                            , sense=pyo.maximize)
            # model.objective = pyo.Objective(expr= sum(-0.57 * (-model.storage_discharging_power[i] + model.storage_charging_power[i]) * unit_converter.s_to_h(self.__simulation_timestep)/energy_capacity[0] * 41.66
            #                                           + (model.storage_discharging_power[i] + model.storage_charging_power[i]) * unit_converter.s_to_h(self.__simulation_timestep) * timestep_energy_costs[i-1][0] for i in model.timesteps)
            #                                 , sense=pyo.maximize)

    def print_results(self, time: float) -> None:
        """
        Prints the optimization results for each timestep.

        Args:
            time (float): Current epoch timestamp.
        """
        try:
            if self.__emissions_driven:
                print('Results for emissions-driven abitrage.')
                objective_title = 'Objective function value in kgCO2: '
            else:
                print('Results for economics-driven abitrage.')
                objective_title = 'Objective function value in EUR: '
            print(objective_title, [round(pyo.value(self.model.objective), 4)])
        except:
            print('Optimization did not converge.')

        for i in range(1, self.timesteps + 1):
            epoch_timestamp = time + (i - 1) * self.simulation_timestep

            ch_power = [round(pyo.value(self.model.storage_charging_power[i,j]),2) for j in self.model.number_storage_components]
            dch_power = [round(pyo.value(self.model.storage_discharging_power[i,j]),2) for j in self.model.number_storage_components]
            soc = [round(pyo.value(self.model.storage_energy_content[i,j]) / self.storage_energy_capacity[j-1], 2) for j in self.model.number_storage_components]
            cyclic_degradation_costs = [round(
                (-dch_power[j-1] + ch_power[j-1]) * unit_converter.s_to_h(self.simulation_timestep) / self.storage_energy_capacity[j-1] * 41.66, 4) for j in self.model.number_storage_components]
            energy_cash_flow = [round(
                (ch_power[j-1] + dch_power[j-1]) * self.energy_prices[i - 1] * unit_converter.s_to_h(
                    self.simulation_timestep), 4) for j in self.model.number_storage_components]
            print('Timestep: ', i)
            print('Epoch timestamp: ', epoch_timestamp)
            print('Time: ', datetime.datetime.utcfromtimestamp(epoch_timestamp))
            print('Specific emissions grid in kgCO2/kWh: ', [self.specific_emissions_grid[i - 1]])
            print('Energy costs in EUR/kWh: ', [self.energy_prices[i - 1]])
            print('Energy-related cash flow in EUR: ', energy_cash_flow)
            print('Cyclic degradation costs in EUR:', cyclic_degradation_costs)
            print('Cash flow in EUR: ', [energy_cash_flow[j-1] + cyclic_degradation_costs[j-1] for j in self.model.number_storage_components])
            try:
                print('Storage charging power in W: ', ch_power)
                print('Storage discharging power in W: ', dch_power)
                print('Storage SOC in p.u.: ', soc)
                print('Storage energy content in kWh: ', [round(pyo.value(self.model.storage_energy_content[i,j]), 2) for j in self.model.number_storage_components])
            except:
                print('Optimization did not converge.')
                pass
            print('\n')

    def update(self, energy_management_state: dict[str, ElectricityRHOptimizationState]) -> None:
        """
        Updates the Energy Management State of the operation strategy.

        Args:
            energy_management_state (dict): Dictionary containing the Energy Management State for each energy form.
        """
        for energy_form in self.energy_forms:
            energy_management_state[energy_form].storage_power = self.__storage_power[energy_form]
            energy_management_state[energy_form].grid_power = self.__grid_power[energy_form]
            energy_management_state[energy_form].residual_load_final = self.__residual_load_final[energy_form]

            energy_management_state[energy_form].storage_reference_power = self.__storage_reference_power[energy_form]
            energy_management_state[energy_form].storage_reference_soc = self.__storage_reference_soc[energy_form]
            if self.__storage_reference_power[energy_form] == 0:
                fulfilment = 1
            else:
                fulfilment = self.__storage_power[energy_form] / self.__storage_reference_power[energy_form]
            energy_management_state[energy_form].storage_power_fulfilment = fulfilment
            # energy_management_state[energy_form].soc_prediction_accuracy = self.__storage_power[energy_form] / \
            #                                                                 self.__storage_reference_power[energy_form]
            energy_management_state[energy_form].cash_flow_timestep = unit_converter.W_to_kW(self.__storage_power[energy_form]) \
                                                                      * unit_converter.s_to_h(self.simulation_timestep) \
                                                                      * self.__energy_costs_timestep  # in EUR
            energy_management_state[energy_form].energy_price_timestep = self.__energy_costs_timestep  # EUR/kWh

    def allocate_energy_consumption_emissions(self, grid_power: list, must_fulfil_load: list,
                                              average_grid_carbon_intensity: float, storage_power: list = None):
        """
        Allocates energy consumption and emissions for the given parameters.

        Args:
            grid_power (list): List of grid power values.
            must_fulfil_load (list): List of load values that must be fulfilled.
            average_grid_carbon_intensity (float): Average carbon intensity of the grid.
            storage_power (list, optional): List of storage power values. Defaults to None.
        """
        pass