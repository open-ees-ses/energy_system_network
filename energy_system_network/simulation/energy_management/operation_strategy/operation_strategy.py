from abc import ABC, abstractmethod
from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState


class OperationStrategy(ABC):
    """
    Abstract base class for defining an algorithm to determine power set-points for components in the next timestep.

    Methods:
        __init__(self): Initializes the OperationStrategy object.
        next(self, time: float, adapted_time: float) -> None: Abstract method to provide power set-points for
            components.
        update(self, energy_management_state: dict[str, EnergyManagementState]) -> None: Abstract method to update
            energy management state.
        close(self) -> None: Closes open resources, if any.
    """

    def __init__(self):
        """
        Initializes the OperationStrategy object.

        Args:
            None

        Return:
            None
        """
        super().__init__()

    @abstractmethod
    def next(self, time: float, adapted_time: float) -> None:
        """
        Abstract method to provide the next power set-points for components in the energy system.

        Args:
            time (float): Current simulation time.
            adapted_time (float): Adapted or adjusted time value for the simulation.

        Return:
            None
        """
        pass

    @abstractmethod
    def update(self, energy_management_state: dict[str, EnergyManagementState]) -> None:
        """
        Abstract method to update energy management state variables.

        Args:
            energy_management_state (dict[str, EnergyManagementState]): Energy management state for components.

        Return:
            None
        """
        pass

    def close(self) -> None:
        """
        Closes any open resources used by the operation strategy.

        Args:
            None

        Return:
            None
        """
        pass

