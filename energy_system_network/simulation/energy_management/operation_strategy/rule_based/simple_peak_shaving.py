from energy_system_network.commons import unit_converter
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_management.simple_peak_shaving.simple_peak_shaving_state import \
    SimplePeakShavingState
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_management.operation_strategy.operation_strategy import OperationStrategy
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent
import statistics


class SimplePeakShaving(OperationStrategy):
    """
    SimplePeakShaving class represents an operation strategy that uses a simplified method for peak shaving.
    This strategy first withdraws energy from the grid and then utilizes storage devices as needed.
    It also takes into account carbon intensities for better energy management decisions.

    Attributes:
    -----------
        __energy_forms (list[EnergyForm]): List of energy forms for peak shaving operations.
        __simulation_duration (int): Total duration of the simulation.
        __simulation_timestep (int): Time step duration for the simulation.
        __simulation_start (int): Start time of the simulation.
        __generation_components (list[GenerationComponent]): Generation components in the energy system.
        __load_components (list[LoadComponent]): Load components in the energy system.
        __storage_components (list[StorageComponent]): Storage components for the operation strategy.
        __grid_components (list[GridComponent]): Grid components that can be utilized.
        __total_grid_import_peak_power (float): Total peak power that can be imported from the grid.
        __grid_power (dict[EnergyForm, float]): Dictionary mapping energy forms to their respective grid powers.
        __storage_power (dict[EnergyForm, float]): Dictionary mapping energy forms to their respective storage powers.
        __must_fulfil_load (dict[EnergyForm, float]): Dictionary mapping energy forms to the load that must be fulfilled.
        __residual_load_final (dict[EnergyForm, float]): Dictionary mapping energy forms to their respective residual loads.

    Methods:
    --------
        __init__(energy_forms: list[EnergyForm], generation_components: list[GenerationComponent],
                   storage_components: list[StorageComponent], load_components: list[LoadComponent],
                   grid_components: list[GridComponent], ems_config: EnergyManagementConfig,
                   general_config: GeneralSimulationConfig)
            Initializes the SimplePeakShaving instance with the provided configurations and components.

        next(time: float, adapted_time: float)
            Drives the operation for a given time step, considering grid, storage, and loads.

        allocate_energy_consumption_emissions(grid_power: list, must_fulfil_load: list,
                                                average_grid_carbon_intensity: float, storage_power: list)
            Allocates emissions based on the energy consumption of different components.

        update(energy_management_state: dict[str, SimplePeakShavingState])
            Updates the energy management state with the latest values from the strategy.
    """
    def __init__(self, energy_forms: list, generation_components: [GenerationComponent],
                 storage_components: [StorageComponent], load_components: [LoadComponent],
                 grid_components: [GridComponent], ems_config: EnergyManagementConfig,
                 general_config: GeneralSimulationConfig):
        """
        Initialize a SimplePeakShaving instance.

        Args:
            energy_forms (list[EnergyForm]): List of energy forms for peak shaving operations.
            generation_components (list[GenerationComponent]): Generation components in the energy system.
            storage_components (list[StorageComponent]): Storage components for the operation strategy.
            load_components (list[LoadComponent]): Load components in the energy system.
            grid_components (list[GridComponent]): Grid components that can be utilized.
            ems_config (EnergyManagementConfig): Configuration for the energy management system.
            general_config (GeneralSimulationConfig): General configuration parameters for the simulation.

        Return:
            None
        """
        super().__init__()
        self.__energy_forms = energy_forms
        self.__simulation_duration = general_config.duration
        self.__simulation_timestep = general_config.timestep
        self.__simulation_start = general_config.start

        self.__generation_components: [GenerationComponent] = generation_components
        self.__load_components: [LoadComponent] = load_components
        self.__storage_components: [StorageComponent] = storage_components
        self.__grid_components: [GridComponent] = grid_components

        # Initialize mean carbon intensity of stored energy at first time step with average grid carbon intensities
        average_grid_carbon_intensities = list()
        for grid_component in self.__grid_components:
            average_grid_carbon_intensities.append(grid_component.average_carbon_intensity)
        average_grid_carbon_intensity = statistics.mean(average_grid_carbon_intensities)
        # TODO Average values taken for now

        for storage_component in self.__storage_components:
            storage_component.set_initial_carbon_intensity_stored_energy(average_grid_carbon_intensity)

        # Maximum grid import power
        self.__total_grid_import_peak_power = 0
        for grid_component in self.__grid_components:
            self.__total_grid_import_peak_power += grid_component.next(0, EnergyForm.ELECTRICITY,
                                                                       -grid_component.peak_power)

        self.__grid_power = {}
        self.__storage_power = {}
        self.__must_fulfil_load = {}
        self.__residual_load_final = {}

    def next(self, time: float, adapted_time: float) -> None:
        """
        Determines the operation for a given time step.

        Withdraws energy from the grid first before using the storage devices.

        Args:
            time (float): Current time step in the simulation.
            adapted_time (float): Adapted time step value.

        Return:
            None
        """
        for energy_form in self.__energy_forms:

            # Grid carbon intensities at present time step
            if self.__grid_components:
                grid_carbon_intensities = list()
                for grid_component in self.__grid_components:
                    grid_carbon_intensities.append(grid_component.carbon_intensity_time_step(time, adapted_time))
                average_grid_carbon_intensity = statistics.mean(grid_carbon_intensities)
                # TODO Average values taken for now
            else:
                average_grid_carbon_intensity = 0.0

            # Load Components
            total_must_fulfil_load = 0
            if self.__load_components:
                must_fulfil_load = list()
                for load_component in self.__load_components:
                    if load_component.must_fulfill is True:
                        must_fulfil_load.append(load_component.next(time, energy_form, adapted_time))
                total_must_fulfil_load = sum(must_fulfil_load)
            self.__must_fulfil_load[energy_form] = total_must_fulfil_load

            residual_load = self.__must_fulfil_load[energy_form]
            storage_reference_power = self.__total_grid_import_peak_power - residual_load
            # storage reference power is -ve when storage should discharge

            # Storage Components
            total_storage_power = 0
            if self.__storage_components:
                storage_power = list()
                if residual_load != 0.0:
                    for storage_component in self.__storage_components:
                        storage_power.append(storage_component.next(time, energy_form, storage_reference_power,
                                                                    average_grid_carbon_intensity, adapted_time))
                        # storage power is +ve when discharging and -ve when charging
                        storage_reference_power += storage_power[-1]
                total_storage_power = sum(storage_power)
                self.__storage_power[energy_form] = total_storage_power

            reference_grid_power = total_storage_power - residual_load

            # Grid Components
            total_grid_power = 0
            if self.__grid_components:
                grid_power = list()
                # if residual_load is not 0.0:
                for grid_component in self.__grid_components:
                    grid_power.append(grid_component.next(time, energy_form, reference_grid_power, adapted_time))
                    # grid power is +ve if energy drawn, -ve if energy fed in
                    reference_grid_power += grid_power[-1]
                total_grid_power = sum(grid_power)
            self.__grid_power[energy_form] = total_grid_power

            # Allocate emissions to load
            try:
                if self.__storage_components and self.__grid_components:
                    self.allocate_energy_consumption_emissions(grid_power, must_fulfil_load,
                                                                average_grid_carbon_intensity, storage_power)
                elif not self.__storage_components:
                    self.allocate_energy_consumption_emissions(grid_power, must_fulfil_load,
                                                               average_grid_carbon_intensity)
            except:
                raise Exception(
                    'Check if Grid, Load, and Storage Components are present in the energy system for the energy form ' + energy_form)

            self.__residual_load_final[energy_form] = reference_grid_power

    def allocate_energy_consumption_emissions(self, grid_power: list, must_fulfil_load: list,
                                              average_grid_carbon_intensity: float, storage_power: list = None):
        """
        Allocates the emissions related to energy consumption.

        Calculates the emissions emitted by storage, grid and load components.

        Args:
            grid_power (list): Power being drawn from the grid.
            must_fulfil_load (list): List of loads that must be fulfilled.
            average_grid_carbon_intensity (float): Average carbon intensity of the grid.
            storage_power (list, optional): Power being drawn from storage devices.

        Return:
            None
        """
        discharge_energy_consumption_emissions = 0
        if storage_power is not None:
            if sum(storage_power) > 0:  # i.e. storage system discharging
                mean_stored_energy_carbon_intensity = list()
                for storage_component in self.__storage_components:
                    idx = self.__storage_components.index(storage_component)
                    mean_stored_energy_carbon_intensity.append(storage_component.carbon_intensity_stored_energy())
                    discharge_energy_consumption_emissions = unit_converter.J_to_kWh(abs(storage_power[idx]) * self.__simulation_timestep) * \
                                                             unit_converter.g_to_kg(mean_stored_energy_carbon_intensity[-1])  # in kgCO2eq

        # Direct energy consumption from grid
        grid_energy_consumption_emissions = 0
        if sum(grid_power) > 0:  # i.e. energy import
            direct_grid_consumption = 0
            if storage_power is not None:
                if sum(storage_power) > 0:  # i.e. storage system discharging
                    direct_grid_consumption = sum(grid_power)  # in W
                elif sum(storage_power) <= 0:  # i.e. storage system charging
                    direct_grid_consumption = sum(grid_power) - abs(sum(storage_power))  # in W
            else:
                direct_grid_consumption = sum(grid_power)  # in W

            grid_energy_consumption_emissions = unit_converter.J_to_kWh(direct_grid_consumption * self.__simulation_timestep) * \
                                                unit_converter.g_to_kg(average_grid_carbon_intensity)  # in kgCO2eq

        for load_component in self.__load_components:
            idx = self.__load_components.index(load_component)
            if must_fulfil_load[idx] < 1e-6:
                load_fraction = 0
            else:
                load_fraction = must_fulfil_load[idx] / sum(must_fulfil_load)
            if load_fraction > 0:
                load_grid_energy_consumption_emissions = load_fraction * grid_energy_consumption_emissions  # in kgCO2eq
                load_discharge_energy_consumption_emissions = load_fraction * discharge_energy_consumption_emissions  # in kgCO2eq
            else:
                load_grid_energy_consumption_emissions = 0
                load_discharge_energy_consumption_emissions = 0
            load_component.allocate_emissions(load_grid_energy_consumption_emissions,
                                              load_discharge_energy_consumption_emissions)  # in kgCO2eq

    def update(self, energy_management_state: dict[str, SimplePeakShavingState]) -> None:
        """
        Updates the internal states based on the energy management state.

        Args:
            energy_management_state (dict[str, SimplePeakShavingState]): Dictionary of energy management states for
                each energy form.

        Return:
            None
        """

        for energy_form in self.__energy_forms:
            energy_management_state[energy_form].grid_power = self.__grid_power[energy_form]
            if self.__storage_components:
                energy_management_state[energy_form].storage_power = self.__storage_power[energy_form]
            energy_management_state[energy_form].must_fulfil_load = self.__must_fulfil_load[energy_form]
            energy_management_state[energy_form].residual_load_final = self.__residual_load_final[energy_form]
