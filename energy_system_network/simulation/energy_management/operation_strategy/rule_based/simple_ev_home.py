from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_management.ev_home.ev_home_state import EVHomeState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_management.operation_strategy.operation_strategy import OperationStrategy
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent
import statistics


class SimpleEVHome(OperationStrategy):
    """
    Represents a simple electric vehicle (EV) home operation strategy.

    This class provides mechanisms for simulating how EV homes integrate with
    the grid and storage components. The strategy includes calculations based
    on carbon intensities, grid interactions, and storage behaviors to emulate
    the operations of an EV home environment.

    Attributes:
        __energy_forms (list): A list of energy forms to be considered in the simulation.
        __simulation_duration (int): The duration of the simulation.
        __simulation_timestep (int): Time interval between each simulation step.
        __simulation_start (float): The starting point of the simulation.
        __storage_components (list[StorageComponent]): List of storage components to interact with.
        __grid_components (list[GridComponent]): List of grid components to integrate with.
        __total_grid_import_peak_power (float): The sum of peak power import capacity from all grid components.
        __grid_power (dict): Dictionary storing the power interactions with the grid by energy form.
        __storage_power (dict): Dictionary storing the power interactions with storage components by energy form.
        __residual_load_final (dict): Dictionary storing the final residual load for each energy form.

    Methods:
        __init__(energy_forms, grid_components, storage_components, general_config): Initialize the SimpleEVHome with required parameters.
        next(time, adapted_time): Simulate the next step in the EV home operation.
        update(energy_management_state): Update the Energy Management State after each simulation step.
    """

    def __init__(self, energy_forms: list,
                 grid_components: [GridComponent],
                 storage_components: [StorageComponent],
                 general_config: GeneralSimulationConfig):
        """
        Initialize the SimpleEVHome with the required parameters.

        Args:
            energy_forms (list): List of energy forms to be considered in the simulation.
            grid_components (list[GridComponent]): List of grid components.
            storage_components (list[StorageComponent]): List of storage components.
            general_config (GeneralSimulationConfig): General configuration settings for the simulation.
        """
        super().__init__()
        self.__energy_forms = energy_forms
        self.__simulation_duration = general_config.duration
        self.__simulation_timestep = general_config.timestep
        self.__simulation_start = general_config.start

        self.__storage_components: [StorageComponent] = storage_components
        self.__grid_components: [GridComponent] = grid_components

        # Initialize mean carbon intensity of stored energy at first time step with average grid carbon intensities
        average_grid_carbon_intensities = list()
        # Maximum grid import power
        self.__total_grid_import_peak_power = 0
        for grid_component in self.__grid_components:  # type: GridComponent
            average_grid_carbon_intensities.append(grid_component.average_carbon_intensity)
            self.__total_grid_import_peak_power += grid_component.next(0, EnergyForm.ELECTRICITY,
                                                                      -grid_component.peak_power)
        average_grid_carbon_intensity = statistics.mean(average_grid_carbon_intensities)
        # TODO Average values taken for now

        for storage_component in self.__storage_components:  # type: StorageComponent
            storage_component.set_initial_carbon_intensity_stored_energy(average_grid_carbon_intensity)

        self.__grid_power = {}
        self.__storage_power = {}
        self.__residual_load_final = {}

    def next(self, time: float, adapted_time: float) -> None:
        """
        Simulate the next step in the EV home operation.

        This method simulates the operations for the given time step, taking into account the grid carbon
        intensities and storage behaviors. The interactions with the grid and storage components are
        calculated and stored for future reference.

        Args:
            time (float): Epoch timestamp indicating the current time.
            adapted_time (float): Adapted time offset, used for simulation reference.

        Returns:
            None
        """
        for energy_form in self.__energy_forms:

            # Grid carbon intensities at present time step
            if self.__grid_components:
                grid_carbon_intensities = list()
                for grid_component in self.__grid_components:  # type: GridComponent
                    grid_carbon_intensities.append(grid_component.carbon_intensity_time_step(time, adapted_time))
                average_grid_carbon_intensity = statistics.mean(grid_carbon_intensities)
            else:
                average_grid_carbon_intensity = 0.0

            # storage reference power is generated by the EV itself
            # storage reference power is -ve when storage should discharge
            # No explicit load components required

            # Storage Components
            total_storage_power = 0
            if self.__storage_components:
                storage_power = list()
                for storage_component in self.__storage_components:  # type: StorageComponent
                    storage_power.append(storage_component.next(time, energy_form, 0.0,
                                                                average_grid_carbon_intensity, adapted_time))
                    # storage power is +ve when discharging and -ve when charging
                total_storage_power = sum(storage_power)
                self.__storage_power[energy_form] = total_storage_power

            reference_grid_power = total_storage_power

            # Grid Components
            total_grid_power = 0
            if self.__grid_components:
                grid_power = list()
                # if residual_load is not 0.0:
                for grid_component in self.__grid_components:  # type: GridComponent
                    grid_power.append(grid_component.next(time, energy_form, reference_grid_power, adapted_time))
                    # grid power is +ve if energy drawn, -ve if energy fed in
                    reference_grid_power += grid_power[-1]
                total_grid_power = sum(grid_power)
            self.__grid_power[energy_form] = total_grid_power

            self.__residual_load_final[energy_form] = total_storage_power + total_grid_power  # in W

    def update(self, energy_management_state: dict[str, EVHomeState]) -> None:
        """
        Update the Energy Management State (EMS) for the strategy after each simulation step.

        This method updates the energy management state with the results of the previous simulation
        step. It updates grid power, storage power, and residual load for each energy form.

        Args:
            energy_management_state (dict[str, EVHomeState]): Current state of the energy management system.

        Returns:
            None
        """
        for energy_form in self.__energy_forms:
            energy_management_state[energy_form].grid_power = self.__grid_power[energy_form]
            if self.__storage_components:
                energy_management_state[energy_form].storage_power = self.__storage_power[energy_form]
            energy_management_state[energy_form].residual_load_final = self.__residual_load_final[energy_form]

