from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState
from energy_system_network.simulation.energy_management.operation_strategy.operation_strategy import OperationStrategy


class NoStrategy(OperationStrategy):
    """
    Implements a no-operation strategy for energy management.

    This strategy performs no actions or adjustments, serving as a placeholder or default strategy when no specific
    operational logic is required. It can be useful as a base or comparison case in simulations and analysis.

    Methods:
        __init__(energy_form: list, config_ems): Initializes the NoStrategy object.
        next(time: float, adapted_time: float) -> float: Performs no action and returns a default value.
        update(energy_management_state: EnergyManagementState) -> None: Performs no update actions.
    """
    def __init__(self, energy_form: list, config_ems):
        """
        Initializes the NoStrategy object with the given energy forms and EMS configuration.

        Args:
            energy_form (list): List of energy forms being considered.
            config_ems (object): Configuration related to the energy management system.

        Return:
            None
        """
        super().__init__()

    def next(self, time: float, adapted_time: float) -> float:
        """
        No operational action is taken. This method returns a default value.

        Args:
            time (float): Current simulation time.
            adapted_time (float): Adapted or adjusted time value for the simulation.

        Return:
            float: A default value.
        """

        pass

    def update(self, energy_management_state: EnergyManagementState) -> None:
        """
        Performs no update actions on the energy management state.

        Args:
            energy_management_state (EnergyManagementState): Current state of the energy management system.

        Return:
            None
        """
        pass
