import numpy as np
from energy_system_network.commons.state.energy_management.simple_deficit_coverage.simple_deficit_coverage_state import \
    SimpleDeficitCoverageState
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_management.operation_strategy.operation_strategy import OperationStrategy
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent


class SimpleDeficitCoverage(OperationStrategy):
    """
    Implements the Simple Deficit Coverage operational strategy. In this strategy, the battery energy storage system
    (BESS) is never charged with grid power, but solely with renewable energy. The emissions from the grid represent
    the emissions of the energy consumed by the load center.

    Attributes
    ----------
        STORAGE_COMPONENTS (str): Key for storage components in priority order.
        GRID_COMPONENTS (str): Key for grid components in priority order.
        CAN_RUN_GENERATION (str): Key for can run generation components in priority order.
        __energy_forms (list): List of energy forms being considered.
        __generation_components ([GenerationComponent]): List of generation components.
        __load_components ([LoadComponent]): List of load components.
        __storage_components ([StorageComponent]): List of storage components.
        __grid_components ([GridComponent]): List of grid components.
        __flexible_component_priority (list): Priority list of flexible components for deficit coverage.
        __export_power_generator (dict): Dictionary holding the export power generator data.
         __simulation_duration (int): Duration of the simulation.
        __simulation_timestep (int): Time step for each simulation cycle.
        __simulation_start (datetime): Starting time of the simulation.
        __must_run_generation (dict): Information about must-run generation components.
        __grid_power (dict): Information about grid power components.
        __can_run_generation (dict): Information about can-run generation components.
        __storage_power (dict): Information about storage power components.
        __must_fulfil_load (dict): Information about must-fulfil load components.
        __can_fulfil_load (dict): Information about can-fulfil load components.
        __residual_load_must_run_must_fulfil (dict): Residual load after must-run and must-fulfil components.
        __residual_load_final (dict): Final residual load.

    Methods
    -------
        __init__(energy_forms, generation_components, storage_components, load_components,
                 grid_components, ems_config, general_config):
            Initializes the SimpleDeficitCoverage object.
        next(time: float, adapted_time: float) -> None:
            Determines the power flow for each energy component based on the Simple Deficit Coverage strategy.
        allocate_energy_consumption_emissions(must_fulfil_load: list, energy_form: str) -> None:
            Calculates the emissions from the grid and load components.
        allocate_export_power_generator() -> None:
            Allocates export power values to each must-run generation component.
        update(energy_management_state: dict[str, SimpleDeficitCoverageState]) -> None:
            Updates the state values for different energy components based on calculated values.
    """
    # Priority keys from simulation.ini
    STORAGE_COMPONENTS: str = 'StorageComponents'
    GRID_COMPONENTS: str = 'GridComponents'
    CAN_RUN_GENERATION: str = 'CanRunGenerationComponents'

    def __init__(self, energy_forms: list, generation_components: [GenerationComponent],
                 storage_components: [StorageComponent], load_components: [LoadComponent],
                 grid_components: [GridComponent], ems_config: EnergyManagementConfig,
                 general_config: GeneralSimulationConfig):
        """
        Initializes the SimpleDeficitCoverage object with the given parameters.

        Args:
            energy_forms (list): List of energy forms being considered.
            generation_components ([GenerationComponent]): List of generation components.
            storage_components ([StorageComponent]): List of storage components.
            load_components ([LoadComponent]): List of load components.
            grid_components ([GridComponent]): List of grid components.
            ems_config (EnergyManagementConfig): Configuration related to energy management.
            general_config (GeneralSimulationConfig): General configuration for the simulation.

        Return:
            None
        """
        super().__init__()
        self.__energy_forms = energy_forms
        self.__simulation_duration = general_config.duration
        self.__simulation_timestep = general_config.timestep
        self.__simulation_start = general_config.start

        self.__generation_components: [GenerationComponent] = generation_components
        self.__load_components: [LoadComponent] = load_components
        self.__storage_components: [StorageComponent] = storage_components
        self.__grid_components: [GridComponent] = grid_components
        self.__flexible_component_priority = ems_config.simple_deficit_coverage_priority

        for storage_component in self.__storage_components:
            storage_component.set_initial_carbon_intensity_stored_energy(0)
            # The storage is never charged with grid energy in this OS, hence updated with 0

        self.__export_power_generator = {}

        self.__must_run_generation = {}
        self.__grid_power = {}
        self.__can_run_generation = {}
        self.__storage_power = {}
        self.__must_fulfil_load = {}
        self.__can_fulfil_load = {}
        self.__residual_load_must_run_must_fulfil = {}
        self.__residual_load_final = {}

    def next(self, time: float, adapted_time: float) -> None:
        """
        Determines the power flow for each energy component based on the Simple Deficit Coverage strategy.

        In this operation strategy, the BESS is never charged with grid power.
        Solely renewable Energy is used to charge the BESS.
        Generation Emissions of the grid actually represent the emissions of the energy consumed by the load centre.

        Args:
            time (float): The current time of the operation.
            adapted_time (float): The adapted time which considers any offsets or adjustments.

        Return:
            None
        """

        for energy_form in self.__energy_forms:

            # Grid carbon intensities at present time step
            if self.__grid_components:
                grid_carbon_intensities = list()
                for grid_component in self.__grid_components:
                    grid_carbon_intensities.append(grid_component.carbon_intensity_time_step(time, adapted_time))
                # TODO Average values taken for now
            # Load Components
            total_must_fulfil_load = 0
            if self.__load_components:
                must_fulfil_load = list()
                for load_component in self.__load_components:  # type: LoadComponent
                    if load_component.must_fulfill is True:
                        must_fulfil_load.append(load_component.next(time, energy_form, adapted_time))
                total_must_fulfil_load = sum(must_fulfil_load)
            self.__must_fulfil_load[energy_form] = total_must_fulfil_load

            # Must-run Generation Components
            total_must_run_generation = 0
            if self.__generation_components:
                must_run_generation = list()
                carbon_intensity_generation = list()
                for generation_component in self.__generation_components:  # type: GenerationComponent
                    if generation_component.must_run is True:
                        must_run_generation.append(generation_component.next(time, energy_form, 0, adapted_time))
                        carbon_intensity_generation.append(generation_component.get_emissions_per_joule())
                total_must_run_generation = sum(must_run_generation)
            self.__must_run_generation[energy_form] = total_must_run_generation

            residual_load_must_run_must_fulfil = self.__must_run_generation[energy_form] - self.__must_fulfil_load[
                energy_form]
            self.__residual_load_must_run_must_fulfil[energy_form] = residual_load_must_run_must_fulfil

            residual_load = residual_load_must_run_must_fulfil

            # Determine run order based on user-specified priority
            key = 0
            while key < len(self.__flexible_component_priority):
                key += 1

                # Storage Components
                if key == self.__flexible_component_priority.index(self.STORAGE_COMPONENTS):
                    total_storage_power = 0
                    if self.__storage_components:
                        storage_power = list()
                        for storage_component in self.__storage_components:  # type: StorageComponent
                            # BESS is never charged with grid power in this OS: 4th argument is 0
                            # storage power is +ve when discharging and -ve when charging
                            storage_power.append(storage_component.next(time, energy_form, residual_load, 0, adapted_time))
                            residual_load += storage_power[-1]
                        total_storage_power = sum(storage_power)
                    self.__storage_power[energy_form] = total_storage_power

                # Grid Components
                if key == self.__flexible_component_priority.index(self.GRID_COMPONENTS):
                    total_grid_power = 0
                    if self.__grid_components:
                        grid_power = list()
                        for grid_component in self.__grid_components:  # type: GridComponent
                            # grid power is +ve if energy drawn, -ve if energy fed in
                            grid_power.append(
                                grid_component.next(time, energy_form, residual_load, adapted_time))
                            residual_load += grid_power[-1]
                        total_grid_power = sum(grid_power)
                        # Allocate emissions to loads
                        try:
                            self.allocate_energy_consumption_emissions(must_fulfil_load, energy_form)
                        except:
                            raise Warning('No load is present in the energy system for the energy form ' + energy_form)

                        # Allocate power exports to Must-Run generation based on highest carbon intensity first
                        if self.__generation_components and total_grid_power < -1e-3:
                            exported_energy_generator = list(np.zeros(len(carbon_intensity_generation)))
                            carbon_intensity_generation_ascending = sorted(
                                carbon_intensity_generation)  # ascending order

                            unallocated_export = -1 * total_grid_power

                            for gen in reversed(carbon_intensity_generation_ascending):
                                idx = carbon_intensity_generation.index(gen)
                                max_gen = must_run_generation[idx]
                                if unallocated_export <= max_gen:
                                    exported_energy_generator[idx] = unallocated_export
                                    unallocated_export = 0
                                else:
                                    exported_energy_generator[idx] = max_gen
                                    unallocated_export -= max_gen
                            self.__export_power_generator[energy_form] = exported_energy_generator
                            self.allocate_export_power_generator()
                    self.__grid_power[energy_form] = total_grid_power

                # Can-run Generation Components
                if key == self.__flexible_component_priority.index(self.CAN_RUN_GENERATION):
                    total_can_run_generation = 0
                    if self.__generation_components:
                        generation_power = list()
                        for generation_component in self.__generation_components:  # type: GenerationComponent
                            if generation_component.must_run is False:
                                # generation power is +ve
                                generation_power.append(generation_component.next(time, energy_form, residual_load))
                                residual_load += generation_power[-1]
                            # TODO write logic here for order of fulfilment by non-must run technologies
                        total_can_run_generation = sum(generation_power)
                    self.__can_run_generation[energy_form] = total_can_run_generation
            self.__residual_load_final[energy_form] = residual_load

    def allocate_energy_consumption_emissions(self, must_fulfil_load: list, energy_form: str):
        """
        Calculates the emissions from the grid and load components.

        Storage is not considered as the strategy only allows renewable energy to charge the storage devices.

        Args:
            must_fulfil_load (list): The list of loads that must be fulfilled.
            energy_form (str): The form of energy being considered.

        Return:
            None
        """
        energy_import_emissions = list()
        for grid_component in self.__grid_components:  # type: GridComponent
            if energy_form in grid_component.energy_form:
                energy_import_emissions.append(grid_component.energy_import_emissions())  # in kg
                total_energy_import_emissions = sum(energy_import_emissions)
        for load_component in self.__load_components:  # type: LoadComponent
            if energy_form in load_component.energy_form:
                idx = self.__load_components.index(load_component)
                if sum(must_fulfil_load) > 0:
                    load_fraction = must_fulfil_load[idx] / sum(must_fulfil_load)
                else:
                    load_fraction = 0
                load_grid_energy_consumption_emissions = load_fraction * total_energy_import_emissions
                load_discharge_energy_consumption_emissions = 0
                load_component.allocate_emissions(load_grid_energy_consumption_emissions,
                                                  load_discharge_energy_consumption_emissions)
                # second argument is 0 because load_discharge_energy_consumption_emissions = 0, as storage system is never charged over the grid

    def allocate_export_power_generator(self) -> None:
        """
        Allocates export power values to each must-run generation component.

        Return:
            None
        """
        for energy_form in self.__energy_forms:
            # Allocate determined export power to Must-run Generation Components
            if self.__generation_components:
                for generation_component, export_power in zip(self.__generation_components, self.__export_power_generator[energy_form]):  # type: GenerationComponent, float
                    if generation_component.must_run is True:
                        generation_component.allocate_export_power(export_power)

    def update(self, energy_management_state: dict[str, SimpleDeficitCoverageState]) -> None:
        """
        Updates the state values for different energy components based on calculated values.

        Args:
            energy_management_state (dict[str, SimpleDeficitCoverageState]): The dictionary containing state values for
                each energy component.

        Return:
            None
        """
        for energy_form in self.__energy_forms:
            energy_management_state[energy_form].must_run_generation_power = self.__must_run_generation[energy_form]
            energy_management_state[energy_form].grid_power = self.__grid_power[energy_form]
            energy_management_state[energy_form].can_run_generation_power = self.__can_run_generation[energy_form]
            energy_management_state[energy_form].storage_power = self.__storage_power[energy_form]
            energy_management_state[energy_form].must_fulfil_load = self.__must_fulfil_load[energy_form]
            energy_management_state[energy_form].can_fulfil_load = 0.0 if not self.__can_fulfil_load else \
                self.__can_fulfil_load[energy_form]
            energy_management_state[energy_form].residual_load_must_run_must_fulfil = \
                self.__residual_load_must_run_must_fulfil[energy_form]
            energy_management_state[energy_form].residual_load_final = self.__residual_load_final[energy_form]
