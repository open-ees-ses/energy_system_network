from configparser import ConfigParser
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.log import Logger
from energy_system_network.commons.state.energy_management.ev_home.electricity_ev_home_state import \
    ElectricityEVHomeState
from energy_system_network.commons.state.energy_management.ev_home.ev_home_state import EVHomeState
from energy_system_network.commons.state.energy_management.multisource_peak_shaving.electricity_multisource_peak_shaving_state import \
    ElectricityMultiSourcePeakShavingState
from energy_system_network.commons.state.energy_management.multisource_peak_shaving.multisource_peak_shaving_state import \
    MultiSourcePeakShavingState
from energy_system_network.commons.state.energy_management.rh_optimization.electricity_rh_optimization_state import \
    ElectricityRHOptimizationState
from energy_system_network.commons.state.energy_management.rh_optimization.heat_rh_optimization_state import \
    HeatRHOptimizationState
from energy_system_network.commons.state.energy_management.rh_optimization.rh_optimization_state import \
    RHOptimizationState
from energy_system_network.commons.state.energy_management.simple_deficit_coverage.electricity_simple_deficit_coverage_state import \
    ElectricitySimpleDeficitCoverageState
from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState
from energy_system_network.commons.state.energy_management.simple_deficit_coverage.heat_simple_deficit_coverage_state import \
    HeatSimpleDeficitCoverageState
from energy_system_network.commons.state.energy_management.simple_deficit_coverage.simple_deficit_coverage_state import \
    SimpleDeficitCoverageState
from energy_system_network.commons.state.energy_management.simple_peak_shaving.electricity_simple_peak_shaving_state import \
    ElectricitySimplePeakShavingState
from energy_system_network.commons.state.energy_management.simple_peak_shaving.simple_peak_shaving_state import \
    SimplePeakShavingState
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.config.simulation.profile_config import ProfileConfig
from energy_system_network.simulation.energy_management.operation_strategy.optimizer_based.arbitrage_optimization import ArbitrageOptimization
from energy_system_network.simulation.energy_management.operation_strategy.optimizer_based.rh_optimization import \
    RHOptimization
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.multi_source_peak_shaving import \
    MultiSourcePeakShaving
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.no_strategy import NoStrategy
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_deficit_coverage import \
    SimpleDeficitCoverage
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_ev_home import SimpleEVHome
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_peak_shaving import \
    SimplePeakShaving
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simses_external_strategy import \
    SimSESExternalStrategy


class EnergyManagementFactory:
    """
    A factory class responsible for creating different operation strategies of the ESN/Energy Systems based on provided
    configurations.

    Attributes:
        LEVEL_NAME (int): Constant representing the level name.
        OS_NAME (int): Constant representing the operation strategy name.
        OS_ENERGY_FORMS (int): Constant representing the operation strategy energy forms.
        __log (Logger): Logger object for logging purposes.
        __config_general (GeneralSimulationConfig): Configuration for the general simulation.
        _config_ems (EnergyManagementConfig): Configuration specific to the energy management system.
        __config_profile (ProfileConfig): Configuration for the profile.

    Methods:
        __init__(): Initializer for the EnergyManagementFactory.
        create_operation_strategy(): Creates an operation strategy based on the given energy system parameters.
        create_energy_management_state(): Creates an energy management state for the specified energy system.
    """
    LEVEL_NAME: int = 0
    OS_NAME: int = 1
    OS_ENERGY_FORMS: int = 2

    def __init__(self, config: ConfigParser, path: str = None):
        """
        Initializes the factory with the provided configurations.

        Args:
            config (ConfigParser): Configuration parser containing the settings for the factory.
            path (str, optional): Path to the configuration file. Defaults to None.
        """
        self.__log: Logger = Logger(type(self).__name__)
        self.__config_general: GeneralSimulationConfig = GeneralSimulationConfig(config, path)
        self._config_ems: EnergyManagementConfig = EnergyManagementConfig(config, path)
        self.__config_profile: ProfileConfig = ProfileConfig(config, path)

    def create_operation_strategy(self, energy_system_id, energy_forms: [str] = None,
                                  generation_components=None, storage_components=None, load_components=None,
                                  grid_components=None):
        """
        Creates an operation strategy based on the energy system ID and other provided parameters.

        Args:
            energy_system_id (int): The ID of the energy system.
            energy_forms (List[str], optional): List of energy forms. Defaults to None.
            generation_components (optional): Components responsible for generation. Defaults to None.
            storage_components (optional): Components responsible for storage. Defaults to None.
            load_components (optional): Components representing the load. Defaults to None.
            grid_components (optional): Components representing the grid. Defaults to None.

        Returns:
            Object: An instance of the operation strategy.

        Raises:
            Exception: If the specified energy forms don't match or if the operation strategy is unknown.
        """
        os = self._config_ems.operation_strategy
        operating_strategy = os[energy_system_id]
        ems_level = operating_strategy[self.LEVEL_NAME]
        os_name = operating_strategy[self.OS_NAME]
        os_energy_forms = operating_strategy[self.OS_ENERGY_FORMS:]

        if energy_system_id != 0 and os_energy_forms != energy_forms:
            raise Exception('Specified Energy Forms for Energy system ' + str(energy_system_id) +
                            ' do not match those specified in its operation strategy. '
                            'Please check that the order and number of specified energy forms is consistent.')

        if os_name == SimpleDeficitCoverage.__name__:
            self.__log.debug('Creating operation strategy as ' + os_name)
            return SimpleDeficitCoverage(energy_forms, generation_components, storage_components, load_components,
                                         grid_components, self._config_ems,
                                         self.__config_general)
        elif os_name == SimplePeakShaving.__name__:
            self.__log.debug('Creating operation strategy as ' + os_name)
            return SimplePeakShaving(energy_forms, generation_components, storage_components, load_components,
                                     grid_components, self._config_ems,
                                     self.__config_general)
        elif os_name == MultiSourcePeakShaving.__name__:
            self.__log.debug('Creating operation strategy as ' + os_name)
            return MultiSourcePeakShaving(energy_forms, generation_components, storage_components, load_components,
                                     grid_components, self._config_ems,
                                     self.__config_general)
        elif os_name == NoStrategy.__name__:
            self.__log.debug('Creating operation strategy as ' + os_name)
            return NoStrategy(energy_forms, self._config_ems)
        elif os_name == RHOptimization.__name__:
            self.__log.debug('Creating operation strategy as ' + os_name)
            return RHOptimization(energy_forms, generation_components, storage_components, load_components,
                                  grid_components, self.__config_general, self._config_ems)
        elif os_name == ArbitrageOptimization.__name__:
            self.__log.debug('Creating operation strategy as ' + os_name)
            return ArbitrageOptimization(energy_forms, storage_components, grid_components, self.__config_general, self._config_ems)
        elif os_name == SimSESExternalStrategy.__name__:
            self.__log.debug('Creating operation strategy as ' + os_name)
            if load_components:
                raise Exception('Load components defined. Strategy ' + os_name + ' does not support load components.')
            return SimSESExternalStrategy(energy_forms, storage_components, grid_components, self.__config_general, self._config_ems)
        elif os_name == SimpleEVHome.__name__:
            if load_components:
                raise Exception('Load components defined. Strategy ' + os_name + ' does not support load components.')
            self.__log.debug('Creating operation strategy as ' + os_name)
            return SimpleEVHome(energy_forms, grid_components, storage_components, self.__config_general)
        else:
            options: [str] = list()
            options.append(SimpleDeficitCoverage.__name__)
            options.append(SimplePeakShaving.__name__)
            options.append(NoStrategy.__name__)
            options.append(RHOptimization.__name__)
            options.append(ArbitrageOptimization.__name__)
            options.append(SimSESExternalStrategy.__name__)
            options.append(SimpleEVHome.__name__)
            raise Exception('Operation strategy ' + os_name + ' is unknown. '
                                                              'Following options are available: ' + str(options))

    def create_energy_management_state(self, energy_system_id: int,
                                       energy_form: str = None) -> EnergyManagementState:
        """
        Creates an energy management state based on the energy system ID and the energy form.

        Args:
            energy_system_id (int): The ID of the energy system.
            energy_form (str, optional): The type of energy form (e.g., Electricity, Heat). Defaults to None.

        Returns:
            EnergyManagementState: An instance of the energy management state suitable for the given parameters.

        Raises:
            Exception: If the operation strategy is unknown.
        """
        os = self._config_ems.operation_strategy
        operation_strategy = os[energy_system_id]
        os_name = operation_strategy[self.OS_NAME]
        if os_name == SimpleDeficitCoverage.__name__:
            if energy_form == EnergyForm.ELECTRICITY:
                state: SimpleDeficitCoverageState = ElectricitySimpleDeficitCoverageState(energy_system_id)
            elif energy_form == EnergyForm.HEAT:
                state: SimpleDeficitCoverageState = HeatSimpleDeficitCoverageState(energy_system_id)
            else:
                state: EnergyManagementState = EnergyManagementState()  # TODO create and add your custom state here
                energy_form = 'General Operating Strategy State'
            self.__log.debug(
                'Creating energy management state as ' + energy_form + '_' + os_name + 'State for data logging')
            state.time = self.__config_general.start
            return state
        elif os_name == SimplePeakShaving.__name__:
            if energy_form == EnergyForm.ELECTRICITY:
                state: SimplePeakShavingState = ElectricitySimplePeakShavingState(energy_system_id)
            # elif energy_form == 'Heat':
            #     state: SimplePeakShavingState = HeatSimplePeakShavingState(energy_system_id)
            else:
                state: EnergyManagementState = EnergyManagementState()  # TODO create and add your custom state here
                energy_form = 'General Operating Strategy State'
            self.__log.debug(
                'Creating energy management state as ' + energy_form + '_' + os_name + 'State for data logging')
            state.time = self.__config_general.start
            return state
        elif os_name == MultiSourcePeakShaving.__name__:
            if energy_form == EnergyForm.ELECTRICITY:
                state: MultiSourcePeakShavingState = ElectricityMultiSourcePeakShavingState(energy_system_id)
            # elif energy_form == 'Heat':
            #     state: SimplePeakShavingState = HeatSimplePeakShavingState(energy_system_id)
            else:
                state: EnergyManagementState = EnergyManagementState()  # TODO create and add your custom state here
                energy_form = 'General Operating Strategy State'
            self.__log.debug(
                'Creating energy management state as ' + energy_form + '_' + os_name + 'State for data logging')
            state.time = self.__config_general.start
            return state
        elif os_name == SimpleEVHome.__name__:
            if energy_form == EnergyForm.ELECTRICITY:
                state: EVHomeState = ElectricityEVHomeState(energy_system_id)
            else:
                state: EnergyManagementState = EnergyManagementState()  # TODO create and add your custom state here
                energy_form = 'General Operating Strategy State'
            self.__log.debug(
                'Creating energy management state as ' + energy_form + '_' + os_name + 'State for data logging')
            state.time = self.__config_general.start
            return state
        elif os_name == NoStrategy.__name__:
            self.__log.debug('Creating energy management state as EnergyManagementState for data logging')
            state: EnergyManagementState = EnergyManagementState()
            state.time = self.__config_general.start
            return state
        elif os_name == RHOptimization.__name__:
            if energy_form == EnergyForm.ELECTRICITY:
                state: RHOptimizationState = ElectricityRHOptimizationState(energy_system_id)
            elif energy_form == EnergyForm.HEAT:
                state: RHOptimizationState = HeatRHOptimizationState(energy_system_id)
            else:
                state: EnergyManagementState = EnergyManagementState()  # TODO create and add your custom state here
                energy_form = 'General Operating Strategy State'
            self.__log.debug('Creating energy management state as ' + energy_form + '_'
                             + os_name + 'State for data logging')
            state.time = self.__config_general.start
            return state
        elif os_name == ArbitrageOptimization.__name__:
            if energy_form == EnergyForm.ELECTRICITY:
                state: RHOptimizationState = ElectricityRHOptimizationState(energy_system_id)
            else:
                state: EnergyManagementState = EnergyManagementState()  # TODO create and add your custom state here
                energy_form = 'General Operating Strategy State'
            self.__log.debug('Creating energy management state as ' + energy_form + '_'
                             + os_name + 'State for data logging')
            state.time = self.__config_general.start
            return state
        elif os_name == SimSESExternalStrategy.__name__:
            if energy_form == EnergyForm.ELECTRICITY:
                state: RHOptimizationState = ElectricityRHOptimizationState(energy_system_id)
            else:
                state: EnergyManagementState = EnergyManagementState()  # TODO create and add your custom state here
                energy_form = 'General Operating Strategy State'
            self.__log.debug('Creating energy management state as ' + energy_form + '_'
                             + os_name + 'State for data logging')
            state.time = self.__config_general.start
            return state
        else:
            raise Exception('Operation strategy ' + os_name + ' is unknown.')
