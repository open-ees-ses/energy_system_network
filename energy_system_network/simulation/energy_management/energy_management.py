from configparser import ConfigParser
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState
from energy_system_network.simulation.energy_management.energy_management_factory import EnergyManagementFactory
from energy_system_network.simulation.energy_management.operation_strategy.operation_strategy import OperationStrategy


class EnergyManagement:
    """
    Energy Management System to control the operation of the Energy System Network and its Energy Systems.

    Attributes:
        __state_keys (List[str]): List of keys representing various energy forms and a general state.
        __data_export (DataHandler): Instance for handling data exports.
        __states (Dict[str, EnergyManagementState]): Dictionary storing states associated with energy forms.
        __os_energy_forms (List[str]): List of energy forms retrieved from the operation strategy.
        __operating_strategy (OperationStrategy): The operation strategy instance for the energy management system.

    Methods:
        __init__(): Initializes the energy management instance.
        next(): Returns the next power value based on the operation strategy.
        get_operation_strategy(): Retrieves the current operating strategy.
        close(): Closes all open resources associated with the instance.
    """
    ENERGY_FORMS_INDEX = 2
    GENERAL_OS: str = 'General OS'  # This is a provision made to incorporate any future energy_form independent OSs

    def __init__(self, data_export: DataHandler, config: ConfigParser, energy_system_id: int,
                 energy_forms: [str] = None, generation_components=None, storage_components=None, load_components=None,
                 grid_components=None):
        """
        Initialize the EnergyManagement instance.

        Args:
            data_export (DataHandler): Instance for handling data exports.
            config (ConfigParser): Configuration parser for energy management.
            energy_system_id (int): Identifier for the energy system.
            energy_forms (List[str], optional): List of energy forms. Default is None.
            generation_components (optional): Components for generation. Default is None.
            storage_components (optional): Components for storage. Default is None.
            load_components (optional): Components for load. Default is None.
            grid_components (optional): Components for the grid. Default is None.
        """
        factory: EnergyManagementFactory = EnergyManagementFactory(config)
        self.__state_keys = list()

        self.__data_export: DataHandler = data_export
        self.__states = {}
        operation_strategies = factory._config_ems.operation_strategy
        operation_strategy = operation_strategies[energy_system_id]

        self.__os_energy_forms = operation_strategy[self.ENERGY_FORMS_INDEX:]

        self.__state_keys = self.__os_energy_forms + [self.GENERAL_OS]

        self.__operating_strategy: OperationStrategy = factory.create_operation_strategy(energy_system_id, energy_forms, generation_components,
                                                                                         storage_components,
                                                                                         load_components,
                                                                                         grid_components)

        self.__states[self.GENERAL_OS]: EnergyManagementState = factory.create_energy_management_state(energy_system_id)  # General OS state
        self.__data_export.add_export_class(self.__states[self.GENERAL_OS])  # Ensures that only those states are logged which are currently used
        self.__data_export.transfer_data(self.__states[self.GENERAL_OS].to_export())  # initial timestep

        for energy_form in self.__os_energy_forms:
            self.__states[energy_form]: EnergyManagementState = factory.create_energy_management_state(energy_system_id,
                                                                                                       energy_form)
            self.__data_export.add_export_class(
                self.__states[energy_form])  # Ensures that only those states are logged which are currently used
            self.__data_export.transfer_data(self.__states[energy_form].to_export())  # initial timestep

    def next(self, time: float, adapted_time: float) -> None:
        """
        Return the next power value of the energy management strategy in W based on the operation strategy.

        Args:
            time (float): Current timestamp.
            adapted_time (float): Adapted timestamp for energy management calculations.

        Returns:
            None
        """
        self.__operating_strategy.next(time, adapted_time)
        self.__operating_strategy.update(self.__states)

        for key in self.__state_keys:
            self.__states[key].time = time
            self.__data_export.transfer_data(self.__states[key].to_export())

    def get_operation_strategy(self) -> OperationStrategy:
        """
        Retrieve the operating strategy.

        Returns:
            OperationStrategy: The current operation strategy of the energy management system.
        """
        return self.__operating_strategy

    def close(self) -> None:
        """
        Closes all open resources
        :return: None
        """
        self.__operating_strategy.close()
        for energy_form in self.__os_energy_forms:
            self.__data_export.transfer_data(self.__states[energy_form].to_export())
