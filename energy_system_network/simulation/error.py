class EndOfLifeError(Exception):
    """
    Custom exception raised when end of life is reached.

    Attributes:
        message (str): An optional message describing the error.

    Methods:
        __init__(self, *args): Constructor for the EndOfLifeError.
        __str__(self): Returns a string representation of the error message.
    """
    def __init__(self, *args):
        """
        Constructor for EndOfLifeError.

        Args:
            args (tuple): Optional argument for a custom error message.
        """
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        """
        Returns a string representation of the error message.

        Return:
            str: The error message.
        """
        if self.message:
            return '{0}'.format(self.message)
        else:
            return 'End of life has been reached'


class EndOfFileError(Exception):
    """
    Custom exception raised when end of file is reached.

    Attributes:
        message (str): An optional message describing the error.

    Methods:
        __init__(self, *args): Constructor for the EndOfFileError.
        __str__(self): Returns a string representation of the error message.
    """
    def __init__(self, *args):
        """
        Constructor for EndOfFileError.

        Args:
            args (tuple): Optional argument for a custom error message.
        """
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        """
        Returns a string representation of the error message.

        Return:
            str: The error message.
        """
        if self.message:
            return '{0}'.format(self.message)
        else:
            return 'End of file has been reached'
