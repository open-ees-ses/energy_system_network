import webbrowser
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.plotting.plotting import Plotting
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig


class EvaluationMerger:
    """
    Utility class to merge a collection of evaluation results into a single HTML file.

    Attributes:
        OUTPUT_NAME (str): Default name of the output HTML file.
        __file_name (str): Complete path for the output HTML file including its name.
        __merge_results (bool): Flag indicating if the results should be merged based on configuration.

    Methods:
        __init__(result_path: str, config: GeneralAnalysisConfig, energy_system_name: str): Initializes the EvaluationMerger.
        merge(evaluations: [Evaluation]): Writes an HTML file aggregating the evaluation results.
    """
    OUTPUT_NAME: str = '_analysis.html'

    def __init__(self, result_path: str, config: GeneralAnalysisConfig, energy_system_name: str):
        """
        Initializes the EvaluationMerger with a path for the results, configuration
        settings, and the name of the energy system being evaluated.

        Args:
            result_path (str): The directory where the evaluation results will be saved.
            config (GeneralAnalysisConfig): Configuration settings that determine the
                                            behavior of the merger.
            energy_system_name (str): The name of the energy system being evaluated.
                                      This is used as a prefix for the output HTML file name.
        """
        self.__file_name = result_path + '/' + energy_system_name + self.OUTPUT_NAME
        self.__merge_results: bool = config.merge_analysis

    def merge(self, evaluations: [Evaluation]) -> None:
        """
        Writes an HTML file that aggregates and presents the evaluation results
        and figures from the given list of evaluations.

        If the merge configuration setting is disabled, this method will do nothing.

        Args:
            evaluations (list[Evaluation]): A list of evaluation objects, each
                                            containing results and figures to be merged.
        """

        if not self.__merge_results:
            return
        with open(self.__file_name, 'w', encoding='utf-8') as outfile:
            outfile.write("<!DOCTYPE html><html><head></head><body>")
            for evaluation in evaluations:
                if evaluation.should_be_considered:
                    outfile.write("<section><b>"+evaluation.get_file_name()+"</b></section>")
                    for result in evaluation.evaluation_results:
                        outfile.write(result.to_console(flag='html') + "<br>")
                    for figure in evaluation.get_figures():
                        outfile.write(Plotting.convert_to_html(figure))
                    outfile.write("<br><br>")
            outfile.write("</body></html>")
        webbrowser.open(self.__file_name, new=2)  # open in new tab
