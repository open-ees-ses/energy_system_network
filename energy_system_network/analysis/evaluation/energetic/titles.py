class Titles:
    """
    Contains string titles representing various energy-related operations and states.

    Attributes:
        GENERATED_ENERGY (str): Title for energy generated.
        CHARGING_ENERGY (str): Title for energy stored.
        DISCHARGING_ENERGY (str): Title for energy discharged.
        DRAWN_ENERGY_GRID (str): Title for energy drawn from the grid.
        FED_IN_ENERGY_GRID (str): Title for energy fed into the grid.
        LOAD_ENERGY (str): Title representing the energy of the load.
        ENERGY_DEFICIT (str): Title representing the deficit of energy.
        ENERGY_SURPLUS (str): Title representing surplus of energy.
        FINAL_ENERGY_CONTENT (str): Title for energy content at the end of a simulation.
        INITIAL_ENERGY_CONTENT (str): Title for energy content at the beginning of a simulation.
        POWER_LOSS_LOAD (str): Title representing power loss due to load.
        POWER_LOSS_GRID (str): Title representing power loss in the grid section.
    """
    GENERATED_ENERGY: str = 'Energy generated'
    CHARGING_ENERGY: str = 'Energy stored'
    DISCHARGING_ENERGY: str = 'Energy discharged'
    DRAWN_ENERGY_GRID: str = 'Energy drawn from grid'
    FED_IN_ENERGY_GRID: str = 'Energy fed into grid'
    LOAD_ENERGY: str = 'Load energy'
    ENERGY_DEFICIT: str = 'Energy deficit'
    ENERGY_SURPLUS: str = 'Energy surplus'
    FINAL_ENERGY_CONTENT: str = 'Energy content at end of simulation'
    INITIAL_ENERGY_CONTENT: str = 'Energy content at beginning of simulation'
    POWER_LOSS_LOAD: str = 'Power loss Load'
    POWER_LOSS_GRID: str = 'Power loss Grid Section'
