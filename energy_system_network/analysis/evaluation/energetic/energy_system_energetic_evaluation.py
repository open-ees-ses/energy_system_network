from energy_system_network.analysis.data.data import Data
import numpy as np
from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.analysis.evaluation.energetic.titles import Titles
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.analysis.plotting.plotly_plotting.plotly_dashboard_plotting import PlotlyDashboardPlotting
from energy_system_network.analysis.plotting.plotly_plotting.sankey_diagram_plotting import SankeyDiagramPlotting
from energy_system_network.commons import unit_converter
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent


class EnergySystemEnergeticEvaluation(Evaluation):
    """
    This class is responsible for evaluating the energetic components of an energy system. It analyzes data
    from various energy system components, including generation, load, storage, and grid components, and computes
    energetic values in kWh for different categories such as energy generated, energy stored, energy drawn from
    the grid, etc.

    Attributes:
        TITLE (str): Title for the evaluation.
        FILENAME (str): Filename for the evaluation.
        GENERATION_COMPONENT (str): Name of the generation component.
        GRID_COMPONENT (str): Name of the grid component.
        STORAGE_COMPONENT (str): Name of the storage component.
        LOAD_COMPONENT (str): Name of the load component.
        NEGATIVE (str): String representing negative values.
        POSITIVE (str): String representing positive values.
        ENERGY_SUM (str): String representing energy summation.
        __energy_system_id (str): Identifier for the energy system.
        __energy_system (EnergySystem): Reference to the energy system object being evaluated.
        __energy_forms (list): List of energy forms present in the energy system.
        __sample_time (int/float): Simulation time step.
        __result_path (str): Path to save any resultant data or plots.
        __ems_config (EnergyManagementConfig): Configuration related to energy management.
        __generation_components (list): List of generation components in the energy system.
        __load_components (list): List of load components in the energy system.
        __storage_components (list): List of storage components in the energy system.
        __grid_components (list): List of grid components in the energy system.

    Methods:
        __init__(data_list: [Data], config: GeneralAnalysisConfig, ems_config: EnergyManagementConfig,
                 simulation_config: GeneralSimulationConfig, path: str, energy_system: EnergySystem): Initializes the class attributes.
        evaluate(evaluations: [Evaluation]) -> None:
            Calculates all component-wise energetic categories in kWh.
        energy_sum(energy_form: str, component_id: str, keyword: str) -> float:
            Filters specified data and returns a sum of energy in kWh.
        calculate_energy_sum(series: Series, keyword: str) -> float:
            Calculates either a positive or negative energy sum from a power time series in kWh.
        find_energy_content(energy_form: str, component_id: str, keyword: str) -> float:
            Obtains the initial and final energy content of storage components in kWh.
        plot() -> None:
            Plots a sankey diagram depicting energetic values.
    """
    TITLE = 'Energetic Evaluation'
    FILENAME = 'EnergeticEvaluation'
    GENERATION_COMPONENT: str = GenerationComponent.__name__
    GRID_COMPONENT: str = GridComponent.__name__
    STORAGE_COMPONENT: str = StorageComponent.__name__
    LOAD_COMPONENT: str = LoadComponent.__name__
    NEGATIVE: str = 'negative'
    POSITIVE: str = 'positive'
    ENERGY_SUM = 'energy_sum'

    def __init__(self, data_list: [Data], config: GeneralAnalysisConfig, ems_config: EnergyManagementConfig,
                 simulation_config: GeneralSimulationConfig, path: str, energy_system: EnergySystem):
        """
        Initializes the EnergySystemEnergeticEvaluation class.

        Args:
            data_list ([Data]): List of data entries related to energy system components.
            config (GeneralAnalysisConfig): Configuration related to general analysis.
            ems_config (EnergyManagementConfig): Configuration related to energy management.
            simulation_config (GeneralSimulationConfig): Configuration related to general simulation.
            path (str): Path to save any resultant data or plots.
            energy_system (EnergySystem): Reference to the energy system object being evaluated.

        Return:
            None
        """
        self.__energy_system_id = energy_system.get_id()
        super().__init__(data_list, config, config.energetic_analysis, self.__energy_system_id, filename=self.FILENAME)  # 1 is a flag for energetic analysis
        self.__energy_system = energy_system
        self.__energy_forms = self.__energy_system.get_energy_form()
        self.__sample_time = simulation_config.timestep
        self.__result_path = path

        self.__ems_config = ems_config

        title_extension: str = ' for Energy System ' + self.__energy_system_id
        self.TITLE += title_extension

        self.__generation_components: [GenerationComponent] = self.__energy_system.get_generation_components()
        self.__load_components: [LoadComponent] = self.__energy_system.get_load_components()
        self.__storage_components: [StorageComponent] = self.__energy_system.get_storage_components()
        self.__grid_components: [GridComponent] = self.__energy_system.get_grid_components()

    def evaluate(self, evaluations: [Evaluation] = None) -> None:
        """
        Calculates all component-wise energetic categories in in kWh
        :param evaluations: List of Evaluations
        :return: None
        """
        for energy_form in self.__energy_forms:
            self.append_result(EvaluationResult(energy_form + ' ' + Titles.ENERGY_SURPLUS,
                                                EvaluationResult.Unit.KWH,
                                                self.energy_sum(energy_form, keyword=Titles.ENERGY_SURPLUS)))
            self.append_result(EvaluationResult(energy_form + ' ' + Titles.ENERGY_DEFICIT,
                                                EvaluationResult.Unit.KWH,
                                                self.energy_sum(energy_form, keyword=Titles.ENERGY_DEFICIT)))

            for component_group in self.__generation_components, \
                                   self.__load_components, \
                                   self.__storage_components, \
                                   self.__grid_components:
                if component_group:
                    for component in component_group:
                        if energy_form in component.energy_form:
                            component_name = component.name
                            component_id = component.component_ID

                            if isinstance(component, GenerationComponent):
                                self.append_result(
                                    EvaluationResult(energy_form + ' ' + Titles.GENERATED_ENERGY + ' ' + component_name,
                                                     EvaluationResult.Unit.KWH,
                                                     self.energy_sum(energy_form, component_id, keyword=Titles.GENERATED_ENERGY),
                                                     component_id, component_name))
                            elif isinstance(component, LoadComponent):
                                self.append_result(
                                    EvaluationResult(energy_form + ' ' + Titles.LOAD_ENERGY + ' ' + component_name,
                                                     EvaluationResult.Unit.KWH,
                                                     self.energy_sum(energy_form, component_id,
                                                                              keyword=Titles.LOAD_ENERGY),
                                                     component_id, component_name))
                                self.append_result(
                                    EvaluationResult(energy_form + ' ' + Titles.POWER_LOSS_LOAD + ' ' + component_name,
                                                     EvaluationResult.Unit.KWH,
                                                     self.energy_sum(energy_form, component_id,
                                                                     keyword=Titles.POWER_LOSS_LOAD),
                                                     component_id, component_name))
                            elif isinstance(component, StorageComponent):
                                self.append_result(
                                    EvaluationResult(energy_form + ' ' + Titles.CHARGING_ENERGY + ' ' + component_name,
                                                     EvaluationResult.Unit.KWH,
                                                     self.energy_sum(energy_form, component_id,keyword=Titles.CHARGING_ENERGY),
                                                     component_id, component_name))
                                self.append_result(
                                    EvaluationResult(
                                        energy_form + ' ' + Titles.DISCHARGING_ENERGY + ' ' + component_name,
                                        EvaluationResult.Unit.KWH,
                                        self.energy_sum(energy_form, component_id,keyword=Titles.DISCHARGING_ENERGY),
                                        component_id, component_name))
                                self.append_result(
                                    EvaluationResult(
                                        energy_form + ' ' + Titles.INITIAL_ENERGY_CONTENT + ' ' + component_name,
                                        EvaluationResult.Unit.KWH,
                                        self.find_energy_content(energy_form, component_id,
                                                                  keyword=Titles.INITIAL_ENERGY_CONTENT),
                                        component_id, component_name))
                                self.append_result(
                                    EvaluationResult(
                                        energy_form + ' ' + Titles.FINAL_ENERGY_CONTENT + ' ' + component_name,
                                        EvaluationResult.Unit.KWH,
                                        self.find_energy_content(energy_form, component_id,
                                                                              keyword=Titles.FINAL_ENERGY_CONTENT),
                                        component_id, component_name))
                            elif isinstance(component, GridComponent):
                                self.append_result(
                                    EvaluationResult(
                                        energy_form + ' ' + Titles.DRAWN_ENERGY_GRID + ' ' + component_name,
                                        EvaluationResult.Unit.KWH,
                                        self.energy_sum(energy_form, component_id,
                                                                              keyword=Titles.DRAWN_ENERGY_GRID),
                                        component_id, component_name))
                                self.append_result(
                                    EvaluationResult(
                                        energy_form + ' ' + Titles.FED_IN_ENERGY_GRID + ' ' + component_name,
                                        EvaluationResult.Unit.KWH,
                                        self.energy_sum(energy_form, component_id,
                                                                              keyword=Titles.FED_IN_ENERGY_GRID),
                                        component_id, component_name))
                                self.append_result(
                                    EvaluationResult(
                                        energy_form + ' ' + Titles.POWER_LOSS_GRID + ' ' + component_name,
                                        EvaluationResult.Unit.KWH,
                                        self.energy_sum(energy_form, component_id,
                                                        keyword=Titles.POWER_LOSS_GRID),
                                        component_id, component_name))
        self.print_results()

    def energy_sum(self, energy_form: str, component_id: str = None, keyword: str = None):
        """
        Filters the specified data from the data list and returns a sum of energy in kWh
        :param energy_form: energy form as str
        :param component_id: energy system component ID as str
        :param keyword: keyword as str
        :return: energy sum as float in kWh
        """
        data_list: Data = self.get_data()
        for data in data_list:
            if energy_form in str(type(data)) and self.GENERATION_COMPONENT in str(type(data)) and data.id == component_id and keyword==Titles.GENERATED_ENERGY:
                series = data.generation_power
                energy_sum = self.calculate_energy_sum(series)
            elif isinstance(data, EnergyManagementData) and self.__energy_system_id in data.id and data.energy_form == energy_form:
                if keyword == Titles.ENERGY_SURPLUS:
                    series = data.residual_load_final
                    energy_sum = self.calculate_energy_sum(series, keyword=self.POSITIVE)  # in kWh
                elif keyword == Titles.ENERGY_DEFICIT:
                    series = data.residual_load_final
                    energy_sum = -1 * self.calculate_energy_sum(series, keyword=self.NEGATIVE)  # in kWh
            elif energy_form in str(type(data)) and self.LOAD_COMPONENT in str(type(data)) and data.id == component_id:
                if keyword == Titles.LOAD_ENERGY:
                    series = data.load_power
                    energy_sum = self.calculate_energy_sum(series)
                elif keyword == Titles.POWER_LOSS_LOAD:
                    series = data.power_loss
                    energy_sum = self.calculate_energy_sum(series)
            elif energy_form in str(type(data)) and self.GRID_COMPONENT in str(type(data)) and data.id == component_id:
                series = data.grid_power
                series_power_loss = data.power_loss
                if keyword == Titles.DRAWN_ENERGY_GRID:
                    energy_sum = self.calculate_energy_sum(series, keyword=self.POSITIVE)  # in kWh
                elif keyword == Titles.FED_IN_ENERGY_GRID:
                    energy_sum = -1 * self.calculate_energy_sum(series, keyword=self.NEGATIVE)  # in kWh
                elif keyword == Titles.POWER_LOSS_GRID:
                    energy_sum = self.calculate_energy_sum(series_power_loss, keyword=self.POSITIVE)  # in kWh
            elif energy_form in str(type(data)) and self.STORAGE_COMPONENT in str(type(data)) and data.id == component_id:
                series = data.power
                if keyword == Titles.CHARGING_ENERGY:
                    energy_sum = self.calculate_energy_sum(series, keyword=self.POSITIVE)
                elif keyword == Titles.DISCHARGING_ENERGY:
                    energy_sum = -1 * self.calculate_energy_sum(series, keyword=self.NEGATIVE)
            if self.ENERGY_SUM in locals(): return energy_sum

    def calculate_energy_sum(self, series, keyword: str = None) -> float:
        """
        Calculates a positive or a negative energy sum from the power time series in kWh
        :param series: Power time series in W
        :param keyword: keyword as str
        :return: energy sum as float in kWh
        """
        nan_free_series = series[~np.isnan(series)]
        if keyword == self.POSITIVE:  # for StorageComponent charge, GridComponent drawn energy
            corrected_series = [a for a in nan_free_series if a > 0]
        elif keyword == self.NEGATIVE:  # for StorageComponent discharge, GridComponent fed-in energy
            corrected_series = [a for a in nan_free_series if a < 0]
        elif keyword is None:
            corrected_series = nan_free_series
        energy_sum = unit_converter.J_to_kWh(sum(corrected_series) * self.__sample_time)  # in kWh
        return energy_sum

    def find_energy_content(self, energy_form: str, component_id: str, keyword: str) -> float:
        """
        Obtains the initial and final energy content of storage components in kWh
        :param energy_form: energy form as str
        :param component_id: energy system component ID as str
        :param keyword: keyword as str
        :return: energy content as float in kWh
        """
        data_list: Data = self.get_data()
        for data in data_list:
            if energy_form in str(type(data)) and self.STORAGE_COMPONENT in str(type(data)) and data.id == component_id and keyword == Titles.FINAL_ENERGY_CONTENT:
                series = data.energy_content
                last_value = unit_converter.Wh_to_kWh(series[-1])  # in kWh
                return last_value
            if energy_form in str(type(data)) and self.STORAGE_COMPONENT in str(type(data)) and data.id == component_id and keyword == Titles.INITIAL_ENERGY_CONTENT:
                series = data.energy_content
                first_value = unit_converter.Wh_to_kWh(series[0])  # in kWh
                return first_value

    def plot(self) -> None:
        """
        Plots sankey diagram depicting energetic values
        :return: None
        """
        plot = PlotlyDashboardPlotting(self.TITLE, path=self.__result_path)
        sankey_diagram = SankeyDiagramPlotting(self.__energy_system, self.__energy_forms)
        parameters: dict = sankey_diagram.create_sankey_diagram_parameters(self.evaluation_results)
        plot.sankey_diagram(parameters)
        self.extend_figures(plot.get_figures())
