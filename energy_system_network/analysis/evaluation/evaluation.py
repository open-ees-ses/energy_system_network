import csv
from abc import ABC, abstractmethod
from energy_system_network.analysis.data.data import Data
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig


class Evaluation(ABC):
    """
    Abstract base class that provides a structure for performing evaluations on energy system data.

    Attributes:
        __data ([Data]): A list containing the energy system data to be evaluated.
        __file_name (str): Name of the file where the evaluation results will be saved.
        __do_evaluation (bool): Flag to determine if an evaluation should be performed.
        __do_plotting (bool): Flag to determine if plotting should be performed after evaluation.
        _print_to_console (bool): Flag to determine if evaluation results should be printed to the console.
        __export_analysis_to_csv (bool): Flag to determine if evaluation results should be exported to a CSV file.
        __evaluation_results ([EvaluationResult]): List of results obtained after performing the evaluation.
        __figures (list): List of figures/plots obtained after performing the evaluation.

    Methods:
        __init__(data: [Data], config: GeneralAnalysisConfig, do_evaluation: bool, energy_system_id: str = None, filename: str = ''): Initializes the Evaluation object.
        evaluation_results() -> [EvaluationResult]: Getter for the evaluation_results attribute.
        append_result(evaluation_result: EvaluationResult) -> None: Appends a result to the evaluation results list.
        extend_results(evaluation_results: [EvaluationResult]) -> None: Extends the evaluation results list.
        append_figure(figure) -> None: Appends a figure to the figures list.
        extend_figures(figures: list) -> None: Extends the figures list.
        get_data() -> [Data]: Getter for the data attribute.
        get_figures() -> list: Getter for the figures attribute.
        get_file_name() -> str: Getter for the file_name attribute.
        get_name() -> str: Getter for the name of the data type.
        should_be_considered() -> bool: Returns the evaluation status flag.
        run(evaluations=None) -> None: Executes the evaluation.
        evaluate(evaluations=None) -> None: Abstract method to define the evaluation process.
        plot() -> None: Method to plot the evaluation results.
        close() -> None: Method to close any resources used during the evaluation.
        print_results() -> None: Prints the evaluation results to the console.
        write_to_csv(path: str) -> None: Writes the evaluation results to a CSV file.
    """
    EXT: str = '.csv'
    TIME: str = 'Time in s'
    TITLE: str = ''

    def __init__(self, data: [Data], config: GeneralAnalysisConfig, do_evaluation: bool, energy_system_id: str = None,
                 filename: str = ''):
        """
        Initializes the Evaluation object with given data, configuration, and flags.

        Args:
            data ([Data]): A list containing the energy system data to be evaluated.
            config (GeneralAnalysisConfig): Configuration object for the evaluation.
            do_evaluation (bool): Flag to determine if an evaluation should be performed.
            energy_system_id (str, optional): ID associated with the energy system being evaluated.
            filename (str, optional): Desired filename for the results. Defaults to empty string.
        """
        if len(data) > 1:
            self.__data: [Data] = data
            if energy_system_id:
                self.__file_name: str = filename + '_EnergySystem_' + energy_system_id + self.EXT
            else:
                self.__file_name: str = filename + self.EXT
        elif filename and not energy_system_id:
            self.__data: Data = data[0]
            self.__file_name: str = filename + self.EXT
        else:
            self.__data: Data = data[0]
            self.__file_name: str = type(self).__name__ + self.__data.id + self.EXT
        self.__do_evaluation: bool = do_evaluation
        self.__do_plotting: bool = config.plotting and do_evaluation
        self._print_to_console: bool = config.print_result_to_console and do_evaluation
        self.__export_analysis_to_csv: bool = config.export_analysis_to_csv and do_evaluation

        self.__evaluation_results: [EvaluationResult] = list()
        self.__figures: list = list()

    @property
    def evaluation_results(self) -> [EvaluationResult]:
        """
        Getter for the evaluation_results attribute.

        Return:
            [EvaluationResult]: List of results obtained after performing the evaluation.
        """
        return self.__evaluation_results

    def append_result(self, evaluation_result: EvaluationResult) -> None:
        """
        Appends a result to the evaluation results list.

        Args:
            evaluation_result (EvaluationResult): The result to be appended.
        """
        self.__evaluation_results.append(evaluation_result)

    def extend_results(self, evaluation_results: [EvaluationResult]) -> None:
        """
        Extends the evaluation results list with multiple results.

        Args:
            evaluation_results ([EvaluationResult]): List of results to be added.
        """
        self.__evaluation_results.extend(evaluation_results)

    def append_figure(self, figure) -> None:
        """
        Appends a figure to the figures list.

        Args:
            figure: Figure to be appended.
        """
        self.__figures.append(figure)

    def extend_figures(self, figures: list) -> None:
        """
        Extends the figures list with multiple figures.

        Args:
            figures (list): List of figures to be added.
        """
        self.__figures.extend(figures)

    def get_data(self) -> [Data]:
        """
        Getter for the data attribute.

        Return:
            [Data]: List containing the energy system data to be evaluated.
        """
        return self.__data

    def get_figures(self) -> list:
        """
        Getter for the figures attribute.

        Return:
            list: List of figures/plots obtained after performing the evaluation.
        """
        return self.__figures

    def get_file_name(self) -> str:
        """
        Getter for the file_name attribute.

        Return:
            str: Name of the file where the evaluation results will be saved.
        """
        return self.__file_name

    @property
    def get_name(self) -> str:
        """
        Getter for the name of the data type.

        Return:
            str: Name of the data type.
        """
        if isinstance(self.__data, list):
            return type(self.__data).__name__

    @property
    def should_be_considered(self) -> bool:
        """
        Returns the evaluation status flag.

        Return:
            bool: True if evaluation should be performed, False otherwise.
        """
        return self.__do_evaluation

    def run(self, evaluations=None) -> None:
        """
        Executes the evaluation and, if configured, the plotting.

        Args:
            evaluations: (Optional) Additional evaluations to consider during execution.
        """
        if self.__do_evaluation:
            self.evaluate(evaluations)
            if self.__do_plotting:
                self.plot()

    @abstractmethod
    def evaluate(self, evaluations=None) -> None:
        """
        Abstract method to define the evaluation process.

        Args:
            evaluations: (Optional) Additional evaluations to consider during the evaluation.
        """
        pass

    def plot(self) -> None:
        """
        Method to plot the evaluation results. Can be overridden by subclasses to provide specific plotting behavior.
        """
        pass

    def close(self) -> None:
        """
        Method to close any resources used during the evaluation. Can be overridden by subclasses if needed.
        """
        pass

    def print_results(self):
        """
        Prints the evaluation results to the console if configured to do so.
        """
        if not self._print_to_console:
            return
        print('\n')
        for evaluation_result in self.evaluation_results:
            print(evaluation_result.to_console())

    def write_to_csv(self, path: str) -> None:
        """
        Writes the evaluation results to a CSV file if configured to do so.

        Args:
            path (str): Path where the CSV file should be saved.
        """
        if not self.__export_analysis_to_csv:
            return
        file = path + self.__file_name
        with open(file, 'w', newline='') as file:
            writer = csv.writer(file, delimiter=',')
            for evaluation_result in self.evaluation_results:
                writer.writerow(evaluation_result.to_csv())
