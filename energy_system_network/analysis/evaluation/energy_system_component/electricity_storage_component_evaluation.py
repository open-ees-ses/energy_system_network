from energy_system_network.analysis.data.energy_system_component_data.storage_component_data.storage_component_data import \
    StorageComponentData
from energy_system_network.analysis.evaluation.energy_system_component.storage_component_evaluation import \
    StorageComponentEvaluation
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig


class ElectricityStorageComponentEvaluation(StorageComponentEvaluation):
    """
    Evaluates the performance and attributes of an electricity-based energy storage component.
    This class provides specific analyses pertinent to electrical energy storage components,
    extending the more general capabilities of the `StorageComponentEvaluation` class.

    Attributes:
        TITLE (str): Title for the evaluation which specifies that the evaluation pertains to
        electricity energy storage components.

    Methods:
        __init__(data: StorageComponentData, config: GeneralAnalysisConfig, path: str):
        Initializes the class attributes and sets up evaluation parameters specific to
        electricity storage components.
    """
    TITLE = EnergyForm.ELECTRICITY + ' ' + 'Energy Storage Analysis'

    def __init__(self, data: StorageComponentData, config: GeneralAnalysisConfig, path: str):
        """
        Initializes the `ElectricityStorageComponentEvaluation` class with specific data,
        configuration settings, and a path for the evaluation outputs.

        Args:
            data (StorageComponentData): The data associated with the storage component.
            config (GeneralAnalysisConfig): The configuration settings for the evaluation.
            path (str): The path where the evaluation results will be saved.
        """
        super().__init__(data, config, path)
