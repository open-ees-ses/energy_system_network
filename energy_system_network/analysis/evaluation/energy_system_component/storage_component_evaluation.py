from statistics import mean
from energy_system_network.analysis.data.energy_system_component_data.storage_component_data.storage_component_data import \
    StorageComponentData
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.analysis.plotting.axis import Axis
from energy_system_network.analysis.plotting.plotly_plotting.plotly_dashboard_plotting import PlotlyDashboardPlotting
from energy_system_network.analysis.plotting.plotting import Plotting
from energy_system_network.commons.state.energy_system_component.storage_component.storage_component_state import \
    StorageComponentState
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
import numpy as np


class StorageComponentEvaluation(Evaluation):
    """
    Evaluates the storage component of an energy system. This class is dedicated to providing a detailed
    analysis of the state of charge (SOC), state of health (SOH), carbon intensity, drivetrain power,
    and availability of a given storage component.

    Attributes:
        TITLE (str): Title for the storage component evaluation.
        ASOCI (str): String representing Average State of Carbon Intensity.
        ACICE (str): String representing Average Carbon Intensity of Charging Energy.
        AVAILABILITY_FACTOR (str): Label for availability factor of the storage component.
        DRIVETRAIN_POWER_FULFILMENT (str): Label for drivetrain power fulfilment ratio.
        __result_path (str): Path to save the resultant plots and data.

    Methods:
        __init__(data: StorageComponentData, config: GeneralAnalysisConfig, path: str): Initializes the class attributes.
        evaluate(evaluations: [Evaluation] = None) -> None: Evaluates the performance of the storage component.
        plot() -> None: Generates relevant plots depicting the performance of the storage component.
    """
    TITLE = ''
    ASOCI: str = 'Average State of Carbon Intensity (SOCI)'
    ACICE: str = 'Average Carbon Intensity of Charging Energy'
    AVAILABILITY_FACTOR = 'Availability Factor'
    DRIVETRAIN_POWER_FULFILMENT = 'Drivetrain Fulfilment'

    def __init__(self, data: StorageComponentData, config: GeneralAnalysisConfig, path: str):
        """
        Initializes the StorageComponentEvaluation class.

        Args:
            data (StorageComponentData): Data entries related to the storage system component.
            config (GeneralAnalysisConfig): Configuration related to general analysis.
            path (str): Path where the results and plots will be saved.

        Return:
            None
        """
        super().__init__([data], config, do_evaluation=True)
        title_extension: str = ' for ' + self.get_data().id
        self.TITLE += title_extension
        self.__result_path = path

    def evaluate(self, evaluations: [Evaluation] = None):
        """
        Evaluates the storage component's performance, computing values such as Average State of Carbon Intensity,
        Average Carbon Intensity of Charging Energy, Availability Factor, and Drivetrain Fulfilment.

        Args:
            evaluations ([Evaluation], optional): List of evaluation objects if needed for context.

        Return:
            None
        """
        data: StorageComponentData = self.get_data()
        self.append_result(EvaluationResult(self.ASOCI, EvaluationResult.Unit.GCO2EQPERKWH, mean(data.soci), data.id))
        power = data.power
        indices = np.where(power > 0)[0]
        if np.any(indices):
            cice = data.carbon_intensity_charging_energy[indices]
            self.append_result(EvaluationResult(self.ACICE, EvaluationResult.Unit.GCO2EQPERKWH, mean(cice), data.id))
        if sum(data.availability) >= 1:  # i.e. if StorageComponent is an ElectricVehicle or a similar object
            availability_factor = round(sum(data.availability)/len(data.availability) * 100, 2)  # in %
            self.append_result(EvaluationResult(self.AVAILABILITY_FACTOR, EvaluationResult.Unit.PERCENTAGE, availability_factor))

            common_elements = [x for x in data.drivetrain_power if any(abs(x - y) < 1e-3 for y in data.reference_drive_power)]
            drivetrain_power_request_fulfilment_ratio = 100 * \
                                                        len(common_elements) / max(len(data.drivetrain_power), len(data.reference_drive_power))
            self.append_result(
                EvaluationResult(self.DRIVETRAIN_POWER_FULFILMENT, EvaluationResult.Unit.PERCENTAGE, drivetrain_power_request_fulfilment_ratio))
        self.print_results()

    def plot(self) -> None:
        """
        Generates plots representing the performance and states of the storage component.
        Specifically, plots for State of Charge (SOC), State of Health (SOH), drivetrain power,
        and availability are created.

        Return:
            None
        """
        data: StorageComponentData = self.get_data()

        # Plot SOC and SOH
        plot: Plotting = PlotlyDashboardPlotting(title=self.TITLE, path=self.__result_path)
        xaxis: Axis = Axis(data=Plotting.format_time(data.time), label=self.TIME)
        yaxis: [Axis] = list()

        yaxis.append(Axis(data.soc, label=StorageComponentState.SOC,
                 color=PlotlyDashboardPlotting.Color.BLUE,
                 linestyle=PlotlyDashboardPlotting.Linestyle.DASHED))
        yaxis.append(Axis(data.soh, label=StorageComponentState.SOH,
                 color=PlotlyDashboardPlotting.Color.RED,
                 linestyle=PlotlyDashboardPlotting.Linestyle.DASHED))
        plot.lines(xaxis, yaxis)
        self.extend_figures(plot.get_figures())

        if sum(data.availability) >= 1:  # i.e. if StorageComponent is an ElectricVehicle or a similar object
            # Plot drive power
            plot: Plotting = PlotlyDashboardPlotting(title='', path=self.__result_path)
            xaxis: Axis = Axis(data=Plotting.format_time(data.time), label=self.TIME)
            yaxis: [Axis] = list()

            yaxis.append(Axis(data.drivetrain_power, label='Drivetrain power in W',
                              color=PlotlyDashboardPlotting.Color.NAVY_BLUE,
                              linestyle=PlotlyDashboardPlotting.Linestyle.DASHED))
            yaxis.append(Axis(data.reference_drive_power, label='Reference drive power in W',
                              color=PlotlyDashboardPlotting.Color.CYAN,
                              linestyle=PlotlyDashboardPlotting.Linestyle.DASHED))
            plot.lines(xaxis, yaxis)
            self.extend_figures(plot.get_figures())

            # Plot availability
            plot: Plotting = PlotlyDashboardPlotting(title='', path=self.__result_path)
            plot.horizontal_binary_heatmap(data.availability, xaxis)
            self.extend_figures(plot.get_figures())
