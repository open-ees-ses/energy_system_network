from energy_system_network.analysis.data.data import Data
from energy_system_network.analysis.evaluation.energetic.energy_system_energetic_evaluation import \
    EnergySystemEnergeticEvaluation
from energy_system_network.analysis.evaluation.energetic.titles import Titles
from energy_system_network.analysis.evaluation.environmental.energy_system_environmental_evaluation import \
    EnergySystemEnvironmentalEvaluation
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.energy_system_components.lithium_battery import LithiumBatteryConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class EnergySystemEnviroEnergeticEvaluation(Evaluation):
    """
    Provides an evaluation mechanism for combined environmental and energetic properties
    of an energy system such as LEES, ε FCR, etc.

    Attributes:
        TITLE (str): Title for the environmental-energetic evaluation.
        FILENAME (str): Name of the file where results are saved.

        __energy_system_id (str): Unique identifier for the energy system.
        __energy_system (EnergySystem): Instance of the energy system being evaluated.
        __energy_forms (list): List of energy forms available within the energy system.
        __generation_components (list): List of generation components present in the energy system.
        __grid_components (list): List of grid components present in the energy system.
        __storage_components (list): List of storage components present in the energy system.
        __load_components (list): List of load components present in the energy system.
        __sample_time (float): Time interval for sampling in the simulation.
        __result_path (str): Path to save the results.
        __general_config (GeneralSimulationConfig): Configuration settings for the general simulation.
        __ems_strategy (list): Strategy for the Energy Management System (EMS).
        __lithium_battery_config (LithiumBatteryConfig): Configuration settings for lithium batteries.
        __simses_external_strategy (str): Strategy for SimSES external operations.

        class Title: Inner class for holding various titles associated with enviro-energetic metrics.

    Methods:
        __init__(data_list: [Data], config: GeneralAnalysisConfig, ems_config: EnergyManagementConfig,
                simulation_config: GeneralSimulationConfig, path: str, energy_system: EnergySystem):
            Initializes the EnergySystemEnviroEnergeticEvaluation class.
        evaluate(evaluations: [Evaluation] = None) -> None:
            Calculates enviro-energetic metric such as LEES, and ϵ FCR with values from EnergySystemEnvironmentalEvaluation.
        plot() -> None: Placeholder function as no plots are currently available for this evaluation.
    """
    TITLE = 'Enviro-Energetic Evaluation'
    FILENAME = 'EnviroEnergeticEvaluation'

    class Title:
        LEES: str = 'Levelized Emissions of Energy Supply (LEES)'
        EPSILON_FCR = 'Total Emissions of FCR Provision'
        LEES_LOAD_LOSSES: str = 'Levelized Emissions of Energy Supply (LEES) incl. Losses in Load Components'
        COMPONENT_CARBON_INTENSITY: str = 'Carbon Intensity of Generated Electricity'

    def __init__(self, data_list: [Data], config: GeneralAnalysisConfig, ems_config: EnergyManagementConfig,
                 simulation_config: GeneralSimulationConfig, path: str, energy_system: EnergySystem):
        """
        Initializes the EnergySystemEnviroEnergeticEvaluation class.

        Args:
            data_list ([Data]): List of data entries to be considered in the evaluation.
            config (GeneralAnalysisConfig): General configuration for analysis.
            ems_config (EnergyManagementConfig): Configuration settings for the Energy Management System.
            simulation_config (GeneralSimulationConfig): Configuration settings for the general simulation.
            path (str): Path where the results are to be stored.
            energy_system (EnergySystem): Instance of the energy system being evaluated.
        """
        self.__energy_system_id = energy_system.get_id()
        super().__init__(data_list, config, config.environmental_analysis, self.__energy_system_id,
                         filename=self.FILENAME)
        self.__energy_system = energy_system
        self.__energy_forms = self.__energy_system.get_energy_form()
        self.__generation_components = energy_system.get_generation_components()
        self.__grid_components = energy_system.get_grid_components()
        self.__storage_components = energy_system.get_storage_components()
        self.__load_components = energy_system.get_load_components()
        self.__sample_time = simulation_config.timestep
        self.__result_path = path
        self.__general_config = simulation_config
        self.__ems_strategy = ems_config.operation_strategy[1:]
        self.__lithium_battery_config = LithiumBatteryConfig()
        self.__simses_external_strategy = self.__lithium_battery_config.operating_strategy

        title_extension: str = ' for Energy System ' + self.__energy_system_id
        self.TITLE += title_extension

    def evaluate(self, evaluations: [Evaluation] = None) -> None:
        """
        Calculates enviro-energetic metric such as LEES, and ϵ FCR with values from EnergySystemEnvironmentalEvaluation
        :param evaluations: List of concluded Evaluations
        :return:None
        """

        # TODO consider these evaluations for multiple energy forms
        for energy_form in self.__energy_forms:
            for evaluation in evaluations:
                if isinstance(evaluation, EnergySystemEnvironmentalEvaluation):
                    total_emissions_log = list()
                    for result in evaluation.evaluation_results:
                        if result.value is None \
                                or EnergySystemEnvironmentalEvaluation.Title.DEC_EMISSIONS_STORAGE in result.description \
                                or EnergySystemEnvironmentalEvaluation.Title.AVERAGE_GRID_CARBON_INTENSITY in result.description:  # Excludes DEC emissions for storage from total emissions
                            pass
                        else:
                            if result.value != 'None':
                                total_emissions_log.append(float(result.value))
                    total_emissions = sum(total_emissions_log)

                    if self.__storage_components:
                        dec_emissions_storage = list()
                        production_emissions_storage = list()
                        operation_emissions_storage = list()
                        eol_emissions_storage = list()
                        for component in self.__storage_components:
                            for result in evaluation.evaluation_results:
                                if result.value != 'None' and result.component_name == component.name:
                                    if EnergySystemEnvironmentalEvaluation.Title.DEC_EMISSIONS_STORAGE in result.description:  # Applicable to grid-connected BESS
                                        dec_emissions_storage.append(float(result.value))
                                    if EnergySystemEnvironmentalEvaluation.Title.PRODUCTION_EMISSIONS in result.description:
                                        production_emissions_storage.append(float(result.value))
                                    if EnergySystemEnvironmentalEvaluation.Title.OPERATION_EMISSIONS in result.description:
                                        operation_emissions_storage.append(float(result.value))
                                    if EnergySystemEnvironmentalEvaluation.Title.END_OF_LIFE_EMISSIONS in result.description:
                                        eol_emissions_storage.append(float(result.value))
                        total_dec_emissions_storage = sum(dec_emissions_storage)  # Applicable to grid-connected BESS
                        total_storage_production_emissions = sum(production_emissions_storage)
                        total_storage_operation_emissions = sum(operation_emissions_storage)
                        total_storage_eol_emissions = sum(eol_emissions_storage)

                if isinstance(evaluation, EnergySystemEnergeticEvaluation):
                    if self.__load_components:
                        load_energy_log = list()
                        load_power_loss_log = list()
                        for component in self.__load_components:
                            for result in evaluation.evaluation_results:
                                if result.value is None:
                                    pass
                                else:
                                    if result.value != 'None' and result.component_name == component.name:
                                        if Titles.LOAD_ENERGY in result.description:
                                            load_energy_log.append(float(result.value))
                                        elif Titles.POWER_LOSS_LOAD in result.description:
                                            load_power_loss_log.append(float(result.value))
                        net_load_energy = sum(load_energy_log) - sum(load_power_loss_log)

                    if self.__storage_components:
                        for component in self.__storage_components:
                            discharged_energy = list()
                            for result in evaluation.evaluation_results:
                                if result.value is None:
                                    pass
                                else:
                                    if result.value != 'None' and result.component_name == component.name:
                                        if Titles.DISCHARGING_ENERGY in result.description:
                                            discharged_energy.append(float(result.value))
                            discharged_energy = sum(discharged_energy)

            if self.__load_components:
                self.append_result(
                    EvaluationResult(
                        energy_form + ' ' + self.Title.LEES + ' for Energy System ' + self.__energy_system_id + ' ',
                        EvaluationResult.Unit.KGCO2EQPERKWH,
                        total_emissions / sum(load_energy_log),
                        'EnergySystem', self.__energy_system_id))
                self.append_result(
                    EvaluationResult(
                        energy_form + ' ' + self.Title.LEES_LOAD_LOSSES + ' for Energy System ' + self.__energy_system_id + ' ',
                        EvaluationResult.Unit.KGCO2EQPERKWH,
                        total_emissions / net_load_energy,
                        'EnergySystem', self.__energy_system_id))

            if self.__storage_components and not self.__load_components:
                # For example with applications serviced by SimSESExternalStrategy and Arbitrage
                if abs(discharged_energy) > 0 and abs(total_dec_emissions_storage) > 0:
                    if 'SimpleEVHome' in self.__ems_strategy[0]:
                        dec_emissions = 0  # As DEC emissions are already included in the total emissions in this case
                    else:
                        dec_emissions = total_dec_emissions_storage
                    lees = (total_emissions + dec_emissions) / discharged_energy
                else:
                    lees = 0

                self.append_result(EvaluationResult(
                    energy_form + ' ' + self.Title.LEES + ' for Energy System ' + self.__energy_system_id + ' ',
                    EvaluationResult.Unit.KGCO2EQPERKWH,
                    lees,
                    'EnergySystem', self.__energy_system_id))
                if 'Fcr' in self.__simses_external_strategy:
                    simulation_duration_in_years = (self.__general_config.end - self.__general_config.start) / 31536000
                    epsilon_fcr = ((
                                               total_storage_production_emissions + total_storage_eol_emissions) / 20 + total_storage_operation_emissions / simulation_duration_in_years) / (
                                              self.__lithium_battery_config.power_fcr / 1e6)
                    self.append_result(
                        EvaluationResult(
                            energy_form + ' ' + self.Title.EPSILON_FCR + ' for Energy System ' + self.__energy_system_id + ' ',
                            EvaluationResult.Unit.KGCO2EQPERMW,
                            epsilon_fcr,
                            'EnergySystem', self.__energy_system_id))

    def plot(self) -> None:
        """
        No plots currently available for EnergySystemEnviroEnergeticEvaluation
        :return: None
        """
        pass
