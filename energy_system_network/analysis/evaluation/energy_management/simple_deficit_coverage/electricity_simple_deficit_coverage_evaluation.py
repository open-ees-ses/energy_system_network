from energy_system_network.analysis.data.energy_management_data.simple_deficit_coverage_data.electricity_simple_deficit_coverage_data import \
    ElectricitySimpleDeficitCoverageData
from energy_system_network.analysis.evaluation.energy_management.simple_deficit_coverage.simple_deficit_coverage_evaluation import \
    SimpleDeficitCoverageEvaluation
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig


class ElectricitySimpleDeficitCoverageEvaluation(SimpleDeficitCoverageEvaluation):
    """
    Evaluates the simple strategy for covering electricity energy deficits in the system.

    Attributes:
        TITLE (str): Description title for the electricity deficit coverage evaluation. Will be appended with data ID.

    Methods:
        __init__(simple_deficit_coverage_data: ElectricitySimpleDeficitCoverageData, config: GeneralAnalysisConfig,
                energy_management_config: EnergyManagementConfig, path: str): Initializer for the ElectricitySimpleDeficitCoverageEvaluation class.
    """
    TITLE = EnergyForm.ELECTRICITY + ' Balance'

    def __init__(self, simple_deficit_coverage_data: ElectricitySimpleDeficitCoverageData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the ElectricitySimpleDeficitCoverageEvaluation class.

        Args:
            simple_deficit_coverage_data (ElectricitySimpleDeficitCoverageData): Data specific to electricity for the simple deficit coverage strategy.
            config (GeneralAnalysisConfig): General configuration settings for the analysis.
            energy_management_config (EnergyManagementConfig): Configuration settings for the energy management.
            path (str): Path for storing the analysis results.
        """
        super().__init__(simple_deficit_coverage_data, config, energy_management_config, path)
