from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.analysis.evaluation.energy_management.energy_management_evaluation import \
    EnergyManagementEvaluation
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig


class SimpleDeficitCoverageEvaluation(EnergyManagementEvaluation):
    """
    Evaluates the simple strategy for covering energy deficits in the system.

    Attributes:
        TITLE (str): Description title for the deficit coverage evaluation. Will be appended with data ID.

    Methods:
        __init__(simple_deficit_coverage_data: EnergyManagementData, config: GeneralAnalysisConfig,
                energy_management_config: EnergyManagementConfig, path: str): Initializer for the SimpleDeficitCoverageEvaluation class.
        evaluate(evaluations: [Evaluation] = None): Evaluates the deficit coverage and appends the result.
    """
    def __init__(self, simple_deficit_coverage_data: EnergyManagementData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the SimpleDeficitCoverageEvaluation class.

        Args:
            simple_deficit_coverage_data (EnergyManagementData): Data for simple deficit coverage strategy.
            config (GeneralAnalysisConfig): General configuration settings for the analysis.
            energy_management_config (EnergyManagementConfig): Configuration settings for the energy management.
            path (str): Path for storing the analysis results.
        """
        super().__init__([simple_deficit_coverage_data], config, config.energy_management_analysis, results_path=path)
        self.TITLE += ' for ' + self.get_data().id

    def evaluate(self, evaluations: [Evaluation] = None):
        """
        Evaluates the deficit coverage strategy and appends the result to the evaluation results.

        Args:
            evaluations (list[Evaluation], optional): List of evaluations to be considered. Defaults to None.

        Return:
            None. The result will be added to the results list.
        """
        result: EvaluationResult = EvaluationResult("Simple Deficit Coverage Evaluation", "", "Completed")
        self.append_result(result)
        self.print_results()
