from energy_system_network.analysis.data.energy_management_data.simple_deficit_coverage_data.heat_simple_deficit_coverage_data import HeatSimpleDeficitCoverageData
from energy_system_network.analysis.evaluation.energy_management.simple_deficit_coverage.simple_deficit_coverage_evaluation import \
    SimpleDeficitCoverageEvaluation
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig


class HeatSimpleDeficitCoverageEvaluation(SimpleDeficitCoverageEvaluation):
    """
    Evaluates the simple strategy for covering heat energy deficits in the system.

    Attributes:
        TITLE (str): Description title for the heat deficit coverage evaluation. Will be appended with data ID.

    Methods:
        __init__(simple_deficit_coverage_data: HeatSimpleDeficitCoverageData, config: GeneralAnalysisConfig,
                energy_management_config: EnergyManagementConfig, path: str): Initializer for the HeatSimpleDeficitCoverageEvaluation class.
    """
    TITLE = EnergyForm.HEAT + ' Balance'

    def __init__(self, simple_deficit_coverage_data: HeatSimpleDeficitCoverageData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the HeatSimpleDeficitCoverageEvaluation class.

        Args:
            simple_deficit_coverage_data (HeatSimpleDeficitCoverageData): Data specific to heat for the simple deficit coverage strategy.
            config (GeneralAnalysisConfig): General configuration settings for the analysis.
            energy_management_config (EnergyManagementConfig): Configuration settings for the energy management.
            path (str): Path for storing the analysis results.
        """
        super().__init__(simple_deficit_coverage_data, config, energy_management_config, path)
