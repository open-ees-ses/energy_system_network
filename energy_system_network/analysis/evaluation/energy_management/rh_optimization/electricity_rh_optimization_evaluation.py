from energy_system_network.analysis.data.energy_management_data.rh_optimization_data.electricity_rh_optimization_data import \
    ElectricityRHOptimizationData
from energy_system_network.analysis.evaluation.energy_management.rh_optimization.rh_optimization_evaluation import \
    RHOptimizationEvaluation
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig


class ElectricityRHOptimizationEvaluation(RHOptimizationEvaluation):
    """
    Evaluates the optimization of electricity in the RH context.

    Attributes:
        TITLE (str): Description for the electricity balance in the context of RH optimization.

    Methods:
        __init__(data: ElectricityRHOptimizationData, config: GeneralAnalysisConfig,
                energy_management_config: EnergyManagementConfig, path: str): Initializer for the ElectricityRHOptimizationEvaluation class.
    """
    TITLE = EnergyForm.ELECTRICITY + ' Balance'

    def __init__(self, data: ElectricityRHOptimizationData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the ElectricityRHOptimizationEvaluation class.

        Args:
            data (ElectricityRHOptimizationData): Data related to electricity optimization in the context of RH.
            config (GeneralAnalysisConfig): General analysis configuration.
            energy_management_config (EnergyManagementConfig): Energy management configuration.
            path (str): Path for storing analysis results.
        """
        super().__init__(data, config, energy_management_config, path)
