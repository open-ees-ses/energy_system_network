from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.analysis.data.energy_management_data.rh_optimization_data.rh_optimization_data import \
    RHOptimizationData
from energy_system_network.analysis.evaluation.energy_management.energy_management_evaluation import \
    EnergyManagementEvaluation
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
import statistics


class RHOptimizationEvaluation(EnergyManagementEvaluation):
    """
    Evaluates the optimization process for RH energy management.

    Attributes:
        TITLE (str): Description title for the RH optimization evaluation.
        __ems_config (EnergyManagementConfig): Configuration settings for energy management.

    Methods:
        __init__(data: EnergyManagementData, config: GeneralAnalysisConfig,
                energy_management_config: EnergyManagementConfig, path: str): Initializer for the RHOptimizationEvaluation class.
        evaluate(evaluations: [Evaluation] = None): Conducts the evaluation of the RH optimization process.
    """
    def __init__(self, data: EnergyManagementData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the RHOptimizationEvaluation class.

        Args:
            data (EnergyManagementData): Energy management data for the RH optimization process.
            config (GeneralAnalysisConfig): General analysis configuration.
            energy_management_config (EnergyManagementConfig): Configuration settings for energy management.
            path (str): Path for storing analysis results.
        """
        super().__init__([data], config, config.energy_management_analysis, results_path=path)
        self.TITLE += ' for ' + self.get_data().id
        self.__ems_config = energy_management_config
        energy_management_config.operation_strategy

    def evaluate(self, evaluations: [Evaluation] = None):
        """
        Conducts the evaluation process for the RH optimization.

        Args:
            evaluations ([Evaluation], optional): List of evaluations to be conducted. Defaults to None.
        """
        data: RHOptimizationData = self.get_data()
        average_storage_power_fulfilment = statistics.mean(data.storage_power_fulfilment)  # in p.u.
        self.append_result(EvaluationResult(self.STORAGE_POWER_FULFILMENT, EvaluationResult.Unit.NONE, average_storage_power_fulfilment))
        result: EvaluationResult = EvaluationResult("RHOptimization", "", "Completed")
        self.append_result(result)
        self.print_results()
