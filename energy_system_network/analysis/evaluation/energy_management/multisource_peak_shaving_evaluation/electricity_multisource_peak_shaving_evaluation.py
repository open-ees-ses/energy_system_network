from energy_system_network.analysis.data.energy_management_data.multisource_peak_shaving_data.electricity_multisource_peak_shaving_data import \
    ElectricityMultiSourcePeakShavingData
from energy_system_network.analysis.evaluation.energy_management.multisource_peak_shaving_evaluation.multisource_peak_shaving_evaluation import \
    MultiSourcePeakShavingEvaluation
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig


class ElectricityMultiSourcePeakShavingEvaluation(MultiSourcePeakShavingEvaluation):
    """
    Evaluates the electricity-based multi-source peak shaving strategy.

    Attributes:
        TITLE (str): Description title specifying that the balance considered is for electricity.

    Methods:
        __init__(peak_shaving_data: ElectricityMultiSourcePeakShavingData, config: GeneralAnalysisConfig,
                energy_management_config: EnergyManagementConfig, path: str): Initializer for the ElectricityMultiSourcePeakShavingEvaluation class.
    """
    TITLE = EnergyForm.ELECTRICITY + ' Balance'

    def __init__(self, peak_shaving_data: ElectricityMultiSourcePeakShavingData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the ElectricityMultiSourcePeakShavingEvaluation class.

        Args:
            peak_shaving_data (ElectricityMultiSourcePeakShavingData): Electricity peak shaving data for the multi-source strategy.
            config (GeneralAnalysisConfig): General configuration settings for the analysis.
            energy_management_config (EnergyManagementConfig): Configuration settings for electricity-based energy management.
            path (str): Path for storing the analysis results.
        """
        super().__init__(peak_shaving_data, config, energy_management_config, path)
