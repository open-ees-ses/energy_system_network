from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.analysis.evaluation.energy_management.energy_management_evaluation import \
    EnergyManagementEvaluation
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig


class MultiSourcePeakShavingEvaluation(EnergyManagementEvaluation):
    """
    Evaluates the peak shaving strategy using multiple energy sources.

    Attributes:
        TITLE (str): Description title for the multi-source peak shaving evaluation.

    Methods:
        __init__(peak_shaving_data: EnergyManagementData, config: GeneralAnalysisConfig,
                energy_management_config: EnergyManagementConfig, path: str): Initializer for the MultiSourcePeakShavingEvaluation class.
        evaluate(evaluations: [Evaluation] = None): Evaluates the peak shaving strategy and appends the results.
    """
    def __init__(self, peak_shaving_data: EnergyManagementData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the MultiSourcePeakShavingEvaluation class.

        Args:
            peak_shaving_data (EnergyManagementData): Peak shaving data for the multi-source strategy.
            config (GeneralAnalysisConfig): General configuration settings for the analysis.
            energy_management_config (EnergyManagementConfig): Configuration settings for energy management.
            path (str): Path for storing the analysis results.
        """
        super().__init__([peak_shaving_data], config, config.energy_management_analysis, results_path=path)
        self.TITLE += ' for ' + self.get_data().id

    def evaluate(self, evaluations: [Evaluation] = None):
        """
        Evaluates the multi-source peak shaving strategy and appends the results.

        Args:
            evaluations ([Evaluation], optional): List of evaluations to be considered. Defaults to None.

        Return:
            None
        """
        result: EvaluationResult = EvaluationResult("MultiSource Peak Shaving Evaluation", "", "Completed")
        self.append_result(result)
        self.print_results()
