from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.analysis.evaluation.energy_management.energy_management_evaluation import \
    EnergyManagementEvaluation
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig


class SimplePeakShavingEvaluation(EnergyManagementEvaluation):
    """
    Provides evaluation for simple peak shaving in energy management.

    Attributes:
        TITLE (str): The base title for the evaluation, which will be updated with the data ID upon initialization.

    Methods:
        __init__(simple_peak_shaving_data: EnergyManagementData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str): Initializer for the SimplePeakShavingEvaluation class.
        evaluate(evaluations: [Evaluation] = None): Evaluates simple peak shaving and logs the results.
    """
    def __init__(self, simple_peak_shaving_data: EnergyManagementData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the SimplePeakShavingEvaluation class.

        Args:
            simple_peak_shaving_data (EnergyManagementData): Data related to energy management for simple peak shaving.
            config (GeneralAnalysisConfig): General analysis configuration.
            energy_management_config (EnergyManagementConfig): Energy management configuration.
            path (str): Path for storing analysis results.
        """
        super().__init__([simple_peak_shaving_data], config, config.energy_management_analysis, results_path=path)
        self.TITLE += ' for ' + self.get_data().id

    def evaluate(self, evaluations: [Evaluation] = None):
        """
        Evaluates simple peak shaving. Appends the results to the evaluation and then prints them.

        Args:
            evaluations ([Evaluation], optional): List of evaluations to be considered. Defaults to None.

        Return:
            None
        """
        result: EvaluationResult = EvaluationResult("Simple Peak Shaving Evaluation", "", "Completed")
        self.append_result(result)
        self.print_results()
