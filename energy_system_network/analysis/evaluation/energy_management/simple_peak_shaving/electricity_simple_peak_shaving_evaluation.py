from energy_system_network.analysis.data.energy_management_data.simple_peak_shaving_data.electricity_simple_peak_shaving_data import \
    ElectricitySimplePeakShavingData
from energy_system_network.analysis.evaluation.energy_management.simple_peak_shaving.simple_peak_shaving_evaluation import \
    SimplePeakShavingEvaluation
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig


class ElectricitySimplePeakShavingEvaluation(SimplePeakShavingEvaluation):
    """
    Evaluates simple peak shaving for electricity.

    Attributes:
        TITLE (str): The title indicating this evaluation is for the electricity balance.

    Methods:
        __init__(simple_peak_shaving_data: ElectricitySimplePeakShavingData, config: GeneralAnalysisConfig,
                energy_management_config: EnergyManagementConfig, path: str): Initializer for the ElectricitySimplePeakShavingEvaluation class.
    """
    TITLE = EnergyForm.ELECTRICITY + ' Balance'

    def __init__(self, simple_peak_shaving_data: ElectricitySimplePeakShavingData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the ElectricitySimplePeakShavingEvaluation class.

        Args:
            simple_peak_shaving_data (ElectricitySimplePeakShavingData): Data related to simple peak shaving for electricity.
            config (GeneralAnalysisConfig): General analysis configuration.
            energy_management_config (EnergyManagementConfig): Energy management configuration.
            path (str): Path for storing analysis results.
        """
        super().__init__(simple_peak_shaving_data, config, energy_management_config, path)

