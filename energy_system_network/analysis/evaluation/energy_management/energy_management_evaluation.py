from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.plotting.axis import Axis
from energy_system_network.analysis.plotting.plotly_plotting.plotly_dashboard_plotting import PlotlyDashboardPlotting
from energy_system_network.analysis.plotting.plotting import Plotting
from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig


class EnergyManagementEvaluation(Evaluation):
    """
    Provides an evaluation mechanism for energy management strategies in an energy system.

    Attributes:
        STORAGE_POWER_FULFILMENT (str): Label for indicating the fulfilment of storage power.
        __result_path (str): Path where the analysis results will be stored.

    Methods:
        __init__(energy_management_data: [EnergyManagementData], config: GeneralAnalysisConfig, do_evaluation: bool,
                energy_system_id: str, filename: str, results_path: str): Initializes the EnergyManagementEvaluation.
        plot() -> None: Plots the power flows of different energy entities during the simulation period.
        evaluate(evaluations=None) -> None: Placeholder for the evaluation method to be implemented in derived classes.
    """
    STORAGE_POWER_FULFILMENT: str = 'Storage power fulfilment'

    def __init__(self, energy_management_data: [EnergyManagementData], config: GeneralAnalysisConfig, do_evaluation: bool, energy_system_id: str = None,
                 filename: str = '', results_path: str = ''):
        """
        Initializes the EnergyManagementEvaluation class.

        Args:
            energy_management_data ([EnergyManagementData]): List of data associated with energy management strategies.
            config (GeneralAnalysisConfig): General configuration settings for the analysis.
            do_evaluation (bool): Indicator if the evaluation should be done.
            energy_system_id (str, optional): ID of the energy system being evaluated. Defaults to None.
            filename (str, optional): Name of the file where results are saved. Defaults to ''.
            results_path (str, optional): Path where the analysis results will be stored. Defaults to ''.
        """
        super().__init__(energy_management_data, config, config.energy_management_analysis)
        self.__result_path = results_path

    def plot(self) -> None:
        """
        Plots the power flows for the chosen Energy Management strategy during the simulation period.

        The function aggregates and displays the power flows of various components such as load, grid, storage,
        and generation over the simulation duration.

        Returns:
            None
        """
        data: EnergyManagementData = self.get_data()
        data_type = type(data)
        data: data_type
        plot: Plotting = PlotlyDashboardPlotting(title=self.TITLE, path=self.__result_path)
        xaxis: Axis = Axis(data=Plotting.format_time(data.time), label=EnergyManagementState.TIME)
        yaxis: [Axis] = list()

        # Load
        if sum([abs(x) for x in data.must_fulfil_load]) > 0:
            yaxis.append(
                Axis(data.must_fulfil_load, label=EnergyManagementState.MUST_FULFIL_LOAD,
                     color=PlotlyDashboardPlotting.Color.BLACK,
                     linestyle=PlotlyDashboardPlotting.Linestyle.DASHED))
        if sum([abs(x) for x in data.residual_load_final]) > 0:
            yaxis.append(
                Axis(data.residual_load_final, label=EnergyManagementState.RESIDUAL_LOAD_FINAL,
                     color=PlotlyDashboardPlotting.Color.MAGENTA,
                     linestyle=PlotlyDashboardPlotting.Linestyle.DASHED))

        # Grid
        if sum([abs(x) for x in data.grid_power]) > 0:
            yaxis.append(
                Axis(data.grid_power, label=EnergyManagementState.GRID_POWER,
                     color=PlotlyDashboardPlotting.Color.BLUE,
                     linestyle=PlotlyDashboardPlotting.Linestyle.DASHED))

        # Storage
        if sum([abs(x) for x in data.storage_power]) > 0:
            yaxis.append(Axis(data.storage_power, label=EnergyManagementState.STORAGE_POWER,
                              color=PlotlyDashboardPlotting.Color.GREEN,
                              linestyle=PlotlyDashboardPlotting.Linestyle.SOLID))
        try:  # Only applicable to optimizer-based s
            if sum([abs(x) for x in data.storage_reference_power]) > 0:
                yaxis.append(Axis(data.storage_reference_power, label=EnergyManagementState.STORAGE_REFERENCE_POWER,
                                  color=PlotlyDashboardPlotting.Color.FLUORESCENT_GREEN,
                                  linestyle=PlotlyDashboardPlotting.Linestyle.SOLID))
        except:
            pass

        # Generation
        if sum([abs(x) for x in data.must_run_generation_power]) > 0:
            yaxis.append(Axis(data.must_run_generation_power, label=EnergyManagementState.MUST_RUN_GENERATION_POWER,
                              color=PlotlyDashboardPlotting.Color.RED,
                              linestyle=PlotlyDashboardPlotting.Linestyle.DASHED))
        if sum([abs(x) for x in data.can_run_generation_power]) > 0:
            yaxis.append(
                Axis(data.can_run_generation_power, label=EnergyManagementState.CAN_RUN_GENERATION_POWER,
                     color=PlotlyDashboardPlotting.Color.YELLOW,
                     linestyle=PlotlyDashboardPlotting.Linestyle.DASHED))

        plot.lines(xaxis, yaxis)
        self.extend_figures(plot.get_figures())

    def evaluate(self, evaluations=None) -> None:
        """
        Placeholder for the evaluation method.

        Args:
            evaluations (optional): Evaluations to consider. Defaults to None.

        Returns:
            None
        """
        pass
