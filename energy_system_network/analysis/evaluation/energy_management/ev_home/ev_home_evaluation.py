from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.analysis.evaluation.energy_management.energy_management_evaluation import \
    EnergyManagementEvaluation
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig


class EVHomeEvaluation(EnergyManagementEvaluation):
    """
    Evaluates the energy management of Electric Vehicle (EV) homes.

    Attributes:
        TITLE (str): Description title for the Electric Vehicle home evaluation.
        __ems_config (EnergyManagementConfig): Configuration settings for energy management.

    Methods:
        __init__(data: EnergyManagementData, config: GeneralAnalysisConfig,
                energy_management_config: EnergyManagementConfig, path: str): Initializer for the EVHomeEvaluation class.
        evaluate(evaluations: [Evaluation] = None): Evaluates the energy management of the Electric Vehicle home.
    """
    def __init__(self, data: EnergyManagementData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the EVHomeEvaluation class.

        Args:
            data (EnergyManagementData): Energy management data for the Electric Vehicle home.
            config (GeneralAnalysisConfig): General configuration for the analysis.
            energy_management_config (EnergyManagementConfig): Configuration settings for energy management.
            path (str): Path for storing the analysis results.
        """
        super().__init__([data], config, config.energy_management_analysis, results_path=path)
        self.TITLE += ' for ' + self.get_data().id
        self.__ems_config = energy_management_config

    def evaluate(self, evaluations: [Evaluation] = None):
        """
        Evaluates the energy management process for Electric Vehicle homes.

        Args:
            evaluations ([Evaluation], optional): List of evaluations to be performed. Defaults to None.
        """
        result: EvaluationResult = EvaluationResult("EV Home Analysis", '', 'Complete')
        self.append_result(result)
        self.print_results()
