from energy_system_network.analysis.data.energy_management_data.ev_home_data.electricity_ev_home_data import \
    ElectricityEVHomeData
from energy_system_network.analysis.evaluation.energy_management.ev_home.ev_home_evaluation import EVHomeEvaluation
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig


class ElectricityEVHomeEvaluation(EVHomeEvaluation):
    """
    Evaluates the electricity balance in Electric Vehicle (EV) homes.

    Attributes:
        TITLE (str): Description title for the electricity balance in Electric Vehicle homes.

    Methods:
        __init__(data: ElectricityEVHomeData, config: GeneralAnalysisConfig,
                energy_management_config: EnergyManagementConfig, path: str): Initializer for the ElectricityEVHomeEvaluation class.
    """
    TITLE = EnergyForm.ELECTRICITY + ' Balance'

    def __init__(self, data: ElectricityEVHomeData, config: GeneralAnalysisConfig,
                 energy_management_config: EnergyManagementConfig, path: str):
        """
        Initializes the ElectricityEVHomeEvaluation class.

        Args:
            data (ElectricityEVHomeData): Electricity data for the Electric Vehicle home.
            config (GeneralAnalysisConfig): General configuration settings for the analysis.
            energy_management_config (EnergyManagementConfig): Configuration settings for energy management.
            path (str): Path for storing the analysis results.
        """
        super().__init__(data, config, energy_management_config, path)
