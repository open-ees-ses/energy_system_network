from energy_system_network.analysis.data.data import Data
from energy_system_network.analysis.data.energy_management_data.rh_optimization_data.rh_optimization_data import \
    RHOptimizationData
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
import numpy as np


class EconomicEvaluation(Evaluation):
    """
    This class evaluates the economic aspects of an energy system, focusing on arbitrage operations.

    Attributes:
        TITLE (str): Title for the economic analysis.
        __ems_config (EnergyManagementConfig): Configuration related to energy management.

    Methods:
        __init__(data: Data, config: GeneralAnalysisConfig, ems_config: EnergyManagementConfig): Initializes the class attributes.
        evaluate(evaluations: [Evaluation] = None) -> None: Evaluates the economic performance of the energy system.
        plot() -> None: Placeholder method for plotting, if desired in future extensions.
    """
    TITLE = 'Economic Analysis '

    def __init__(self, data: Data, config: GeneralAnalysisConfig, ems_config: EnergyManagementConfig):
        """
        Initializes the EconomicEvaluation class.

        Args:
            data (Data): Data entries related to economic system components.
            config (GeneralAnalysisConfig): Configuration related to general analysis.
            ems_config (EnergyManagementConfig): Configuration related to energy management.

        Return:
            None
        """
        super().__init__([data], config, config.economic_analysis, filename='EconomicAnalysis')
        self.__ems_config = ems_config

    def evaluate(self, evaluations: [Evaluation] = None) -> None:
        """
        Evaluates the economic performance of the energy system, considering cost of energy purchase,
        revenue from energy sold, and net revenue from arbitrage.

        Args:
            evaluations ([Evaluation], optional): List of evaluation objects if needed for context.

        Return:
            None
        """
        if 'ArbitrageOptimization' in self.__ems_config.operation_strategy[1]:
            # Calculate only if economic arbitrage chosen in simulation.ini
            data_list: RHOptimizationData = self.get_data()
            cost_energy_purchase = sum(data_list.cash_flow_timestep[np.where(data_list.cash_flow_timestep < 0)])
            revenue_energy_sold = sum(data_list.cash_flow_timestep[np.where(data_list.cash_flow_timestep > 0)])
            net_revenue = sum(data_list.cash_flow_timestep)
            self.append_result(EvaluationResult('Cost energy purchase ',
                                                EvaluationResult.Unit.EURO,
                                                cost_energy_purchase))
            self.append_result(EvaluationResult('Revenue energy sold ',
                                                EvaluationResult.Unit.EURO,
                                                revenue_energy_sold))
            self.append_result(EvaluationResult('Arbitrage net revenue ',
                                                EvaluationResult.Unit.EURO,
                                                net_revenue))
        result: EvaluationResult = EvaluationResult("Economic Evaluation", "", "Completed")
        self.append_result(result)
        self.print_results()

    def plot(self) -> None:
        """
        Placeholder method for plotting, if desired in future extensions.

        Return:
            None
        """
        pass
