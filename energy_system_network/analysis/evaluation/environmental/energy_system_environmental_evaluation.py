import statistics
import numpy as np
from energy_system_network.analysis.data.data import Data
from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.analysis.data.energy_system_component_data.grid_component_data.grid_component_data import \
    GridComponentData
from energy_system_network.analysis.data.energy_system_component_data.storage_component_data.storage_component_data import \
    StorageComponentData
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.analysis.plotting.axis import Axis
from energy_system_network.analysis.plotting.plotly_plotting.plotly_dashboard_plotting import PlotlyDashboardPlotting
from energy_system_network.analysis.plotting.plotting import Plotting
from energy_system_network.commons.state.energy_system_component.grid_component.grid_component_state import \
    GridComponentState
from energy_system_network.commons.state.energy_system_component.storage_component.storage_component_state import \
    StorageComponentState
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.energy_system_component import EnergySystemComponent
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent


class EnergySystemEnvironmentalEvaluation(Evaluation):
    """
    Evaluation of the environmental impacts, specifically carbon dioxide emissions, of an energy system.

    Attributes:
        TITLE (str): Default title for the environmental evaluation.
        FILENAME (str): Default filename for storing the results.
        GENERATION_COMPONENT (str): Name of the generation component class.
        GRID_COMPONENT (str): Name of the grid component class.
        STORAGE_COMPONENT (str): Name of the storage component class.
        LOAD_COMPONENT (str): Name of the load component class.
        PLOT_AXIS_TITLE (str): Title for the plot's y-axis.
        BAR_CHART_TITLE (str): Title for the bar chart showing emissions breakdown.

        class Title:
            Collection of string titles for various emissions types.

        CATEGORY_LIST (list): List of emissions categories.
        TOTAL_EMISSIONS (str): String identifier for total emissions.
        LABELS (str): String identifier for labels.

        __energy_system_id (str): ID of the energy system.
        __energy_system (EnergySystem): Instance of the energy system being evaluated.
        __energy_forms (list): Different forms of energy in the system.
        __generation_components (list): Components that generate energy.
        __grid_components (list): Grid-related components.
        __storage_components (list): Energy storage components.
        __load_components (list): Load components in the energy system.
        __sample_time (float): Time step for simulation.
        __result_path (str): Path to store the results.

    Methods:
        __init__(data_list, config, ems_config, simulation_config, path, energy_system): Initializes the environmental evaluation class.
        evaluate(evaluations: [Evaluation] = None) -> None: Calculates all component-wise emissions categories in kgCO2eq.
        emissions_sum(component_id: str, keyword: str, calculation_type: str) -> float: Filters the specified data from
            the data list and returns a sum of emissions in kgCO2eq.
        create_plot_labels() -> [str]: creates plot labels for use in def plot.
        get_plot_parameters() -> dict: Pre-calculates plot parameters and returns formatted data for use in def plot.
        plot() -> None: Plots line charts, bar charts, and pie chart depicting emission values.
    """
    TITLE = 'Environmental Evaluation'
    FILENAME = 'EnvironmentalEvaluation'
    GENERATION_COMPONENT: str = GenerationComponent.__name__
    GRID_COMPONENT: str = GridComponent.__name__
    STORAGE_COMPONENT: str = StorageComponent.__name__
    LOAD_COMPONENT: str = LoadComponent.__name__
    PLOT_AXIS_TITLE: str = 'Carbon Dioxide Emissions (kg)'
    BAR_CHART_TITLE: str = 'Component-Wise Carbon Dioxide Emissions'

    class Title:
        PRODUCTION_EMISSIONS: str = 'Production emissions'
        GENERATION_EMISSIONS: str = 'Power generation emissions'
        OPERATION_EMISSIONS: str = 'Operation phase emissions'
        GRID_ENERGY_CONSUMPTION_EMISSIONS: str = 'Grid energy consumption emissions'
        DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS: str = 'Discharge energy consumption emissions'
        DEC_EMISSIONS_STORAGE: str = 'DEC emissions at storage'
        END_OF_LIFE_EMISSIONS: str = 'End-of-life emissions'
        POWER_EXPORT_EMISSIONS: str = 'Power export emissions'
        AVERAGE_GRID_CARBON_INTENSITY: str = 'Average grid carbon intensity'
    CATEGORY_LIST = [Title.PRODUCTION_EMISSIONS, Title.GENERATION_EMISSIONS, Title.OPERATION_EMISSIONS, Title.POWER_EXPORT_EMISSIONS,
                     Title.GRID_ENERGY_CONSUMPTION_EMISSIONS, Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS,
                     Title.END_OF_LIFE_EMISSIONS]
    TOTAL_EMISSIONS: str = 'Total emissions'
    LABELS: str = 'labels'

    def __init__(self, data_list: [Data], config: GeneralAnalysisConfig, ems_config: EnergyManagementConfig,
                 simulation_config: GeneralSimulationConfig, path: str, energy_system: EnergySystem):
        """
        Initializes the EnergySystemEnvironmentalEvaluation class, setting up its various components and configurations.

        Args:
            data_list ([Data]): List of data entries to be considered in the evaluation.
            config (GeneralAnalysisConfig): General configuration for analysis.
            ems_config (EnergyManagementConfig): Configuration settings for the Energy Management System.
            simulation_config (GeneralSimulationConfig): Configuration settings for the general simulation.
            path (str): Path where the results are to be stored.
            energy_system (EnergySystem): Instance of the energy system being evaluated.
        """
        self.__energy_system_id = energy_system.get_id()
        super().__init__(data_list, config, config.environmental_analysis, self.__energy_system_id, filename=self.FILENAME)
        self.__energy_system = energy_system
        self.__energy_forms = self.__energy_system.get_energy_form()
        self.__generation_components = energy_system.get_generation_components()
        self.__grid_components = energy_system.get_grid_components()
        self.__storage_components = energy_system.get_storage_components()
        self.__load_components = energy_system.get_load_components()
        self.__sample_time = simulation_config.timestep
        self.__result_path = path

        title_extension: str = ' for Energy System ' + self.__energy_system_id
        self.TITLE += title_extension

    def evaluate(self, evaluations: [Evaluation] = None) -> None:
        """
        Calculates all component-wise emissions categories in kgCO2eq
        :param evaluations: List of Evaluations
        :return: None
        """
        for component_group in self.__generation_components, \
                               self.__storage_components, \
                               self.__grid_components,\
                               self.__load_components:
            data = self.get_data()
            if component_group:
                for component in component_group:  # type: EnergySystemComponent
                    component_name: str = component.name
                    component_id: str = component.component_ID

                    if not isinstance(component, LoadComponent):
                        self.append_result(
                            EvaluationResult(self.Title.PRODUCTION_EMISSIONS + ' ' + component_name,
                                             EvaluationResult.Unit.KGCO2EQ,
                                             component.production_emissions,
                                             component_id, component_name))
                    else:
                        self.append_result(
                            EvaluationResult(self.Title.PRODUCTION_EMISSIONS + ' ' + component_name,
                                             EvaluationResult.Unit.KGCO2EQ,
                                             0,
                                             component_id, component_name))  # These values are zero just to conform to the plotting format
                    if isinstance(component, GenerationComponent):
                        self.append_result(
                            EvaluationResult(self.Title.OPERATION_EMISSIONS + ' ' + component_name,
                                             EvaluationResult.Unit.KGCO2EQ,
                                             self.emissions_sum(component_id, keyword=self.GENERATION_COMPONENT, calculation_type=self.Title.OPERATION_EMISSIONS),
                                             component_id, component_name))
                        self.append_result(
                            EvaluationResult(self.Title.GENERATION_EMISSIONS + ' ' + component_name,
                                             EvaluationResult.Unit.KGCO2EQ,
                                             self.emissions_sum(component_id,
                                                                            keyword=self.GENERATION_COMPONENT, calculation_type=self.Title.GENERATION_EMISSIONS),
                                             component_id, component_name))
                        self.append_result(
                            EvaluationResult(
                                self.Title.GRID_ENERGY_CONSUMPTION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ, 0,
                                component_id,
                                component_name))  # These values are zero just to conform to the plotting format
                        self.append_result(
                            EvaluationResult(
                                self.Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ, 0,
                                component_id,
                                component_name))  # These values are zero just to conform to the plotting format
                        self.append_result(
                            EvaluationResult(
                                self.Title.POWER_EXPORT_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ,
                                self.emissions_sum(component_id, keyword=self.GENERATION_COMPONENT, calculation_type=self.Title.POWER_EXPORT_EMISSIONS),
                                component_id,
                                component_name))
                    elif isinstance(component, StorageComponent):
                        self.append_result(
                            EvaluationResult(
                                self.Title.OPERATION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ,
                                self.emissions_sum(component_id, keyword=self.STORAGE_COMPONENT, calculation_type=self.Title.OPERATION_EMISSIONS),
                                component_id, component_name))
                        self.append_result(
                            EvaluationResult(self.Title.GENERATION_EMISSIONS + ' ' + component_name,
                                             EvaluationResult.Unit.KGCO2EQ,
                                             self.emissions_sum(component_id,
                                                                keyword=self.STORAGE_COMPONENT,
                                                                calculation_type=self.Title.GENERATION_EMISSIONS),
                                             component_id, component_name))
                        self.append_result(
                            EvaluationResult(
                                self.Title.GRID_ENERGY_CONSUMPTION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ, 0,
                                component_id,
                                component_name))  # These values are zero just to conform to the plotting format
                        self.append_result(
                            EvaluationResult(
                                self.Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ, self.emissions_sum(component_id, keyword=self.STORAGE_COMPONENT,
                                                   calculation_type=self.Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS),
                                component_id,
                                component_name))  # Only valid for EV (StorageComponent)
                        self.append_result(
                            EvaluationResult(
                                self.Title.DEC_EMISSIONS_STORAGE + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ,
                                self.emissions_sum(component_id,keyword=self.STORAGE_COMPONENT,calculation_type=self.Title.DEC_EMISSIONS_STORAGE),
                                component_id,
                                component_name))  # Only valid for grid-connected BESS (such as for ancillary services)

                        self.append_result(
                            EvaluationResult(
                                self.Title.POWER_EXPORT_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ,
                                0,
                                component_id,
                                component_name))
                    elif isinstance(component, GridComponent):
                        self.append_result(
                            EvaluationResult(
                                self.Title.OPERATION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ,
                                self.emissions_sum(component_id, keyword=self.GRID_COMPONENT, calculation_type=self.Title.OPERATION_EMISSIONS),
                                component_id, component_name))
                        self.append_result(
                            EvaluationResult(self.Title.GENERATION_EMISSIONS + ' ' + component_name,
                                             EvaluationResult.Unit.KGCO2EQ,
                                             self.emissions_sum(component_id,
                                                                keyword=self.GRID_COMPONENT,
                                                                calculation_type=self.Title.GENERATION_EMISSIONS),
                                             component_id, component_name))
                        for set in data:
                            if isinstance(set, GridComponentData):
                                carbon_intensity = set.carbon_intensity
                        self.append_result(
                            EvaluationResult(
                                self.Title.AVERAGE_GRID_CARBON_INTENSITY + ' ' + component_name,
                                EvaluationResult.Unit.GCO2EQPERKWH, statistics.mean(carbon_intensity),
                                component_id, component_name))
                        self.append_result(
                            EvaluationResult(
                                self.Title.GRID_ENERGY_CONSUMPTION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ, 0,
                                component_id, component_name))  # These values are zero just to conform to the plotting format
                        self.append_result(
                            EvaluationResult(
                                self.Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ, 0,
                                component_id, component_name))  # These values are zero just to conform to the plotting format
                        self.append_result(
                            EvaluationResult(
                                self.Title.POWER_EXPORT_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ,
                                0,
                                component_id,
                                component_name))
                    elif isinstance(component, LoadComponent):
                        self.append_result(
                            EvaluationResult(
                                self.Title.OPERATION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ, 0,
                                component_id, component_name))  # These values are zero just to conform to the plotting format
                        self.append_result(
                            EvaluationResult(self.Title.GENERATION_EMISSIONS + ' ' + component_name,
                                             EvaluationResult.Unit.KGCO2EQ,
                                             0,
                                             component_id, component_name))  # These values are zero just to conform to the plotting format
                        self.append_result(
                            EvaluationResult(
                                self.Title.GRID_ENERGY_CONSUMPTION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ,
                                self.emissions_sum(component_id, keyword=self.LOAD_COMPONENT,
                                                   calculation_type=self.Title.GRID_ENERGY_CONSUMPTION_EMISSIONS),
                                component_id, component_name))
                        self.append_result(
                            EvaluationResult(
                                self.Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ,
                                self.emissions_sum(component_id, keyword=self.LOAD_COMPONENT,
                                                   calculation_type=self.Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS),
                                component_id, component_name))
                        self.append_result(
                            EvaluationResult(
                                self.Title.POWER_EXPORT_EMISSIONS + ' ' + component_name,
                                EvaluationResult.Unit.KGCO2EQ,
                                0,
                                component_id,
                                component_name))

                    if not isinstance(component, LoadComponent):
                        self.append_result(
                            EvaluationResult(self.Title.END_OF_LIFE_EMISSIONS + ' ' + component_name,
                                             EvaluationResult.Unit.KGCO2EQ,
                                             component.end_of_life_emissions,
                                             component_id, component_name))
                    else:
                        self.append_result(
                            EvaluationResult(self.Title.END_OF_LIFE_EMISSIONS + ' ' + component_name,
                                             EvaluationResult.Unit.KGCO2EQ,
                                             0,
                                             component_id, component_name))  # These values are zero just to conform to the plotting format
        self.print_results()

    def emissions_sum(self, component_id: str, keyword: str, calculation_type: str) -> float:
        """
        Filters the specified data from the data list and returns a sum of emissions in kgCO2eq
        :param component_id: energy system component ID as str
        :param keyword: keyword as str
        :param calculation_type: calculation type as str
        :return: emissions sum as float in kgCO2eq
        """
        data_list: Data = self.get_data()
        emissions = 0

        def calculate_emissions_sum(component_data) -> float:
            if calculation_type == self.Title.OPERATION_EMISSIONS:
                series = component_data.operation_emissions
            elif calculation_type == self.Title.GENERATION_EMISSIONS:
                series = component_data.generation_emissions
            elif calculation_type == self.Title.GRID_ENERGY_CONSUMPTION_EMISSIONS:
                series = component_data.grid_energy_consumption_emissions
            elif calculation_type == self.Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS:
                series = component_data.discharge_energy_consumption_emissions
            elif calculation_type == self.Title.DEC_EMISSIONS_STORAGE:
                series = component_data.dec_emissions
            elif calculation_type == self.Title.POWER_EXPORT_EMISSIONS:
                series = component_data.power_export_emissions
            corrected_series = series[~np.isnan(series)]
            emissions_sum = sum(corrected_series)  # in kg
            return emissions_sum

        for data in data_list:
            if keyword in str(type(data)) and data.id == component_id:
                emissions += calculate_emissions_sum(data)
        return emissions

    def create_plot_labels(self) -> [str]:
        """
        creates plot labels for use in def plot
        :return: labels as list of str
        """

        labels = list()

        for component_group in self.__generation_components, \
                               self.__storage_components, \
                               self.__grid_components, \
                               self.__load_components:
            for component in component_group:
                labels.append(component.name)
        return labels

    def get_plot_parameters(self) -> dict:
        """
        Pre-calculates plot parameters and returns formatted data for use in def plot
        :return: bar_pie_plot_parameters as dict
        """
        labels: list = self.create_plot_labels()
        results: [EvaluationResult] = self.evaluation_results

        production_emissions = list()
        generation_emissions = list()
        operation_emissions = list()
        power_export_emissions = list()
        grid_energy_consumption_emissions = list()
        discharge_energy_consumption_emissions = list()
        end_of_life_emissions = list()
        total_emissions = list()
        component_directory = list()

        for component_group in self.__generation_components, \
                               self.__storage_components, \
                               self.__grid_components, \
                               self.__load_components:
            for component in component_group:
                component_name = component.name
                for result in results:
                    if component_name in result.description and self.Title.PRODUCTION_EMISSIONS in result.description:
                        production_emissions.append(float(result.value))
                        component_directory.append(component_name)
                    if component_name in result.description and self.Title.GENERATION_EMISSIONS in result.description:
                        generation_emissions.append(float(result.value))
                        component_directory.append(component_name)
                    if component_name in result.description and self.Title.OPERATION_EMISSIONS in result.description:
                        operation_emissions.append(float(result.value))
                        component_directory.append(component_name)
                    if component_name in result.description and self.Title.POWER_EXPORT_EMISSIONS in result.description:
                        power_export_emissions.append(float(result.value))
                        component_directory.append(component_name)
                    if component_name in result.description and self.Title.GRID_ENERGY_CONSUMPTION_EMISSIONS in result.description:
                        grid_energy_consumption_emissions.append(float(result.value))
                        component_directory.append(component_name)
                    if component_name in result.description and self.Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS in result.description:
                        discharge_energy_consumption_emissions.append(float(result.value))
                        component_directory.append(component_name)
                    if component_name in result.description and self.Title.END_OF_LIFE_EMISSIONS in result.description:
                        end_of_life_emissions.append(float(result.value))
                        component_directory.append(component_name)
                total_emissions.append(
                    float(production_emissions[-1]) + float(operation_emissions[-1]) + float(power_export_emissions[-1])
                    + float(end_of_life_emissions[-1])
                    + float(generation_emissions[-1])
                    + float(grid_energy_consumption_emissions[-1]) + float(discharge_energy_consumption_emissions[-1]))

        bar_pie_plot_parameters = dict()
        bar_pie_plot_parameters[self.LABELS] = labels
        bar_pie_plot_parameters[self.Title.PRODUCTION_EMISSIONS] = production_emissions
        bar_pie_plot_parameters[self.Title.GENERATION_EMISSIONS] = generation_emissions
        bar_pie_plot_parameters[self.Title.OPERATION_EMISSIONS] = operation_emissions
        bar_pie_plot_parameters[self.Title.POWER_EXPORT_EMISSIONS] = power_export_emissions
        bar_pie_plot_parameters[self.Title.GRID_ENERGY_CONSUMPTION_EMISSIONS] = grid_energy_consumption_emissions
        bar_pie_plot_parameters[self.Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS] = discharge_energy_consumption_emissions
        bar_pie_plot_parameters[self.Title.END_OF_LIFE_EMISSIONS] = end_of_life_emissions
        bar_pie_plot_parameters[self.TOTAL_EMISSIONS] = total_emissions
        return bar_pie_plot_parameters

    def plot(self) -> None:
        """
        Plots line charts, bar charts, and pie chart depicting emission values
        :return: None
        """
        plot = PlotlyDashboardPlotting(self.TITLE, path=self.__result_path)
        parameters: dict = self.get_plot_parameters()

        # Line Plot
        data_list = self.get_data()
        yaxis: [Axis] = list()
        for data in data_list:
            if isinstance(data, EnergyManagementData):
                time = data.time
                xaxis: Axis = Axis(data=Plotting.format_time(time), label=self.TIME)
            if isinstance(data, StorageComponentData):
                soci = data.soci
                yaxis.append(Axis(soci, label=StorageComponentState.SOCI,
                                  color=PlotlyDashboardPlotting.Color.GREEN,
                                  linestyle=PlotlyDashboardPlotting.Linestyle.SOLID))
                carbon_intensity_charging_energy = data.carbon_intensity_charging_energy
                yaxis.append(Axis(carbon_intensity_charging_energy,
                                  label=StorageComponentState.CARBON_INTENSITY_CHARGING_ENERGY,
                                  color=PlotlyDashboardPlotting.Color.BROWN,
                                  linestyle=PlotlyDashboardPlotting.Linestyle.SOLID))
            if isinstance(data, GridComponentData):
                carbon_intensity = data.carbon_intensity
                yaxis.append(Axis(carbon_intensity, label=GridComponentState.CARBON_INTENSITY,
                                  color=PlotlyDashboardPlotting.Color.BLACK,
                                  linestyle=PlotlyDashboardPlotting.Linestyle.SOLID))
        plot.lines(xaxis, yaxis)

        labels: list = parameters[self.LABELS]
        production_emissions: list = parameters[self.Title.PRODUCTION_EMISSIONS]
        generation_emissions: list = parameters[self.Title.GENERATION_EMISSIONS]
        operation_emissions: list = parameters[self.Title.OPERATION_EMISSIONS]
        power_export_emissions: list = parameters[self.Title.POWER_EXPORT_EMISSIONS]
        grid_energy_consumption_emissions: list = parameters[self.Title.GRID_ENERGY_CONSUMPTION_EMISSIONS]
        discharge_energy_consumption_emissions: list = parameters[self.Title.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS]
        end_of_life_emissions: list = parameters[self.Title.END_OF_LIFE_EMISSIONS]
        total_emissions: list = parameters[self.TOTAL_EMISSIONS]

        # Bar plot
        yaxis: [Axis] = list()
        yaxis.append(Axis(production_emissions, label=labels))
        yaxis.append(Axis(generation_emissions, label=labels))
        yaxis.append(Axis(operation_emissions, label=labels))
        yaxis.append(Axis(power_export_emissions, label=labels))
        yaxis.append(Axis(grid_energy_consumption_emissions, label=labels))
        yaxis.append(Axis(discharge_energy_consumption_emissions, label=labels))
        yaxis.append(Axis(end_of_life_emissions, label=labels))
        plot.bar(self.CATEGORY_LIST, yaxis, ytitle=self.PLOT_AXIS_TITLE, mode='stack')

        # Pie chart
        plot.pie_chart(labels, total_emissions, title=self.BAR_CHART_TITLE)

        self.extend_figures(plot.get_figures())
