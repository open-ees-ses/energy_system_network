import numpy as np


class EvaluationResult:
    """
    Provides a structure for evaluation results in order to organize data management for
    printing to the console, exporting to csv files, etc.

    Attributes:
        __description (str): Description of the evaluation result.
        __unit (str): The unit in which the result is expressed.
        __value (float, str): The value of the evaluation result.
        __component_ID (str, optional): The ID associated with the component the result pertains to.
        __component_name (str, optional): The name of the component the result pertains to.

    Methods:
        __init__(description: str, unit: str, value: (float, str), component_id: str = None, component_name: str = None): Initializes an EvaluationResult.
        description() -> str: Getter for the description attribute.
        unit() -> str: Getter for the unit attribute.
        value() -> str: Getter for the value attribute.
        component_id() -> str: Getter for the component_id attribute.
        component_name() -> str: Getter for the component_name attribute.
        to_csv() -> [str]: Converts the EvaluationResult to a list of strings.
        to_console(flag: str='console') -> str: Formats the EvaluationResult as a string.
        __str__() -> str: Default string representation for the EvaluationResult.
        __repr__() -> str: Default representation for the EvaluationResult.
    """

    class Unit:
        NONE = ''
        PERCENTAGE = '%'
        MINUTES = 'min'
        EURO = 'EUR'
        WATT = 'W'
        KWH = 'kWh'
        KGCO2EQ = 'kgCO2eq'
        KGCO2EQPERKWH = 'kgCO2eq/kWh'
        GCO2EQPERKWH = 'gCO2eq/kWh'
        KGCO2EQPERMW = 'kgCO2eq/MW'

    def __init__(self, description: str, unit: str, value: (float, str), component_id: str = None,
                 component_name: str = None):
        """
        Initializes an EvaluationResult with a given description, unit, value, and optional component details.

        Args:
            description (str): Description of the evaluation result.
            unit (str): The unit in which the result is expressed.
            value (float, str): The value of the evaluation result.
            component_id (str, optional): The ID associated with the component the result pertains to.
            component_name (str, optional): The name of the component the result pertains to.
        """
        self.__description: str = description
        self.__unit: str = unit
        self.__value = value
        self.__component_ID = component_id
        self.__component_name = component_name

    @property
    def description(self) -> str:
        """Description of the result"""
        return self.__description

    @property
    def unit(self) -> str:
        """Unit of the result"""
        return self.__unit

    @property
    def value(self) -> str:
        """Value of the result"""
        return str(self.__value)

    @property
    def component_id(self) -> str:
        """Component ID of the result"""
        return self.__component_ID

    @property
    def component_name(self) -> str:
        """Component name of the result"""
        return self.__component_name

    def to_csv(self) -> [str]:
        """Returns EvaluationResult as a list of strings."""
        res = list()
        res.append(self.description)
        res.append(self.value)
        res.append(self.unit)
        return res

    def to_console(self, flag: str = 'console') -> str:
        """
        Formats the EvaluationResult as a string in a format suitable for the specified output (console or HTML).

        Args:
            flag (str): Output format (either 'console' or 'html'). Defaults to 'console'.

        Return:
            str: Formatted string representation of the EvaluationResult.
        """
        round_to_decimal_numbers = 3
        res = ''
        if 'console' in flag:
            res += '{:<100}'.format(self.description)
            if isinstance(self.__value, (int, float, complex)):
                if ~np.isnan(self.__value):
                    res += str(round(self.__value, round_to_decimal_numbers)) + ' '
                else:
                    res += self.value + ' '
            else:
                res += self.value + ' '
            res += self.unit

        elif 'html' in flag:
            res += f'<span style="width: 600px; display: inline-block;">{self.description}</span>'
            if isinstance(self.__value, (int, float, complex)):
                if ~np.isnan(self.__value):
                    res += f'<span>{round(self.__value, round_to_decimal_numbers)}</span> '
                else:
                    res += f'<span>{self.value}</span> '
            else:
                res += f'<span>{self.value}</span> '
            res += f'<span>{self.unit}</span>'
        return res

    def __str__(self):
        return self.to_csv()

    def __repr__(self):
        return self.to_csv()
