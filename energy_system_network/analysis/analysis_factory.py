from configparser import ConfigParser
from energy_system_network.analysis.data.data import Data
from energy_system_network.analysis.data.energy_management_data.ev_home_data.electricity_ev_home_data import \
    ElectricityEVHomeData
from energy_system_network.analysis.data.energy_management_data.multisource_peak_shaving_data.electricity_multisource_peak_shaving_data import \
    ElectricityMultiSourcePeakShavingData
from energy_system_network.analysis.data.energy_management_data.rh_optimization_data.electricity_rh_optimization_data import \
    ElectricityRHOptimizationData
from energy_system_network.analysis.data.energy_management_data.simple_peak_shaving_data.electricity_simple_peak_shaving_data import \
    ElectricitySimplePeakShavingData
from energy_system_network.analysis.data.energy_system_component_data.generation_component_data.electricity_generation_component_data import ElectricityGenerationComponentData
from energy_system_network.analysis.data.energy_system_component_data.grid_component_data.electricity_grid_component_data import \
    ElectricityGridComponentData
from energy_system_network.analysis.data.energy_system_component_data.load_component_data.electricity_load_component_data import ElectricityLoadComponentData
from energy_system_network.analysis.data.energy_management_data.simple_deficit_coverage_data.electricity_simple_deficit_coverage_data import \
    ElectricitySimpleDeficitCoverageData
from energy_system_network.analysis.data.energy_system_component_data.storage_component_data.electricity_storage_component_data import ElectricityStorageComponentData
from energy_system_network.analysis.data.energy_system_component_data.generation_component_data.heat_generation_component_data import HeatGenerationComponentData
from energy_system_network.analysis.data.energy_system_component_data.load_component_data.heat_load_component_data import HeatLoadComponentData
from energy_system_network.analysis.data.energy_management_data.simple_deficit_coverage_data.heat_simple_deficit_coverage_data import HeatSimpleDeficitCoverageData
from energy_system_network.analysis.data.energy_system_component_data.storage_component_data.heat_storage_component_data import HeatStorageComponentData
from energy_system_network.analysis.evaluation.economic.economic_evaluation import EconomicEvaluation
from energy_system_network.analysis.evaluation.energetic.energy_system_energetic_evaluation import \
    EnergySystemEnergeticEvaluation
from energy_system_network.analysis.evaluation.energy_management.ev_home.electricity_ev_home_evaluation import \
    ElectricityEVHomeEvaluation
from energy_system_network.analysis.evaluation.energy_management.multisource_peak_shaving_evaluation.electricity_multisource_peak_shaving_evaluation import \
    ElectricityMultiSourcePeakShavingEvaluation
from energy_system_network.analysis.evaluation.energy_management.rh_optimization.electricity_rh_optimization_evaluation import \
    ElectricityRHOptimizationEvaluation
from energy_system_network.analysis.evaluation.energy_management.simple_deficit_coverage.heat_simple_deficit_coverage_evaluation import \
    HeatSimpleDeficitCoverageEvaluation
from energy_system_network.analysis.evaluation.energy_management.simple_deficit_coverage.electricity_simple_deficit_coverage_evaluation import \
    ElectricitySimpleDeficitCoverageEvaluation
from energy_system_network.analysis.evaluation.energy_management.simple_peak_shaving.electricity_simple_peak_shaving_evaluation import \
    ElectricitySimplePeakShavingEvaluation
from energy_system_network.analysis.evaluation.energy_system_component.electricity_storage_component_evaluation import \
    ElectricityStorageComponentEvaluation
from energy_system_network.analysis.evaluation.enviro_energetic.energy_system_enviro_energetic_evaluation import \
    EnergySystemEnviroEnergeticEvaluation
from energy_system_network.analysis.evaluation.environmental.energy_system_environmental_evaluation import \
    EnergySystemEnvironmentalEvaluation
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_merger import EvaluationMerger
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.log import Logger
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.config.simulation.energy_management_config import EnergyManagementConfig
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_management.operation_strategy.optimizer_based.arbitrage_optimization import ArbitrageOptimization
from energy_system_network.simulation.energy_management.operation_strategy.optimizer_based.rh_optimization import \
    RHOptimization
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.multi_source_peak_shaving import \
    MultiSourcePeakShaving
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_deficit_coverage import \
    SimpleDeficitCoverage
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_ev_home import SimpleEVHome
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_peak_shaving import \
    SimplePeakShaving
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simses_external_strategy import \
    SimSESExternalStrategy
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class AnalysisFactory:
    """
    The AnalysisFactory class performs various analysis tasks for an energy system.
    It creates relevant data objects and evaluation objects based on the energy system
    configuration and operation strategy.

    Attributes:
        STRATEGIES_OPTIMIZATION (List): Contains a list of optimization strategies.
        __log (Logger): Logger for this class.
        __result_path (str): Path to the file or directory.
        __analysis_config (GeneralAnalysisConfig): Configuration for general analysis.
        __simulation_config (GeneralSimulationConfig): Configuration for general simulation.
        __energy_management_config (EnergyManagementConfig): Configuration for energy management.
        __energy_system (EnergySystem): EnergySystem object to be analyzed.
        __do_energy_management_analysis (bool): Flag to decide if energy management analysis is needed.
        __do_economic_analysis (bool): Flag to decide if economic analysis is needed.
        __do_energetic_analysis (bool): Flag to decide if energetic analysis is needed.
        __do_environmental_analysis (bool): Flag to decide if environmental analysis is needed.
        __do_enviro_energetic_analysis (bool): Flag to decide if enviro-energetic analysis is needed.
        __data_lists (dict): Container to hold data lists.

    Methods:
        __init__(path: str, config: ConfigParser, energy_system: EnergySystem):
            Initializes the AnalysisFactory class.

        __create_data_list() -> [Data]:
            Creates a list of data objects based on the configurations and energy system provided to the factory.

        create_evaluations() -> [Evaluation]:
            Creates a list of evaluation objects based on the data list created by __create_data_list method.

        create_evaluation_merger(energy_system_name: str) -> EvaluationMerger:
            Creates an EvaluationMerger object which likely consolidates various evaluations.

        close():
            Closes the logger.
    """
    STRATEGIES_OPTIMIZATION = [RHOptimization.__name__, ArbitrageOptimization.__name__, SimSESExternalStrategy.__name__]

    def __init__(self, path: str, config: ConfigParser, energy_system: EnergySystem):
        """
        Initializes the AnalysisFactory class.

        Args:
            path (str):
                Path to the file or directory.
            config (ConfigParser):
                Configuration for the energy system.
            energy_system (EnergySystem):
                EnergySystem object that represents the energy system to be analysed.
        """
        self.__log: Logger = Logger(type(self).__name__)
        self.__result_path: str = path
        self.__analysis_config: GeneralAnalysisConfig = GeneralAnalysisConfig(config)
        self.__simulation_config: GeneralSimulationConfig = GeneralSimulationConfig(config=None,
                                                                                    path=self.__result_path)
        self.__energy_management_config: EnergyManagementConfig = EnergyManagementConfig(config=None,
                                                                                         path=self.__result_path)
        self.__energy_system = energy_system

        self.__do_energy_management_analysis: bool = self.__analysis_config.energy_management_analysis
        self.__do_economic_analysis: bool = self.__analysis_config.economic_analysis
        self.__do_energetic_analysis: bool = self.__analysis_config.energetic_analysis
        self.__do_environmental_analysis: bool = self.__analysis_config.environmental_analysis
        self.__do_enviro_energetic_analysis: bool = self.__analysis_config.enviro_energetic_analysis

        self.__data_lists = {}

    def __create_data_list(self) -> [Data]:
        """
        This method creates a list of data objects based on the configurations and energy system provided to the factory.
        It checks for the type of energy management strategy and energy form involved, creating and adding appropriate
        data objects to the list.

        Returns:
            List of created Data objects based on the energy system and configuration.
        """
        config = self.__simulation_config
        path = self.__result_path
        energy_forms = self.__energy_system.get_energy_form()
        energy_management_strategies = self.__energy_management_config.operation_strategy
        data_list: [Data] = list()

        if self.__do_energy_management_analysis:
            for ems_strategy in energy_management_strategies:
                if self.__energy_system.get_name() in ems_strategy:
                    if SimpleDeficitCoverage.__name__ in ems_strategy:
                        if EnergyForm.ELECTRICITY in ems_strategy:
                            data_list.extend(ElectricitySimpleDeficitCoverageData.get_system_data(path, config, self.__energy_system))
                        if EnergyForm.HEAT in ems_strategy:
                            data_list.extend(HeatSimpleDeficitCoverageData.get_system_data(path, config, self.__energy_system))
                    elif SimplePeakShaving.__name__ in ems_strategy:
                        if EnergyForm.ELECTRICITY in ems_strategy:
                            data_list.extend(ElectricitySimplePeakShavingData.get_system_data(path, config, self.__energy_system))
                    elif MultiSourcePeakShaving.__name__ in ems_strategy:
                        if EnergyForm.ELECTRICITY in ems_strategy:
                            data_list.extend(ElectricityMultiSourcePeakShavingData.get_system_data(path, config, self.__energy_system))
                    elif SimpleEVHome.__name__ in ems_strategy:
                        if EnergyForm.ELECTRICITY in ems_strategy:
                            data_list.extend(ElectricityEVHomeData.get_system_data(path, config, self.__energy_system))
                    elif any(strategy in ems_strategy for strategy in self.STRATEGIES_OPTIMIZATION):
                        if EnergyForm.ELECTRICITY in ems_strategy:
                            data_list.extend(ElectricityRHOptimizationData.get_system_data(path, config, self.__energy_system))

        if self.__do_energetic_analysis or self.__do_environmental_analysis:
            if EnergyForm.ELECTRICITY in energy_forms:
                data_list.extend(ElectricityGenerationComponentData.get_system_data(path, config, self.__energy_system))
                data_list.extend(ElectricityLoadComponentData.get_system_data(path, config, self.__energy_system))
                data_list.extend(ElectricityGridComponentData.get_system_data(path, config, self.__energy_system))
                data_list.extend(ElectricityStorageComponentData.get_system_data(path, config, self.__energy_system))
            if EnergyForm.HEAT in energy_forms:
                data_list.extend(HeatGenerationComponentData.get_system_data(path, config, self.__energy_system))
                data_list.extend(HeatLoadComponentData.get_system_data(path, config, self.__energy_system))
                data_list.extend(HeatStorageComponentData.get_system_data(path, config, self.__energy_system))

        for data in data_list:
            self.__log.info('Created ' + type(data).__name__)
        return data_list

    def create_evaluations(self) -> [Evaluation]:
        """
        This method creates a list of evaluation objects based on the data list.
        It checks the types of the data objects and creates corresponding evaluation objects. If certain types of
        analyses are flagged to be performed, it adds specific evaluations to the list.

        Returns:
            List of created Evaluation objects based on the Data objects.
        """
        data_list: [Data] = self.__create_data_list()
        evaluations: [Evaluation] = list()
        config: GeneralAnalysisConfig = self.__analysis_config
        ems_config: EnergyManagementConfig = self.__energy_management_config
        path: str = self.__result_path

        # Energetic Evaluation
        if self.__do_energetic_analysis:
            evaluations.append(EnergySystemEnergeticEvaluation(data_list, config, ems_config, self.__simulation_config, path, self.__energy_system))

        for data in data_list:

            # Energy Management Evaluation
            if isinstance(data, ElectricitySimpleDeficitCoverageData) and self.__do_energy_management_analysis:
                evaluations.append(ElectricitySimpleDeficitCoverageEvaluation(data, config, ems_config, path))
            elif isinstance(data, HeatSimpleDeficitCoverageData) and self.__do_energy_management_analysis:
                evaluations.append(HeatSimpleDeficitCoverageEvaluation(data, config, ems_config, path))
            elif isinstance(data, ElectricitySimplePeakShavingData) and self.__do_energy_management_analysis:
                evaluations.append(ElectricitySimplePeakShavingEvaluation(data, config, ems_config, path))
            elif isinstance(data, ElectricityMultiSourcePeakShavingData) and self.__do_energy_management_analysis:
                evaluations.append(ElectricityMultiSourcePeakShavingEvaluation(data, config, ems_config, path))
            elif isinstance(data, ElectricityEVHomeData) and self.__do_energy_management_analysis:
                evaluations.append(ElectricityEVHomeEvaluation(data, config, ems_config, path))
            elif isinstance(data, ElectricityRHOptimizationData) and self.__do_energy_management_analysis:
                evaluations.append(ElectricityRHOptimizationEvaluation(data, config, ems_config, path))
                # Economic Evaluation
                if self.__do_economic_analysis:
                    evaluations.append(EconomicEvaluation(data, config, ems_config))

            # Energy System Component Evaluation
            elif isinstance(data, ElectricityStorageComponentData):
                evaluations.append(ElectricityStorageComponentEvaluation(data, config, path))

        # Environmental Evaluation
        if self.__do_environmental_analysis:
            evaluations.append(EnergySystemEnvironmentalEvaluation(data_list, config, ems_config, self.__simulation_config, path, self.__energy_system))

        # Enviro-Energetic Evaluation
        if self.__do_enviro_energetic_analysis and self.__do_energetic_analysis and self.__do_environmental_analysis:
            evaluations.append(
                EnergySystemEnviroEnergeticEvaluation(data_list, config, ems_config, self.__simulation_config, path,
                                                      self.__energy_system))

        return evaluations

    def create_evaluation_merger(self, energy_system_name: str) -> EvaluationMerger:
        """
        This method creates an EvaluationMerger object which merges various evaluations.

        Args:
            energy_system_name (str):
                Name of the energy system for which to create the EvaluationMerger.

        Returns:
            An EvaluationMerger object that can merge various evaluations.
        """
        return EvaluationMerger(self.__result_path, self.__analysis_config, energy_system_name)

    def close(self):
        """
        This method simply closes the logger.
        """
        self.__log.close()
