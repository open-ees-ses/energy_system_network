from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.plotting.axis import Axis
from energy_system_network.analysis.plotting.plotly_plotting.plotly_dashboard_plotting import PlotlyDashboardPlotting
from energy_system_network.analysis.plotting.plotting import Plotting


class BatchComparisonPlotting(Evaluation):
    """
    Class for plotting comparisons in batches.

    Attributes:
        title (str): Title of the comparison.
        plot_param (list): List of parameters for plotting.
        data (list): Data for plotting.
        path (str): Path for exporting or saving plots.

    Methods:
        __init__(bc_data: list, param): Initializes the class with batch comparison data and plot parameters.
        evaluate(evaluations: [Evaluation] = None): Placeholder method for evaluation (to be overridden or implemented).
        plot(): Uses given data and parameters to generate a plot.
    """
    title = 'Comparison Results'
    plot_param = list()
    data = list()
    path ='C:/PythonPlots/'

    def __init__(self, bc_data: list, param):
        """
        Initializes the class with batch comparison data and plot parameters.

        Args:
            bc_data (list): Data for batch comparison.
            param: Parameters for plotting.
        """
        # super().__init__(data, self.EXPORT_TO_CSV)
        self.data = bc_data
        self.plot_param = param
        #  title_extension: str = ' for system ' + self.get_data().id
        self.title = 'Comparison'
        # self.__result_path = path

    def evaluate(self, evaluations: [Evaluation] = None):
        """
        Placeholder method for evaluation. It can be overridden or further implemented if necessary.

        Args:
            evaluations (list of Evaluation, optional): List of evaluation objects.

        Return:
            None
        """
        pass

    def plot(self):
        """
        Uses given data and plot parameters to generate a plot.

        Return:
            None
        """
        data = self.data
        # df = pd.DataFrame(data)
        df = data
        plot_param = self.plot_param
        plot: Plotting = PlotlyDashboardPlotting(title=self.title, path=self.path)
        xaxis: Axis = Axis(data=list(range(0, len(df[0]))), label='Plot no.')
        yaxis: [Axis] = list()
        print(plot_param)

        for i in range(0,len(plot_param)):
            print(df)
            label_now = plot_param[i]
            yaxis.append(Axis(data=df[i], label=label_now, color=PlotlyDashboardPlotting.Color.BLACK, linestyle=PlotlyDashboardPlotting.Linestyle.SOLID))

#           yaxis.append([Axis(data=df, label=plot_param(i))])
        # plot.subplots(xaxis=xaxis, yaxis=yaxis)
        plot.lines(xaxis, yaxis)
