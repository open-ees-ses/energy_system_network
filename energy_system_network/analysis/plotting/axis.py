from energy_system_network.analysis.data.data import Data


class Axis:
    """
    Represents an axis in a plot.

    Attributes:
        __data (Union[list, Data]): Private attribute to store data for the axis.
        __label (Union[str, list[str]]): Private attribute to store the label or labels for the axis.
        __color (str): Private attribute to store the color for the axis.
        __linestyle (str): Private attribute to store the linestyle for the axis.

    Methods:
        __init__(data: Union[list, Data], label: Union[str, list[str]], color: str, linestyle: str):
            Constructor for the Axis class.
        data() -> Data: Getter method for the data attribute.
        label() -> list[str]: Getter method for the label attribute.
        color() -> str: Getter method for the color attribute.
        linestyle() -> str: Getter method for the linestyle attribute.
    """
    def __init__(self, data: (list, Data), label: (str, [str]), color: str = '#0074D9', linestyle: str = '-'):
        """
        Initializes a new instance of the Axis class.

        Args:
            data (Union[list, Data]): Data for the axis.
            label (Union[str, list[str]]): Label or labels for the axis.
            color (str, optional): Color for the axis. Defaults to '#0074D9'.
            linestyle (str, optional): Linestyle for the axis. Defaults to '-'.

        Return:
            None
        """
        self.__data = data
        self.__label = label
        self.__color = color
        self.__linestyle = linestyle

    @property
    def data(self) -> Data:
        """
        Getter for the data attribute.

        Return:
            Data: The data associated with this axis.
        """
        return self.__data

    @property
    def label(self) -> [str]:
        """
        Getter for the label attribute.

        Return:
            list[str]: The label or labels associated with this axis.
        """
        return self.__label

    @property
    def color(self) -> str:
        """
        Getter for the color attribute.

        Return:
            str: The color associated with this axis.
        """
        return self.__color

    @property
    def linestyle(self) -> str:
        """
        Getter for the linestyle attribute.

        Return:
            str: The linestyle associated with this axis.
        """
        return self.__linestyle
