from energy_system_network.analysis.evaluation.energetic.titles import Titles
from energy_system_network.analysis.evaluation.evaluation_result import EvaluationResult
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.simulation.energy_management.operation_strategy.operation_strategy import OperationStrategy
from energy_system_network.simulation.energy_management.operation_strategy.optimizer_based.arbitrage_optimization import ArbitrageOptimization
from energy_system_network.simulation.energy_management.operation_strategy.optimizer_based.rh_optimization import \
    RHOptimization
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.multi_source_peak_shaving import \
    MultiSourcePeakShaving
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_deficit_coverage import \
    SimpleDeficitCoverage
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_ev_home import SimpleEVHome
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_peak_shaving import \
    SimplePeakShaving
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simses_external_strategy import \
    SimSESExternalStrategy
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent


class SankeyDiagramPlotting:
    """
    Represents a Sankey diagram plot for energy systems. It facilitates the plotting
    of energy flows between different components of an energy system.

    Attributes:
        ELECTRICITY (str): Represents the form of energy as electricity.
        HEAT (str): Represents the form of energy as heat.
        NODE_LABELS (str): Key for node labels in the Sankey diagram.
        SOURCES (str): Key for source nodes in the Sankey diagram.
        TARGETS (str): Key for target nodes in the Sankey diagram.
        VALUES (str): Key for values in the Sankey diagram.
        SURPLUS (str): Denotes surplus energy.
        DEFICIT (str): Denotes deficit energy.
        RENEWABLE (str): Denotes renewable energy source.
        CONVENTIONAL (str): Denotes conventional energy source.
        GENERATION (str): Denotes energy generation.
        FEED_IN (str): Represents feed-in energy in the diagram.
        DRAWN (str): Represents drawn energy in the diagram.
        LOAD (str): Represents load energy in the diagram.
        CHARGING (str): Represents energy charging.
        DISCHARGING (str): Represents energy discharging.
        INITIAL_STORED_ENERGY (str): Initial energy content in storage.
        FINAL_STORED_ENERGY (str): Final energy content in storage after operations.
        CHARGED_ENERGY_GRID (str): Represents energy charging from the grid.
        LOSSES (str): Represents energy losses.
        DRIVETRAIN (str): Represents the drivetrain energy aspect.
        REGENERATIVE_BRAKING (str): Energy retrieved from regenerative braking.
        Color: Inner class representing various color codes.

        __operation_strategy (OperationStrategy): The operational strategy used in the energy system.
        __energy_forms (list): The list of energy forms in the energy system.
        __generation_components ([GenerationComponent]): Components responsible for energy generation.
        __load_components ([LoadComponent]): Components consuming energy.
        __storage_components ([StorageComponent]): Components storing energy.
        __grid_components ([GridComponent]): Components representing the energy grid.
        __precalculated_quantities (dict): Stores any precalculated quantities for plotting.
        __node_labels (list): Labels for the nodes in the Sankey diagram.
        __sources (list): Source nodes for the Sankey links.
        __targets (list): Target nodes for the Sankey links.
        __values (list): Values associated with each Sankey link.

    Methods:
        __init__(energy_system: EnergySystem, energy_forms: list): Initializes the SankeyDiagramPlotting instance.
        create_node_labels(energy_forms: list): Generates a list of node labels for the Sankey diagram.
        create_sankey_diagram_parameters(results: [EvaluationResult]): Creates parameters required for generating a Sankey diagram based on operation strategy.
        precalculate_quantities(results): Calculates necessary quantities for the Sankey diagram based on the provided results.
        grid_ancillary_service_parameters(): Prepares parameters for the grid ancillary service diagram.
        simple_ev_home_parameters(): Prepares parameters for the simple EV home diagram.
        pyomo_rolling_horizon_optimization_parameters(): Prepares parameters for the Pyomo rolling horizon optimization diagram.
        multisource_peak_shaving_diagram_parameters(): Prepares parameters for the multisource peak shaving diagram.
        simple_deficit_coverage_diagram_parameters(): Prepares parameters for the simple deficit coverage diagram.
        simple_peak_shaving_diagram_parameters(): Calculate and return parameters for plotting a simple peak shaving Sankey diagram.
        add_link(source: int, target: int, value: float): Add a new link to the Sankey diagram with the given source, target, and value.
        connect_generation(energy_form: str): Connect the given energy form to its generation components.
        connect_generation_feed_in(energy_form: str): Connect the given energy form's generation to its feed-in.
        connect_grid(energy_form: str): Connect the given energy form to its grid components.
        connection_generation_to_feed_in(energy_form: str): Connect the given energy form's generation to its feed-in.
        connect_storage(energy_form: str): Connect the given energy form to its storage components.
        connect_discharged_energy_to_load(energy_form: str): Connect the discharged energy of the given energy form to its load.
        connect_surplus_deficit_to_energy_form(energy_form: str): Connect the surplus and deficit of the given energy form.
        connect_surplus_deficit(energy_form: str): Connect the surplus and deficit of the given energy form to its generation and load, respectively.
        connect_load(energy_form: str): Connect the given energy form to its load components.
        package_diagram_parameters(): Package the Sankey diagram parameters into a dictionary and return it.
        node_energy_balance_check(node: str, node_in: [float], node_out: [float]): Check the energy balance for the given node and raise an exception if the balance is not satisfied.
"""
    ELECTRICITY: str = 'Electricity'
    HEAT: str = 'Heat'

    NODE_LABELS: str = 'node labels'
    SOURCES: str = 'sources'
    TARGETS: str = 'targets'
    VALUES: str = 'values'

    SURPLUS: str = 'Surplus'
    DEFICIT: str = 'Deficit'
    RENEWABLE: str = 'Renewable'
    CONVENTIONAL: str = 'Conventional'

    GENERATION: str = 'Generation'
    FEED_IN: str = 'Feed-in'
    DRAWN: str = 'Drawn'
    LOAD: str = 'Load'
    CHARGING: str = 'Charging'
    DISCHARGING: str = 'Discharging'
    INITIAL_STORED_ENERGY: str = 'Initial Energy Content'
    FINAL_STORED_ENERGY: str = 'in Storage'
    CHARGED_ENERGY_GRID: str = 'Charging Energy Grid'
    LOSSES: str = 'Losses'

    DRIVETRAIN: str = 'Drivetrain'
    REGENERATIVE_BRAKING: str = 'Regenerative Braking'

    class Color:  # Using the new defaults from https://clrs.cc/
        BLUE = '#0074D9'
        YELLOW = '#FFDC00'
        GREEN = '#3D9970'
        RED = '#FF4136'
        CYAN = '#7FDBFF'
        MAGENTA = '#F012BE'
        BLACK = '#111111'
        WHITE = '#FFFFFF'

    def __init__(self, energy_system: EnergySystem, energy_forms: list):
        """
        Initializes the SankeyDiagramPlotting instance using data from a provided energy system and a list of energy forms.

        Args:
            energy_system (EnergySystem): The energy system instance from which data is sourced.
            energy_forms (list): A list of energy forms considered in the energy system.

        Return:
            None
        """
        super().__init__()

        self.__operation_strategy: OperationStrategy = energy_system.get_energy_management().get_operation_strategy()
        self.__energy_forms = energy_forms
        self.__generation_components: [GenerationComponent] = energy_system.get_generation_components()
        self.__load_components: [LoadComponent] = energy_system.get_load_components()
        self.__storage_components: [StorageComponent] = energy_system.get_storage_components()
        self.__grid_components: [GridComponent] = energy_system.get_grid_components()
        self.__precalculated_quantities = {}

        self.__node_labels = list()
        self.__sources = list()
        self.__targets = list()
        self.__values = list()

    def create_node_labels(self, energy_forms: list) -> [str]:
        """
        Generates a list of node labels for the Sankey diagram.

        Args:
           energy_forms (list): List of energy forms considered in the system.

        Return:
            [str]: List of node labels.
        """
        node_labels = list()

        for component_group in self.__generation_components, \
                               self.__load_components, \
                               self.__storage_components, \
                               self.__grid_components:
            if component_group:
                for component in component_group:
                    node_labels.append(component.name)
        for energy_form in energy_forms:
            node_labels.append(self.RENEWABLE + ' ' + energy_form + ' ' + self.GENERATION)
            node_labels.append(self.CONVENTIONAL + ' ' + energy_form + ' ' + self.GENERATION)
            node_labels.append(energy_form + ' ' + self.GENERATION)

            node_labels.append(energy_form + ' ' + self.CHARGING)
            node_labels.append(energy_form + ' ' + self.SURPLUS)
            node_labels.append(energy_form + ' ' + self.FEED_IN)

            node_labels.append(energy_form + ' ' + self.DISCHARGING)
            node_labels.append(energy_form + ' ' + self.DRAWN)

            node_labels.append(energy_form)

            node_labels.append(energy_form + ' ' + self.DEFICIT)
            node_labels.append(energy_form + ' ' + self.INITIAL_STORED_ENERGY)
            node_labels.append(energy_form + ' ' + self.FINAL_STORED_ENERGY)
            node_labels.append(energy_form + ' ' + self.LOAD)

            node_labels.append(energy_form + ' ' + self.DRIVETRAIN)  # For ElectricVehicle
            node_labels.append(energy_form + ' ' + self.REGENERATIVE_BRAKING)  # For ElectricVehicle

        node_labels.append(self.LOSSES)

        return node_labels

    def create_sankey_diagram_parameters(self, results: [EvaluationResult]) -> dict:
        """
        Creates parameters required for generating a Sankey diagram based on the type of operation strategy.

        Args:
           results ([EvaluationResult]): List of evaluation results from the energy system.

        Return:
            dict: Dictionary containing parameters necessary for plotting the Sankey diagram.
        """
        self.__precalculated_quantities: dict = self.precalculate_quantities(results)
        if isinstance(self.__operation_strategy, SimpleDeficitCoverage):
            sankey_diagram_parameters: dict = self.simple_deficit_coverage_diagram_parameters()
            return sankey_diagram_parameters
        elif isinstance(self.__operation_strategy, SimplePeakShaving):
            sankey_diagram_parameters: dict = self.simple_peak_shaving_diagram_parameters()
            return sankey_diagram_parameters
        elif isinstance(self.__operation_strategy, MultiSourcePeakShaving):
            sankey_diagram_parameters: dict = self.multisource_peak_shaving_diagram_parameters()
            return sankey_diagram_parameters
        elif isinstance(self.__operation_strategy, RHOptimization):
            sankey_diagram_parameters: dict = self.pyomo_rolling_horizon_optimization_parameters()
            return sankey_diagram_parameters
        elif isinstance(self.__operation_strategy, (ArbitrageOptimization, SimSESExternalStrategy)):
            sankey_diagram_parameters: dict = self.grid_ancillary_service_parameters()
            return sankey_diagram_parameters
        elif isinstance(self.__operation_strategy, SimpleEVHome):
            sankey_diagram_parameters: dict = self.simple_ev_home_parameters()
            return sankey_diagram_parameters
        else:
            raise Exception('Sankey diagram functionality is not yet supported for the chosen operation strategy.')

    def precalculate_quantities(self, results) -> dict:
        """
        Calculates necessary quantities for the Sankey diagram based on the provided results.

        Args:
           results: Evaluation results from the energy system components.

        Return:
            dict: Dictionary containing precalculated quantities used for plotting the Sankey diagram.
        """
        precalculated_quantities = {}
        for energy_form in self.__energy_forms:

            # Generation Components
            renewable_energy_generation = 0
            conventional_energy_generation = 0
            total_energy_form_generation = 0
            if self.__generation_components:
                for component in self.__generation_components:
                    if energy_form in component.energy_form:
                        if component.must_run is True:
                            for result in results:
                                if result.component_name == component.name:
                                    value = float(result.value)
                                    precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.GENERATION] = value
                                    renewable_energy_generation += value

                        elif component.must_run is False:
                            for result in results:
                                if result.component_name == component.name and energy_form in result.description:
                                    value = float(result.value)
                                    precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.GENERATION] = value
                                    conventional_energy_generation += value

            total_energy_form_generation = renewable_energy_generation + conventional_energy_generation
            precalculated_quantities[self.RENEWABLE + ' ' + energy_form + ' ' + self.GENERATION] = renewable_energy_generation if abs(renewable_energy_generation) > 1e-3 else 0
            precalculated_quantities[self.CONVENTIONAL + ' ' + energy_form + ' ' + self.GENERATION] = conventional_energy_generation if abs(conventional_energy_generation) > 1e-3 else 0
            precalculated_quantities[energy_form + ' ' + self.GENERATION] = total_energy_form_generation if abs(total_energy_form_generation) > 1e-3 else 0

            # Grid Components
            total_fed_in_energy = 0
            total_drawn_energy = 0
            if self.__grid_components:
                for component in self.__grid_components:
                    if energy_form in component.energy_form:
                        for result in results:
                            if result.component_name == component.name and energy_form in result.description and Titles.FED_IN_ENERGY_GRID in result.description:
                                value = float(result.value)
                                precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.FEED_IN] = value
                                total_fed_in_energy += value  # maintain a ledger of total energy fed in

                        for result in results:
                            if result.component_name == component.name and energy_form in result.description and Titles.DRAWN_ENERGY_GRID in result.description:
                                value = float(result.value)
                                precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.DRAWN] = value
                                total_drawn_energy += value  # maintain a ledger of total

            precalculated_quantities[energy_form + ' ' + self.FEED_IN] = total_fed_in_energy if abs(total_fed_in_energy) > 1e-3 else 0
            precalculated_quantities[energy_form + ' ' + self.DRAWN] = total_drawn_energy if abs(total_drawn_energy) > 1e-3 else 0

            # Storage Components
            total_charged_energy = 0
            total_discharged_energy = 0
            total_charged_energy_grid = 0

            if self.__storage_components:
                for component in self.__storage_components:
                    charged_energy = 0
                    discharged_energy = 0
                    final_energy_storage_content = 0
                    initial_energy_storage_content = 0

                    if energy_form in component.energy_form:

                        for result in results:
                            if result.component_name == component.name and energy_form in result.description and Titles.CHARGING_ENERGY in result.description:
                                value = float(result.value)
                                precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.CHARGING] = value
                                charged_energy += value
                                total_charged_energy += value

                        for result in results:
                            if result.component_name == component.name and energy_form in result.description and Titles.DISCHARGING_ENERGY in result.description:
                                value = float(result.value)
                                precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.DISCHARGING] = value
                                discharged_energy += value
                                total_discharged_energy += value

                        for result in results:
                            if result.component_name == component.name and energy_form in result.description and Titles.FINAL_ENERGY_CONTENT in result.description:
                                value = float(result.value)
                                precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.FINAL_STORED_ENERGY] = value
                                final_energy_storage_content = value

                            elif result.component_name == component.name and energy_form in result.description and Titles.INITIAL_ENERGY_CONTENT in result.description:
                                value = float(result.value)
                                precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.INITIAL_STORED_ENERGY] = value
                                initial_energy_storage_content = value

                        storage_losses = initial_energy_storage_content + charged_energy - discharged_energy - final_energy_storage_content
                        precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.LOSSES] = storage_losses

            precalculated_quantities[energy_form + ' ' + self.CHARGING] = total_charged_energy if abs(total_charged_energy) > 1e-3 else 0
            precalculated_quantities[energy_form + ' ' + self.DISCHARGING] = total_discharged_energy if abs(total_discharged_energy) > 1e-3 else 0
            precalculated_quantities[energy_form + ' ' + self.CHARGED_ENERGY_GRID] = total_charged_energy_grid

            # Energy Surplus (Terminus) and Deficit (Origin)
            energy_surplus = 0
            energy_deficit = 0
            for result in results:
                if energy_form in result.description and Titles.ENERGY_SURPLUS in result.description:
                    energy_surplus = float(result.value)
                elif energy_form in result.description and Titles.ENERGY_DEFICIT in result.description:
                    energy_deficit = float(result.value)

            precalculated_quantities[energy_form + ' ' + self.SURPLUS] = energy_surplus if abs(energy_surplus) > 1e-3 else 0
            precalculated_quantities[energy_form + ' ' + self.DEFICIT] = energy_deficit if abs(energy_deficit) > 1e-3 else 0

            # Load Components
            if self.__load_components:
                for component in self.__load_components:
                    if energy_form in component.energy_form:
                        for result in results:
                            if result.component_name == component.name and energy_form in result.description:
                                if Titles.LOAD_ENERGY in result.description:
                                    value = float(result.value)
                                    precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.LOAD] = value
            return precalculated_quantities

    def grid_ancillary_service_parameters(self) -> dict:
        """
        Prepares the parameters for the grid ancillary service diagram.

        This method determines the flow of electricity through the grid,
        incorporating elements like drawn electricity, storage connections, charging,
        and discharging of storage. The outcome is represented as a Sankey diagram.

        Returns:
            dict: Dictionary containing parameters for the Sankey diagram.
        """
        self.__node_labels = self.create_node_labels(self.__energy_forms)
        self.connect_grid(EnergyForm.ELECTRICITY)

        # OS-specific link and node
        # energy_form drawn (through) to energy_form (through)
        self.add_link(self.__node_labels.index(EnergyForm.ELECTRICITY + ' ' + self.DRAWN),
                      self.__node_labels.index(EnergyForm.ELECTRICITY),
                      self.__precalculated_quantities[EnergyForm.ELECTRICITY + ' ' + self.DRAWN])

        self.connect_storage(EnergyForm.ELECTRICITY)

        # OS-specific link and node
        # energy_form (through) to energy_form charging (through)
        self.add_link(self.__node_labels.index(EnergyForm.ELECTRICITY),
                      self.__node_labels.index(EnergyForm.ELECTRICITY + ' ' + self.CHARGING),
                      self.__precalculated_quantities[EnergyForm.ELECTRICITY + ' ' + self.CHARGING])

        # OS-specific link and node
        # energy_form discharging (through) to grid feed-in (through)
        self.add_link(self.__node_labels.index(EnergyForm.ELECTRICITY + ' ' + self.DISCHARGING),
                      self.__node_labels.index(EnergyForm.ELECTRICITY + ' ' + self.FEED_IN),
                      self.__precalculated_quantities[EnergyForm.ELECTRICITY + ' ' + self.DISCHARGING])

        sankey_diagram_parameters: dict = self.package_diagram_parameters()
        return sankey_diagram_parameters

    def simple_ev_home_parameters(self) -> dict:
        """
        Prepares the parameters for the simple electric vehicle (EV) home diagram.

        This method visualizes the flow of electricity in an EV home setup,
        illustrating aspects like drawn electricity, storage, charging,
        discharging, and regenerative braking. The outcome is shown in a Sankey diagram.

        Returns:
            dict: Dictionary containing parameters for the Sankey diagram.
        """
        self.__node_labels = self.create_node_labels(self.__energy_forms)
        self.connect_grid(EnergyForm.ELECTRICITY)

        # OS-specific link and node
        # energy_form drawn (through) to energy_form (through)
        self.add_link(self.__node_labels.index(EnergyForm.ELECTRICITY + ' ' + self.DRAWN),
                      self.__node_labels.index(EnergyForm.ELECTRICITY),
                      self.__precalculated_quantities[EnergyForm.ELECTRICITY + ' ' + self.DRAWN])

        self.connect_storage(EnergyForm.ELECTRICITY)

        # OS-specific link and node
        # energy_form (through) to energy_form charging (through)
        self.add_link(self.__node_labels.index(EnergyForm.ELECTRICITY),
                      self.__node_labels.index(EnergyForm.ELECTRICITY + ' ' + self.CHARGING),
                      self.__precalculated_quantities[EnergyForm.ELECTRICITY + ' ' + self.CHARGING])

        # OS-specific link and node
        # energy_form discharging (through) to energy_form Drivetrain (Through)
        self.add_link(self.__node_labels.index(EnergyForm.ELECTRICITY + ' ' + self.DISCHARGING),
                      self.__node_labels.index(EnergyForm.ELECTRICITY + ' ' + self.DRIVETRAIN),
                      self.__precalculated_quantities[EnergyForm.ELECTRICITY + ' ' + self.DISCHARGING])

        # Drivetrain (Through) to energy_form (through) for regenerative charging
        regenerative_charging = self.__precalculated_quantities[EnergyForm.ELECTRICITY + ' ' + self.CHARGING] \
                                - self.__precalculated_quantities[EnergyForm.ELECTRICITY + ' ' + self.DRAWN]
        self.add_link(self.__node_labels.index(EnergyForm.ELECTRICITY + ' ' + self.REGENERATIVE_BRAKING),
                      self.__node_labels.index(EnergyForm.ELECTRICITY),
                      regenerative_charging)

        sankey_diagram_parameters: dict = self.package_diagram_parameters()
        return sankey_diagram_parameters

    def pyomo_rolling_horizon_optimization_parameters(self) -> dict:
        """
        Prepares parameters for the Pyomo rolling horizon optimization diagram.

        This method details the electricity flow considering different energy forms,
        which include energy generation, storage, grid interactions, and load.
        The distribution of the different energy forms is represented through a Sankey diagram.

        Returns:
            dict: Dictionary containing parameters for the Sankey diagram.
        """
        self.__node_labels = self.create_node_labels(self.__energy_forms)

        for energy_form in self.__energy_forms:

            self.connect_generation(energy_form)

            # OS-specific link and node
            # generation energy_form (through) to energy_form (through)
            generation_to_energy_form = self.__precalculated_quantities[energy_form + ' ' + self.GENERATION] - \
                                        self.__precalculated_quantities[energy_form + ' ' + self.FEED_IN]

            self.add_link(self.__node_labels.index(energy_form + ' ' + self.GENERATION),
                          self.__node_labels.index(energy_form),
                          generation_to_energy_form)

            self.connect_grid(energy_form)
            # OS-specific link and node
            # energy_form drawn (through) to energy_form (through)
            self.add_link(self.__node_labels.index(energy_form + ' ' + self.DRAWN),
                          self.__node_labels.index(energy_form),
                          self.__precalculated_quantities[energy_form + ' ' + self.DRAWN])

            self.connect_storage(energy_form)
            # OS-specific link and node
            # energy_form (through) to energy_form charging (through)
            self.add_link(self.__node_labels.index(energy_form),
                          self.__node_labels.index(energy_form + ' ' + self.CHARGING),
                          self.__precalculated_quantities[energy_form + ' ' + self.CHARGING])
            self.connect_discharged_energy_to_load(energy_form)

            # OS-specific link and node
            self.connect_surplus_deficit_to_energy_form(energy_form)

            self.connect_load(energy_form)
            # OS-specific link and node
            # energy_form (through) to energy_form load (through)
            value = self.__precalculated_quantities[energy_form + ' ' + self.DRAWN] + \
                    self.__precalculated_quantities[energy_form + ' ' + self.GENERATION] - \
                    self.__precalculated_quantities[energy_form + ' ' + self.FEED_IN] + \
                    self.__precalculated_quantities[energy_form + ' ' + self.DEFICIT] - \
                    self.__precalculated_quantities[energy_form + ' ' + self.CHARGING] - \
                    self.__precalculated_quantities[energy_form + ' ' + self.SURPLUS]
            self.add_link(self.__node_labels.index(energy_form),
                          self.__node_labels.index(energy_form + ' ' + self.LOAD),
                          value)

            # energy_form generation (through) to grid feed-in (through)
            self.connect_generation_feed_in(energy_form)

        sankey_diagram_parameters: dict = self.package_diagram_parameters()
        return sankey_diagram_parameters

    def multisource_peak_shaving_diagram_parameters(self) -> dict:
        """
        Prepares parameters for the multisource peak shaving diagram.

        This method uses the Pyomo rolling horizon optimization parameters to
        depict the electricity flow across various energy forms in a multisource peak shaving context.
        The data is visualized using a Sankey diagram.

        Returns:
            dict: Dictionary containing parameters for the Sankey diagram.
        """
        return self.pyomo_rolling_horizon_optimization_parameters()

        # self.__node_labels = self.create_node_labels(self.__energy_forms)
        #
        # for energy_form in self.__energy_forms:
        #
        #     self.connect_generation(energy_form)
        #     self.connect_grid(energy_form)
        #     self.connect_storage(energy_form)
        #     self.connect_load(energy_form)
        #
        #     # OS-specific links
        #     self.connect_surplus_deficit(energy_form)
        #
        #     # calculate values
        #     charged_energy_generation = self.__precalculated_quantities[energy_form + ' ' + self.CHARGING] - \
        #                                 self.__precalculated_quantities[energy_form + ' ' + self.CHARGED_ENERGY_GRID]
        #     load_energy_generation = self.__precalculated_quantities[energy_form + ' ' + self.GENERATION] - \
        #                              self.__precalculated_quantities[energy_form + ' ' + self.SURPLUS] - \
        #                              self.__precalculated_quantities[energy_form + ' ' + self.FEED_IN] - \
        #                              charged_energy_generation
        #     load_energy_grid = self.__precalculated_quantities[energy_form + ' ' + self.DRAWN] - \
        #                        self.__precalculated_quantities[energy_form + ' ' + self.CHARGED_ENERGY_GRID]
        #
        #     # energy_form generation (through) to energy_form load (through)
        #     self.add_link(self.__node_labels.index(energy_form + ' ' + self.GENERATION),
        #                   self.__node_labels.index(energy_form + ' ' + self.LOAD),
        #                   load_energy_generation)
        #
        #     # energy_form drawn (through) to energy_form load (through)
        #     self.add_link(self.__node_labels.index(energy_form + ' ' + self.DRAWN),
        #                   self.__node_labels.index(energy_form + ' ' + self.LOAD),
        #                   load_energy_grid)
        #
        #     # energy_form generation (through) to energy_form charging (through)
        #     self.add_link(self.__node_labels.index(energy_form + ' ' + self.GENERATION),
        #                   self.__node_labels.index(energy_form + ' ' + self.CHARGING),
        #                   charged_energy_generation)
        #
        #     # energy_form drawn (through) to energy_form charging (through)
        #     self.add_link(self.__node_labels.index(energy_form + ' ' + self.DRAWN),
        #                   self.__node_labels.index(energy_form + ' ' + self.CHARGING),
        #                   self.__precalculated_quantities[energy_form + ' ' + self.CHARGED_ENERGY_GRID])
        #
        # sankey_diagram_parameters: dict = self.package_diagram_parameters(self.__node_labels, self.__sources, self.__targets, self.__values)
        # return sankey_diagram_parameters

    def simple_deficit_coverage_diagram_parameters(self) -> dict:
        """
        Prepares parameters for the simple deficit coverage diagram.

        This method maps the flow of electricity focusing on energy deficit coverage.
        The flow involves aspects like energy generation, grid interactions, storage, and load.
        The resulting relationships are presented in a Sankey diagram.

        Returns:
            dict: Dictionary containing parameters for the Sankey diagram.
        """
        self.__node_labels = self.create_node_labels(self.__energy_forms)

        for energy_form in self.__energy_forms:

            self.connect_generation(energy_form)
            self.connect_grid(energy_form)

            # OS-specific links and nodes
            # energy_form drawn (through) to energy_form load (through)
            self.add_link(self.__node_labels.index(energy_form + ' ' + self.DRAWN),
                          self.__node_labels.index(energy_form + ' ' + self.LOAD),
                          self.__precalculated_quantities[energy_form + ' ' + self.DRAWN])

            self.connect_storage(energy_form)
            # energy_form generation (through) to energy_form charging (through)
            self.add_link(self.__node_labels.index(energy_form + ' ' + self.GENERATION),
                          self.__node_labels.index(energy_form + ' ' + self.CHARGING),
                          self.__precalculated_quantities[energy_form + ' ' + self.CHARGING])

            self.connect_discharged_energy_to_load(energy_form)

            self.connect_load(energy_form)

            # OS-specific links
            self.connect_surplus_deficit(energy_form)

            generation_to_load = self.__precalculated_quantities[energy_form + ' ' + self.GENERATION] - \
                                 self.__precalculated_quantities[energy_form + ' ' + self.CHARGING] - \
                                 self.__precalculated_quantities[energy_form + ' ' + self.FEED_IN] - \
                                 self.__precalculated_quantities[energy_form + ' ' + self.SURPLUS]
            # energy_form generation (through) to energy_form load (through)
            self.add_link(self.__node_labels.index(energy_form + ' ' + self.GENERATION),
                          self.__node_labels.index(energy_form + ' ' + self.LOAD),
                          generation_to_load)

            # energy_form generation (through) to grid feed-in (through)
            self.connect_generation_feed_in(energy_form)

        sankey_diagram_parameters: dict = self.package_diagram_parameters()
        return sankey_diagram_parameters

    def simple_peak_shaving_diagram_parameters(self) -> dict:
        """
        Create parameters for a simple peak shaving Sankey diagram.

        Returns:
            dict: Dictionary containing the necessary parameters for plotting the Sankey diagram.
        """
        self.__node_labels = self.create_node_labels(self.__energy_forms)

        for energy_form in self.__energy_forms:
            self.connect_grid(energy_form)
            self.connect_storage(energy_form)
            self.connect_discharged_energy_to_load(energy_form)
            self.connect_load(energy_form)

            # energy_form drawn (through) to energy_form charging (through)
            self.add_link(self.__node_labels.index(energy_form + ' ' + self.DRAWN),
                          self.__node_labels.index(energy_form + ' ' + self.CHARGING),
                          self.__precalculated_quantities[energy_form + ' ' + self.CHARGING])

            value = self.__precalculated_quantities[energy_form + ' ' + self.DRAWN] - \
                    self.__precalculated_quantities[energy_form + ' ' + self.CHARGING]

            # energy_form drawn (through) to energy_form load (through)
            self.add_link(self.__node_labels.index(energy_form + ' ' + self.DRAWN),
                          self.__node_labels.index(energy_form + ' ' + self.LOAD),
                          value)

            # OS-specific links
            self.connect_surplus_deficit(energy_form)

        sankey_diagram_parameters: dict = self.package_diagram_parameters()
        return sankey_diagram_parameters

    def add_link(self, source: int, target: int, value: float) -> None:
        """
        Add a link (connection) between source and target with the given value.

        Args:
            source (int): Index of the source node.
            target (int): Index of the target node.
            value (float): Value of the flow between source and target.
        """
        self.__sources.append(source)
        self.__targets.append(target)
        self.__values.append(value)

    def connect_generation(self, energy_form: str) -> None:
        """
        Connects generation components to their respective energy forms.

        Args:
            energy_form (str): Energy form to connect.
        """
        if self.__generation_components:
            for component in self.__generation_components:
                if energy_form in component.energy_form:
                    # renewable generation component (origin) to renewable energy_form generation (through)

                    if component.must_run is True:
                        self.add_link(self.__node_labels.index(component.name),
                                      self.__node_labels.index(self.RENEWABLE + ' ' + energy_form + ' ' + self.GENERATION),
                                      self.__precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.GENERATION])

                    # conventional generation component (origin) to conventional energy_form generation (through)
                    elif component.must_run is False:
                        self.add_link(self.__node_labels.index(component.name),
                                      self.__node_labels.index(self.CONVENTIONAL + ' ' + energy_form + ' ' + self.GENERATION),
                                      self.__precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.GENERATION])

            # renewable energy_form generation (through) to energy_form generation (through)
            self.add_link(self.__node_labels.index(self.RENEWABLE + ' ' + energy_form + ' ' + self.GENERATION),
                          self.__node_labels.index(energy_form + ' ' + self.GENERATION),
                          self.__precalculated_quantities[self.RENEWABLE + ' ' + energy_form + ' ' + self.GENERATION])

            # conventional energy_form generation (through) to energy_form generation (through)
            self.add_link(self.__node_labels.index(self.CONVENTIONAL + ' ' + energy_form + ' ' + self.GENERATION),
                          self.__node_labels.index(energy_form + ' ' + self.GENERATION),
                          self.__precalculated_quantities[self.CONVENTIONAL + ' ' + energy_form + ' ' + self.GENERATION])

    def connect_generation_feed_in(self, energy_form: str) -> None:
        """
        Connect the energy form's generation to its feed-in.

        Args:
            energy_form (str): Energy form to connect.
        """
        # energy_form generation (through) to grid feed-in (through)
        self.add_link(self.__node_labels.index(energy_form + ' ' + self.GENERATION),
                      self.__node_labels.index(energy_form + ' ' + self.FEED_IN),
                      self.__precalculated_quantities[energy_form + ' ' + self.FEED_IN])

    def connect_grid(self, energy_form: str) -> None:
        """
        Connect the energy form's feed-in to the grid components and drawn.

        Args:
            energy_form (str): Energy form to connect.
        """
        if self.__grid_components:
            for component in self.__grid_components:
                if energy_form in component.energy_form:
                    # energy_form feed-in (through) to energy_form grid-component (terminus)
                    self.add_link(self.__node_labels.index(energy_form + ' ' + self.FEED_IN),
                                  self.__node_labels.index(component.name),
                                  self.__precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.FEED_IN])

                    # energy_form grid-component (origin) to energy_form drawn (through)
                    self.add_link(self.__node_labels.index(component.name),
                                  self.__node_labels.index(energy_form + ' ' + self.DRAWN),
                                  self.__precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.DRAWN])

    def connection_generation_to_feed_in(self, energy_form: str) -> None:
        """
        Connect the energy form's generation to its feed-in.

        Args:
            energy_form (str): Energy form to connect.
        """
        # energy_form generation (through) to energy_form feed-in (through)
        self.add_link(self.__node_labels.index(energy_form + ' ' + self.GENERATION),
                      self.__node_labels.index(energy_form + ' ' + self.FEED_IN),
                      self.__precalculated_quantities[energy_form + ' ' + self.FEED_IN])

    def connect_storage(self, energy_form: str) -> None:
        """
        Connect the energy form to its storage components.

        Args:
            energy_form (str): Energy form to connect.
        """
        if self.__storage_components:
            for component in self.__storage_components:

                if energy_form in component.energy_form:

                    # energy_form initial stored energy (origin) to storage component (through)
                    self.add_link(self.__node_labels.index(energy_form + ' ' + self.INITIAL_STORED_ENERGY),
                                  self.__node_labels.index(component.name),
                                  self.__precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.INITIAL_STORED_ENERGY])

                    # energy_form charging (through) to storage component (through)
                    self.add_link(self.__node_labels.index(energy_form + ' ' + self.CHARGING),
                                  self.__node_labels.index(component.name),
                                  self.__precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.CHARGING])

                    # storage component (through) to energy_form discharging (through)
                    self.add_link(self.__node_labels.index(component.name),
                                  self.__node_labels.index(energy_form + ' ' + self.DISCHARGING),
                                  self.__precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.DISCHARGING])

                    # storage component (through) to energy_form in storage (terminus)
                    self.add_link(self.__node_labels.index(component.name),
                                  self.__node_labels.index(energy_form + ' ' + self.FINAL_STORED_ENERGY),
                                  self.__precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.FINAL_STORED_ENERGY])

                    # storage component (through) to losses (terminus)
                    self.add_link(self.__node_labels.index(component.name),
                                  self.__node_labels.index(self.LOSSES),
                                  self.__precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.LOSSES])

    def connect_discharged_energy_to_load(self, energy_form: str) -> None:
        """
        Connect the discharged energy of an energy form to its load.

        Args:
            energy_form (str): Energy form to connect.
        """
        # energy_form discharging (through) to energy_form load (through)
        self.add_link(self.__node_labels.index(energy_form + ' ' + self.DISCHARGING),
                      self.__node_labels.index(energy_form + ' ' + self.LOAD),
                      self.__precalculated_quantities[energy_form + ' ' + self.DISCHARGING])

    def connect_surplus_deficit_to_energy_form(self, energy_form: str) -> None:
        """
        Connect surplus and deficit of an energy form.

        Args:
            energy_form (str): Energy form to connect.
        """
        # energy_form (through) to energy_form surplus (terminus)
        self.add_link(self.__node_labels.index(energy_form),
                      self.__node_labels.index(energy_form + ' ' + self.SURPLUS),
                      self.__precalculated_quantities[energy_form + ' ' + self.SURPLUS])

        # energy_form deficit (origin) to energy_form (through)
        self.add_link(self.__node_labels.index(energy_form + ' ' + self.DEFICIT),
                      self.__node_labels.index(energy_form),
                      self.__precalculated_quantities[energy_form + ' ' + self.DEFICIT])

    def connect_surplus_deficit(self, energy_form: str) -> None:
        """
        Connect surplus and deficit of an energy form to generation and load.

        Args:
            energy_form (str): Energy form to connect.
        """
        # energy_form generation (through) to energy_form surplus (terminus)
        self.add_link(self.__node_labels.index(energy_form + ' ' + self.GENERATION),
                      self.__node_labels.index(energy_form + ' ' + self.SURPLUS),
                      self.__precalculated_quantities[energy_form + ' ' + self.SURPLUS])

        # energy_form deficit (origin) to energy_form load (through)
        self.add_link(self.__node_labels.index(energy_form + ' ' + self.DEFICIT),
                      self.__node_labels.index(energy_form + ' ' + self.LOAD),
                      self.__precalculated_quantities[energy_form + ' ' + self.DEFICIT])

    def connect_load(self, energy_form: str) -> None:
        """
        Connect the energy form's load to its load components.

        Args:
            energy_form (str): Energy form to connect.
        """
        if self.__load_components:
            for component in self.__load_components:
                if energy_form in component.energy_form:
                    # load component (terminus) to energy_form load (through)
                    self.add_link(self.__node_labels.index(energy_form + ' ' + self.LOAD),
                                  self.__node_labels.index(component.name),
                                  self.__precalculated_quantities[component.name + ' ' + energy_form + ' ' + self.LOAD])

    def package_diagram_parameters(self) -> dict:
        """
        Package the Sankey diagram parameters.

        Returns:
            dict: Dictionary containing the necessary parameters for plotting the Sankey diagram.
        """
        sankey_diagram_parameters = dict()
        sankey_diagram_parameters[self.NODE_LABELS] = self.__node_labels
        sankey_diagram_parameters[self.SOURCES] = self.__sources
        sankey_diagram_parameters[self.TARGETS] = self.__targets
        sankey_diagram_parameters[self.VALUES] = self.__values

        return sankey_diagram_parameters

    def node_energy_balance_check(self, node: str, node_in: [float], node_out: [float]) -> None:
        """
        Check the balance of energy at a given node.

        Args:
            node (str): Node to check.
            node_in ([float]): List of incoming energy values to the node.
            node_out ([float]): List of outgoing energy values from the node.

        Raises:
            Exception: If the balance at the node is not satisfied.
        """
        if abs(sum(node_in) - sum(node_out)) < 1e-05:
            pass
        else:
            raise Exception('Node balance at node ' + node + ' not satisfied.')
