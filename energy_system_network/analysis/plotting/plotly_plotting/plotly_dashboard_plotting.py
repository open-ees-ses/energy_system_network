import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from energy_system_network.analysis.plotting.axis import Axis
from energy_system_network.analysis.plotting.plotly_plotting.sankey_diagram_plotting import SankeyDiagramPlotting
from energy_system_network.analysis.plotting.plotting import Plotting


class PlotlyDashboardPlotting(Plotting):
    """
    A plotting class that utilizes Plotly to generate various types of plots for an energy system network dashboard.

    Attributes:
        static_figure_index (int): A static index for figures.
        Color: A class providing color constants.
        Linestyle: A class providing line style constants.
        __title (str): The title of the plotting dashboard.
        __path (str): The path where the plot should be saved or accessed.
        __figures (list): A list of figures generated.

    Methods:
        __init__(title: str, path: str): Initializes the plotting dashboard with a title and path.
        layout(fig, xaxis: Axis=None, yaxis: [Axis]=None) -> None: Configures the layout for a given figure.
        lines(xaxis: Axis, yaxis: [Axis], secondary= []) -> None: Generates line plots.
        battery_swapping_slot_subplots(xaxis: Axis, yaxis: [[Axis, Axis, Axis]], yaxis_1: [Axis]) -> None: Creates subplots for battery swapping slots.
        get_figures() -> list: Retrieves the list of generated figures.
        show(fig): Displays a given figure.
        histogram(): (Not implemented) Generates a histogram.
        bar(series_names: list, yaxis_values: [Axis], ytitle: str, mode: str) -> None: Creates a bar plot.
        pie_chart(labels: list, values: list, title: str) -> None: Creates a pie chart.
        sankey_diagram(node_links: dict) -> None: Generates a Sankey diagram.
        horizontal_binary_heatmap(data: np.ndarray, xaxis: Axis) -> None: Creates a horizontal binary heatmap.
        subplots(xaxis: Axis, yaxis: [[Axis]]) -> None: Generates subplots.
    """
    static_figure_index = 1

    class Color: #Using the new defaults from https://clrs.cc/
        BLUE = '#0074D9'
        NAVY_BLUE = '#000080'
        YELLOW = '#FFDC00'
        GREEN = '#3D9970'
        FLUORESCENT_GREEN = '#39FF14'
        RED = '#FF4136'
        CYAN = '#7FDBFF'
        MAGENTA = '#F012BE'
        BLACK = '#111111'
        BROWN = '#964B00'
        WHITE = '#FFFFFF'

    class Linestyle: #TODO Unused
        DOTTED = ':'
        SOLID = '-'
        DASHED = '--'
        DASH_DOT = '-.'

    def __init__(self, title: str, path: str):
        """
        Initializes the plotting dashboard with a title and path.

        Args:
            title (str): The title for the dashboard.
            path (str): The path where the plot should be saved or accessed.
        """
        super().__init__()
        self.__title = title
        self.__path = path
        self.__figures: list = list()

    def layout(self, fig, xaxis: Axis=None, yaxis: [Axis]=None) -> None:
        """
        Configures the layout for a given figure.

        Args:
            fig: The figure object to be configured.
            xaxis (Axis, optional): Configuration for the x-axis.
            yaxis (list of Axis, optional): Configuration for the y-axis.

        Return:
            None
        """
        fig.layout.title = self.__title
        fig.layout.font = dict(
            family="Times New Roman, monospace",  # TUM Guideline: Helvetica
            size=18,
            # color="#7f7f7f"
        )

        if xaxis and yaxis:
            if len(yaxis) == 1:
                yaxistitle = yaxis[0].label
            else:
                yaxistitle = ""

            fig.update_layout(
                xaxis_title=xaxis.label,
                yaxis_title=yaxistitle,
                yaxis=dict(
                    showexponent='all',
                    # exponentformat='e'
                ),
                showlegend=True, autosize=True,
                template="none"
            )
            fig.update_xaxes(showgrid=True, gridwidth=2, gridcolor='Lightgray',
                             showline=True, linewidth=2, linecolor='black')
            fig.update_yaxes(showgrid=True, gridwidth=2, gridcolor='Lightgray',
                             showline=True, linewidth=2, linecolor='black')

    def lines(self, xaxis: Axis, yaxis: [Axis], secondary= []) -> None:
        """
        Generates line plots.

        Args:
            xaxis (Axis): Configuration for the x-axis.
            yaxis (list of Axis): Configuration for the y-axis.
            secondary (list, optional): Additional configurations for secondary plots.

        Return:
            None
        """
        fig = make_subplots(specs=[[{"secondary_y": True}]])
        self.layout(fig, xaxis, yaxis)
        fig.update_layout(legend=dict(x=0,y=1,traceorder="normal",))

        for i in range(len(yaxis)):
            if i in secondary:
                secondary_y_axis = True
            else:
                secondary_y_axis = False
            fig.add_trace(go.Scatter(
                x=xaxis.data,
                y=yaxis[i].data,
                mode='lines',
                name=yaxis[i].label,
                line_color=yaxis[i].color
                ), secondary_y=secondary_y_axis
            )
        self.show(fig)

    def battery_swapping_slot_subplots(self, xaxis: Axis, yaxis: [[Axis, Axis, Axis]], yaxis_1: [Axis]) -> None:
        """
        Creates subplots for battery swapping slots.

        Args:
            xaxis (Axis): Configuration for the x-axis.
            yaxis (list of lists of Axis): Configuration for the y-axis for each subplot.
            yaxis_1 (list of Axis): Configuration for the primary y-axis.

        Return:
            None
        """
        fig = make_subplots(rows=len(yaxis)+1, cols=1, shared_xaxes=True, specs=[[{"secondary_y": True}]]*(len(yaxis)+1))
        self.layout(fig, xaxis, yaxis)
        fig.update_layout(legend=dict(x=0,y=1,traceorder="normal",),
            autosize=True,
            height=100*(len(yaxis)+1))
        show_legend = True
        for axis in yaxis:
            idx = yaxis.index(axis)
            # Plot Power
            fig.add_trace(go.Scatter(
                x=xaxis.data,
                y=axis[0].data,
                mode='lines',
                name=axis[0].label,
                line_color=axis[0].color, legendgroup=axis[0].label, showlegend=show_legend),
                secondary_y=False, row=idx+1, col=1)
            # Plot SOC
            fig.add_trace(go.Scatter(
                x=xaxis.data,
                y=axis[1].data,
                mode='lines',
                name=axis[1].label,
                line_color=axis[1].color, legendgroup=axis[1].label, showlegend=show_legend),
                secondary_y=True, row=idx+1, col=1)

            if idx < (len(yaxis) - 1):
                # # Plot SOH
                fig.add_trace(go.Scatter(
                    x=xaxis.data,
                    y=axis[2].data,
                    mode='lines',
                    name=axis[2].label,
                    line_color=axis[2].color, legendgroup=axis[2].label, showlegend=show_legend),
                    secondary_y=True, row=idx+1, col=1)
                fig['layout']['yaxis' + str(idx*2+1)]['title'] = 'Slot ' + str(idx+1)
                fig['layout']['yaxis' + str(idx*2+2)]['title'] = 'Value'
                fig['layout']['yaxis' + str(idx*2+1)]['dtick'] = 4e4
            else:
                fig['layout']['yaxis' + str(idx*2+1)]['title'] = 'Power'
                fig['layout']['yaxis' + str(idx*2+2)]['title'] = 'SOC'
                fig['layout']['yaxis' + str(idx*2+1)]['dtick'] = 4e5

            fig['layout']['yaxis' + str(idx*2+1)]['tick0'] = 0
            fig['layout']['yaxis' + str(idx*2+2)]['tick0'] = 0
            fig['layout']['yaxis' + str(idx*2+2)]['dtick'] = 0.5
            fig['layout']['xaxis' + str(idx+1)]['title'] = ''

            show_legend = False
        fig.add_trace(go.Scatter(
            x=xaxis.data,
            y=yaxis_1[0].data,
            mode='lines',
            name=yaxis_1[0].label,
            line_color=yaxis_1[0].color,),
            secondary_y=False, row=len(yaxis)+1, col=1)
        fig.add_trace(go.Scatter(
            x=xaxis.data,
            y=yaxis_1[1].data,
            mode='lines',
            name=yaxis_1[1].label,
            line_color=yaxis_1[1].color,),
            secondary_y=False, row=len(yaxis)+1, col=1)
        fig['layout']['yaxis' + str((len(yaxis)+1)*2-1)]['title'] = 'Swaps'
        fig['layout']['yaxis' + str((len(yaxis)+1)*2-1)]['tick0'] = 0
        fig['layout']['yaxis' + str((len(yaxis)+1)*2-1)]['dtick'] = 1

        fig['layout']['xaxis' + str((len(yaxis)+1))]['title'] = 'Time'

        self.show(fig)

    def get_figures(self) -> list:
        """
        Retrieves the list of generated figures.

        Return:
            list: List of generated figures.
        """
        return self.__figures

    def show(self, fig):
        """
        Displays a given figure.

        Args:
            fig: The figure object to be displayed.

        Return:
            None
        """
        self.__figures.append(fig)
        self.static_figure_index += 1

    def histogram(self):
        """
        Generates a histogram. (Not yet implemented)

        Return:
            None
        """
        pass

    def bar(self, series_names: list, yaxis_values: [Axis], ytitle: str, mode: str) -> None:
        """
        Creates a bar plot.

        Args:
            series_names (list): Names for each series in the bar plot.
            yaxis_values (list of Axis): Configuration for the y-axis values for each series.
            ytitle (str): Title for the y-axis.
            mode (str): Mode for the bar plot (e.g., stack, group).

        Return:
            None
        """
        fig = make_subplots(rows=1, cols=1)
        self.layout(fig, yaxis=yaxis_values)
        for value, name in zip(yaxis_values, series_names):
            fig.add_trace(
                go.Bar(name=name, x=value.label, y=value.data), row=1, col=1)
        fig.update_layout(barmode=mode, yaxis=dict(title=ytitle))
        fig.update_layout(barmode='relative')
        self.show(fig)

    def pie_chart(self, labels: list, values: list, title: str) -> None:
        """
        Creates a pie chart.

        Args:
            labels (list): Labels for each section of the pie chart.
            values (list): Values for each section of the pie chart.
            title (str): Title of the pie chart.

        Return:
            None
        """
        fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.3)])
        # fig = go.Pie(labels=labels, values=values, name=title, hole=.3)
        self.layout(fig)
        fig.update_layout(title_text=title)
        self.show(fig)

    def sankey_diagram(self, node_links: dict) -> None:
        """
        Generates a Sankey diagram.

        Args:
            node_links (dict): Dictionary containing node-link data for the Sankey diagram.

        Return:
            None
        """
        fig = go.Figure(data=[go.Sankey(
            node=dict(
                pad=15,
                thickness=20,
                line=dict(color="black", width=0.5),
                label=node_links[SankeyDiagramPlotting.NODE_LABELS]),
            link=dict(
                source=node_links[SankeyDiagramPlotting.SOURCES],
                target=node_links[SankeyDiagramPlotting.TARGETS],
                value=node_links[SankeyDiagramPlotting.VALUES],
            ))])
        self.layout(fig)
        self.show(fig)

    def horizontal_binary_heatmap(self, data: np.ndarray, xaxis: Axis) -> None:
        """
        Creates a horizontal binary heatmap.

        Args:
            data (np.ndarray): Data for the heatmap.
            xaxis (Axis): Configuration for the x-axis.

        Return:
            None
        """
        colorscale = [[0, 'rgb(255, 0, 0)'], [1, 'rgb(0, 0, 255)']]  # Custom colorscale, red for 0 and blue for 1
        data = data.reshape(1, -1)
        fig = go.Figure(data=go.Heatmap(z=data, x= xaxis.data, colorscale=colorscale, showscale=False))

        fig.update_layout(
            xaxis=dict(showticklabels=True, showgrid=False, title=xaxis.label),
            yaxis=dict(showticklabels=False, showgrid=False, title='Availability'),
            autosize=True, height=180,
            margin=dict(l=80, r=185, b=10, t=25),
        )

        self.layout(fig)
        self.show(fig)

    def subplots(self, xaxis: Axis, yaxis: [[Axis]]) -> None:
        """
        Generates subplots.

        Args:
            xaxis (Axis): Configuration for the x-axis.
            yaxis (list of lists of Axis): Configuration for the y-axis for each subplot.

        Return:
            None
        """
        fig = make_subplots(rows=len(yaxis), cols=len(yaxis[0]))
        for x in range(len(yaxis)):
            ydata = yaxis[x]
            for y in range(len(ydata)):

                fig.append_trace(go.Scatter(
                    x=xaxis.data,
                    y=ydata[y].data,
                    name=ydata[y].label,
                    line_color=ydata[y].color,
                ),
                row=y+1, col=x+1)
        self.layout(fig, xaxis, yaxis)
        self.show(fig)
