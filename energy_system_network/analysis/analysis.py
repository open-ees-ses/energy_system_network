from configparser import ConfigParser
from energy_system_network.analysis.analysis_factory import AnalysisFactory
from energy_system_network.analysis.evaluation.enviro_energetic.energy_system_enviro_energetic_evaluation import \
    EnergySystemEnviroEnergeticEvaluation
from energy_system_network.analysis.evaluation.evaluation import Evaluation
from energy_system_network.analysis.evaluation.evaluation_merger import EvaluationMerger
from energy_system_network.commons.log import Logger
from energy_system_network.config.analysis.general_analysis_config import GeneralAnalysisConfig
from energy_system_network.simulation.energy_system.energy_system_network import EnergySystemNetwork


class ESNAnalysis:
    """
    This class represents an energy system network (ESN) analysis. It's designed to run evaluations on different
    energy systems in the network.

    Attributes:
        __path (str): The path where analysis results will be saved.
        __log (Logger): Instance of Logger class for logging information.
        __config (GeneralAnalysisConfig): The configuration for the analysis.
        __analysis_config (ConfigParser): The raw configuration parser object.
        __energy_systems (list): List of energy systems in the network.

    Methods:
        __init__(self, path: str, energy_system_network: EnergySystemNetwork, config: ConfigParser):
            Initializes an instance of the ESNAnalysis class.
        run(self): Runs the analysis on the energy systems of the network.
        close(self): Closes the Logger instance after the analysis is complete.
    """

    def __init__(self, path: str, energy_system_network: EnergySystemNetwork, config: ConfigParser):
        """
        Initializes an instance of the ESNAnalysis class.

        Args:
            path (str): The path where analysis results will be saved.
            energy_system_network (EnergySystemNetwork): The energy system network on which the analysis is to be run.
            config (ConfigParser): The configuration for the analysis.
        """
        self.__path: str = path
        self.__log: Logger = Logger(__name__)
        self.__config: GeneralAnalysisConfig = GeneralAnalysisConfig(config)
        self.__analysis_config: ConfigParser = config
        self.__energy_systems = energy_system_network.get_energy_systems()

    def run(self):
        """
        Runs the analysis on the energy systems of the network.

        For each energy system, it:
        - Initializes an AnalysisFactory
        - Creates and runs evaluations, writes their results to CSV
        - Merges the results of evaluations
        - Writes the configuration to the result path
        """
        for energy_system in self.__energy_systems:
            result_path: str = self.__config.get_result_for(self.__path)
            factory: AnalysisFactory = AnalysisFactory(result_path, self.__analysis_config, energy_system)
            evaluations: [Evaluation] = factory.create_evaluations()
            evaluation_merger: EvaluationMerger = factory.create_evaluation_merger(energy_system.get_name())
            self.__log.info('Entering analysis')
            for evaluation in evaluations:
                self.__log.info('Running evaluation ' + type(evaluation).__name__)
                # Pass specific evaluations such as energetic and environmental evaluations to hybrid evaluations,
                # such as enviro-energetic evaluations
                if isinstance(evaluation, EnergySystemEnviroEnergeticEvaluation):
                    evaluation.run(evaluations)
                else:
                    evaluation.run()
                evaluation.write_to_csv(result_path)
                # evaluation.write_to_batch(path=str(Path(self.__path).parent).replace('\\','/') + '/' + BATCH_DIR,
                #                           name=basename(dirname(self.__path)), run=basename(dirname(result_path)))
                evaluation.close()
            self.__config.write_config_to(result_path)
            evaluation_merger.merge(evaluations)
            factory.close()
        self.close()

    def close(self):
        """
        Closes the Logger instance after the analysis is complete.
        """
        self.__log.close()
