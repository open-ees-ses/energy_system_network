def get_sum_for(data) -> float:
    """
    Computes the sum of all values in the data.

    Args:
        data (list or similar data structure): Input data for which the sum needs to be calculated.

    Return:
        float: The sum of all values in the data.
    """
    return data[:].sum()


def get_mean_for(data) -> float:
    """
    Computes the mean of all values in the data.

    Args:
        data (list or similar data structure): Input data for which the mean needs to be calculated.

    Return:
        float: The mean of all values in the data.
    """
    return data[:].mean()


def get_min_for(data) -> float:
    """
    Retrieves the minimum value in the data.

    Args:
        data (list or similar data structure): Input data from which the minimum value needs to be determined.

    Return:
        float: The minimum value in the data.
    """
    return data[:].min()


def get_max_for(data) -> float:
    """
    Retrieves the maximum value in the data.

    Args:
        data (list or similar data structure): Input data from which the maximum value needs to be determined.

    Return:
        float: The maximum value in the data.
    """
    return data[:].max()


def get_positive_values_from(data):
    """
    Filters and retrieves only the positive values from the data.

    Args:
        data (list or similar data structure): Input data from which the positive values need to be extracted.

    Return:
        list or similar data structure: Data containing only the positive values.
    """
    _data = data[:].copy()
    _data[_data < 0] = 0
    return _data


def get_negative_values_from(data):
    """
    Filters and retrieves only the negative values from the data.

    Args:
        data (list or similar data structure): Input data from which the negative values need to be extracted.

    Return:
        list or similar data structure: Data containing only the negative values.
    """
    _data = data[:].copy()
    _data[_data > 0] = 0
    return _data
