import pandas
from energy_system_network.analysis.data.energy_management_data.ev_home_data.ev_home_data import EVHomeData
from energy_system_network.commons.state.energy_management.ev_home.electricity_ev_home_state import \
    ElectricityEVHomeState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class ElectricityEVHomeData(EVHomeData):
    """
    Represents data specific to the electric vehicle (EV) home management for electricity within the energy system network.

    This class extends `EVHomeData` and focuses on capturing data specifically related to electricity in the context of EV home
    management. It provides methods to retrieve electricity-specific system data for EV home management.

    Attributes:
        config (GeneralSimulationConfig): Configuration data for the simulation.
        data (pandas.DataFrame): Data related to the electricity EV home management.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame): Initializes the ElectricityEVHomeData instance.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list: Retrieves electricity-specific system data related to EV home management.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes the ElectricityEVHomeData with the given configuration and data.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data related to the electricity EV home management.
        """
        super().__init__(config, data, 'Electricity')

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Retrieves electricity-specific system data related to EV home management.

        Args:
            path (str): Path to retrieve data.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): The energy system for which the data is to be retrieved.

        Returns:
            list[ElectricityEVHomeData]: List of data objects related to the electricity EV home management.
        """
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, ElectricityEVHomeState,
                                                                   ElectricityEVHomeState.TIME,
                                                                   ElectricityEVHomeState.ENERGY_SYSTEM_ID,
                                                                   ems_flag=True, energy_system_id=int(energy_system.get_id()))
        res: [ElectricityEVHomeData] = list()
        res.append(ElectricityEVHomeData(config, system_data))
        return res
