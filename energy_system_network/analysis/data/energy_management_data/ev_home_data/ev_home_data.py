import numpy
import pandas
from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.commons.state.energy_management.ev_home.ev_home_state import EVHomeState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class EVHomeData(EnergyManagementData):
    """
    Represents data specific to the Electric Vehicle (EV) home energy management within the energy system network.

    This class extends `EnergyManagementData` and is intended for capturing data associated with the management
    of energy in the context of EV home settings. It provides methods to retrieve various EV home-related data,
    such as cash flow per timestep and storage power fulfilment.

    Attributes:
        config (GeneralSimulationConfig): Configuration data for the simulation.
        data (pandas.DataFrame): Data related to the EV home energy management.
        __energy_form (str): Form of energy being managed (e.g., 'Electricity').

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame, energy_form: str): Initializes the EVHomeData instance.
        id() -> str: Returns a unique identifier for the EV home data analysis.
        energy_form() -> str: Returns the form of energy being managed.
        cash_flow_timestep() -> numpy.ndarray: Returns the cash flow for each timestep.
        storage_power_fulfilment() -> numpy.ndarray: Returns the storage power fulfilment for each timestep.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list: Method template to retrieve system-specific data related to EV home management.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame, energy_form: str):
        """
        Initializes the EVHomeData with the given configuration, data, and energy form.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data related to the EV home energy management.
            energy_form (str): Form of energy being managed (e.g., 'Electricity').
        """
        super().__init__(config, data)
        self.__energy_form = energy_form

    @property
    def id(self) -> str:
        """
        Returns a unique identifier for the EV home data analysis based on the energy system ID.

        Returns:
            str: A unique identifier string.
        """
        return 'EV Home Analysis' + ' for Energy System ' + str(int(self._get_first_value(EVHomeState.ENERGY_SYSTEM_ID)))

    @property
    def energy_form(self) -> str:
        """
        Returns the form of energy being managed.

        Returns:
            str: Energy form (e.g., 'Electricity').
        """
        return self.__energy_form

    @property
    def cash_flow_timestep(self) -> numpy.ndarray:
        """
        Returns series of cash flow in current timestep in EUR as numpy ndarray (pos: revenue, neg: expense)
        :return: series of cash flow in EUR
        """
        return self._get_data(EVHomeState.CASH_FLOW_TIMESTEP)

    @property
    def storage_power_fulfilment(self) -> numpy.ndarray:
        """
        Returns series of storage power fulfilment in each timestep as numpy ndarray
        :return: series of storage power fulfiment ratos
        """
        return self._get_data(EVHomeState.STORAGE_POWER_FULFILMENT)

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Retrieves system-specific data related to EV home management from the given path and configuration.

        Args:
            path (str): The path to the location containing the relevant data files.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): The specific energy system under consideration.

        Returns:
            list: A list containing data related to EV home energy management for the specified energy system.
        """
        pass
