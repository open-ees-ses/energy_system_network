import pandas
from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.commons.state.energy_management.multisource_peak_shaving.multisource_peak_shaving_state import \
    MultiSourcePeakShavingState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.multi_source_peak_shaving import \
    MultiSourcePeakShaving
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class MultiSourcePeakShavingData(EnergyManagementData):
    """
    Represents data specific to the multi-source peak shaving strategy within the energy system network.

    This class extends the base `EnergyManagementData` and focuses on capturing data for strategies aimed at
    managing energy demand during peak periods by leveraging multiple energy sources.

    Attributes:
        config (GeneralSimulationConfig): Configuration data for the simulation.
        data (pandas.DataFrame): Data related to the multi-source peak shaving strategy.
        __energy_form (str): The form of energy being managed (e.g., 'Electricity', 'Heat').

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame, energy_form: str): Initializes the MultiSourcePeakShavingData instance.
        id() -> str: Provides a unique identifier for the multi-source peak shaving strategy for a specific energy system.
        energy_form() -> str: Returns the form of energy being managed.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem): Abstract method to retrieve system data related to the multi-source peak shaving strategy.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame, energy_form: str):
        """
        Initializes the MultiSourcePeakShavingData with given configuration, data, and energy form.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data related to the multi-source peak shaving strategy.
            energy_form (str): The form of energy being managed (e.g., 'Electricity', 'Heat').
        """
        super().__init__(config, data)
        self.__energy_form = energy_form

    @property
    def id(self) -> str:
        """
        Provides a unique identifier for the multi-source peak shaving strategy for a specific energy system.

        Returns:
            str: A unique identifier combining the strategy's name and the associated energy system's ID.
        """
        return MultiSourcePeakShaving.__name__ + ' for Energy System ' + str(int(self._get_first_value(MultiSourcePeakShavingState.ENERGY_SYSTEM_ID)))

    @property
    def energy_form(self) -> str:
        """
        Returns the form of energy being managed by the strategy.

        Returns:
            str: Form of energy (e.g., 'Electricity', 'Heat').
        """
        return self.__energy_form

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Abstract method to retrieve system data related to the multi-source peak shaving strategy.

        Args:
            path (str): Path to retrieve data.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): The energy system for which the data is to be retrieved.

        Returns:
            list: Placeholder for the list of data objects related to the multi-source peak shaving strategy.
        """
        pass
