import pandas
from energy_system_network.analysis.data.energy_management_data.multisource_peak_shaving_data.multisource_peak_shaving_data import \
    MultiSourcePeakShavingData
from energy_system_network.commons.state.energy_management.multisource_peak_shaving.electricity_multisource_peak_shaving_state import \
    ElectricityMultiSourcePeakShavingState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class ElectricityMultiSourcePeakShavingData(MultiSourcePeakShavingData):
    """
    Represents data specific to the multi-source peak shaving strategy for electricity within the energy system network.

    This class extends `MultiSourcePeakShavingData` and focuses on capturing data specifically related to electricity. It provides
    methods to retrieve electricity-specific system data for the multi-source peak shaving strategy.

    Attributes:
        config (GeneralSimulationConfig): Configuration data for the simulation.
        data (pandas.DataFrame): Data related to the electricity multi-source peak shaving strategy.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame): Initializes the ElectricityMultiSourcePeakShavingData instance.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list: Retrieves electricity-specific system data related to the multi-source peak shaving strategy.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes the ElectricityMultiSourcePeakShavingData with given configuration and data.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data related to the electricity multi-source peak shaving strategy.
        """
        super().__init__(config, data, 'Electricity')

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Retrieves electricity-specific system data related to the multi-source peak shaving strategy.

        Args:
            path (str): Path to retrieve data.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): The energy system for which the data is to be retrieved.

        Returns:
            list[ElectricityMultiSourcePeakShavingData]: List of data objects related to the electricity multi-source peak shaving strategy.
        """
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, ElectricityMultiSourcePeakShavingState,
                                                                   ElectricityMultiSourcePeakShavingState.TIME,
                                                                   ElectricityMultiSourcePeakShavingState.ENERGY_SYSTEM_ID,
                                                                   ems_flag=True, energy_system_id=int(energy_system.get_id()))
        res: [ElectricityMultiSourcePeakShavingData] = list()
        res.append(ElectricityMultiSourcePeakShavingData(config, system_data))
        return res
