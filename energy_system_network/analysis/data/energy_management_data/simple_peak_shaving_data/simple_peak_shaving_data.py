import pandas
from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.commons.state.energy_management.simple_peak_shaving.simple_peak_shaving_state import \
    SimplePeakShavingState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_peak_shaving import \
    SimplePeakShaving
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class SimplePeakShavingData(EnergyManagementData):
    """
    Data handler for Simple Peak Shaving operations.

    Attributes:
        config (GeneralSimulationConfig): Configuration for the simulation.
        data (pandas.DataFrame): Data frame holding the energy management data.
        __energy_form (str): Form of energy being managed, e.g., 'Electricity'.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame, energy_form: str): Initializes the object.
        id() -> str: Returns the identifier for the Simple Peak Shaving operation for a given energy system.
        energy_form() -> str: Returns the form of energy being managed.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list: Placeholder method for retrieving system-specific data.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame, energy_form: str):
        """
        Initializes the SimplePeakShavingData object.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data frame holding the energy management data.
            energy_form (str): Form of energy being managed, e.g., 'Electricity'.
        """
        super().__init__(config, data)
        self.__energy_form = energy_form

    @property
    def id(self) -> str:
        """
        Returns the identifier for the Simple Peak Shaving operation for a given energy system.

        Return:
            str: Identifier string.
        """
        return SimplePeakShaving.__name__ + ' for Energy System ' + str(int(self._get_first_value(SimplePeakShavingState.ENERGY_SYSTEM_ID)))

    @property
    def energy_form(self) -> str:
        """
        Returns the form of energy being managed.

        Return:
            str: Form of energy, e.g., 'Electricity'.
        """
        return self.__energy_form

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Placeholder method for retrieving system-specific data related to Simple Peak Shaving.

        Args:
            path (str): Path to the location containing relevant data files.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): Energy system under consideration.

        Return:
            list: Placeholder return indicating a list of system-specific data.
        """
        pass
