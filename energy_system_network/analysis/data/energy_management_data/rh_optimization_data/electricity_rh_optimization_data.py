import pandas
from energy_system_network.analysis.data.energy_management_data.rh_optimization_data.rh_optimization_data import \
    RHOptimizationData
from energy_system_network.commons.state.energy_management.rh_optimization.electricity_rh_optimization_state import \
    ElectricityRHOptimizationState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class ElectricityRHOptimizationData(RHOptimizationData):
    """
    Data handler for RH (Residential Heating) Optimization specific to electricity.

    Attributes:
        config (GeneralSimulationConfig): Configuration for the simulation.
        data (pandas.DataFrame): Data frame holding the energy management data.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame): Initializes the object.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list: Retrieves electricity-specific data related to RH optimization.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes the ElectricityRHOptimizationData object.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data frame holding the energy management data.
        """
        super().__init__(config, data, 'Electricity')

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Retrieves electricity-specific data related to RH optimization.

        Args:
            path (str): Path to the location containing relevant data files.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): Energy system under consideration.

        Return:
            list: List of electricity-specific data related to RH optimization for the specified energy system.
        """
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, ElectricityRHOptimizationState,
                                                                   ElectricityRHOptimizationState.TIME,
                                                                   ElectricityRHOptimizationState.ENERGY_SYSTEM_ID,
                                                                   ems_flag=True, energy_system_id=int(energy_system.get_id()))
        res: [ElectricityRHOptimizationData] = list()
        res.append(ElectricityRHOptimizationData(config, system_data))
        return res
