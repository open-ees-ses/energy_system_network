import numpy
import pandas
from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.commons.state.energy_management.rh_optimization.rh_optimization_state import \
    RHOptimizationState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_management.operation_strategy.optimizer_based.rh_optimization import \
    RHOptimization
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class RHOptimizationData(EnergyManagementData):
    """
    Data handler for RH (Residential Heating) Optimization.

    This class manages data related to the RH optimization strategy within a specific energy system.
    It extends the base EnergyManagementData to provide properties and methods tailored to RH optimization.

    Attributes:
        config (GeneralSimulationConfig): Configuration for the simulation.
        data (pandas.DataFrame): Data frame holding the energy management data.
        __energy_form (str): Form of energy being managed (e.g., electricity, heat).

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame, energy_form: str): Initializes the object.
        id() -> str: Generates an identifier for this instance of RH optimization data.
        energy_form() -> str: Returns the form of energy being managed.
        cash_flow_timestep() -> numpy.ndarray: Retrieves series of cash flow for each timestep.
        storage_power_fulfilment() -> numpy.ndarray: Retrieves series of storage power fulfilment for each timestep.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list: Retrieves system-specific data related to RH optimization.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame, energy_form: str):
        """
        Initialize the RHOptimizationData object.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data frame holding the energy management data.
            energy_form (str): The form of energy being managed.
        """
        super().__init__(config, data)
        self.__energy_form = energy_form

    @property
    def id(self) -> str:
        """
        Generate an identifier for this instance of RH optimization data.

        Returns:
            str: Identifier combining the RH optimization name and associated energy system ID.
        """
        return RHOptimization.__name__ + ' for Energy System ' + str(int(self._get_first_value(RHOptimizationState.ENERGY_SYSTEM_ID)))

    @property
    def energy_form(self) -> str:
        """
        Get the form of energy being managed.

        Returns:
            str: Energy form (e.g., electricity, heat).
        """
        return self.__energy_form

    @property
    def cash_flow_timestep(self) -> numpy.ndarray:
        """
        Returns series of cash flow in current timestep in EUR as numpy ndarray (pos: revenue, neg: expense)
        :return: series of cash flow in EUR
        """
        return self._get_data(RHOptimizationState.CASH_FLOW_TIMESTEP)

    @property
    def storage_power_fulfilment(self) -> numpy.ndarray:
        """
        Returns series of storage power fulfilment in each timestep as numpy ndarray
        :return: series of storage power fulfiment ratos
        """
        return self._get_data(RHOptimizationState.STORAGE_POWER_FULFILMENT)

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Retrieve system-specific data related to RH optimization.

        Args:
            path (str): Path to the location containing relevant data files.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): Energy system under consideration.

        Returns:
            list: List of data related to RH optimization for the specified energy system.
        """
        pass
