import pandas
from energy_system_network.analysis.data.energy_management_data.simple_deficit_coverage_data.simple_deficit_coverage_data import \
    SimpleDeficitCoverageData
from energy_system_network.commons.state.energy_management.simple_deficit_coverage.electricity_simple_deficit_coverage_state import \
    ElectricitySimpleDeficitCoverageState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class ElectricitySimpleDeficitCoverageData(SimpleDeficitCoverageData):
    """
    Represents data specific to simple deficit coverage strategies for electricity within the energy system network.

    This class extends the base `SimpleDeficitCoverageData` to provide functionality specific to electricity.

    Attributes:
        config (GeneralSimulationConfig): Configuration data for the simulation.
        data (pandas.DataFrame): Data related to simple deficit coverage for electricity.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame): Initializes the ElectricitySimpleDeficitCoverageData instance.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem): Method to retrieve electricity-specific system data for simple deficit coverage.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes the ElectricitySimpleDeficitCoverageData with given configuration and data.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data related to the simple deficit coverage for electricity.
        """
        super().__init__(config, data, 'Electricity')

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Retrieve electricity-specific system data for simple deficit coverage.

        Args:
            path (str): Path to retrieve data.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): The energy system for which the data is to be retrieved.

        Returns:
            list[ElectricitySimpleDeficitCoverageData]: List containing the data specific to simple deficit coverage for electricity.
        """
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, ElectricitySimpleDeficitCoverageState,
                                                                   ElectricitySimpleDeficitCoverageState.TIME,
                                                                   ElectricitySimpleDeficitCoverageState.ENERGY_SYSTEM_ID,
                                                                   ems_flag=True, energy_system_id=int(energy_system.get_id()))
        res: [ElectricitySimpleDeficitCoverageData] = list()
        res.append(ElectricitySimpleDeficitCoverageData(config, system_data))
        return res
