import pandas
from energy_system_network.analysis.data.energy_management_data.simple_deficit_coverage_data.simple_deficit_coverage_data import \
    SimpleDeficitCoverageData
from energy_system_network.commons.state.energy_management.simple_deficit_coverage.heat_simple_deficit_coverage_state import \
    HeatSimpleDeficitCoverageState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class HeatSimpleDeficitCoverageData(SimpleDeficitCoverageData):
    """
    Represents data specific to heat simple deficit coverage within the energy system network.

    Attributes:
        config (GeneralSimulationConfig): Configuration data for the simulation.
        data (pandas.DataFrame): Data pertaining to heat simple deficit coverage.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame): Initializes the HeatSimpleDeficitCoverageData instance.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem): Retrieves system data for heat simple deficit coverage.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes the HeatSimpleDeficitCoverageData with given configuration and data.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data pertaining to the heat simple deficit coverage.
        """
        super().__init__(config, data, 'Heat')

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Retrieves system data for heat simple deficit coverage.

        Args:
            path (str): Path to retrieve data.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): The energy system for which the data is to be retrieved.

        Returns:
            list[HeatSimpleDeficitCoverageData]: A list containing the retrieved heat simple deficit coverage data.
        """
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, HeatSimpleDeficitCoverageState,
                                                                   HeatSimpleDeficitCoverageState.TIME,
                                                                   HeatSimpleDeficitCoverageState.ENERGY_SYSTEM_ID,
                                                                   ems_flag=True, energy_system_id=int(energy_system.get_id()))
        res: [HeatSimpleDeficitCoverageData] = list()
        res.append(HeatSimpleDeficitCoverageData(config, system_data))
        return res
