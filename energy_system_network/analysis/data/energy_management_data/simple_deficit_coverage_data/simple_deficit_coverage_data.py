import pandas
from energy_system_network.analysis.data.energy_management_data.energy_management_data import EnergyManagementData
from energy_system_network.commons.state.energy_management.simple_deficit_coverage.simple_deficit_coverage_state import \
    SimpleDeficitCoverageState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_management.operation_strategy.rule_based.simple_deficit_coverage import \
    SimpleDeficitCoverage
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class SimpleDeficitCoverageData(EnergyManagementData):
    """
    Represents data specific to simple deficit coverage strategies within the energy system network.

    Attributes:
        config (GeneralSimulationConfig): Configuration data for the simulation.
        data (pandas.DataFrame): Data related to simple deficit coverage.
        __energy_form (str): The form of energy for which this deficit coverage applies.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame, energy_form: str): Initializes the SimpleDeficitCoverageData instance.
        id(): Property that returns the unique identifier for the simple deficit coverage.
        energy_form(): Property that returns the form of energy for the deficit coverage.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem): Placeholder method to retrieve system data for simple deficit coverage.

    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame, energy_form: str):
        """
        Initializes the SimpleDeficitCoverageData with given configuration, data, and energy form.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data related to the simple deficit coverage.
            energy_form (str): The form of energy for which this deficit coverage applies.
        """
        super().__init__(config, data)
        self.__energy_form = energy_form

    @property
    def id(self) -> str:
        """
        Returns the unique identifier for the simple deficit coverage, constructed using its name and the energy system's ID.

        Returns:
            str: The unique identifier for the simple deficit coverage.
        """
        return SimpleDeficitCoverage.__name__ + ' for Energy System ' + str(int(self._get_first_value(SimpleDeficitCoverageState.ENERGY_SYSTEM_ID)))

    @property
    def energy_form(self) -> str:
        """
        Returns the form of energy for which this deficit coverage applies.

        Returns:
            str: The energy form.
        """
        return self.__energy_form

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Placeholder method to retrieve system data for simple deficit coverage.

        Args:
            path (str): Path to retrieve data.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): The energy system for which the data is to be retrieved.

        Returns:
            list: A placeholder return for the method.
        """
        pass
