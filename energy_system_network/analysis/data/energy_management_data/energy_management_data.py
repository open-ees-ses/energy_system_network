import numpy
import numpy as np
from energy_system_network.analysis.data.data import Data
from energy_system_network.commons import unit_converter
from energy_system_network.commons.state.energy_management.energy_management_state import EnergyManagementState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
import pandas


class EnergyManagementData(Data):
    """
    Data handler for managing various energy-related metrics.

    Attributes:
        config (GeneralSimulationConfig): Configuration data for the simulation.
        data (pandas.DataFrame): Data frame holding the energy management data.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame): Initializes the object.
        time() -> numpy.ndarray: Returns a series of epoch timestamps.
        must_fulfil_load() -> numpy.ndarray: Returns a series of load power in W.
        grid_power() -> numpy.ndarray: Returns a series of grid power in W.
        storage_power() -> numpy.ndarray: Returns a series of storage power in W.
        storage_reference_power() -> numpy.ndarray: Returns a series of storage reference power in W.
        residual_load_final() -> numpy.ndarray: Returns a series of residual load power in W.
        must_run_generation_power() -> numpy.ndarray: Returns a series of must-run generation power in W.
        can_run_generation_power() -> numpy.ndarray: Returns a series of can-run generation power in W.
        charging_energy() -> np.ndarray: Returns a series of charging energy in kWh.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes the EnergyManagementData object.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data frame holding the energy management data.
        """
        super().__init__(config, data)

    @property
    def time(self) -> numpy.ndarray:
        """
        returns series of epoch timestamps as numpy ndarray
        :return: returns series of epoch timestamps
        """
        return self._get_data(EnergyManagementState.TIME)

    @property
    def must_fulfil_load(self) -> numpy.ndarray:
        """
        Returns series of load power in W as numpy ndarray (pos)
        :return: series of load power in W
        """
        return self._get_data(EnergyManagementState.MUST_FULFIL_LOAD)

    @property
    def grid_power(self) -> numpy.ndarray:
        """
        Returns series of grid power in W as numpy ndarray (pos: drawn/import, neg: feed-in/export)
        :return: series of grid power in W
        """
        return self._get_data(EnergyManagementState.GRID_POWER)

    @property
    def storage_power(self) -> numpy.ndarray:
        """
        Returns series of storage power in W as numpy ndarray (pos: dch, neg: ch)
        :return: series of storage power in W
        """
        return self._get_data(EnergyManagementState.STORAGE_POWER)

    @property
    def storage_reference_power(self) -> numpy.ndarray:
        """
        Returns series of storage reference power in W as numpy ndarray (pos: dch, neg: ch)
        :return: series of storage reference power in W
        """
        return self._get_data(EnergyManagementState.STORAGE_REFERENCE_POWER)

    @property
    def residual_load_final(self) -> numpy.ndarray:
        """
        Returns series of residual load power in W as numpy ndarray (pos: surplus, neg: deficit)
        :return: series of residual load power in W
        """
        return self._get_data(EnergyManagementState.RESIDUAL_LOAD_FINAL)

    @property
    def must_run_generation_power(self) -> numpy.ndarray:
        """
        Returns series of must-run generation power in W as numpy ndarray (pos)
        :return: series of must-run generation load power in W
        """
        return self._get_data(EnergyManagementState.MUST_RUN_GENERATION_POWER)

    @property
    def can_run_generation_power(self) -> numpy.ndarray:
        """
        Returns series of can-run generation power in W as numpy ndarray (pos)
        :return: series of can-run generation load power in W
        """
        return self._get_data(EnergyManagementState.CAN_RUN_GENERATION_POWER)

    @property
    def charging_energy(self) -> np.ndarray:
        """
        Returns series of charging energy in kWh as numpy ndarray (pos)
        :return: charging_energy as numpy ndarray
        """
        ch_power = unit_converter.W_to_kW(self.get_negative_values(self.storage_power))  # in kW
        charging_energy = ch_power * unit_converter.s_to_h(self.config.timestep)  # kWh
        return charging_energy

