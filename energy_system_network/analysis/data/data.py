from abc import ABC, abstractmethod
import numpy
import numpy as np
import pandas
from energy_system_network.commons.data.data_handler import DataHandler
from energy_system_network.commons.log import Logger
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class Data(ABC):
    """
    Abstract class representing Data with its methods and attributes.

    Attributes:
        __log (Logger): A logging instance.
        __data (pandas.DataFrame): A DataFrame containing the data.
        __config (GeneralSimulationConfig): Configuration data.

    Methods:
        __init__(config, data): Initializes the Data instance.
        _get_data(key): Returns data series for a given key as a numpy array.
        _get_first_value(key): Returns the first value for a data series of the given key.
        _get_last_value(key): Returns the last value for a data series of the given key.
        _get_difference(key): Calculates the difference between the last and first values of the data series for the given key.
        get_positive_values(array): Replaces non-positive values in the input array with zeros.
        get_negative_values(array): Replaces non-negative values in the input array with zeros.
        id(): Abstract property that should return the data id as a string.
        time(): Abstract property that should return the time series.
        get_system_data(path, config, energy_system): Abstract method to extract unique system data from storage data files.
        _get_system_data_for(path, state_cls, time_key, ...): Extracts unique systems from storage data files.
        config(): Returns the configuration data.
        close(): Closes the logger instance.
    """
    __log: Logger = Logger(__name__)

    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes the Data object.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data frame holding the energy management data.
        """
        self.__data: pandas.DataFrame = data
        self.__config = config

    def _get_data(self, key: str) -> numpy.ndarray:
        """
        Returns the data series for a given key as a numpy array.

        Args:
            key (str): The key for which the data is to be fetched.

        Returns:
            numpy.ndarray: Array containing the data corresponding to the given key.
        """
        return self.__data[key].to_numpy()

    def _get_first_value(self, key: str) -> float:
        """
        Returns the first value for the data series of a given key.

        Args:
            key (str): The key for which the first data value is to be fetched.

        Returns:
            float: The first data value corresponding to the given key.
        """
        return float(self._get_data(key)[0])

    def _get_last_value(self, key: str) -> float:
        """
        Returns the last value for the data series of a given key.

        Args:
            key (str): The key for which the last data value is to be fetched.

        Returns:
            float: The last data value corresponding to the given key.
        """
        return float(self._get_data(key)[-1])

    def _get_difference(self, key: str) -> float:
        """
        Calculates the difference between the last and first values of the data series for a given key.

        Args:
            key (str): The key for which the difference is to be calculated.

        Returns:
            float: The difference between the last and first values.
        """
        return self._get_last_value(key) - self._get_first_value(key)

    def get_positive_values(self, array: np.ndarray) -> np.ndarray:
        """
        Replace non-positive values in an input NumPy ndarray with zeros, while maintaining the original array's shape
        :param array: input NumPy ndarray
        :return: ndarray of the same shape as the input array, with non-positive values replaced by zeros
        """
        positive_array = np.copy(array)
        positive_array[array <= 0] = 0
        return positive_array

    def get_negative_values(self, array: np.ndarray) -> np.ndarray:
        """
        Replace non-negative values in an input NumPy ndarray with zeros, while maintaining the original array's shape
        :param array: input NumPy ndarray
        :return: ndarray of the same shape as the input array, with non-negative values replaced by zeros
        """
        negative_array = np.copy(array)
        negative_array[array >= 0] = 0
        return negative_array

    @property
    @abstractmethod
    def id(self) -> str:
        """

        Returns
        -------
        str:
            Data id as string
        """
        pass

    @property
    @abstractmethod
    def time(self):
        """
        Time series in s

        Returns
        -------

        """
        pass

    @classmethod
    @abstractmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Extracts unique systems data from storage data files in the given path.

        Args:
            path (str): Path to the folder containing data.
            config (GeneralSimulationConfig): Simulation configuration data.
            energy_system (EnergySystem): Instance of the energy system being simulated.

        Return:
            list: List of data relevant to the unique systems.
        """

        pass

    @classmethod
    def _get_system_data_for(cls, path, state_cls, time_key: str, system_key: str = None, component_key: str = None,
                             ems_flag: bool = False, energy_system_id: int = None, number_of_components: int = None) -> [pandas.DataFrame]:
        """
        Extracts unique systems from storage data files.

        Args:
            path (str): Path to the folder containing data.
            state_cls: Storage data state class.
            time_key (str): Key for time of storage data state class.
            system_key (str, optional): Key for system id of storage data state class.
            component_key (str, optional): Key for storage id of storage data state class.
            ems_flag (bool): Flag indicating whether the EMS system is active.
            energy_system_id (int): ID of the energy system.
            number_of_components (int, optional): The number of components in the system.

        Return:
            list[pandas.DataFrame]: List of data for the unique systems.
        """
        # data_import: DataImport = DataImport(path, state_cls)
        # data: pandas.DataFrame = data_import.return_data()
        try:
            data: pandas.DataFrame = DataHandler.get_data_from(path, state_cls)
            # return data without id selection

            if ems_flag is False:
                if system_key is None or component_key is None:
                    # get unique components
                    if system_key is None and component_key:
                        systems: pandas.DataFrame = data[[component_key]].copy()
                        systems.drop_duplicates(inplace=True)
                        if isinstance(number_of_components, int):
                            systems = systems[0:number_of_components]
                            res: [pandas.DataFrame] = list()
                            for row in range(len(systems.index)):
                                system = systems.iloc[row]
                                component_id = system[component_key]
                                system_data: pandas.DataFrame = data.loc[(data[component_key] == component_id)].copy()
                                system_data.sort_values(by=[component_key, time_key], inplace=True)
                                if system_data.isna().values.any():
                                    cls.__log.error('Data with component id '
                                                    + str(int(component_id)) + ' has NaN values! This data will be neglected for analysis!')
                                else:
                                    res.append(system_data)
                            return res
                    else:
                        data.sort_values(by=[time_key], inplace=True)
                        return [data]
                elif system_key and component_key:
                    systems: pandas.DataFrame = data[[component_key]].copy()
                    systems.drop_duplicates(inplace=True)
                    if isinstance(number_of_components, int):
                        systems = systems[0:number_of_components]
                    # get data for unique systems
                    res: [pandas.DataFrame] = list()
                    for row in range(len(systems.index)):
                        system = systems.iloc[row]
                        system_id = energy_system_id
                        component_id = system[component_key]
                        system_data: pandas.DataFrame = data.loc[
                            (data[system_key] == system_id) & (data[component_key] == component_id)].copy()
                        system_data.sort_values(by=[system_key, component_key, time_key], inplace=True)
                        if system_data.isna().values.any():
                            cls.__log.error('Data with energy system id ' + str(int(system_id)) + ' and component id '
                                            + str(
                                int(component_id)) + ' has NaN values! This data will be neglected for analysis!')
                        else:
                            res.append(system_data)
                    return res
            else:
                system_data: pandas.DataFrame = data.loc[(data[system_key] == energy_system_id)].copy()
                system_data.sort_values(by=[system_key, time_key], inplace=True)
                if system_data.isna().values.any():
                    cls.__log.error('Data with energy system id ' + str(
                        int(energy_system_id)) + ' has NaN values! This data will be neglected for analysis!')
                else:
                    return system_data

        except FileNotFoundError:
            cls.__log.warn('File could not be found for ' + path + state_cls.__name__)
            return list()

    @property
    def config(self) -> GeneralSimulationConfig:
        """
        Returns the configuration data for the simulation.

        Returns:
            GeneralSimulationConfig: Configuration data for the simulation.
        """
        return self.__config

    @classmethod
    def close(cls):
        """
        Closes the logger associated with the class.
        """
        cls.__log.close()
