import numpy
import pandas
from energy_system_network.analysis.data.data import Data
from energy_system_network.commons.state.energy_system_component.load_component.load_component_state import LoadComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig


class LoadComponentData(Data):
    """
    Represents data specific to load components.

    Attributes:
        config (GeneralSimulationConfig): Configuration for the general simulation.
        data (pandas.DataFrame): DataFrame containing data related to the load component.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame):
            Initializes an instance of the LoadComponentData class.
        id() -> str: Retrieves the combined ID of the energy system and load component.
        time() -> numpy.ndarray: Retrieves the time series data.
        load_power() -> numpy.ndarray: Retrieves the load power series data.
        power_loss() -> numpy.ndarray: Retrieves the power loss series data.
        discharge_energy_consumption_emissions() -> numpy.ndarray: Retrieves the discharge energy consumption emissions series data.
        grid_energy_consumption_emissions() -> numpy.ndarray: Retrieves the grid energy consumption emissions series data.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes an instance of the LoadComponentData class.

        Args:
            config (GeneralSimulationConfig): Configuration for the general simulation.
            data (pandas.DataFrame): DataFrame containing data related to the load component.

        Return:
            None.
        """
        super().__init__(config, data)

    @property
    def id(self) -> str:
        """
        Retrieves the combined ID of the energy system and load component.

        Return:
            str: Combined ID.
        """
        return str(int(self._get_first_value(LoadComponentState.ENERGY_SYSTEM_ID))) + '.' + \
               str(int(self._get_first_value(LoadComponentState.LOAD_COMPONENT_ID)))

    @property
    def time(self) -> numpy.ndarray:
        """
        Retrieves the time series data.

        Return:
            numpy.ndarray: Time series data.
        """
        return self._get_data(LoadComponentState.TIME)

    @property
    def load_power(self) -> numpy.ndarray:
        """
        Retrieves the load power series data.

        Return:
            numpy.ndarray: Load power series data.
        """
        return self._get_data(LoadComponentState.POWER)

    @property
    def power_loss(self) -> numpy.ndarray:
        """
        Retrieves the power loss series data.

        Return:
            numpy.ndarray: Power loss series data.
        """
        return self._get_data(LoadComponentState.POWER_LOSS)

    @property
    def discharge_energy_consumption_emissions(self) -> numpy.ndarray:
        """
        Retrieves the discharge energy consumption emissions series data.

        Return:
            numpy.ndarray: Discharge energy consumption emissions series data.
        """
        return self._get_data(LoadComponentState.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS)

    @property
    def grid_energy_consumption_emissions(self) -> numpy.ndarray:
        """
        Retrieves the grid energy consumption emissions series data.

        Return:
            numpy.ndarray: Grid energy consumption emissions series data.
        """
        return self._get_data(LoadComponentState.GRID_ENERGY_CONSUMPTION_EMISSIONS)
