import pandas
from energy_system_network.analysis.data.energy_system_component_data.load_component_data.load_component_data import LoadComponentData
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_system_component.load_component.heat_load_component_state import HeatLoadComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent


class HeatLoadComponentData(LoadComponentData):
    """
    Represents data specific to heat load components.

    Attributes:
        config (GeneralSimulationConfig): Configuration for the general simulation.
        data (pandas.DataFrame): DataFrame containing data related to the heat load component.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame):
            Initializes an instance of the HeatLoadComponentData class.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
            Collects and returns a list of HeatLoadComponentData for each heat load component in the energy system.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes an instance of the HeatLoadComponentData class.

        Args:
            config (GeneralSimulationConfig): Configuration for the general simulation.
            data (pandas.DataFrame): DataFrame containing data related to the heat load component.

        Return:
            None.
        """
        super().__init__(config, data)

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Collects and returns a list of HeatLoadComponentData for each heat load component in the energy system.

        Args:
            path (str): The path to retrieve data.
            config (GeneralSimulationConfig): Configuration for the general simulation.
            energy_system (EnergySystem): The energy system object.

        Return:
            list[HeatLoadComponentData]: List of HeatLoadComponentData objects.
        """
        components: [LoadComponent] = energy_system.get_load_components()
        number_components = 0
        for component in components:
            if EnergyForm.HEAT in component.energy_form:
                number_components += 1
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, HeatLoadComponentState,
                                                                   HeatLoadComponentState.TIME,
                                                                   HeatLoadComponentState.ENERGY_SYSTEM_ID,
                                                                   HeatLoadComponentState.LOAD_COMPONENT_ID,
                                                                   energy_system_id=int(energy_system.get_id()),
                                                                   number_of_components=number_components)
        res: [HeatLoadComponentData] = list()
        for data in system_data:
            res.append(HeatLoadComponentData(config, data))
        return res
