import pandas
from energy_system_network.analysis.data.energy_system_component_data.load_component_data.load_component_data import \
    LoadComponentData
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_system_component.load_component.electricity_load_component_state import \
    ElectricityLoadComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.load_components.load_component import LoadComponent


class ElectricityLoadComponentData(LoadComponentData):
    """
    Represents data specific to electricity load components.

    Attributes:
        config (GeneralSimulationConfig): Configuration for the general simulation.
        data (pandas.DataFrame): DataFrame containing data related to the electricity load component.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame):
            Initializes an instance of the ElectricityLoadComponentData class.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
            Collects and returns a list of ElectricityLoadComponentData for each electricity load component in the energy system.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes an instance of the ElectricityLoadComponentData class.

        Args:
            config (GeneralSimulationConfig): Configuration for the general simulation.
            data (pandas.DataFrame): DataFrame containing data related to the electricity load component.

        Return:
            None.
        """
        super().__init__(config, data)

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Collects and returns a list of ElectricityLoadComponentData for each electricity load component in the energy system.

        Args:
            path (str): The path to retrieve data.
            config (GeneralSimulationConfig): Configuration for the general simulation.
            energy_system (EnergySystem): The energy system object.

        Return:
            list[ElectricityLoadComponentData]: List of ElectricityLoadComponentData objects.
        """
        components: [LoadComponent] = energy_system.get_load_components()
        number_components = 0
        for component in components:
            if EnergyForm.ELECTRICITY in component.energy_form:
                number_components += 1
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, ElectricityLoadComponentState,
                                                                   ElectricityLoadComponentState.TIME,
                                                                   ElectricityLoadComponentState.ENERGY_SYSTEM_ID,
                                                                   ElectricityLoadComponentState.LOAD_COMPONENT_ID,
                                                                   energy_system_id=int(energy_system.get_id()),
                                                                   number_of_components=number_components)
        res: [ElectricityLoadComponentData] = list()
        for data in system_data:
            res.append(ElectricityLoadComponentData(config, data))
        return res
