import pandas
from energy_system_network.analysis.data.energy_system_component_data.generation_component_data.generation_component_data import \
    GenerationComponentData
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_system_component.generation_component.heat_generation_component_state import \
    HeatGenerationComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.generation_components.generation_component import \
    GenerationComponent


class HeatGenerationComponentData(GenerationComponentData):
    """
    Represents data specific to the heat generation components within an energy system network.

    Attributes:
        config (GeneralSimulationConfig): Configuration data for the simulation.
        data (pandas.DataFrame): Data pertaining to the heat generation component.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame): Initializes the HeatGenerationComponentData instance.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem): Retrieves system-wide data related to all heat generation components.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes the HeatGenerationComponentData with given configuration and data.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data pertaining to the heat generation component.
        """
        super().__init__(config, data)

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Retrieves system-wide data for all heat generation components within the given energy system.

        Args:
            path (str): Path to retrieve or store data.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): The energy system for which data is to be retrieved.

        Return:
            list[HeatGenerationComponentData]: A list of HeatGenerationComponentData instances representing data for each heat generation component in the system.
        """
        components: [GenerationComponent] = energy_system.get_generation_components()
        number_components = 0
        for component in components:
            if EnergyForm.HEAT in component.energy_form:
                number_components += 1
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, HeatGenerationComponentState,
                                                                   HeatGenerationComponentState.TIME,
                                                                   HeatGenerationComponentState.ENERGY_SYSTEM_ID,
                                                                   HeatGenerationComponentState.GENERATION_COMPONENT_ID,
                                                                   energy_system_id=int(energy_system.get_id()),
                                                                   number_of_components=number_components)
        res: [HeatGenerationComponentData] = list()
        for data in system_data:
            res.append(HeatGenerationComponentData(config, data))
        return res
