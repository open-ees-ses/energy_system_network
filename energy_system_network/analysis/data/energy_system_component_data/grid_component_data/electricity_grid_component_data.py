import pandas
from energy_system_network.analysis.data.energy_system_component_data.grid_component_data.grid_component_data import \
    GridComponentData
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_system_component.grid_component.electricity_grid_component_state import \
    ElectricityGridComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.grid_components.grid_component import GridComponent


class ElectricityGridComponentData(GridComponentData):
    """
    Represents data specific to electricity grid components.

    Attributes:
        config (GeneralSimulationConfig): Configuration for the general simulation.
        data (pandas.DataFrame): DataFrame containing data related to the electricity grid component.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame):
            Initializes an instance of the ElectricityGridComponentData class.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
            Retrieves system data for electricity grid components.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes an instance of the ElectricityGridComponentData class.

        Args:
            config (GeneralSimulationConfig): Configuration for the general simulation.
            data (pandas.DataFrame): DataFrame containing data related to the electricity grid component.

        Return:
            None.
        """
        super().__init__(config, data)

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Retrieves system data for electricity grid components and constructs instances of
        ElectricityGridComponentData for each.

        Args:
            path (str): Path to where the data files are stored.
            config (GeneralSimulationConfig): Configuration for the general simulation.
            energy_system (EnergySystem): The energy system instance from which grid components are derived.

        Return:
            list: A list of ElectricityGridComponentData instances.
        """
        components: [GridComponent] = energy_system.get_grid_components()
        number_components = 0
        for component in components:
            if EnergyForm.ELECTRICITY in component.energy_form:
                number_components += 1
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, ElectricityGridComponentState,
                                                                   ElectricityGridComponentState.TIME,
                                                                   ElectricityGridComponentState.ENERGY_SYSTEM_ID,
                                                                   ElectricityGridComponentState.GRID_COMPONENT_ID,
                                                                   energy_system_id=int(energy_system.get_id()),
                                                                   number_of_components=number_components)
        res: [ElectricityGridComponentData] = list()
        for data in system_data:
            res.append(ElectricityGridComponentData(config, data))
        return res
