import numpy
import pandas
from energy_system_network.analysis.data.data import Data
from energy_system_network.commons.state.energy_system_component.grid_component.grid_component_state import \
    GridComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig


class GridComponentData(Data):
    """
    Represents data specific to grid components.

    Attributes:
        config (GeneralSimulationConfig): Configuration for the general simulation.
        data (pandas.DataFrame): DataFrame containing data related to the grid component.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame):
            Initializes an instance of the GridComponentData class.
        id() -> str: Retrieves the combined ID of the energy system and grid component.
        time() -> numpy.ndarray: Retrieves the time series data.
        grid_power() -> numpy.ndarray: Retrieves the grid power series data.
        power_loss() -> numpy.ndarray: Retrieves the power loss series data.
        operation_emissions() -> numpy.ndarray: Retrieves the operation emissions series data.
        generation_emissions() -> numpy.ndarray: Retrieves the generation emissions series data.
        carbon_intensity() -> numpy.ndarray: Retrieves the carbon intensity series data.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes an instance of the GridComponentData class.

        Args:
            config (GeneralSimulationConfig): Configuration for the general simulation.
            data (pandas.DataFrame): DataFrame containing data related to the grid component.

        Return:
            None.
        """
        super().__init__(config, data)

    @property
    def id(self) -> str:
        """
        Retrieves the combined ID of the energy system and grid component.

        Return:
            str: Combined ID.
        """
        return str(int(self._get_first_value(GridComponentState.ENERGY_SYSTEM_ID))) + '.' + \
               str(int(self._get_first_value(GridComponentState.GRID_COMPONENT_ID)))

    @property
    def time(self) -> numpy.ndarray:
        """
        Retrieves the time series data.

        Return:
            numpy.ndarray: Time series data.
        """
        return self._get_data(GridComponentState.TIME)

    @property
    def grid_power(self) -> numpy.ndarray:
        """
        Retrieves the grid power series data.

        Return:
            numpy.ndarray: Grid power series data.
        """
        return self._get_data(GridComponentState.POWER)

    @property
    def power_loss(self) -> numpy.ndarray:
        """
        Retrieves the power loss series data.

        Return:
            numpy.ndarray: Power loss series data.
        """
        return self._get_data(GridComponentState.POWER_LOSS)

    @property
    def operation_emissions(self) -> numpy.ndarray:
        """
        Retrieves the operation emissions series data.

        Return:
            numpy.ndarray: Operation emissions series data.
        """
        return self._get_data(GridComponentState.OPERATION_EMISSIONS)

    @property
    def generation_emissions(self) -> numpy.ndarray:
        """
        Retrieves the generation emissions series data.

        Return:
            numpy.ndarray: Generation emissions series data.
        """
        return self._get_data(GridComponentState.GENERATION_EMISSIONS)

    @property
    def carbon_intensity(self) -> numpy.ndarray:
        """
        Retrieves the carbon intensity series data.

        Return:
            numpy.ndarray: Carbon intensity series data.
        """
        return self._get_data(GridComponentState.CARBON_INTENSITY)
