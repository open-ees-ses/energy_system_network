import pandas
from energy_system_network.analysis.data.energy_system_component_data.storage_component_data.storage_component_data import StorageComponentData
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_system_component.storage_component.heat_storage_component_state import \
    HeatStorageComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent


class HeatStorageComponentData(StorageComponentData):
    """
    Represents data specific to the heat storage components within an energy system network.

    Attributes:
        config (GeneralSimulationConfig): Configuration data for the simulation.
        data (pandas.DataFrame): Data pertaining to the heat storage component.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame): Initializes the HeatStorageComponentData instance.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem): Retrieves system-wide data related to all heat storage components.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes the HeatStorageComponentData with given configuration and data.

        Args:
            config (GeneralSimulationConfig): Configuration data for the simulation.
            data (pandas.DataFrame): Data pertaining to the heat storage component.
        """
        super().__init__(config, data)

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Retrieves system-wide data for all heat storage components within the given energy system.

        Args:
            path (str): Path to retrieve or store data.
            config (GeneralSimulationConfig): Configuration data for the simulation.
            energy_system (EnergySystem): The energy system for which data is to be retrieved.

        Return:
            list[HeatStorageComponentData]: A list of HeatStorageComponentData instances representing data for each heat storage component in the system.
        """
        components: [StorageComponent] = energy_system.get_storage_components()
        number_components = 0
        for component in components:
            if EnergyForm.HEAT in component.energy_form:
                number_components += 1
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, HeatStorageComponentState,
                                                                   HeatStorageComponentState.TIME,
                                                                   HeatStorageComponentState.ENERGY_SYSTEM_ID,
                                                                   HeatStorageComponentState.STORAGE_COMPONENT_ID,
                                                                   energy_system_id=int(energy_system.get_id()),
                                                                   number_of_components=number_components)
        res: [HeatStorageComponentData] = list()
        for data in system_data:
            res.append(HeatStorageComponentData(config, data))
        return res
