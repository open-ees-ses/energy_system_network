import pandas
from energy_system_network.analysis.data.energy_system_component_data.storage_component_data.storage_component_data import StorageComponentData
from energy_system_network.commons.energy_form import EnergyForm
from energy_system_network.commons.state.energy_system_component.storage_component.electricity_storage_component_state import \
    ElectricityStorageComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
from energy_system_network.simulation.energy_system.energy_system import EnergySystem
from energy_system_network.simulation.energy_system.storage_components.storage_component import StorageComponent


class ElectricityStorageComponentData(StorageComponentData):
    """
    Represents data specific to an electricity storage component, detailing its various state attributes.

    Attributes:
        config (GeneralSimulationConfig): Configuration parameters for the general simulation.
        data (pandas.DataFrame): DataFrame containing electricity storage component related data.

    Methods:
        __init__(config: GeneralSimulationConfig, data: pandas.DataFrame):
            Initializes an instance of the ElectricityStorageComponentData class.
        get_system_data(path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
            Collects and returns a list of ElectricityStorageComponentData for each electricity storage component
            in the specified energy system.

    Note:
        The class extends `StorageComponentData`, inheriting its base properties and methods.
        It further filters the data based on the 'EnergyForm.ELECTRICITY' attribute, ensuring that only
        electricity storage components are considered.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes an instance of the ElectricityStorageComponentData class.

        Args:
            config (GeneralSimulationConfig): Configuration parameters for the general simulation.
            data (pandas.DataFrame): DataFrame containing electricity storage component related data.
        """
        super().__init__(config, data)

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Collects and returns a list of ElectricityStorageComponentData for each electricity storage component
        in the specified energy system.

        Args:
            path (str): Path to the location where system data is stored.
            config (GeneralSimulationConfig): Configuration parameters for the general simulation.
            energy_system (EnergySystem): The energy system for which the electricity storage component data
                                          needs to be fetched.

        Returns:
            list[ElectricityStorageComponentData]: A list of ElectricityStorageComponentData objects for each
                                                   electricity storage component in the energy system.
        """
        components: [StorageComponent] = energy_system.get_storage_components()
        number_components = 0
        for component in components:
            if EnergyForm.ELECTRICITY in component.energy_form:
                number_components += 1
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, ElectricityStorageComponentState,
                                                                   ElectricityStorageComponentState.TIME,
                                                                   ElectricityStorageComponentState.ENERGY_SYSTEM_ID,
                                                                   ElectricityStorageComponentState.STORAGE_COMPONENT_ID,
                                                                   energy_system_id=int(energy_system.get_id()),
                                                                   number_of_components=number_components)
        res: [ElectricityStorageComponentData] = list()
        for data in system_data:
            res.append(ElectricityStorageComponentData(config, data))
        return res
