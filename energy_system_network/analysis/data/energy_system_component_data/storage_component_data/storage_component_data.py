import numpy
import pandas
from energy_system_network.analysis.data.data import Data
from energy_system_network.commons import unit_converter
from energy_system_network.commons.state.energy_system_component.storage_component.storage_component_state import StorageComponentState
from energy_system_network.config.simulation.general_config import GeneralSimulationConfig
import numpy as np
from energy_system_network.simulation.energy_system.energy_system import EnergySystem


class StorageComponentData(Data):
    """
    Represents data specific to a storage component, detailing various state attributes.

    Attributes:
        config (GeneralSimulationConfig): Configuration parameters for the general simulation.
        data (pandas.DataFrame): DataFrame containing storage component related data.

    Methods:
        __init__(config, data): Initializes an instance of the StorageComponentData class.
        id(): Returns the storage component's ID.
        time(): Retrieves the time data associated with the storage component.
        power(): Retrieves power data of the storage component.
        power_loss(): Retrieves power loss data of the storage component.
        energy_content(): Retrieves the energy content data of the storage component.
        operation_emissions(): Retrieves operational emissions data of the storage component.
        generation_emissions(): Retrieves generation emissions data of the storage component.
        soc(): Retrieves the state of charge (SOC) data of the storage component.
        soh(): Retrieves the state of health (SOH) data of the storage component.
        soci(): Retrieves the state of carbon intensity (SOCI) data of the storage component.
        dec_emissions(): Calculates and retrieves the DEC emissions data.
        discharge_energy_consumption_emissions(): Retrieves discharge energy consumption emissions data.
        carbon_intensity_charging_energy(): Retrieves carbon intensity for charging energy data.
        availability(): Retrieves the availability data of the storage component (used for Electric Vehicles).
        drivetrain_power(): Calculates and retrieves drivetrain power data.
        reference_drive_power(): Retrieves reference drive power data.
        regenerative_charging_power(): Calculates and retrieves power data during regenerative charging events.
        regenerative_charging_energy(): Calculates and retrieves energy data for regenerative charging events.
        get_system_data(path, config, energy_system): Collects and returns storage component data for the specified energy system.
    """
    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        """
        Initializes an instance of the StorageComponentData class.

        Args:
            config (GeneralSimulationConfig): Configuration parameters for the general simulation.
            data (pandas.DataFrame): DataFrame containing storage component related data.
        """
        super().__init__(config, data)

    @property
    def id(self) -> str:
        """
        Computes and returns the ID of the storage component.

        Return:
            str: Storage component's ID in the format 'ENERGY_SYSTEM_ID.STORAGE_COMPONENT_ID'.
        """
        return str(int(self._get_first_value(StorageComponentState.ENERGY_SYSTEM_ID))) + '.' + \
               str(int(self._get_first_value(StorageComponentState.STORAGE_COMPONENT_ID)))

    @property
    def time(self) -> numpy.ndarray:
        """
        Retrieves the time data associated with the storage component.

        Return:
            numpy.ndarray: Array of time data.
        """
        return self._get_data(StorageComponentState.TIME)

    @property
    def power(self) -> numpy.ndarray:
        """
        Retrieves power data of the storage component.

        Return:
            numpy.ndarray: Array of power data.
        """
        return self._get_data(StorageComponentState.POWER)

    @property
    def power_loss(self) -> numpy.ndarray:
        """
        Retrieves the power loss data of the storage component.

        Return:
            numpy.ndarray: Array of power loss data.
        """
        return self._get_data(StorageComponentState.POWER_LOSS)

    @property
    def energy_content(self) -> numpy.ndarray:
        """
        Retrieves the energy content data of the storage component.

        Return:
            numpy.ndarray: Array of energy content data.
        """
        return self._get_data(StorageComponentState.ENERGY_CONTENT)

    @property
    def operation_emissions(self) -> numpy.ndarray:
        """
        Retrieves the operational emissions data of the storage component.

        Return:
            numpy.ndarray: Array of operational emissions data.
        """
        return self._get_data(StorageComponentState.OPERATION_EMISSIONS)

    @property
    def generation_emissions(self) -> numpy.ndarray:
        """
        Retrieves the generation emissions data of the storage component.

        Return:
            numpy.ndarray: Array of generation emissions data.
        """
        return self._get_data(StorageComponentState.GENERATION_EMISSIONS)

    @property
    def soc(self) -> numpy.ndarray:
        """
        Retrieves the state of charge (SOC) data for the storage component.

        Return:
            numpy.ndarray: Array of SOC data.
        """
        return self._get_data(StorageComponentState.SOC)

    @property
    def soh(self) -> numpy.ndarray:
        """
        Retrieves the state of health (SOH) data for the storage component.

        Return:
            numpy.ndarray: Array of SOH data.
        """
        return self._get_data(StorageComponentState.SOH)

    @property
    def soci(self) -> numpy.ndarray:
        """
        Retrieves the state of carbon intensity (SOCI) data for the storage component.

        Return:
            numpy.ndarray: Array of SOCI data.
        """
        return self._get_data(StorageComponentState.SOCI)

    @property
    def dec_emissions(self) -> numpy.ndarray:  # This property used for grid-connected BESS (ex. for Arbitrage, FCR)
        """
        Returns DEC emissions at each timestep in kgCO2eq as numpy array
        :return: dec_emissions as numpy ndarray
        """
        nan_free_series = self.power[~np.isnan(self.power)]
        dch_power = unit_converter.W_to_kW(self.get_negative_values(nan_free_series))  # in kW
        soci = unit_converter.g_to_kg(self.soci)  # in kgCO2eq/kWh
        timestep_h = unit_converter.s_to_h(self.config.timestep)  # in h
        dec_emissions = [-a*b*timestep_h for a,b in zip(dch_power, soci)]  # in kgCO2eq
        return np.array(dec_emissions)

    @property
    def discharge_energy_consumption_emissions(self) -> numpy.ndarray:  # This property used for Electric Vehicle
        """
        Retrieves the discharge energy consumption emissions data of the storage component.

        Return:
            numpy.ndarray: Array of discharge energy consumption emissions data.
        """
        return self._get_data(StorageComponentState.DISCHARGE_ENERGY_CONSUMPTION_EMISSIONS)  # in kgCO2eq

    @property
    def carbon_intensity_charging_energy(self) -> numpy.ndarray:
        """
        Retrieves the carbon intensity of charging energy for the storage component.

        Return:
            numpy.ndarray: Array of carbon intensity data.
        """
        return self._get_data(StorageComponentState.CARBON_INTENSITY_CHARGING_ENERGY)

    @property
    def availability(self) -> numpy.ndarray:  # availability of ElectricVehicle
        """
        Retrieves the availability data of the storage component, especially for Electric Vehicles.

        Return:
            numpy.ndarray: Array indicating availability status.
        """
        return self._get_data(StorageComponentState.AVAILABILITY)

    @property
    def drivetrain_power(self) -> numpy.ndarray:
        """
        Calculates and retrieves the drivetrain power data for the storage component when the vehicle is not available.

        Return:
            numpy.ndarray: Array of drivetrain power data.
        """
        return (1 - self.availability) * self.power  # Power values for when EV not available

    @property
    def reference_drive_power(self) -> numpy.ndarray:
        """
        Retrieves the reference drive power data for the storage component.

        Return:
            numpy.ndarray: Array of reference drive power data.
        """
        return self._get_data(StorageComponentState.REFERENCE_DRIVE_POWER)  # Reference drive power values in W

    @property
    def regenerative_charging_power(self) -> numpy.ndarray:
        """
        Calculates and retrieves the regenerative charging power data for the storage component.

        Return:
            numpy.ndarray: Array of regenerative charging power data.
        """
        return (1 - self.availability) * self.get_positive_values(self.power)  # Only regenerative charging events

    @property
    def regenerative_charging_energy(self) -> numpy.ndarray:
        """
        Calculates and retrieves energy data for regenerative charging events.

        Return:
            numpy.ndarray: Array of energy data for regenerative charging events.
        """
        return unit_converter.J_to_kWh(self.regenerative_charging_power * self.config.timestep)  # in kWh

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig, energy_system: EnergySystem) -> list:
        """
        Collects and returns storage component data for the specified energy system.

        Args:
            path (str): Path to the location where system data is stored.
            config (GeneralSimulationConfig): Configuration parameters for the general simulation.
            energy_system (EnergySystem): The energy system for which the storage component data needs to be fetched.

        Return:
            list[StorageComponentData]: A list of StorageComponentData objects corresponding to storage components
                                        in the energy system.
        """
        pass
