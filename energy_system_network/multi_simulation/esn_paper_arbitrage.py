from configparser import ConfigParser
from energy_system_network.multi_simulation.multi_processing import MultiProcessing


class ESNPaperArbitrage(MultiProcessing):
    """
    A class responsible for performing energy system simulations and analyses related to arbitrage.

    This class manages simulations and analyses that concern different types of arbitrage, such as economic or emissions arbitrage.
    It inherits from the `MultiProcessing` class to leverage parallel processing capabilities.

    Attributes:
        (Inherited from the parent class)

    Methods:
        __init__(): Initializes the ESNPaperArbitrage object.
        _setup_config() -> dict: Sets up the configuration for various types of energy arbitrage simulations.
    """
    def __init__(self):
        """
        Initializes the ESNPaperArbitrage object.
        Simulation and analysis flags are set to True by default.
        """
        super().__init__(do_simulation=True, do_analysis=True)

    def _setup_config(self) -> dict:
        """
        Sets up the configuration for energy arbitrage simulations.

        Arbitrage types considered include 'Economic' and 'Emissions'. The method returns a dictionary
        where the key represents the name of the simulation, and the value is the respective configuration.

        Returns:
            dict: A dictionary containing configurations for various types of energy arbitrage simulations.
        """
        simulation_names = ['A1', 'A2']
        arbitrage_types = ['Economic', 'Emissions']
        configs: dict = dict()
        for name, type in zip(simulation_names, arbitrage_types):
            config: ConfigParser = ConfigParser()
            config.add_section('ENERGY_MANAGEMENT')
            config.set('ENERGY_MANAGEMENT', 'ARBITRAGE_TYPE', type)
            configs[name] = config
        return configs


if __name__ == "__main__":
    multi_simulation: ESNPaperArbitrage = ESNPaperArbitrage()
    multi_simulation.run()
