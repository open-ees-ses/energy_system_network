from configparser import ConfigParser
from energy_system_network.multi_simulation.multi_processing import MultiProcessing


class MultiSimulationDemonstration(MultiProcessing):
    """
    Represents a demonstration for setting up multiple simulations.

    Attributes:
        __do_simulation (bool): A flag to determine if the simulation should be executed.
        __do_analysis (bool): A flag to determine if the analysis should be executed.

    Methods:
        __init__(): Initializes an instance of the MultiSimulationDemonstration class.
        _setup_config() -> dict: Creates and provides configuration for multiple simulation scenarios.
    """
    def __init__(self):
        """
        Initializes an instance of the MultiSimulationDemonstration class with simulation and analysis flags set to True.
        """
        super().__init__(do_simulation=True, do_analysis=True)

    def _setup_config(self) -> dict:
        """
        Creates and provides configuration for multiple simulation scenarios including
        energy management strategies and simple deficit coverage priorities.

        Return:
            dict: A dictionary containing configurations for multiple simulation scenarios.
        """
        # Example for config setup
        # Demonstrates setting up 4 parallel simulations -
        # 2 for operation strategy, and 2 for simple deficit coverage priority

        priority_1: str = '\nStorageComponents\nGridComponents\nCanRunGenerationComponents'
        priority_2: str = '\nGridComponents\nStorageComponents\nCanRunGenerationComponents'
        config_set_1: dict = dict()
        config_set_1['priority_1'] = priority_1
        config_set_1['priority_2'] = priority_2
        configs: dict = dict()
        for name, value in config_set_1.items():
            config: ConfigParser = ConfigParser()
            config.add_section('ENERGY_MANAGEMENT')
            config.set('ENERGY_MANAGEMENT', 'SIMPLE_DEFICIT_COVERAGE_PRIORITY', value)
            configs[name] = config

        config_1_strategy: str = '\nenergy_system_network,NoStrategy,Electricity\nsystem_1,SimpleDeficitCoverage,Electricity'
        config_2_strategy: str = '\nenergy_system_network,NoStrategy,Electricity\nsystem_1,SimplePeakShaving,Electricity'
        config_set_2: dict = dict()
        config_set_2['strategy_1'] = config_1_strategy
        config_set_2['strategy_2'] = config_2_strategy
        for name, value in config_set_2.items():
            config: ConfigParser = ConfigParser()
            config.add_section('ENERGY_MANAGEMENT')
            config.set('ENERGY_MANAGEMENT', 'STRATEGY', value)
            configs[name] = config
        return configs


if __name__ == "__main__":
    multi_simulation: MultiProcessing = MultiSimulationDemonstration()
    multi_simulation.run()
