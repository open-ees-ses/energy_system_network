# Energy System Network Parallel Simulations

This guide explains how to use the provided script to run parallel simulations of the Energy System Network (ESN).

## Getting Started

Before running any simulations, make sure that all necessary Python dependencies are installed. The main dependencies for these scripts are `multiprocessing` and `configparser`.

## Setting Up Parallel Simulations

Parallel simulations are set up by subclassing `MultiProcessing` and implementing the `_setup_config` method. 

### Example:

```python
from configparser import ConfigParser
from energy_system_network.multi_simulation.multi_processing import MultiProcessing

class MyESNSimulation(MultiProcessing):

    def __init__(self):
        super().__init__(do_simulation=True, do_analysis=True)

    def _setup_config(self) -> dict:
        simulation_names = ['Sim1', 'Sim2']
        some_parameter_values = ['Value1', 'Value2']

        configs: dict = dict()
        for name, value in zip(simulation_names, some_parameter_values):
            config: ConfigParser = ConfigParser()
            config.add_section('GENERAL')
            config.set('GENERAL', 'SOME_PARAMETER', value)
            configs[name] = config
        return configs
```

In this example, we create a subclass of `MultiProcessing` called `MyESNSimulation`. We override the `_setup_config` method to return a dictionary where keys are names of simulations and values are `ConfigParser` objects that contain configurations for those simulations.

## Configurations
Configurations for each simulation are stored in `ConfigParser` objects. You can create a `ConfigParser` object, add sections to it, and set key-value pairs within each section to add individual settings.

Here's how you can create a `ConfigParser` object, add a section 'GENERAL', and set 'SOME_PARAMETER' to 'Value1' within that section:
```python
config: ConfigParser = ConfigParser()
config.add_section('GENERAL')
config.set('GENERAL', 'SOME_PARAMETER', 'Value1')
```
## Running Simulations
Once you've defined your subclass of `MultiProcessing` and set up the configurations for each simulation, you can create an instance of your subclass and call the `run` method to start the simulations:
```python
if __name__ == "__main__":
    multi_simulation: MyESNSimulation = MyESNSimulation()
    multi_simulation.run()
```

## Other Example Scripts
You can find some example scripts in the following list to see that how you can use this class to parallelize your simulations with different parameters:
- Run simultaneously `Economic` and `Emissions` arbitrage types [here](esn_paper_arbitrage.py);
- Run simultaneously a frequency containment reserve system with different `LOOP` values and time-steps [here](esn_paper_fcr.py);
- Run simultaneously a home energy management system with different generation and storage components [here](esn_paper_hems.py).