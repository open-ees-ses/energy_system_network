from configparser import ConfigParser
from energy_system_network.multi_simulation.multi_processing import MultiProcessing


class ESNPaperHEMS(MultiProcessing):
    """
    A class responsible for conducting energy system simulations and analyses related to Home Energy Management Systems (HEMS).

    This class manages simulations and analyses that explore different configurations of generation and storage components
    in HEMS. It inherits from the `MultiProcessing` class to make use of parallel processing capabilities.

    Attributes:
        (Inherited from the parent class)

    Methods:
        __init__(): Initializes the ESNPaperHEMS object.
        _setup_config() -> dict: Configures various HEMS settings with different generation and storage components.
    """
    def __init__(self):
        """
        Initializes the ESNPaperHEMS object.
        Simulation and analysis flags are set to True by default.
        """
        super().__init__(do_simulation=True, do_analysis=True)

    def _setup_config(self) -> dict:
        """
        Configures energy system simulations for different Home Energy Management Systems (HEMS).

        Three simulation settings are considered:
        1. Without generation and storage components.
        2. With Photovoltaic Solar generation component but without storage.
        3. With both Photovoltaic Solar generation and Lithium Battery System storage components.

        Returns:
            dict: A dictionary containing configurations for the different HEMS simulations.
        """
        simulation_names = ['HEMS0', 'HEMS1', 'HEMS2']
        generation_components = ['',
                                 '\nsystem_1,PhotovoltaicSolar,5e3',
                                 '\nsystem_1,PhotovoltaicSolar,5e3']
        storage_components = ['',
                              '',
                              '\nsystem_1,LithiumBatterySystem,5e3,5e3']
        configs: dict = dict()
        for name, gen, stor in zip(simulation_names, generation_components, storage_components):
            config: ConfigParser = ConfigParser()
            config.add_section('ENERGY_SYSTEM_NETWORK')
            config.set('ENERGY_SYSTEM_NETWORK', 'GENERATION_COMPONENTS', gen)
            config.set('ENERGY_SYSTEM_NETWORK', 'STORAGE_COMPONENTS', stor)
            configs[name] = config
        return configs


if __name__ == "__main__":
    multi_simulation: ESNPaperHEMS = ESNPaperHEMS()
    multi_simulation.run()
