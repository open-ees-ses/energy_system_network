import multiprocessing
from multiprocessing import Queue
from energy_system_network.commons.utils import remove_all_files_from, create_directory_for
from energy_system_network.constants_energy_sys_network import ROOT_PATH, BATCH_DIR
from main import ESN


class MultiSimulation(multiprocessing.Process):
    """
    MultiSimulation is responsible for managing, initiating, and tracking multiple energy system simulations concurrently.

    This class extends the multiprocessing.Process class and provides an abstraction to handle a batch of energy system
    simulations. It ensures that the simulations are set up, run, and properly managed in a parallelized manner.

    Attributes:
        __path (str): Path where the results of the simulation are stored.
        __batch_size (int): Number of simulations to run concurrently before waiting for their completion.
        __printer_queue (Queue): Queue used to communicate with the printer for logging purposes.
        __config_set (dict): Dictionary containing configurations for each simulation.
        __do_simulation (bool): Flag indicating if simulations should be executed.
        __do_analysis (bool): Flag indicating if analyses should be executed after simulations.

    Methods:
        __init__(config_set, printer_queue, path, batch_size, do_simulation, do_analysis): Initializes the MultiSimulation object.
        run(): Initiates the simulations in batches.
        __setup(): Sets up the simulations for running.
        __wait_for(simulations): Waits for a given list of simulations to complete.
        __check_simulation_names(simulations): Checks if the simulation names are unique.
    """
    def __init__(self, config_set: dict, printer_queue: Queue, path: str = ROOT_PATH + 'results/', batch_size: int = 1,
                 do_simulation: bool = True, do_analysis: bool = True):
        """
        Initializes the MultiSimulation object with the given parameters.

        Args:
            config_set (dict): Dictionary of configurations for each simulation.
            printer_queue (Queue): Queue used for logging purposes.
            path (str, optional): Path where the results are stored. Default is ROOT_PATH + 'results/'.
            batch_size (int, optional): Number of concurrent simulations. Default is 1.
            do_simulation (bool, optional): If simulations should be executed. Default is True.
            do_analysis (bool, optional): If analysis should be executed after simulations. Default is True.

        Return:
            None.
        """
        super().__init__()
        create_directory_for(path + BATCH_DIR)
        remove_all_files_from(path + BATCH_DIR)
        self.__path: str = path
        self.__batch_size: int = batch_size
        self.__printer_queue: Queue = printer_queue
        self.__config_set: dict = config_set
        self.__do_simulation: bool = do_simulation
        self.__do_analysis: bool = do_analysis

    def __setup(self) -> [ESN]:
        """
        Sets up and returns a list of ESN simulation instances based on the given configuration set.

        Return:
            list[ESN]: A list of ESN simulation instances.
        """
        simulations: [ESN] = list()
        for name, config in self.__config_set.items():
            simulations.append(ESN(self.__path, name, do_simulation=self.__do_simulation, do_analysis=self.__do_analysis,
                                      simulation_config=config, queue=self.__printer_queue))
        return simulations

    def run(self):
        """
        Initiates and manages the simulations in batches.
        """
        started: [ESN] = list()
        simulations: [ESN] = self.__setup()
        self.__check_simulation_names(simulations)
        for simulation in simulations:
            print('\rStarting ' + simulation.name)
            simulation.start()
            started.append(simulation)
            if len(started) >= self.__batch_size:
                self.__wait_for(started)
                started.clear()
        self.__wait_for(started)

    @staticmethod
    def __wait_for(simulations: [ESN]):
        for simulation in simulations:
            simulation.join()

    @staticmethod
    def __check_simulation_names(simulations: [ESN]) -> None:
        names: [str] = list()
        for simulation in simulations:
            name: str = simulation.name
            if name in names:
                raise Exception(name + ' is not unique. Please check your simulation setup!')
            names.append(name)
