import multiprocessing
import time
from abc import abstractmethod, ABC
from multiprocessing import Queue
from energy_system_network.commons.console_printer import ConsolePrinter
from energy_system_network.commons.utils import format_float
from energy_system_network.multi_simulation.multi_simulation import MultiSimulation


class MultiProcessing(ABC):
    """
    Abstract class responsible for orchestrating parallel simulations and analyses using multiprocessing.

    This class is designed to efficiently handle a large number of energy system simulations and analyses.
    It provides a framework for defining configurations for the simulations, distributing them across multiple CPUs,
    and tracking their progress.

    Attributes:
        UPDATE (int): Time interval (in seconds) for checking the status of running processes.
        __do_simulation (bool): Flag indicating if simulations should be executed.
        __do_analysis (bool): Flag indicating if analyses should be executed.
        __max_parallel_processes (int): Maximum number of processes to run in parallel.

    Methods:
        __init__(do_simulation: bool = True, do_analysis: bool = True): Initializes the MultiProcessing object.
            Sets flags indicating whether to execute simulations
            and/or analyses and determines the maximum number of parallel processes based on available CPU cores.
        _setup_config() -> dict: Abstract method to set up the simulation configurations.
        run(): Orchestrates and initiates parallel simulations.
        __check_running_process(processes: [multiprocessing.Process]) -> None: Checks and manages running processes to ensure they don't exceed the maximum limit.
    """
    UPDATE: int = 1  # s

    def __init__(self, do_simulation: bool = True, do_analysis: bool = True):
        """
        Initializes the MultiProcessing object.

        Parameters:
            do_simulation (bool): Flag indicating if simulations should be executed. Defaults to True.
            do_analysis (bool): Flag indicating if analyses should be executed. Defaults to True.
        """
        self.__do_simulation: bool = do_simulation
        self.__do_analysis: bool = do_analysis
        self.__max_parallel_processes: int = multiprocessing.cpu_count()

    @abstractmethod
    def _setup_config(self) -> dict:
        """
        Abstract method that should be implemented in subclasses to set up the simulation configurations.

        Returns:
            dict: A dictionary containing the configurations for the simulations.
        """
        pass

    def run(self):
        """
        Orchestrates and initiates parallel simulations based on the configurations provided by `_setup_config`.

        This method:
        1. Sets up the configurations using `_setup_config`.
        2. Initializes a queue for console printing.
        3. Creates a list of simulation jobs based on the configurations.
        4. Starts the console printer.
        5. Begins simulations in parallel, ensuring the number of running processes doesn't exceed `__max_parallel_processes`.
        6. Joins the simulation jobs.
        7. Prints a summary upon completion.
        """
        config_set: dict = self._setup_config()
        printer_queue: Queue = Queue(maxsize=len(config_set) * 2)
        printer: ConsolePrinter = ConsolePrinter(printer_queue)
        jobs: [MultiSimulation] = list()
        for key, value in config_set.items():
            jobs.append(MultiSimulation(config_set={key: value}, printer_queue=printer_queue,
                                        do_simulation=self.__do_simulation, do_analysis=self.__do_analysis))
        printer.start()
        started: [MultiSimulation] = list()
        start = time.time()
        for job in jobs:
            job.start()
            started.append(job)
            self.__check_running_process(started)
        for job in jobs:
            job.join()
        duration: float = (time.time() - start) / 60.0
        job_count: int = len(jobs)
        print('\nMultiprocessing finished ' + str(job_count) + ' simulations in ' + format_float(duration) + ' min '
              '(' + format_float(duration / job_count) + ' min per simulation)')
        printer.stop_immediately()

    def __check_running_process(self, processes: [multiprocessing.Process]) -> None:
        """
        Checks and manages running processes to ensure they don't exceed the maximum number (`__max_parallel_processes`).

        Periodically checks the number of currently running processes. If the number exceeds the limit,
        it waits (`UPDATE` seconds) and checks again.

        Args:
            processes ([multiprocessing.Process]): List of processes to monitor.
        """
        while True:
            running_jobs: int = 0
            for process in processes:
                if process.is_alive():
                    running_jobs += 1
            if running_jobs < self.__max_parallel_processes:
                break
            time.sleep(self.UPDATE)
