from configparser import ConfigParser
from energy_system_network.multi_simulation.multi_processing import MultiProcessing


class ESNPaperFCR(MultiProcessing):
    """
    Represents simulations for the Frequency Containment Reserve (FCR) studies.

    Attributes:
        __do_simulation (bool): A flag to determine if the simulation should be executed.
        __do_analysis (bool): A flag to determine if the analysis should be executed.

    Methods:
        __init__(): Initializes an instance of the ESNPaperFCR class.
        _setup_config() -> dict: Provides configuration for different FCR simulation scenarios.
    """
    def __init__(self):
        """
        Initializes an instance of the ESNPaperFCR class with simulation and analysis flags set to True.
        """
        super().__init__(do_simulation=True, do_analysis=True)

    def _setup_config(self) -> dict:
        """
        Provides configuration for different FCR simulation scenarios.

        Return:
            dict: A dictionary containing configurations for different FCR scenarios.
        """
        simulation_names = ['FCR_1yr_1s', 'FCR_1yr_900s', 'FCR_20yr']
        loop_values: [str] = ['1', '1', '20']
        timestep_values: [str] = ['1', '900', '900']

        configs: dict = dict()
        for name, loop, ts in zip(simulation_names, loop_values, timestep_values):
            config: ConfigParser = ConfigParser()
            config.add_section('GENERAL')
            config.set('GENERAL', 'LOOP', loop)
            config.set('GENERAL', 'TIME_STEP', ts)
            configs[name] = config
        return configs


if __name__ == "__main__":
    multi_simulation: ESNPaperFCR = ESNPaperFCR()
    multi_simulation.run()
