<div align="center">
  <img src="documentation/figures/esn_logo.svg" width="600" />
</div>

# Table of Contents
1. [Energy System Network](#energy-system-network)
2. [Getting Started](#getting-started)
3. [Running a Simulation](#running-a-simulation)
4. [Configuration Examples](#examples)
5. [Results Overview](#results-overview)

## <a name="energy-system-network"></a>Energy System Network

The Energy System Network (ESN) is a robust Python program for simulating diverse scenarios in the analysis of energy flows within a system. With ESN, users can model complex multi-energy systems involving electricity and heat, and investigate various operation strategies, such as simple deficit coverage. The program integrates an array of generation components, including wind, PV, and diesel generators, with storage components like Li batteries and grid components like AC/DC converters, in a single simulation.

Utilizing ESN allows users to conduct simulations using time-series data, including load profiles and renewable generation profiles. Such simulations provide in-depth insights into the energy system's performance and help quantify losses both in the short and long term. This data assists in the decision-making processes related to the design of new energy systems, empowering users to optimize system efficiency and make informed choices.

Furthermore, you can find some useful overview regarding this program [here](code_maintenance/code_base_report.csv). This file gives the user some information such as how many lines of code, how many functional lines, and how many classes and functions the program has.

## <a name="getting-started"></a>Getting Started

Here is a list of programs you need to install:
1. Python 3.7 or higher
2. Python packages (refer to [requirements.txt](requirements.txt))
3. Python editor - we recommend [PyCharm](https://www.jetbrains.com/pycharm/)

Now, you have clone the project using the following line:
```shell
git clone https://gitlab.lrz.de/open-ees-ses/energy_system_network.git
```
Or, for a less hands-on usage, you can use a Docker image explained [here](documentation/docker.md).

## <a name="running-a-simulation"></a>Running a Simulation

The diagram below outlines the overall steps in this program.
<div align="center">
  <img src="documentation/figures/ESN.drawio.png" width="600" />
</div>

Follow these steps to use the ESN program:

1. **Configuration**: The program includes *.default.ini files that you can modify and save as *.local.ini files. You can [read the description](documentation/configuration.md) of the default configuration. If you wish, you can skip this step to use the default settings or modify only those files you want to change and use default settings for the others.

2. **Profile Specification**: In this step, you can either download the required *.csv files as per the configuration specified in the profile section in the simulation.ini file, or you can use the appropriate directories in the profile section of the [configuration file](documentation/configuration.md) to include your own profiles for the simulation.

3. **Network Construction**: Steps 3, 4, and 5 involve building the energy network as per the parameters in the *.ini files. In step 4, you create the components required for the system. In step 5, these components are connected to form an energy system. The energy systems are then interconnected to construct the energy network, which is now ready for simulation.

4. **Execution**: Run main.py to execute the simulation. The results will be saved in your specified directory.

5. **Analysis**: If you enable the `do_analysis` argument in the ESN class, the system will perform an analysis of the simulation results. For added customization, you can create analysis.local.ini and technology.local.ini files.

The diagrams below depict the workings of the energy system components (e.g., storage, generation, electric vehicle, etc.) and the energy management system.
<div align="center">
  <img src="documentation/figures/components.png" width="500" />
  <img src="documentation/figures/ems.png" width="500" /> 
</div>

After the simulation is completed, graphical results will be automatically loaded onto a web browser. You will also find CSV files under ./energy_system_network/results/.

## <a name="examples"></a>Configuration Examples
To test the program, you can run one of the examples located [here](./configuration_examples/README.md). To do so, please:
1. Create `simulation.local.ini` and `lithium_battery_settup.local.ini` files in the main root of the program where `main.py` located; 
2. Copy the configs written in `simulation.example.ini` and `lithium_battery_settup.example.ini` and paste them to the `simulation.local.ini` and `lithium_battery_settup.local.ini` files created in the first step, respectively;
3. Run `main.py`.

## <a name="results-overview"></a>Results Overview

* **Sankey Diagram**: Visualizes the energetic losses of the system 
![Sankey Diagram](documentation/figures/EnergeticEvaluation_EnergySystem_1.png)

* **Load and Power Generation**: Graphical display of the loads and power generation 
![Load & Power Generation](documentation/figures/ElectricitySimpleDeficitCoverageEvaluationSimpleDeficitCoverage for Energy System 1.png)

* **Storage System Analysis**: Graphical display of the storage system analysis
![Storage System Analysis](documentation/figures/ElectricityStorageComponentEvaluation1.1.png)

* **Environmental Evaluation**: Provides graphical displays of:
  * Carbon Intensity
  ![Carbon Intensity](documentation/figures/EnvironmentalEvaluation_EnergySystem_1_1.png)
  * Component-wise Carbon Dioxide Emissions
  ![CO2 Emissions 1](documentation/figures/EnvironmentalEvaluation_EnergySystem_1_2.png)
  ![CO2 Emissions 2](documentation/figures/EnvironmentalEvaluation_EnergySystem_1_3.png)

* **Tabulated Results**: Several results are also available in CSV format.

* **Highlighted SimSES Results**:
  * Technical Evaluation (Tabulated)
  ![Technical Evaluation](documentation/SimSES/Technical evaluation.png)
  * Power Losses
  ![Power Losses](documentation/SimSES/Power losses.png)
  * Thermal Parameters
  ![Thermal Parameters](documentation/SimSES/Thermal parameters.png)
  * Energy Flow
  ![Energy Flow](documentation/SimSES/Energy flow.png)
  * Losses Distribution
  ![Losses Distribution](documentation/SimSES/Losses distribution.png)
  * State of Charge (SOC), Internal Resistance, Temperature, and Capacity for Lithium-ion Batteries
  ![SOC for Li-ion Batteries](documentation/SimSES/SOC for lithium-ion batteries.png)
  * Current and Voltage for Lithium-ion Batteries
  ![Current & Voltage for Li-ion Batteries](documentation/SimSES/Current and voltage for lithium-ion batteries.png)
  * Degradation of Capacity and Increase of Internal Resistance for Lithium-ion Batteries
  ![Degradation & Increase of Resistance for Li-ion Batteries](documentation/SimSES/Degradation of capacity and increase of internal resistance for lithium-ion batteries.png)

Please note that these are only some highlighted results of the SimSES results.
