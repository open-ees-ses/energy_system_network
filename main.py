import threading
from configparser import ConfigParser
from multiprocessing import Queue
from energy_system_network.constants_energy_sys_network import ROOT_PATH
from energy_system_network.analysis.analysis import ESNAnalysis
from energy_system_network.commons.utils import create_directory_for
from energy_system_network.simulation.simulation import Simulation


class ESN(threading.Thread):
    """
    This class represents an Energy System Network (ESN) and extends Python's built-in threading.Thread. It is
    designed to handle simulation and analysis of an energy system network, running these operations in separate
    threads.

    Attributes:
        __do_simulation (bool): Determines if the simulation should be performed.
        __do_analysis (bool): Determines if the analysis should be performed.
        __name (str): The name of the Energy System Network (ESN).
        __esn_simulation (Simulation): The simulation instance to be run.
        __esn_analysis (ESNAnalysis): The analysis instance to be run.

    Methods:
        __init__(self, path: str, name: str, do_simulation: bool = True, do_analysis: bool = True,
                 simulation_config: ConfigParser = None, analysis_config: ConfigParser = None, queue: Queue = None):
            Initializes an instance of the ESN class.
        run(self): Runs the simulation and analysis of the ESN if they are set to be performed.
    """

    def __init__(self, path: str, name: str, do_simulation: bool = True, do_analysis: bool = True,
                 simulation_config: ConfigParser = None, analysis_config: ConfigParser = None, queue: Queue = None):
        """
        Initializes an instance of the ESN class.

        Args:
            path (str): The path where simulation and analysis results will be saved.
            name (str): The name of the Energy System Network (ESN).
            do_simulation (bool, optional): Determines if the simulation should be performed. Defaults to True.
            do_analysis (bool, optional): Determines if the analysis should be performed. Defaults to True.
            simulation_config (ConfigParser, optional): The configuration for the simulation.
            analysis_config (ConfigParser, optional): The configuration for the analysis.
            queue (Queue, optional): Queue object to manage thread execution order.
        """
        super().__init__()
        self.__do_simulation = do_simulation
        self.__do_analysis = do_analysis
        self.__name: str = name
        path = path + name + '/'
        create_directory_for(path)
        self.__esn_simulation: Simulation = Simulation(path, simulation_config, queue)
        if self.__do_analysis:
            energy_system_network = self.__esn_simulation.get_energy_system_network()
            self.__esn_analysis: ESNAnalysis = ESNAnalysis(path, energy_system_network, analysis_config)

    def run(self):
        """
        Runs the simulation and analysis of the ESN.

        It first checks if the simulation and analysis are set to be performed. If they are, it runs them accordingly.
        """
        if self.__do_simulation:
            try:
                self.__esn_simulation.run()
            except:
                print('An error was encountered. Simulated data will be analysed')
        if self.__do_analysis:
            self.__esn_analysis.run()


if __name__ == "__main__":
    result_path: str = ROOT_PATH + 'results/'
    simulation_name: str = 'energy_system_simulation'
    esn: ESN = ESN(result_path, simulation_name, do_simulation=True, do_analysis=True)
    esn.start()
    esn.join()
